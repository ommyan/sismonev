<?php

namespace App\Http\Controllers\sismonev\data;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\sismonev\data\pembayaran;
use DB;
class pembayaranController extends Controller
{
   
    static function paybysegmen($segmen,$bulan,$tahun,$kanwil=null,$cabang=null){
        $paybysegmen=pembayaran::payClaimBySegmen($segmen,$bulan,$tahun,$kanwil,$cabang);
        $data=$paybysegmen;
       
        return ($data);
    }
}
