<?php

namespace App\Http\Controllers\sismonev\data;

use Illuminate\Http\Request;
use Response;

use App\Http\Controllers\Controller;
use App\Models\sismonev\data\peserta;
use App\Models\sismonev\charts\KepesertaanCharts;


class pesertaController extends Controller
{
    public function jmlpstbysegmenperprogram($segmen,$bulan,$tahun,$kanwil=null,$cabang=null){
        $jmlpstbysegmenperprogram=peserta::jmlpesertaBysegmen($segmen,$bulan,$tahun,$kanwil,$cabang);
        $data=$jmlpstbysegmenperprogram;
       
        return ($data);
    }

     public function jmlpstbyprogrampersegmen($program,$bulan,$tahun,$kanwil=null,$cabang=null){
        $jmlpstbyprogrampersegmen=peserta::jmlpesertaByprogram($program,$bulan,$tahun,$kanwil,$cabang);
        $data= $jmlpstbyprogrampersegmen;
       
        return ($data);
     }
     public function BUbyskalaperprogram($program,$bulan,$tahun,$kanwil=null,$cabang=null){

        $BUbyskalaperprogram=peserta::jmlbuBysegmen($program,$bulan,$tahun,$kanwil,$cabang);
        $data= $BUbyskalaperprogram;
       
        return ($data);
     }

     public function BUbyprogramSkala($program,$bulan,$tahun,$kanwil=null,$cabang=null){
        $BUbyprogramSkala=peserta::BUbyprogramSkala($program,$bulan,$tahun,$kanwil,$cabang);
        $data= $BUbyprogramSkala;
         return ($data);
     }
     //BUbysegmenSkala
     public function BUbySkala($program,$bulan,$tahun,$kanwil=null,$cabang=null){
        $BUbySkala=peserta::BUbysegmenSkala($program,$bulan,$tahun,$kanwil,$cabang);
        $data= $BUbySkala;
         return ($data);
     }
      
}
