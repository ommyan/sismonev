<?php

namespace App\Http\Controllers\sismonev;

use App\Models\sismonev\data\cakupan;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\sismonev\dashboard;
use App\Models\sismonev\chartjsGrafik;
use App\Models\sismonev\charts\KepesertaanCharts;
use App\Models\sismonev\charts\PendapatanCharts;
use App\Models\sismonev\charts\PiutangCharts;
use App\Models\sismonev\charts\PembayaranCharts;
use App\Models\sismonev\data\pendapatan;
use App\Models\sismonev\data\pembayaran;
use App\Models\sismonev\data\manfaat;
use App\Models\sismonev\data\kanwil;
use App\Models\sismonev\data\cabang;
use App\Models\sismonev\data\peserta;
use App\Models\sismonev\data\Potensi;
use App\Models\sismonev\data\CakupanBu;


use Visitor;
use Tracker;
use App\Models\sismonev\data\organisasi;

class aspekController extends Controller
{
    //
    public function peserta(Request $request, $program, $bulan, $tahun, $kanwil = null, $cabang = null)
    {
        $program = $request->route('program');
        $bulan = $request->route('bulan');
        $tahun = $request->route('tahun');
        $kanwil=$request->route('kanwil');
        $cabang=$request->route('cabang');
        $nk=$request->input('namakanwil');
        $nc=$request->input('namacabang');


        $ptnik1=cakupan::getPTNIK(1, $bulan, $tahun, $kanwil, $cabang);
        $kartu1=cakupan::getKartuTerbit(1, $bulan, $tahun, $kanwil, $cabang);
        $tka1=cakupan::getTKA(1, $bulan, $tahun, $kanwil, $cabang);
        //dd($ptnik1);
        $ptnik2=cakupan::getPTNIK(2, $bulan, $tahun, $kanwil, $cabang);
        $kartu2=cakupan::getKartuTerbit(2, $bulan, $tahun, $kanwil, $cabang);
        $tka2=cakupan::getTKA(2, $bulan, $tahun, $kanwil, $cabang);

        $ptnik3=cakupan::getPTNIK(3, $bulan, $tahun, $kanwil, $cabang);
        $kartu3=cakupan::getKartuTerbit(3, $bulan, $tahun, $kanwil, $cabang);
        $tka3=cakupan::getTKA(3, $bulan, $tahun, $kanwil, $cabang);

        $ptnik4=cakupan::getPTNIK(4, $bulan, $tahun, $kanwil, $cabang);
        $kartu4=cakupan::getKartuTerbit(4, $bulan, $tahun, $kanwil, $cabang);
        $tka4=cakupan::getTKA(4, $bulan, $tahun, $kanwil, $cabang);

        $rbulan=$bulan;
        $rtahun=$tahun;

        $resume = peserta::resumeCakupan($program, $bulan, $tahun, $kanwil, $cabang);
        $resumeprogram = peserta::resumeCakupanProgram($program, $bulan, $tahun, $kanwil, $cabang);

        $kep1 = KepesertaanCharts::kep01(1, $bulan, $tahun, $kanwil, $cabang);
        $kepbu1 = KepesertaanCharts::kep02(1, $bulan, $tahun, $kanwil, $cabang);
        $kep031 = KepesertaanCharts::kep03(1, $bulan, $tahun, $kanwil, $cabang);

        $kep2 = KepesertaanCharts::kep01(2, $bulan, $tahun, $kanwil, $cabang);
        $kepbu2 = KepesertaanCharts::kep02(2, $bulan, $tahun, $kanwil, $cabang);
        $kep032 = KepesertaanCharts::kep03(2, $bulan, $tahun, $kanwil, $cabang);

        $kep3 = KepesertaanCharts::kep01(3, $bulan, $tahun, $kanwil, $cabang);
        $kepbu3 = KepesertaanCharts::kep02(3, $bulan, $tahun, $kanwil, $cabang);
        $kep033 = KepesertaanCharts::kep03(3, $bulan, $tahun, $kanwil, $cabang);

        $kep4 = KepesertaanCharts::kep01(4, $bulan, $tahun, $kanwil, $cabang);
        $kepbu4 = KepesertaanCharts::kep02(4, $bulan, $tahun, $kanwil, $cabang);
        $kep034 = KepesertaanCharts::kep03(4, $bulan, $tahun, $kanwil, $cabang);

        $kepbu04 = KepesertaanCharts::kepbu04('All', $bulan, $tahun, $kanwil, $cabang);
        $potensi=Potensi::getPotensi($tahun);
        //dd($tahun);
        //dd($potensi);
        $buprogram1=CakupanBu::getBUbyprogram(1, $bulan, $tahun, $kanwil, $cabang);
        $buprogram2=CakupanBu::getBUbyprogram(2, $bulan, $tahun, $kanwil, $cabang);
        $buprogram3=CakupanBu::getBUbyprogram(3, $bulan, $tahun, $kanwil, $cabang);
        $buprogram4=CakupanBu::getBUbyprogram(4, $bulan, $tahun, $kanwil, $cabang); 

      //  dd($buprogram1);

        $buJenis1=CakupanBu::getBUjenis(1,$bulan,$tahun,$kanwil,$cabang);
        $buJenis2=CakupanBu::getBUjenis(2,$bulan,$tahun,$kanwil,$cabang);
        $buJenis3=CakupanBu::getBUjenis(3,$bulan,$tahun,$kanwil,$cabang);
        $buJenis4=CakupanBu::getBUjenis(4,$bulan,$tahun,$kanwil,$cabang);

        $kanwils = kanwil::All();
        $cabangs = cabang::All();


        $bulan = peserta::getBulanTahun();
        $month = array("January", "February", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Okotober", "Nopember", "Desember");
        $bln = peserta::getMaxBulanTahun('CAKUPAN');
        $bulanini = $month[$rbulan - 1];
        $mbulan = $month[$rbulan - 1];
        $ribuan = 1000;
        $kanwul=peserta::getKanwil();

        return view('sismonev.aspek.kepesertaan', [
            'page_title' => 'Dashboard Monitoring  Evaluasi Program Jaminan Sosial Ketenagakerjaan',
            'sub_page_title' => 'Akumulasi S/D bulan ' . $mbulan . '/' . $rtahun . ' dalam Ribuan ',
            'kanwul'=>$kanwul,
            'kep1' => $kep1,
            'kep2' => $kep2,
            'kep3' => $kep3,
            'kep4' => $kep4,
            'kepbu1' => $kepbu1,
            'kepbu2' => $kepbu2,
            'kepbu3' => $kepbu3,
            'kepbu4' => $kepbu4,
            'kep031' => $kep031,
            'kep032' => $kep032,
            'kep033' => $kep033,
            'kep034' => $kep034,
            'kepbu04' => $kepbu04,
            'resume' => $resume,
            'resumeprogram' => $resumeprogram,

            'kanwil' => $kanwils,
            'cabang' => $cabangs,
            'bulan' => $bulan,
            'month' => $month,
            'bln' => $bln,
            'rprogram' => $program,
            'rbulan' => $rbulan,
            'rtahun' => $rtahun,
            'ribuan' => $ribuan,
            'potensi' => $potensi,

            'buJenis1' => $buJenis1,
            'buJenis2' => $buJenis2,
            'buJenis3' => $buJenis3,
            'buJenis4' => $buJenis4,
            'buprogram1' => $buprogram1,
            'buprogram2' => $buprogram2,
            'buprogram3' => $buprogram3,
            'buprogram4' => $buprogram4,

            'ptnik1' => $ptnik1,
            'kartu1' => $kartu1,
            'tka1' => $tka1,

            'ptnik2' => $ptnik2,
            'kartu2' => $kartu2,
            'tka2' => $tka2,

            'ptnik3' => $ptnik3,
            'kartu3' => $kartu3,
            'tka3' => $tka3,

            'ptnik4' => $ptnik4,
            'kartu4' => $kartu4,
            'tka4' => $tka4,
            'nk' => $nk,
            'nc' => $nc

        ]);

    }

    public function pendapatan(Request $request, $program, $bulan, $tahun, $kanwil = null, $cabang = null)
    {
        $nk=$request->input('namakanwil');
        $nc=$request->input('namacabang');
        
        //dd($nk,$nc);

        $iuran = pendapatan::PendapatanBysegmen($program, $bulan, $tahun, $kanwil, $cabang);
        $iuranprogram = pendapatan::PendapatanByprogram($program, $bulan, $tahun, $kanwil, $cabang);

        $chartiuranjkk = PendapatanCharts::PendapatanBySegmen(1, $bulan, $tahun, $kanwil, $cabang);
        $chartiuranjkm = PendapatanCharts::PendapatanBySegmen(2, $bulan, $tahun, $kanwil, $cabang);
        $chartiuranjht = PendapatanCharts::PendapatanBySegmen(3, $bulan, $tahun, $kanwil, $cabang);
        $chartiuranjp = PendapatanCharts::PendapatanBySegmen(4, $bulan, $tahun, $kanwil, $cabang);

        $chartTumbuhjkk = PendapatanCharts::TumbuhPendapatanBySegmen(1, $bulan, $tahun, $kanwil, $cabang);
        $chartTumbuhjkm = PendapatanCharts::TumbuhPendapatanBySegmen(2, $bulan, $tahun, $kanwil, $cabang);
        $chartTumbuhjht = PendapatanCharts::TumbuhPendapatanBySegmen(3, $bulan, $tahun, $kanwil, $cabang);
        $chartTumbuhjp = PendapatanCharts::TumbuhPendapatanBySegmen(4, $bulan, $tahun, $kanwil, $cabang);

        $piutangAgjkk = PiutangCharts::PiutangBySegmen(1, $bulan, $tahun, $kanwil, $cabang);
        $piutangAgjkm = PiutangCharts::PiutangBySegmen(2, $bulan, $tahun, $kanwil, $cabang);
        $piutangAgjht = PiutangCharts::PiutangBySegmen(3, $bulan, $tahun, $kanwil, $cabang);
        $piutangAgjp = PiutangCharts::PiutangBySegmen(4, $bulan, $tahun, $kanwil, $cabang);

        $resume = peserta::resumeCakupan($program, $bulan, $tahun, $kanwil = null, $cabang = null);
        $resume = pendapatan::resumeIuranProgram($program, $bulan, $tahun, $kanwil = null, $cabang = null);
        $resumeProgramSegmen = pendapatan::resumeIuranProgramSegmen($program, $bulan, $tahun, $kanwil = null, $cabang = null);
        $potensi=Potensi::getPotensi($tahun);
        //$resumeProgramSegmen
        $iuranTumbuh=PendapatanCharts::pend03(1, $bulan, $tahun, $kanwil, $cabang);
        //dd($resumeProgramSegmen);

        $kanwil = kanwil::All();
        $cabang = cabang::All();
        $bulan = peserta::getBulanTahun();
        $month = array("January", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Okotober", "Nopember", "Desember");
        $bln = peserta::getMaxBulanTahun('IURAN');
        $rprogram = $request->route('program');
        $rbulan = $request->route('bulan');
        $rtahun = $request->route('tahun');
        $bulanini = $month[$rbulan - 1];
        $mbulan = $month[$rbulan - 1];
        $kanwul=peserta::getKanwil();

        return view('sismonev.aspek.pendapatan', [
            'page_title' => 'Dashboard Monitoring  Evaluasi Program Jaminan Sosial Bidang Ketenagakerjaan',
            'sub_page_title' => 'Akumulasi S/D bulan ' . $mbulan . '/' . $rtahun . ' dalam Ribuan  ',
            'kanwul'=>$kanwul,
            'iuran' => $iuran,
            'iuranprogram' => $iuranprogram,
            'chartiuranjkk' => $chartiuranjkk,
            'chartiuranjkm' => $chartiuranjkm,
            'chartiuranjht' => $chartiuranjht,
            'chartiuranjp' => $chartiuranjp,
            'chartTumbuhjkk' => $chartTumbuhjkk,
            'chartTumbuhjkm' => $chartTumbuhjkm,
            'chartTumbuhjht' => $chartTumbuhjht,
            'chartTumbuhjp' => $chartTumbuhjp,
            'piutangAgjkk' => $piutangAgjkk,
            'piutangAgjkm' => $piutangAgjkm,
            'piutangAgjht' => $piutangAgjht,
            'piutangAgjp' => $piutangAgjp,
            'kanwil' => $kanwil,
            'cabang' => $cabang,
            'bulan' => $bulan,
            'month' => $month,
            'bln' => $bln,
            'rprogram' => $rprogram,
            'rbulan' => $rbulan,
            'rtahun' => $rtahun,
            'resume' => $resume,
            'bulanini' => $bulanini,
            'resumeProgramSegmen' => $resumeProgramSegmen,
            'potensi' => $potensi,
            'nk' => $nk,
            'nc' => $nc,
            'iuranTumbuh' =>  $iuranTumbuh
        ]);

    }

    public function pembayaran(Request $request, $program, $bulan, $tahun, $kanwil = null, $cabang = null)
    {
        $program = $request->route('program');
        $rbulan = $request->route('bulan');
        $rtahun = $request->route('tahun');

        $nk=$request->input('namakanwil');
        $nc=$request->input('namacabang');

        $resume = pembayaran::resumeManfaatProgram($program, $bulan, $tahun, $kanwil = null, $cabang = null);
        //dd($resume);
        $resumeProgramSegmen = pembayaran::resumeManfaatProgramSegmen($program, $bulan, $tahun, $kanwil = null, $cabang = null);
        //dd($resumeProgramSegmen);
        $resumeprogram = pembayaran::resumeManfaatPerProgram($program, $bulan, $tahun, $kanwil = null, $cabang = null);

        //dd($resumeprogram);    

        $ManfaatBySegmen1 = PembayaranCharts::ManfaatBySegmen(1, $bulan, $tahun, $kanwil, $cabang);
        $ManfaatBySegmen2 = PembayaranCharts::ManfaatBySegmen(2, $bulan, $tahun, $kanwil, $cabang);
        $ManfaatBySegmen3 = PembayaranCharts::ManfaatBySegmen(3, $bulan, $tahun, $kanwil, $cabang);
        $ManfaatBySegmen4 = PembayaranCharts::ManfaatBySegmen(4, $bulan, $tahun, $kanwil, $cabang);

        $ManfaatByJK1 = PembayaranCharts::ManfaatByjenisklaim(1, $bulan, $tahun, $kanwil, $cabang);
        $ManfaatByJK2 = PembayaranCharts::ManfaatByjenisklaim(2, $bulan, $tahun, $kanwil, $cabang);
        $ManfaatByJK3 = PembayaranCharts::ManfaatByjenisklaim(3, $bulan, $tahun, $kanwil, $cabang);
        $ManfaatByJK4 = PembayaranCharts::ManfaatByjenisklaim(4, $bulan, $tahun, $kanwil, $cabang);

        $ManfaatByJKasus1 = PembayaranCharts::ManfaatByjenisklaimkasus(1, $bulan, $tahun, $kanwil, $cabang);
        $ManfaatByJKasus2 = PembayaranCharts::ManfaatByjenisklaimkasus(2, $bulan, $tahun, $kanwil, $cabang);
        $ManfaatByJKasus3 = PembayaranCharts::ManfaatByjenisklaimkasus(3, $bulan, $tahun, $kanwil, $cabang);
        $ManfaatByJKasus4 = PembayaranCharts::ManfaatByjenisklaimkasus(4, $bulan, $tahun, $kanwil, $cabang);

        $ManfaatBysegmenkasus1 = PembayaranCharts::ManfaatBySegmenKasus(1, $bulan, $tahun, $kanwil, $cabang);
        $ManfaatBysegmenkasus2 = PembayaranCharts::ManfaatBySegmenKasus(2, $bulan, $tahun, $kanwil, $cabang);
        $ManfaatBysegmenkasus3 = PembayaranCharts::ManfaatBySegmenKasus(3, $bulan, $tahun, $kanwil, $cabang);
        $ManfaatBysegmenkasus4 = PembayaranCharts::ManfaatBySegmenKasus(4, $bulan, $tahun, $kanwil, $cabang);
         $potensi=Potensi::getPotensi($tahun);
        $kanwil = kanwil::All();
        $cabang = cabang::All();
        $bulan = peserta::getBulanTahun();
        $month = array("January", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Okotober", "Nopember", "Desember");
        $bln = peserta::getMaxBulanTahun('MANFAAT');
        $rprogram = $request->route('program');
        $rbulan = $request->route('bulan');
        $rtahun = $request->route('tahun');
        $mbulan = $month[$rbulan - 1];
        $ribuan = 1000;
        $kanwul=peserta::getKanwil();

        return view('sismonev.aspek.pembayaran', [
            'page_title' => 'Dashboard Monitoring  Evaluasi Program Jaminan Sosial Bidang Ketenagakerjaan',
            'sub_page_title' => 'Akumulasi S/D bulan ' . $mbulan . '/' . $rtahun . ' dalam Ribuan  ',
            'page_description' => '',
            'kanwul'=>$kanwul,
            'ManfaatBySegmen1' => $ManfaatBySegmen1,
            'ManfaatBySegmen2' => $ManfaatBySegmen2,
            'ManfaatBySegmen3' => $ManfaatBySegmen3,
            'ManfaatBySegmen4' => $ManfaatBySegmen4,
            'ManfaatByJK1' => $ManfaatByJK1,
            'ManfaatByJK2' => $ManfaatByJK2,
            'ManfaatByJK3' => $ManfaatByJK3,
            'ManfaatByJK4' => $ManfaatByJK4,

            'ManfaatByJKasus1' => $ManfaatByJKasus1,
            'ManfaatByJKasus2' => $ManfaatByJKasus2,
            'ManfaatByJKasus3' => $ManfaatByJKasus3,
            'ManfaatByJKasus4' => $ManfaatByJKasus4,


            'ManfaatBysegmenkasus1' => $ManfaatBysegmenkasus1,
            'ManfaatBysegmenkasus2' => $ManfaatBysegmenkasus2,
            'ManfaatBysegmenkasus3' => $ManfaatBysegmenkasus3,
            'ManfaatBysegmenkasus4' => $ManfaatBysegmenkasus4,
            'potensi' => $potensi,

            'kanwil' => $kanwil,
            'cabang' => $cabang,
            'bulan' => $bulan,
            'month' => $month,
            'bln' => $bln,
            'rprogram' => $rprogram,
            'rbulan' => $rbulan,
            'rtahun' => $rtahun,
            'ribuan' => $ribuan,
            'resumeProgramSegmen' => $resumeProgramSegmen,
            'resumeprogram' => $resumeprogram,
            'resume' => $resume,

            'nk' => $nk,
            'nc' => $nc

        ]);

    }

    public function keuangan(Request $request)
    {

        $kanwil = kanwil::All();
        $cabang = cabang::All();
        $bulan = peserta::getBulanTahun();
        $month = array("January", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Okotober", "Nopember", "Desember");
        $bln = peserta::getMaxBulanTahun('ORGANISASI');
        $rprogram = $request->route('program');
        $rbulan = $request->route('bulan');
        $rtahun = $request->route('tahun');
        $mbulan = $month[$rbulan - 1];
        $visitor = Tracker::currentSession();
        $kanwul=peserta::getKanwil();

        $nk=$request->input('namakanwil');
        $nc=$request->input('namacabang');

        return view('sismonev.aspek.keuangan', [
            'page_title' => 'Dashboard Monitoring  Evaluasi',
            'page_description' => 'Program Jaminan Sosial Ketenagakerjaan',
            'sub_page_title' => 'Akumulasi S/D bulan ' . $mbulan . '/' . $rtahun,
            'kanwul'=>$kanwul,
            'kanwil' => $kanwil,
            'cabang' => $cabang,
            'bulan' => $bulan,
            'month' => $month,
            'bln' => $bln,
            'rprogram' => $rprogram,
            'rbulan' => $rbulan,
            'rtahun' => $rtahun,
            'visitor' => $visitor,
              'nk' => $nk,
            'nc' => $nc

        ]);

    }

    public function organisasi(Request $request,$bulan,$tahun,$kanwil=null,$cabang=null)
    {



        $bulan = peserta::getBulanTahun();
        $month = array("January", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Okotober", "Nopember", "Desember");
        $bln = peserta::getMaxBulanTahun('ORGANISASI');
        $rbulan = $request->route('bulan');
        $rtahun = $request->route('tahun');
        $rkanwil = $request->route('kanwil');
        $rcabang = $request->route('cabang');

        $mbulan = $month[$rbulan - 1];
        $kanwul=peserta::getKanwil();

        $nk=$request->input('namakanwil');
        $nc=$request->input('namacabang');
       // $orgall=organisasi::getJumlahkaryawankanwil($rbulan,$rtahun,$rkanwil,$rcabang);
        //dd($orgall);

       // $buJenis1=CakupanBu::getBUjenis(1,$bulan,$tahun,$kanwil,$cabang);


        $kanwil = kanwil::All();
        $cabang = cabang::All();
        return view('sismonev.aspek.organisasi', [
            'page_title' => 'Dashboard Monitoring  Evaluasi',
            'page_description' => 'Program Jaminan Sosial Ketenagakerjaan',
            'sub_page_title' => 'Akumulasi S/D bulan ' . $mbulan . '/' . $rtahun,
            'kanwul'=>$kanwul,
            'kanwil' => $kanwil,
            'cabang' => $cabang,
            'bulan' => $bulan,
            'month' => $month,
            'bln' => $bln,
            'rbulan' => $rbulan,
            'rtahun' => $rtahun,
              'nk' => $nk,
            'nc' => $nc
           // 'orgall' => $orgall
         //   'buJenis1' => $buJenis1

        ]);

    }
}
