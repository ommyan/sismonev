<?php

namespace App\Http\Controllers\sismonev;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Models\sismonev\data\kanwil;
use App\Models\sismonev\data\cabang;
use App\Models\sismonev\data\peserta;
use App\Models\sismonev\data\pembayaran;
use Yajra\DataTables\DataTables;
use DB;

class wilayahmanfaatController extends Controller
{
    public function index(Request $request){
         $nk=$request->input('namakanwil');
        $nc=$request->input('namacabang');
        

        $program = $request->route('program');
        $rbulan = $request->route('bulan');
        $rtahun = $request->route('tahun');
        $bln=peserta::getMaxBulanTahun('IURAN');
        $kanwul=peserta::getKanwil();
        $cabang=cabang::All();
        $bulan=peserta::getBulanTahun();
        $month=array("January","February","Maret","April","Mei","Juni","Juli","Agustus","September","Okotober","Nopember","Desember");
        $mprogram=array("Jaminan Kecelakaan Kerja","Jaminan Kematian","Jaminan Hari Tua", "Jaminan Pensiun");
        $mprogram=$mprogram[$program-1];
        $mbulan=$month[$rbulan-1];
    	return view('Sismonev.wilayah.pembayaran.manfaat',[
    		'page_title' => 'Dashboard Monitoring  Evaluasi Program Jaminan Sosial Bidang Ketenagakerjaan',
            'sub_page_title' => 'Akumulasi S/D bulan ' . $mbulan . '/' . $rtahun,
            'kanwul' => $kanwul,
            'cabang' => $cabang,
            'bulan' => $bulan,
            'month' => $month,
            'program' => $program,
            'rbulan' => $rbulan,
            'rtahun' => $rtahun,
            'mprogram' => $mprogram,
            'bln' => $bln,
            'nk' => $nk,
            'nc' => $nc
    		]);
    }

   public function getData(){
        $program=$_POST['program'];
        $bulan=$_POST['bulan'];
        $tahun=$_POST['tahun'];
        $data=pembayaran::AjaxListManfaatKanwil($program,$bulan,$tahun);
   	return Datatables::of($data)->make(true);
          
   } 
   public function getDetailData(){
        $idc=$_POST['idc'];
        $bulan=$_POST['bulan'];
        $tahun=$_POST['tahun'];
        $program=$_POST['program'];
        $data=pembayaran::AjaxListManfaatCabang($program,$idc,$bulan,$tahun);
    return Datatables::of($data)->make(true);
   } 
}
