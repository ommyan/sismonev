<?php

namespace App\Http\Controllers\sismonev\backend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\sismonev\data\cakupan;
use App\Models\sismonev\data\kanwil;
use App\Models\sismonev\data\cabang;
use App\Models\sismonev\data\peserta;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;

class PesertaController extends Controller {
    public function index(){
        $kanwil=kanwil::All();
        $cabang=cabang::All();
        $bulan=peserta::getBulanTahun();
        $month=array("January","February","Maret","April","Mei","Juni","Juli","Agustus","September","Okotober","Nopember","Desember");
        
        return view('Sismonev.backend.admin.upload',[
            'page_title' => 'Manajemen Data',
            'kanwil' => $kanwil,
            'cabang' => $cabang,
            'bulan' => $bulan,
            'month' => $month


            ]);
    }

 public function importFileIntoDB(Request $request){
                
        if($request->hasFile('sample_file')){
    
  
       // try {
			$datas= \Excel::selectSheets('1a. Cakupan Kepesertaan')->load($request->file('sample_file')->getRealPath(), function ($reader) {
				//dd($reader);
                $n=0;
                         
				foreach ($reader->toArray() as $key => $row) {
                    $n=$n+1;
						for ($x = 0; $x < count($row); $x++) {
                     try {       
                        $data[]= array(

                            'BULAN'  => $row[$x]['bulan'],
                            'TAHUN' => $row[$x]['tahun'],            
                            'ID_KANWIL' => $row[$x]['id_kanwil'],
                            'ID_CABANG' => $row[$x]['id_kacab'],
                            'ID_PROGRAM' => $row[$x]['id_program'],
                            'ID_SEGMEN' => $row[$x]['id_segmen'],
                            'JUMLAH_KARTU_TERBIT' => $row[$x]['jumlah_kartu_terbit'],
                            'JUMLAH_PTNIK' => $row[$x]['jumlah_ptnik'],
                            'JUMLAH_TKA' => $row[$x]['jumlah_tkasing'],
                            'JUMLAH_PESERTA' => $row[$x]['jumlah_pp_tk'],

                        );
                     }
                     catch (\Exception $e) {
               
                   // return redirect()->route('upload'); //->with('message', [ 'Format kolom tidak sesuai' . return back(); $e->getMessage()]);
                   //return redirect('/pesertaupload');
                   return Redirect::intended('/pesertaupload');
                    }
                    
                    
                 } //loop kolom
                 //dd($data);
                try {
                 DB::table('CAKUPAN') -> insert($data);
                }
                catch (\Exception $e) {
                    dd('gagal');
                   // return $e->getMessage();
                }
            dd('sukses');
				} //loop row	
            });
        }
    } // this function
}
     
