<?php

namespace App\Http\Controllers\sismonev;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\sismonev\dashboard;
use App\Models\sismonev\data\kanwil;
use App\Models\sismonev\data\cabang;
use App\Models\sismonev\data\peserta;
use App\Models\sismonev\data\pendapatan;
use App\Models\sismonev\data\pembayaran;
use App\Models\sismonev\data\iuran;
use App\Models\sismonev\data\manfaat;
use App\Models\sismonev\data\Potensi;
use App\Models\sismonev\data\CakupanBu;
use App\Models\sismonev\charts\KepesertaanCharts;
use App\Models\sismonev\data\Cakupan;

use DB;
use Tracker;

class homeController extends Controller
{
    //
    public function index(Request $request)
    {
        $program = $request->route('program');
        $rbulan = $request->route('bulan');
        $rtahun = $request->route('tahun');

        $bulan = $request->route('id_bulan');
        $kanwilnya = $request->route('id_kanwil');
        $cabangnya = $request->route('id_cabang');

        $cabang = cabang::All(); //DB::table('KANWIL')->pluck("KANWIL","ID_KANWIL")->all();
        $bulans = peserta::getBulanTahun();
        $maxmonth = peserta::getMaxBulanTahun('CAKUPAN');
        $bln = peserta::getMaxBulanTahun('CAKUPAN');
        $visitor = Tracker::currentSession();

        $sbulan = peserta::getMaxBulan('CAKUPAN');
        $stahun = peserta::getMaxTahun('CAKUPAN');

        $xbulan = peserta::getMaxBulan('MANFAAT');
        $xtahun = peserta::getMaxTahun('MANFAAT');

        $cbulan = peserta::getMaxBulan('CAKUPAN_BU');
        $ctahun = peserta::getMaxTahun('CAKUPAN_BU');


        $bulan = peserta::getBulanTahun();
        $month = array("January", "February", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Okotober", "Nopember", "Desember");
        $rprogram = $request->route('program');


        $b = substr($bln, 0, 1);
        $mbulan = $month[substr($bln, 0, 1) - 1];
        $ribuan = 1000;

        $resume = peserta::resumeCakupan('All', $sbulan, $stahun, $kanwilnya, $cabangnya);
        $resumeprogram = peserta::resumeCakupanProgram('All', $sbulan, $stahun, $kanwilnya, $cabangnya);
        $resumeProgramSegmen = pendapatan::resumeIuranProgramSegmenTotal('All', $sbulan, $stahun, $kanwilnya, $cabangnya);

        $resumeSegmenManfaat = pembayaran::resumeManfaatProgramSegmenTotal('All', $xbulan, $xtahun, $kanwilnya, $cabangnya);
       // dd($resumeSegmenManfaat);

        $potensi=Potensi::getPotensi($stahun);
        //dd( $sbulan, $stahun);
        $buprogram1=CakupanBu::getBUbyprogram(1, $sbulan, $stahun, $kanwilnya, $cabangnya);
        $buprogram2=CakupanBu::getBUbyprogram(2, $sbulan, $stahun, $kanwilnya, $cabangnya);
        $buprogram3=CakupanBu::getBUbyprogram(3, $sbulan, $stahun, $kanwilnya, $cabangnya);
        $buprogram4=CakupanBu::getBUbyprogram(4, $sbulan, $stahun, $kanwilnya, $cabangnya);
       // dd($buprogram1);
        $buskalaprogram=CakupanBu::getBUSkalaAll(1,$cbulan, $ctahun, $kanwilnya, $cabangnya);
        
        //dd($buskalaprogram);

        $buJenis1=CakupanBu::getBUjenis(1,$sbulan,$stahun,$kanwilnya,$cabangnya);
        $buJenis2=CakupanBu::getBUjenis(2,$sbulan,$stahun,$kanwilnya,$cabangnya);
        $buJenis3=CakupanBu::getBUjenis(3,$sbulan,$stahun,$kanwilnya,$cabangnya);
        $buJenis4=CakupanBu::getBUjenis(4,$sbulan,$stahun,$kanwilnya,$cabangnya);

        $ptnik1=cakupan::getPTNIK(1, $sbulan, $stahun, $kanwilnya, $cabangnya);
        $kartu1=cakupan::getKartuTerbit(1, $sbulan, $stahun, $kanwilnya, $cabangnya);
        $tka1=cakupan::getTKA(1, $sbulan, $stahun, $kanwilnya, $cabangnya);

        //  dd($resumeProgramSegmen);
        $kanwils = kanwil::All();
        $cabangs = cabang::All();
        $nk=$request->input('namakanwil');
        $nc=$request->input('namacabang');

        $kanwul = DB::table('KANWIL')->pluck('KANWIL', 'ID_KANWIL');

        return view('Sismonev.start', [
            'page_title' => 'Dashboard Monitoring  Evaluasi',
             'sub_page_title' => 'Akumulasi S/D bulan ' . $mbulan . '/' . $rtahun . ' dalam Ribuan  ',
            'page_description' => 'Program Jaminan Sosial Ketenagakerjaan',
            'resume' => $resume,
            'resumeprogram' => $resumeprogram,
            'resumeProgramSegmen' => $resumeProgramSegmen,
            'kanwils' => $kanwils,
            'kanwul' => $kanwul,
            'cabangs' => $cabangs,
            'bulan' => $bulan,
            'month' => $month,
            'bln' => $bln,
            'rprogram' => $rprogram,
            'rbulan' => $rbulan,
            'rtahun' => $rtahun,
            'ribuan' => $ribuan,
            'visitor' => $visitor,
            'potensi' => $potensi,
            'buJenis1' => $buJenis1,
            'buJenis2' => $buJenis2,
            'buJenis3' => $buJenis3,
            'buJenis4' => $buJenis4,
            'buprogram1' => $buprogram1,
            'buprogram2' => $buprogram2,
            'buprogram3' => $buprogram3,
            'buprogram4' => $buprogram4,
            'ptnik1' => $ptnik1,
            'kartu1' => $kartu1,
            'tka1' => $tka1,
             'buskalaprogram' =>  $buskalaprogram,
              'resumeSegmenManfaat' =>  $resumeSegmenManfaat,
                 'nk' => $nk,
            'nc' => $nc

        ]);

    }

    public function cabangAjax(Request $request){
    //	dd($id_kacab);
        
           $cabang = DB::table('CABANG')->where('ID_KANWIL',$request->id_kacab)->pluck("CABANG","ID_KACAB")->all();
    		$data = view('ajax-select',compact('cabang'))->render();
    		return response()->json(['options'=>$data]);
        
        
    }
}
