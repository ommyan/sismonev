<?php

namespace App\Http\Controllers\sismonev;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\sismonev\dashboard;
use App\Models\sismonev\chartjsGrafik;
use App\Models\sismonev\charts\KepesertaanCharts;
use App\Models\sismonev\charts\PendapatanCharts;
use App\Models\sismonev\charts\PiutangCharts;
use App\Models\sismonev\charts\PembayaranCharts;
use Fx3costa\LaravelChartJs\Providers\Chartjs;
use App\Models\sismonev\data\pendapatan;
use App\Models\sismonev\data\pembayaran;
use App\Models\sismonev\data\manfaat;
use App\Models\sismonev\data\kanwil;
use App\Models\sismonev\data\cabang;
use App\Models\sismonev\data\peserta;      
use Tracker;

class AdminController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
      //   $this->middleware('auth');
     }
 
     /**
      * Show the application dashboard.
      *
      * @return \Illuminate\Http\Response
      */
     public function index(Request $request)
     {
      
        
        $visitor=Tracker::currentSession();
         return view('Sismonev.backend.admin.home',[
                'page_title' => '',
                'sub_page_title' => 'Data Managemen',   
                'visitor' => $visitor         
        
         ]);




     }
}
