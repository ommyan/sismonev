<?php

namespace App\Http\Controllers\sismonev;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\sismonev\charts\KepesertaanCharts;
use Fx3costa\LaravelChartJs\Providers\Chartjs;

class knobController extends Controller
{
    //
       
    public function index() {
       $bulan=7;$tahun=2017;
       
     
       $kep031=KepesertaanCharts::kep03(1,$bulan,$tahun);
       $kep032=KepesertaanCharts::kep03(2,$bulan,$tahun);
       $kep033=KepesertaanCharts::kep03(3,$bulan,$tahun);
       $kep034=KepesertaanCharts::kep03(4,$bulan,$tahun);

  return view('sismonev.charts',
            [
                'kep031'=>$kep031,
                'kep032'=>$kep032,
                'kep033'=>$kep033,
                'kep034'=>$kep034
            
            ]);

    }
}
