<?php

namespace App\Http\Controllers\sismonev;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\sismonev\dashboard;
use App\Models\sismonev\data\kanwil;
use App\Models\sismonev\data\cabang;
use App\Models\sismonev\data\peserta;
use App\Models\sismonev\data\pendapatan;
use App\Models\sismonev\data\iuran;
use App\Models\sismonev\data\manfaat;
use DB;
use App\Models\sismonev\charts\KepesertaanCharts;

class AwalController extends Controller
{
    public function index(Request $request ){
        $program = $request->route('program');
        $rbulan = $request->route('bulan');
        $rtahun = $request->route('tahun');


        $bulan = $request->route('id_bulan');
        $kanwilnya = $request->route('id_kanwil');
        $cabangnya = $request->route('id_cabang');

        $cabang = cabang::All(); //DB::table('KANWIL')->pluck("KANWIL","ID_KANWIL")->all();
        $bulans = peserta::getBulanTahun();
        $maxmonth = peserta::getMaxBulanTahun('CAKUPAN');
        $bln = peserta::getMaxBulanTahun('CAKUPAN');

        $sbulan = peserta::getMaxBulan('CAKUPAN');
        $stahun = peserta::getMaxTahun('CAKUPAN');

        $kep031 = KepesertaanCharts::kep03(1, 6, 2017, null, null);
        $kep1 = KepesertaanCharts::kep01(1, 6, 2017, null, null);

        $bulan = peserta::getBulanTahun();
        $month = array("January", "February", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Okotober", "Nopember", "Desember");
        $rprogram = $request->route('program');


        $b = substr($bln, 0, 1);
        $mbulan = $month[substr($bln, 0, 1) - 1];
        $ribuan = 1000;

        $resume = peserta::resumeCakupan('All', $sbulan, $stahun, $kanwilnya, $cabangnya);
        $resumeprogram = peserta::resumeCakupanProgram('All', $sbulan, $stahun, $kanwilnya, $cabangnya);
        $resumeProgramSegmen = pendapatan::resumeIuranProgramSegmen($program, $sbulan, $stahun, $kanwilnya, $cabangnya);
        $kanwils = kanwil::All();
        $cabangs = cabang::All();

        $kanwul = DB::table('KANWIL')->pluck('KANWIL', 'ID_KANWIL');
        return view('Sismonev.aspek.kepesertaan.awal', [
            'page_title' => 'Dashboard Monitoring  Evaluasi',
            'page_description' => 'Program Jaminan Sosial Ketenagakerjaan',
            'resume' => $resume,
            'resumeprogram' => $resumeprogram,
            'resumeProgramSegmen' => $resumeProgramSegmen,
            'kanwils' => $kanwils,
            'kanwul' => $kanwul,
            'cabangs' => $cabangs,
            'bulan' => $bulan,
            'month' => $month,
            'bln' => $bln,
            'rprogram' => $rprogram,
            'rbulan' => $rbulan,
            'rtahun' => $rtahun,
            'ribuan' => $ribuan,
            'kep031' => $kep031,
            'kep1' => $kep1

        ]);
    }
}
