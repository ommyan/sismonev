<?php

namespace App\Http\Controllers\sismonev;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Excel;
use App\Models\sismonev\peserta;
use Input;

class importController extends Controller
{
   public function index(){
       return view ('sismonev.admin.import');
   } 

   public function importExcel()
	{
		if(Input::hasFile('import_file')){
			$path = Input::file('import_file')->getRealPath();
			$data = Excel::load($path, function($reader) {         
			})->get();
            $workbookTitle = $reader->getTitle();
            insertoTable();
            // get sheet title
            $sheetTitle = $sheet->getTitle();
            echo "<script type='text/javascript'>alert('$sheetTitle');</script>";
            }
        
        
		}
		return back();
	}


    public function intertoTable(){
     
			if(!empty($data) && $data->count()){
				foreach ($data as $key => $value) {
					$insert[] = [
   
                    `BULAN` => $value->BULAN,
                    `TAHUN` => $value->TAHUN,
                    `ID_KANWIL` => $value->ID_KANWIL,
                    `ID_CABANG` => $value->ID_CABANG,
                    `ID_PROGRAM`=> $value->ID_PROGRAM,
                    `ID_SEGMEN` => $value->ID_SEGMEN,
                    `JUMLAH_KARTU_TERBIT` => $value->JUMLAH_KARTU_TERBIT,
                    `JUMLAH_PTNIK` => $value->JUMLAH_PTNIK,
                    `JUMLAH_TKA`  => $value->JUMLAH_TKA,
                    `JUMLAH_PESERTA`  => $value->JUMLAH_PESERTA,
                    `JUMLAH_PA` => $value->JUMLAH_PA

                        ];
				}
				if(!empty($insert)){
					DB::table('cakupan')->insert($insert);
					alert('Insert Record successfully.');
				}
			}




    }
}
