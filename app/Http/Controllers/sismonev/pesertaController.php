<?php

namespace App\Http\Controllers\sismonev;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\sismonev\dashboard;
use App\Models\sismonev\charts\KepesertaanCharts;

class pesertaController extends Controller
{
    //
    public function index(){
       //$membershipChart=dashboard::membershipChart();
       //$chartH=dashboard::higchart();
       //$ChartsConsole=dashboard::ChartsConsole();
       $gaugemeter=dashboard::gaugemeter();
       $persenkartuterbit=dashboard::persenkartuterbit();
       $BU_JumlahSkala=dashboard::BU_JumlahSkala();
       //$manfaatbyprogram=dashboard::manfaatbyprogram();
       //$PM_segmen_perprogram=dashboard::PM_segmen_perprogram();
       //$manfaatbyprogramjkm=dashboard::manfaatbyprogramjkm();
       //$manfaatbyprogramjht=dashboard::manfaatbyprogramjht();
       //$manfaatbyprogramjp=dashboard::manfaatbyprogramjp();
       //$pertumbuhanTK=dashboard::pertumbuhanTK();
       $gmtka=dashboard::gmtka();
       $kep1=KepesertaanCharts::kep1(1,3,2017);
       $kep2=KepesertaanCharts::kep1(2,3,2017);
       $kep3=KepesertaanCharts::kep1(3,3,2017);
       $kep4=KepesertaanCharts::kep1(4,3,2017);

     
      // dd ($manfaatbyprogramjkm);

         return view('sismonev.kepesertaan', [
         'kep1' => $kep1,
         'membershipChart' => $membershipChart,
         'chartH' => $chartH,
         'ChartsConsole' => $ChartsConsole,
         'gaugemeter' => $gaugemeter,
         'persenkartuterbit' => $persenkartuterbit,
         'BU_JumlahSkala' => $BU_JumlahSkala,
         'manfaatbyprogram' => $manfaatbyprogram,
         'PM_segmen_perprogram' => $PM_segmen_perprogram,
         'manfaatbyprogramjkm' => $manfaatbyprogramjkm,
        'manfaatbyprogramjht' => $manfaatbyprogramjht,
        'manfaatbyprogramjp' => $manfaatbyprogramjp,
        'pertumbuhanTK' => $pertumbuhanTK,
        'gmtka' => $gmtka
        
         ]);




    }
}
