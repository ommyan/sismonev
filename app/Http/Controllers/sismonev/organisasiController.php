<?php

namespace App\Http\Controllers\sismonev;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\sismonev\data\organisasi;
use DB;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\DataTables;


class organisasiController extends Controller
{
    //
    public function index($bulan,$tahun,$kanwil,$cabang){
        $orgall=organisasi::getJumlahkaryawan($bulan,$tahun,$kanwil,$cabang);
        dd($orgall);

         return view('sismonev.organisasi',[
             'orgall' => $orgall
         ]);
    }

    public function getData()
    {
        $data = DB::table('ORGANISASI')
            -> join('KANWIL','ORGANISASI.ID_KANWIL','=','KANWIL.ID_KANWIL')
            -> join('CABANG','ORGANISASI.ID_CABANG','=','CABANG.ID_CABANG')
            ->select('ID_ORGANISASI as AD','BULAN','TAHUN','KANWIL.KANWIL as KANWIL','CABANG.CABANG as CABANG','JUMLAH_KARYAWAN')
            ->orderBy('BULAN','TAHUN','KANWIL.KANWIL','CABANG.CABANG', 'asc')
            ->get();

        //      $data = json_decode(json_encode($data), true);
        return DataTables::of($data)
           // ->addColumn('action', '<button type="button" class="btn btn-danger">Edit</button>')
            // ->addColumn('action','<class="btn btn-xs"><span class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Delete"aria-hidden="true"></span>')
            ->make(true);
    }
}
