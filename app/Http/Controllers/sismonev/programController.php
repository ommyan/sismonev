<?php

namespace App\Http\Controllers\sismonev;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\sismonev\dashboard;
use App\Models\sismonev\charts\KepesertaanCharts;
use App\Models\sismonev\charts\PendapatanCharts;
use App\Models\sismonev\charts\PiutangCharts;
use App\Models\sismonev\charts\PembayaranCharts;
use App\Models\sismonev\data\pendapatan;
use App\Models\sismonev\data\manfaat;
use App\Models\sismonev\data\peserta;
use App\Models\sismonev\data\kanwil;
use App\Models\sismonev\data\cabang;
use App\Models\sismonev\data\cakupanBu;


class programController extends Controller
{
    //
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function jkk(REQUEST $request, $program, $bulan, $tahun, $kanwil=null, $cabang=null){

         /* kepesertaan  */

        $gaugemeter = dashboard::gaugemeter($program, $bulan, $tahun, $kanwil, $cabang);
        $persenkartuterbit = dashboard::persenkartuterbit($program, $bulan, $tahun, $kanwil, $cabang);
        $gmtka = dashboard::gmtka($program, $bulan, $tahun, $kanwil, $cabang);

        $BU_JumlahSkala=dashboard::BU_JumlahSkala();

        $manfaatbyprogram=dashboard::manfaatbyprogram();
        $PM_segmen_perprogram=dashboard::PM_segmen_perprogram();

        $buprogram=cakupanBu::getBUProgram(1,$bulan,$tahun,$kanwil,$cabang);
     //   dd($buprogram);
        $pertumbuhanTK=dashboard::pertumbuhanTK();

        $kep1=KepesertaanCharts::kep01(1,$bulan,$tahun,$kanwil,$cabang);
        $kepbu1=KepesertaanCharts::kep02(1,$bulan,$tahun,$kanwil,$cabang);
        $kep031=KepesertaanCharts::kep03(1,$bulan,$tahun,$kanwil,$cabang);
        /* iuran  */
        $iuran = pendapatan::PendapatanBysegmen($program,$bulan,$tahun,$kanwil,$cabang);
        //dd($iuran);
        $iuranprogram=pendapatan::PendapatanByprogram($program,$bulan,$tahun,$kanwil,$cabang);
      //  dd($iuranprogram);
        $chartiuranjkk=PendapatanCharts::PendapatanBySegmen(1,$bulan,$tahun,$kanwil,$cabang);
        $chartTumbuhjkk=PendapatanCharts::TumbuhPendapatanBySegmen(1,$bulan,$tahun,$kanwil,$cabang);
        $piutangAgjkk=PiutangCharts::PiutangBySegmen(1,$bulan,$tahun,$kanwil,$cabang);
        /* pembayaran  */
        $manfaatprogram=manfaat::PembayaranTotalBysegmen($program,$bulan,$tahun,$kanwil,$cabang);
        $manfaat=manfaat::PembayaranTotalBysegmen($program,$bulan,$tahun,$kanwil,$cabang);
        $ManfaatBySegmen1 = PembayaranCharts::ManfaatBySegmen(1,$bulan,$tahun,$kanwil,$cabang);
        $ManfaatByJK1 = PembayaranCharts::ManfaatByjenisklaim(1,$bulan,$tahun,$kanwil,$cabang);
        $ManfaatBysegmenkasus1 = PembayaranCharts::ManfaatBySegmenKasus(1,$bulan,$tahun,$kanwil,$cabang);
        $resumeCakupan=peserta::resumeCakupan($program,$bulan,$tahun,$kanwil,$cabang);
        //print_r($resumeCakupan);
        //dd($resumeCakupan);
         $kanwil=kanwil::All();
        $cabang=cabang::All();
        $bulan=peserta::getBulanTahun();
        $month=array("January","February","Maret","April","Mei","Juni","Juli","Agustus","September","Okotober","Nopember","Desember");
        $bulan=peserta::getBulanTahun();
        $month=array("January","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Okotober","Nopember","Desember");
        $bln=peserta::getMaxBulanTahun('CAKUPAN');
        $rprogram = $request->route('program');
        $rbulan = $request->route('bulan');
        $rtahun = $request->route('tahun');
        $mbulan = $month[$rbulan-1];
        $kanwul=peserta::getKanwil();

        return view('sismonev.program.jkk.cover', [
             'kanwil' => $kanwil,
            'cabang' => $cabang,
            'page_title' => 'Dashboard Monitoring  Evaluasi Program Jaminan Sosial Bidang Ketenagakerjaan',
            'sub_page_title' => 'Akumulasi S/D bulan ' . $mbulan . '/' . $rtahun,
             'program' => 'Jaminan Kecelakaan Kerja',
            'kanwul' => $kanwul,
            
            /* Kepesertaan */

             'kep1' => $kep1,
             'kepbu1' => $kepbu1,
             'kep031' => $kep031,
             'gaugemeter' => $gaugemeter,
             'persenkartuterbit' => $persenkartuterbit,
             'BU_JumlahSkala' => $BU_JumlahSkala,
             'gmtka' => $gmtka,
             'resumeCakupan' => $resumeCakupan,
         /* iuran */

             'iuran' => $iuran ,
             'iuranprogram' => $iuranprogram,
             'chartiuranjkk' => $chartiuranjkk,
             'chartTumbuhjkk' => $chartTumbuhjkk,
             'piutangAgjkk' => $piutangAgjkk,

        /* pembayaran */
            'manfaatprogram'=>  $manfaatprogram,
            'manfaat' => $manfaat,
            'ManfaatBySegmen1' => $ManfaatBySegmen1,
            'ManfaatByJK1' => $ManfaatByJK1,
            'ManfaatBysegmenkasus1' => $ManfaatBysegmenkasus1,
            'bulan' => $bulan,
            'month' => $month,
            'bln' => $bln,
            'rprogram' => $rprogram,
            'rbulan' => $rbulan,
            'rtahun' => $rtahun,
            'buprogram' => $buprogram

         ]);
        
    }
     public function jkm(REQUEST $request,$program, $bulan, $tahun, $kanwil=null, $cabang=null){


         /* kepesertaan  */
         $buprogram=cakupanBu::getBUProgram(2,$bulan,$tahun,$kanwil,$cabang);

         $gaugemeter = dashboard::gaugemeter($program, $bulan, $tahun, $kanwil, $cabang);
         $persenkartuterbit = dashboard::persenkartuterbit($program, $bulan, $tahun, $kanwil, $cabang);
         $gmtka = dashboard::gmtka($program, $bulan, $tahun, $kanwil, $cabang);

         $BU_JumlahSkala=dashboard::BU_JumlahSkala();
        $manfaatbyprogramjkm=dashboard::manfaatbyprogramjkm();
        $manfaatbyprogramjht=dashboard::manfaatbyprogramjht();
        $manfaatbyprogramjp=dashboard::manfaatbyprogramjp();
        $pertumbuhanTK=dashboard::pertumbuhanTK();


         $kep2=KepesertaanCharts::kep01(2,$bulan,$tahun,$kanwil,$cabang);
         $kepbu2=KepesertaanCharts::kep02(2,$bulan,$tahun,$kanwil,$cabang);
         $kep032=KepesertaanCharts::kep03(2,$bulan,$tahun,$kanwil,$cabang);


         /* iuran  */
        $iuran = pendapatan::PendapatanBysegmen($program,$bulan,$tahun,$kanwil,$cabang);
        $iuranprogram=pendapatan::PendapatanByprogram($program,$bulan,$tahun,$kanwil,$cabang);
        $chartiuranjkm=PendapatanCharts::PendapatanBySegmen(2,$bulan,$tahun,$kanwil,$cabang);
        $chartTumbuhjkm=PendapatanCharts::TumbuhPendapatanBySegmen(2,$bulan,$tahun,$kanwil,$cabang);
        $piutangAgjkm=PiutangCharts::PiutangBySegmen(2,$bulan,$tahun,$kanwil,$cabang);
        /* pembayaran  */
        $manfaatprogram=manfaat::PembayaranTotalBysegmen($program,$bulan,$tahun,$kanwil,$cabang);
        $manfaat=manfaat::PembayaranTotalBysegmen($program,$bulan,$tahun,$kanwil,$cabang);
        $ManfaatBySegmen2 = PembayaranCharts::ManfaatBySegmen(2,$bulan,$tahun,$kanwil,$cabang);
        $ManfaatByJK2 = PembayaranCharts::ManfaatByjenisklaim(2,$bulan,$tahun,$kanwil,$cabang);
        $ManfaatBysegmenkasus2 = PembayaranCharts::ManfaatBySegmenKasus(2,$bulan,$tahun,$kanwil,$cabang);
        $resumeCakupan=peserta::resumeCakupan($program,$bulan,$tahun,$kanwil,$cabang);
        $kanwil=kanwil::All();
        $cabang=cabang::All();
  
        $bulan=peserta::getBulanTahun();
        $month=array("January","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Okotober","Nopember","Desember");
        $bln=peserta::getMaxBulanTahun('CAKUPAN');
        $rprogram = $request->route('program');
        $rbulan = $request->route('bulan');
        $rtahun = $request->route('tahun');
        $mbulan = $month[$rbulan-1];
         $kanwul=peserta::getKanwil();
        
          return view('sismonev.program.jkm.cover', [
    'kanwil' => $kanwil,
            'cabang' => $cabang,
            'page_title' => 'Dashboard Monitoring  Evaluasi Program Jaminan Sosial Bidang Ketenagakerjaan',
            'sub_page_title' => 'Akumulasi S/D bulan ' . $mbulan . '/' . $rtahun,
              'program' => 'Jaminan Kematian',
              'kanwul' => $kanwul,
            /* Kepesertaan */

             'kep2' => $kep2,
             'kepbu2' => $kepbu2,
             'kep032' => $kep032,
             'resumeCakupan' =>$resumeCakupan,
             'BU_JumlahSkala' => $BU_JumlahSkala,
             
         /* iuran */

             'iuran' => $iuran ,
             'iuranprogram' => $iuranprogram,
             'chartiuranjkm' => $chartiuranjkm,
             'chartTumbuhjkm' => $chartTumbuhjkm,
             'piutangAgjkm' => $piutangAgjkm,

        /* pembayaran */
            'manfaatprogram'=>  $manfaatprogram,
            'manfaat' => $manfaat,
            'ManfaatBySegmen2' => $ManfaatBySegmen2,
            'ManfaatByJK2' => $ManfaatByJK2,
            'ManfaatBysegmenkasus2' => $ManfaatBysegmenkasus2,
            'bulan' => $bulan,
        'month' => $month,
        'bln' => $bln,
        'rprogram' => $rprogram,
        'rbulan' => $rbulan,
        'rtahun' => $rtahun,
              'buprogram' => $buprogram
          ]);
    }

    public function jht(REQUEST $request,$program, $bulan, $tahun, $kanwil=null, $cabang=null)
    {


        /* kepesertaan  */
        $buprogram = cakupanBu::getBUProgram(3, $bulan, $tahun, $kanwil, $cabang);

        $gaugemeter = dashboard::gaugemeter($program, $bulan, $tahun, $kanwil, $cabang);
        $persenkartuterbit = dashboard::persenkartuterbit($program, $bulan, $tahun, $kanwil, $cabang);
        $gmtka = dashboard::gmtka($program, $bulan, $tahun, $kanwil, $cabang);

        $kep3 = KepesertaanCharts::kep01(3, $bulan, $tahun, $kanwil, $cabang);
        $kepbu3 = KepesertaanCharts::kep02(3, $bulan, $tahun, $kanwil, $cabang);
        $kep033 = KepesertaanCharts::kep03(3, $bulan, $tahun, $kanwil, $cabang);
        $BU_JumlahSkala = dashboard::BU_JumlahSkala();
        /* iuran  */
        $iuran = pendapatan::PendapatanBysegmen($program, $bulan, $tahun, $kanwil, $cabang);
        $iuranprogram = pendapatan::PendapatanByprogram($program, $bulan, $tahun, $kanwil, $cabang);
       // dd($iuranprogram);
        $chartiuranjht = PendapatanCharts::PendapatanBySegmen(3, $bulan, $tahun, $kanwil, $cabang);
        $chartTumbuhjht = PendapatanCharts::TumbuhPendapatanBySegmen(3, $bulan, $tahun, $kanwil, $cabang);
        $piutangAgjht = PiutangCharts::PiutangBySegmen(3, $bulan, $tahun, $kanwil, $cabang);
        /* pembayaran  */
        $manfaatprogram = manfaat::PembayaranTotalBysegmen($program, $bulan, $tahun, $kanwil, $cabang);
        $manfaat = manfaat::PembayaranTotalBysegmen($program, $bulan, $tahun, $kanwil, $cabang);
        $ManfaatBySegmen3 = PembayaranCharts::ManfaatBySegmen(3, $bulan, $tahun, $kanwil, $cabang);
        $ManfaatByJK3 = PembayaranCharts::ManfaatByjenisklaim(3, $bulan, $tahun, $kanwil, $cabang);
        $ManfaatBysegmenkasus3 = PembayaranCharts::ManfaatBySegmenKasus(3, $bulan, $tahun, $kanwil, $cabang);
        $resumeCakupan = peserta::resumeCakupan($program, $bulan, $tahun, $kanwil, $cabang);
        //   dd($resumeCakupan);
        $kanwil = kanwil::All();
        $cabang = cabang::All();
        $bulan = peserta::getBulanTahun();
        $month = array("January", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Okotober", "Nopember", "Desember");
        $bln = peserta::getMaxBulanTahun('CAKUPAN');
        $rprogram = $request->route('program');
        $rbulan = $request->route('bulan');
        $rtahun = $request->route('tahun');
        $mbulan = $month[$rbulan - 1];
        $bln = peserta::getMaxBulanTahun('CAKUPAN');
        $kanwul=peserta::getKanwil();

         return view('sismonev.program.jht.cover', [
             'kanwil' => $kanwil,
             'kanwil' => $kanwil,
             'cabang' => $cabang,
             'page_title' => 'Dashboard Monitoring  Evaluasi Program Jaminan Sosial Bidang Ketenagakerjaan',
             'sub_page_title' => 'Akumulasi S/D bulan ' . $mbulan . '/' . $rtahun,
             'program' => 'Jaminan Hari Tua',
             'kanwul' => $kanwul,
             /* Kepesertaan */

             'kep3' => $kep3,
             'kepbu3' => $kepbu3,
             'kep033' => $kep033,
             'resumeCakupan' => $resumeCakupan,
             'BU_JumlahSkala' => $BU_JumlahSkala,
             'buprogram' => $buprogram,
             /* iuran */

             'iuran' => $iuran,
             'iuranprogram' => $iuranprogram,
             'chartiuranjht' => $chartiuranjht,
             'chartTumbuhjht' => $chartTumbuhjht,
             'piutangAgjht' => $piutangAgjht,

             /* pembayaran */
             'manfaatprogram' => $manfaatprogram,
             'manfaat' => $manfaat,
             'ManfaatBySegmen3' => $ManfaatBySegmen3,
             'ManfaatByJK3' => $ManfaatByJK3,
             'ManfaatBysegmenkasus3' => $ManfaatBysegmenkasus3,
             'bulan' => $bulan,
             'month' => $month,
             'bln' => $bln
         ]);
     }
     public function jp(REQUEST $request,$program, $bulan, $tahun, $kanwil=null, $cabang=null){


         /* kepesertaan  */
         $buprogram=cakupanBu::getBUProgram(4,$bulan,$tahun,$kanwil,$cabang);

         $gaugemeter = dashboard::gaugemeter($program, $bulan, $tahun, $kanwil, $cabang);
         $persenkartuterbit = dashboard::persenkartuterbit($program, $bulan, $tahun, $kanwil, $cabang);
         $gmtka = dashboard::gmtka($program, $bulan, $tahun, $kanwil, $cabang);

        $BU_JumlahSkala=dashboard::BU_JumlahSkala();
        $manfaatbyprogramjkm=dashboard::manfaatbyprogramjkm();
        $manfaatbyprogramjht=dashboard::manfaatbyprogramjht();
        $manfaatbyprogramjp=dashboard::manfaatbyprogramjp();
        $pertumbuhanTK=dashboard::pertumbuhanTK();
         $kep4=KepesertaanCharts::kep01(4,$bulan,$tahun,$kanwil,$cabang);
         $kepbu4=KepesertaanCharts::kep02(4,$bulan,$tahun,$kanwil,$cabang);
         $kep034=KepesertaanCharts::kep03(4,$bulan,$tahun,$kanwil,$cabang);

         /* iuran  */
        $iuran = pendapatan::PendapatanBysegmen($program,$bulan,$tahun,$kanwil,$cabang);
        $iuranprogram=pendapatan::PendapatanByprogram($program,$bulan,$tahun,$kanwil,$cabang);
        $chartiuranjp=PendapatanCharts::PendapatanBySegmen(4,$bulan,$tahun,$kanwil,$cabang);
        $chartTumbuhjp=PendapatanCharts::TumbuhPendapatanBySegmen(4,$bulan,$tahun,$kanwil,$cabang);
        $piutangAgjp=PiutangCharts::PiutangBySegmen(4,$bulan,$tahun,$kanwil,$cabang);
        /* pembayaran  */
        $manfaatprogram=manfaat::PembayaranTotalBysegmen($program,$bulan,$tahun,$kanwil,$cabang);
        $manfaat=manfaat::PembayaranTotalBysegmen($program,$bulan,$tahun,$kanwil,$cabang);
        $ManfaatBySegmen4 = PembayaranCharts::ManfaatBySegmen(4,$bulan,$tahun,$kanwil,$cabang);
        $ManfaatByJK4 = PembayaranCharts::ManfaatByjenisklaim(4,$bulan,$tahun,$kanwil,$cabang);
        $ManfaatBysegmenkasus4 = PembayaranCharts::ManfaatBySegmenKasus(4,$bulan,$tahun,$kanwil,$cabang);
        $resumeCakupan=peserta::resumeCakupan($program,$bulan,$tahun,$kanwil,$cabang);
        $kanwil=kanwil::All();
        $cabang=cabang::All();
        $bulan=peserta::getBulanTahun();
        $month=array("January","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Okotober","Nopember","Desember");
        $bln=peserta::getMaxBulanTahun('CAKUPAN');
        $rprogram = $request->route('program');
        $rbulan = $request->route('bulan');
        $rtahun = $request->route('tahun');
         $mbulan = $month[$rbulan-1];

         $kanwul=peserta::getKanwil();


          return view('sismonev.program.jp.cover', [
            'kanwil' => $kanwil,
            'cabang' => $cabang,
            'page_title' => 'Dashboard Monitoring  Evaluasi Program Jaminan Sosial Bidang Ketenagakerjaan',
            'sub_page_title' => 'Akumulasi S/D bulan ' . $mbulan . '/' . $rtahun,
              'program' => 'Jaminan Pensiun',
              'kanwul' => $kanwul,
            /* Kepesertaan */

             'kep4' => $kep4,
             'kepbu4' => $kepbu4,
             'kep034' => $kep034,
             'resumeCakupan' =>$resumeCakupan,
             'BU_JumlahSkala' => $BU_JumlahSkala,
              'buprogram' => $buprogram,
             
         /* iuran */

             'iuran' => $iuran ,
             'iuranprogram' => $iuranprogram,
             'chartiuranjp' => $chartiuranjp,
             'chartTumbuhjp' => $chartTumbuhjp,
             'piutangAgjp' => $piutangAgjp,

        /* pembayaran */
            'manfaatprogram'=>  $manfaatprogram,
            'manfaat' => $manfaat,
            'ManfaatBySegmen4' => $ManfaatBySegmen4,
            'ManfaatByJK4' => $ManfaatByJK4,
            'ManfaatBysegmenkasus4' => $ManfaatBysegmenkasus4,
            'bulan' => $bulan,
            'month' => $month,
            'bln' => $bln,
            'rprogram' => $rprogram,
            'rbulan' => $rbulan,
            'rtahun' => $rtahun
          ]);
        
    }
}
