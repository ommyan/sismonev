<?php

namespace App\Http\Controllers\sismonev;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\sismonev\data\cakupan;
use App\Models\sismonev\data\cakupanBu;
use App\Models\sismonev\data\iuran;
use App\Models\sismonev\data\kanwil;
use App\Models\sismonev\data\peserta;
use App\Http\Requests;
use Input;
use App\Post;
use DB;
use Session;
use Maatwebsite\Excel\Facades\Excel;

class MaatwebsiteController extends Controller
{
     public function importExport(Request $request)
    {
        $kanwils = kanwil::All(); //DB::table('KANWIL')->pluck("KANWIL","ID_KANWIL")->all();
       
        $bulans=peserta::getBulanTahun();
        $month=array("January","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Okotober","Nopember","Desember");
        $maxmonth=peserta::getMaxBulanTahun();
    return view('sismonev/admin/import',[
        'kanwil' => $kanwils,
        'bulan' => $bulans,
        'month' => $month,
        'maxmonth' => $maxmonth
     

          ]);
    }
    public function downloadExcel($type)
    {
        $data = Cakupan::get()->toArray();
        return Excel::create('laravelcode', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }
    public function importKepesertaan(Request $request)
    {
        //echo $request;
      var_dump($request->hasFile('import_file1'));
       // if($request->hasFile('import_file1')){
         $datas= Excel::selectSheets('1a. Cakupan Kepesertaan')->load($request->hasFile('import_file1')->getRealPath(), function ($reader) {;
          $x=0;
          dd($datas) ;
               $cakupa = new CAKUPAN;

              // echo count($reader);
                foreach ($reader->toArray() as $key => $row) {
                 for ($x = 0; $x < count($row); $x++) {
                        $data[]= array(

                            'BULAN'  => $row[$x]['bulan'],
                            'TAHUN' => $row[$x]['tahun'],            
                            'ID_KANWIL' => $row[$x]['id_kanwil'],
                            'ID_CABANG' => $row[$x]['id_kacab'],
                            'ID_PROGRAM' => $row[$x]['id_program'],
                            'ID_SEGMEN' => $row[$x]['id_segmen'],
                            'JUMLAH_KARTU_TERBIT' => $row[$x]['jumlah_kartu_terbit'],
                            'JUMLAH_PTNIK' => $row[$x]['jumlah_ptnik'],
                            'JUMLAH_TKA' => $row[$x]['jumlah_tkasing'],
                            'JUMLAH_PESERTA' => $row[$x]['jumlah_pp_tk'],

                        );
                    
                 } 
                } 
               // dd($data);
                DB::table('CAKUPAN') -> insert($data); 
                Session::put('success', 'Youe file successfully import in database!!!' );
                    

            });
       // }

        Session::put('gagal', 'Youe file successfully import in database!!!' );
       

      return back();
    }

    public function importPendapatan(Request $request)
    {
        
        if($request->hasFile('import_file2')){
            try {
                Excel::selectSheets('2a1. Iuran dan Piutang Iuran')->load($request->file('import_file2')->getRealPath(), function ($reader) {
                    $x=0;
                         $iura = new IURAN;
                          foreach ($reader->toArray() as $key => $row) {
                          echo count($row);
                           for ($s = 0; $s < count($row); $s++) {
                                  $data[]= array(
          
                                      'BULAN'  => $row[$s]['bulan'],
                                      'TAHUN' => $row[$s]['tahun'],            
                                      'ID_KANWIL' => $row[$s]['id_kanwil'],
                                      'ID_CABANG' => $row[$s]['id_kacab'],
                                      'ID_PROGRAM' => $row[$s]['id_program'],
                                      'ID_SEGMEN' => $row[$s]['id_segmen'],
                                      'PENDAPATAN_IURAN' => $row[$s]['pendapatan_iuran'],                           
                                  );
                           } 
                          } 
                          DB::table('IURAN') -> insert($data); 
                          Session::put('success', 'Youe file successfully import in database!!!' );          
                      });            
            } catch (Exception $e) {
                      return $e;
            }    
        }
      return back();
    }

    public function importBu(Request $request)
    {
      
        if($request->hasFile('import_file3')){
            try {
                Excel::filter('chunk')->selectSheets('1b. Cakupan Badan Usaha')
                ->load($request->file('import_file3')->getRealPath())->chunk(250, function ($reader) {
                    $x=0;
                         $cakupanbu = new CAKUPANBU;
                         $headings=[
                             'bulan',
                             'tahun',
                             'id_kanwil',
                             'id_kacab',
                             'id_program',
                             'id_skala_bu',
                             'id_jenis_bu',
                             'jumlah_pp_tk',
                             'jumlah_pp_bu',
                         ];
                         /*
                          foreach ($reader->toArray() as $key => $row) {
                          echo count($row);
                           for ($s = 0; $s < count($row); $s++) {
                                  $data[]= array(
                                    'BULAN' => $row[$s]['bulan'],
                                    'TAHUN' => $row[$s]['tahun'],        
                                    'ID_KANWIL' => $row[$s]['id_kanwil'],
                                    'ID_KACAB' => $row[$s]['id_kacab'],
                                    'ID_PROGRAM' => $row[$s]['id_program'],
                                    'ID_SKALA' => $row[$s]['id_skalabu'],
                                    'ID_JENIS_BU' => $row[$s]['id_jenis_bu'],
                                    'JUMLAH' => $row[$s]['jumlah_pp_tk'],
                                    'JUMLAH_BU' => $row[$s]['jumlah_pp_bu'],
          
                                  );
                                  DB::table('CAKUPAN_BU') -> insert($data); 
                                  $data=array();
                           } 
                          } 
                          */
                       $semua=MaatwebsiteController::add_valid_rows_to_array($reader,$headings);
                         if (!isset($semua)){
                            MaatwebsiteController::insert_records_into_location_table($semua);
                            Session::put('success', 'Youe file successfully import in database!!!' );          
                         }
                         
                });            
            } catch (Exception $e) {
                      return $e;
            }    
        }
      return back();
    }
    static function add_valid_rows_to_array($reader, $headings)
    {
        $allRows = [];
        $allRowsInterval = 0;
        $rowCounter = 0;

        foreach ($reader as $index => $row)
        {
            $values = [];
            foreach ($headings as $key => $position)
            {
                if (!isset($row[$position]) || is_null($row[$position]))
                {
                    $row[$position] = '';
                }

                $values[0][$key] = $row[$position];
            }
            
            $allRows[$allRowsInterval][] = $values[0];
           
            $rowCounter ++;
            if ($rowCounter > 1000)
            {
                $allRowsInterval ++;
                $rowCounter = 0;
            }
        }

        return $allRows;
    }

   static function insert_records_into_location_table($allRows)
    {
        foreach ($allRows as $set)
        {
            DB::table('CAKUPAN_BU') -> insert($allRows); 
        }
    }

}
