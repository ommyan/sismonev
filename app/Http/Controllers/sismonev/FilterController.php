<?php

namespace App\Http\Controllers\sismonev;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterController extends Controller
{
    public function index(Request $request){
        $bulan = $request->input('bulan');
        $kanwilnya = $request->input('kanwil');
        $cabangnya = $request->input('cabang');
        $alamat =$request->input('alamat');
      //  echo $request->All();    
       
        $bln=explode("/", $bulan);
        $month=(int)($bln[0]);
        $year=(int)($bln[1]);
        $namakanwil=$request->input('namakanwil');
        $namacabang=$request->input('namacabang');
     //   dd($namakanwil,$namacabang);

        switch ($alamat) {
            case '/':
                return redirect()->route('home');
            case 'peserta':
                return redirect()->route('peserta', [
                    'kanwil' => $kanwilnya,
                    'cabang' => $cabangnya,
                    'bulan' => $month,
                    'tahun' => $year,
                    'program' =>'All',
                    'namakanwil' => $namakanwil . '/' . $namacabang

                ]);
                break;
            case 'pendapatan':
                return redirect()->route('pendapatan', [
                    'kanwil' => $kanwilnya,
                    'cabang' => $cabangnya,
                    'bulan' => $month,
                    'tahun' => $year,
                    'program' =>'All',
                     'namakanwil' => $namakanwil . '/' . $namacabang
                ]);
                break;
            case 'pembayaran':
                return redirect()->route('pembayaran', [
                    'kanwil' => $kanwilnya,
                    'cabang' => $cabangnya,
                    'bulan' => $month,
                    'tahun' => $year,
                    'program' =>'All',
                   'namakanwil' => $namakanwil . '/' . $namacabang
                ]);
                break;
            case 'organisasi':
                return redirect()->route('organisasi', [
                    'kanwil' => $kanwilnya,
                    'cabang' => $cabangnya,
                    'bulan' => $month,
                    'tahun' => $year,
                    'program' =>'All',
                    'namakanwil' => $namakanwil . '/' . $namacabang
                ]);
                break;
            case 'keuangan':
                return redirect()->route('keuangan', [
                    'kanwil' => $kanwilnya,
                    'cabang' => $cabangnya,
                    'bulan' => $month,
                    'tahun' => $year,
                    'program' =>'All',
                    'namakanwil' => $namakanwil . '/' . $namacabang
                ]);
                break;
        }


    }
}
