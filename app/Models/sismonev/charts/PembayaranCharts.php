<?php

namespace App\Models\sismonev\charts;

use Illuminate\Database\Eloquent\Model;
use Charts;
use App\Models\sismonev\data\manfaat;
class PembayaranCharts extends Model
{
    static function ManfaatBySegmen($program,$bulan,$tahun,$kanwil=null,$cabang=null){
        // $bulan=2;$tahun=2017;$program='All';
         $iuranLabel=[];$iuranValue=[];$iurantarget=[];$kasusValue=[];
         $iuran = manfaat::PembayaranBysegmen($program,$bulan,$tahun,$kanwil,$cabang);
      // DD($iuran);
      //   $iuran=sort($iuran);
        
             foreach($iuran as $iur) {
     
                    array_push($iuranLabel,$iur->SEGMEN);
                    array_push($iuranValue,$iur->JUMLAH);
                                     
                    array_push($iurantarget,$iur->JUMLAH + ((20/100) * $iur->JUMLAH));
             }
            
    // print_r($iuranLabel);
    // print_r($iuranValue);
     //dd($iuran);
     $pendapatan01=Charts::multi('bar-over', 'highcharts')
                 ->title('Pembayaran Manfaat  Per Segmen [Rp]')
                 ->elementLabel("Target")
                 ->legend(true)
                 ->dimensions(430, 400) 
               
                 ->colors(['rgb(255, 0, 0)', 'rgb(255, 245, 230,0.8)'])
                 ->pointPadding(['0.2','0.1'])
                 ->labels( $iuranLabel)
                 ->dataset('Aktual',$iuranValue )
                 ->dataset('Target',$iurantarget);
         // dd($pendapatan01); 
          return $pendapatan01;
          
     }
     
     static function ManfaatBySegmenKasus($program,$bulan,$tahun,$kanwil=null,$cabang=null){
        // $bulan=2;$tahun=2017;$program='All';
         $iuranLabel=[];$iuranValue=[];$iurantarget=[];$kasValue=[];
         $iuran = manfaat::PembayaranBysegmen($program,$bulan,$tahun,$kanwil,$cabang);
      // DD($iuran);
      //   $iuran=sort($iuran);
       
             foreach($iuran as $iur) {
     
                    array_push($iuranLabel,$iur->SEGMEN);
                    array_push($kasValue,$iur->JUMLAHKASUS);                    
                    array_push($iurantarget,0);
             }
            
    // print_r($iuranLabel);
  //  print_r($kasValue);
  //  dd($iuran);
     $pendapatan01=Charts::multi('bar-over-1', 'highcharts')
                 ->title('Pembayaran Manfaat  Per Segmen [Kasus]')
                 ->elementLabel("Target")
                 ->legend(true)
                 ->dimensions(430, 400) 
               
                 ->colors(['rgb(204, 235, 255)', 'rgb(0, 92, 153,0.2)'])
                 ->pointPadding(['0.2','0.1'])
                 ->labels( $iuranLabel)
                 ->dataset('Aktual',$kasValue)
                 ->dataset('Target',$iurantarget);
         // dd($pendapatan01); 
          return $pendapatan01;
          
     }

     static function ManfaatByJenisklaim($program,$bulan,$tahun,$kanwil=null,$cabang=null){
        // $bulan=2;$tahun=2017;$program='All';
         $iuranLabel=[];$iuranValue=[];$iurantarget=[];$kasusValue=[];
         $iuran = manfaat::PembayaranByjenisklaim($program,$bulan,$tahun,$kanwil,$cabang);
     
             foreach($iuran as $iur) {
     
                    array_push($iuranLabel,$iur->KLAIM);
                    array_push($iuranValue,$iur->JUMLAH);
                    array_push($kasusValue,$iur->JUMLAHKASUS);                    
                    array_push($iurantarget,$iur->JUMLAH + ((20/100) * $iur->JUMLAH));
             }
    
     $pendapatan01=Charts::create('column', 'highcharts')
                    ->title('Pembayaran Klaim berdasarkan Jenis Klaim [Rupiah]')
                    ->elementLabel('Klaim')
                    ->labels($iuranLabel) //$data->pluck('shoppingDate'))
                    ->values($iuranValue) //$data->pluck('price'))
                    ->dimensions(550, 500) ;
              
          return $pendapatan01;

     }

      static function ManfaatByJenisklaimkasus($program,$bulan,$tahun,$kanwil=null,$cabang=null){
        // $bulan=2;$tahun=2017;$program='All';
         $iuranLabel=[];$iuranValue=[];$iurantarget=[];$kasusValue=[];
         $iuran = manfaat::PembayaranByjenisklaim($program,$bulan,$tahun,$kanwil,$cabang);
     
             foreach($iuran as $iur) {
     
                    array_push($iuranLabel,$iur->KLAIM);
                    array_push($iuranValue,$iur->JUMLAH);
                    array_push($kasusValue,($iur->JUMLAHKASUS*1000));                    
                    array_push($iurantarget,($iur->JUMLAHKASUS*1000) + ((20/100) * ($iur->JUMLAHKASUS*1000)));
             }
    
     $pendapatankasus=Charts::create('column', 'highcharts')
                    ->title('Pembayaran Klaim berdasarkan Jenis Klaim (Jumlah Kasus) ')
                    ->elementLabel('Klaim')
                    ->labels($iuranLabel) //$data->pluck('shoppingDate'))
                    ->values($kasusValue) //$data->pluck('price'))
                    ->dimensions(550, 500) ;
              
          return $pendapatankasus;

     }
}
