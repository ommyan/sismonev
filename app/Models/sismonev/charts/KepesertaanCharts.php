<?php

namespace App\Models\sismonev\charts;

use Illuminate\Database\Eloquent\Model;
use Charts;
use App\Models\sismonev\data\peserta;
use App\Models\sismonev\data\cakupanBu;


class KepesertaanCharts extends Model
{
    static function bujenis($program,$bulan,$tahun,$kanwil,$cabang){
        $buJenis=CakupanBu::getBUjenis($program,$bulan,$tahun,$kanwil,$cabang);
       // dd($buJenis);
        $labelbuj=[];$valuebuj=[];
        foreach ($buJenis as $buJ){
            array_push($labelbuj,$buJ->JENIS_BU);
            array_push($valuebuj,$buJ->JUMLAH);
        }
      $char= Charts::create('bar-aktual', 'highcharts')
            ->title('Komposisi Peserta Badan Usaha Menurut Jenis Badan Usaha')
            ->labels($labelbuj)
            ->values($valuebuj)
            ->responsive(true)
          ->elementLabel('Jenis Badan Usaha')
          ->template('jamsos-orange')
        ->dimensions(200,100);
        return $char;
    }
    static function kep01($program,$segmen,$bulan,$tahun,$kanwil=null,$cabang=null)
    {
     $data=peserta::CakupanTotal($program,$segmen,$bulan,$tahun,$kanwil,$cabang);
     //dd($data);
     $CakBSValue=[];$CakBSLabel=[];$CakBSTarget=[];
        foreach($data as $CakBS) {
            $CakBSLabel[]=$CakBS['SEGMEN'];
            $targit=$CakBS['TARGET'];
            array_push($CakBSValue,$CakBS['JUMLAH']);
            array_push($CakBSTarget,$targit);
           
          }

    $kep01=Charts::multi('bar-over', 'highcharts')
                                    ->title('Perbandingan Aktual dengan Target Jumlah Peserta Menurut Segmen Kepesertaan')
                                    ->elementLabel("Jumlah Peserta")
                                    ->legend(true)
                                    ->dimensions(400, 400) 
                                    ->colors( ['rgb(252, 159, 211,0.7)','rgb(255, 7, 48,0.2)'])
                                    ->labels($CakBSLabel)
                                    ->pointPadding([0.2,0.1])
                                    ->dataset('Target', $CakBSTarget)
                                    ->dataset('Aktual', $CakBSValue);

   
        return $kep01;


    }

   static function kep02($program,$bulan,$tahun,$kanwil,$cabang)
    {
      //data/BU/skala/All/3/2017
    $data=peserta::BUbysegmenSkala($program,$bulan,$tahun,$kanwil,$cabang);
  // dd($data);
     $CakBSValue=[];$CakBSLabel=[];$BUTarget=[];
        foreach($data as $CakBS) {
                            $CakBSLabel[]=$CakBS->SKALA;
                            array_push($BUTarget,$CakBS->TARGET);
                            array_push($CakBSValue,$CakBS->JUMLAHBU);
        }

    $kep02=Charts::multi('bar-over', 'highcharts')
                                    ->title('Perbandingan Aktual dengan Target Jumlah Badan Usaha  Menurut Skala Usaha')
                                    ->elementLabel("Cakupan Peserta")
                                    ->legend(true)
                                    ->dimensions(400, 400) 
                                  //  ->responsive(true)
                                    ->colors(['rgb(0, 102, 204,0.5)','rgb(0, 204, 255,0.2)'])
                                    ->labels($CakBSLabel)
                                    ->pointPadding(['0.2','0.1'])


                                    ->dataset('Target',$BUTarget)
        ->dataset('Aktual', $CakBSValue);

                                    

                                    // Setup what the values mean
                                    
       // dd($kep01);
      //echo "target1". $target1; 
     return $kep02;


    }  

    static function kep03($segmen,$bulan,$tahun,$kanwil=null,$cabang=null)
    {
        $data = peserta::BUbyprogramtumbuh($segmen, $bulan, $tahun, $kanwil, $cabang);
        $CakBSValue = [];
        $tambah = [];
        $tumbuh = [];
        $CakBSLabel = [];
        $CakBSBulan = [];
        $GValue = [];
        $n = 0;

        //dd($data);
        foreach ($data as $CakBS) {
            $CakBSLabel[] = $CakBS->PROGRAM;
            $CakBSBulan[] = $CakBS->BULAN;
            array_push($CakBSValue, $CakBS->JUMLAH);
            $n++;
        }

        sort($CakBSValue);
        sort($CakBSBulan);
      //  sort($tumbuh);

        //   dd($CakBS);
        // dd($CakBSValue);
        $naik = 0;
        $t = 0;

        for ($x = 0; $x < (count($CakBSValue)); $x++) {
            if (isset($CakBSValue[$x])) {
                if ($x <> 0) {
                    $naik = (($CakBSValue[$x] - $CakBSValue[$x - 1]) / $CakBSValue[$x - 1]) * 100;
                } else {
                    $naik = 0;
                }
                array_push($tumbuh, $naik);

            }
        };


        if (count($tumbuh == 1)) {
            array_unshift($tumbuh, 0);
        }
        if (count($CakBSValue == 1)) {
            array_unshift($CakBSValue, 0);
        }

        $kep03 = Charts::multi('bar-lin', 'highcharts')
            ->title('Pertumbuhan Jumlah Peserta Per Program (Bulanan)')
            ->elementLabel("Target")
            ->legend(true)
            ->dimensions(430, 400)
            ->pointPadding(['0.1', '0.2'])
            //  ->responsive(true)
            ->colors(['rgb(0, 191, 255)', 'rgb(255, 87, 51)'])
            ->template('orange-matrial')
            ->program($segmen)
            ->labels($CakBSBulan)
            ->types(['column', 'spline'])

            ->dataset('Aktual', $CakBSValue)
            ->dataset('Pertumbuhan', $tumbuh);

        // dd($kep03);
        return $kep03;


    }  


    static function kepbu04($segmen,$bulan,$tahun,$kanwil=null,$cabang=null)
    {
      //data/BU/skala/All/3/2017
    $data=peserta::BUbyprogram($segmen,$bulan,$tahun,$kanwil,$cabang);
     // dd($data);
    // usort($data,1);
     $strjkk=[];$strjkm=[];$strjht=[];$strjp=[];$t=0;
      foreach ( $data as  $value){
        if($value->PROGRAM == 'JKK'){
          array_push($strjkk,$value->JUMLAH);
        }
        if($value->PROGRAM == 'JKM'){
          array_push($strjkm,$value->JUMLAH);
        }
        if($value->PROGRAM == 'JHT'){
          array_push($strjht,$value->JUMLAH);
        }
        if($value->PROGRAM == 'JP'){
          array_push($strjp,$value->JUMLAH);
        }
        $t++;
      }
      
      $BU_JumlahSkala=Charts::multi('bar', 'highcharts')
                                          ->title("Peserta PPU Badan Usaha Menurut Skala Usaha")
                                          ->dimensions(0, 400) // Width x Height
                                          ->legend(true)
                                          //->dataLabels(true)
                                          ->template('orange-material')
                                          ->labels(['Skala Mikro','Skala Kecil','Skala Menengah','Skala Besar']) ;  
      
                                          $BU_JumlahSkala->dataset('JKK',$strjkk);
                                          $BU_JumlahSkala->dataset('JKM',$strjkm);
                                          $BU_JumlahSkala->dataset('JHT',$strjht);
                                          $BU_JumlahSkala->dataset('JP',$strjp);
                                          
                                          //dd($kepbu04);
      return  $BU_JumlahSkala;
      
      
      }


  public function cagr($awal,$akhir){
    $cagr=(($akhir-$awal)/$awal)*100;
    return $cagr;
  }  
}
