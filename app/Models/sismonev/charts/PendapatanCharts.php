<?php

namespace App\Models\sismonev\charts;

use Illuminate\Database\Eloquent\Model;
use Charts;
use App\Models\sismonev\data\pendapatan;


class PendapatanCharts extends Model
{

 static function pend03($segmen,$bulan,$tahun,$kanwil=null,$cabang=null)
    {
        $data = pendapatan::TumbuhPendapatanBysegmenprogram($segmen, $bulan, $tahun, $kanwil, $cabang);
       
        $tambah = [];
        $tumbuh = [];
        
        $CakBSValue1 = [];
        $CakBSLabel1 = [];
        $CakBSBulan1 = [];

        $CakBSValue2 = [];
        $CakBSLabel2 = [];
        $CakBSBulan2 = [];

        $CakBSValue3 = [];
        $CakBSLabel3 = [];
        $CakBSBulan3 = [];

        $GValue = [];
        $n = 0;

       // dd($data);
        foreach ($data as $CakBS) {
          if($CakBS->SEGMEN='PPU')
          {
                $CakBSBulan1[] = $CakBS->BULAN;
                $CakBSLabel1[] = $CakBS->SEGMEN;
                array_push($CakBSValue1, $CakBS->JUMLAH);
          } elseif ($CakBS->SEGMEN='BPU')
          {
                $CakBSBulan2[] = $CakBS->BULAN;
                $CakBSLabel2[] = $CakBS->SEGMEN;
                array_push($CakBSValue2, $CakBS->JUMLAH);

          } else 
          {
                $CakBSBulan3[] = $CakBS->BULAN;
                $CakBSLabel3[] = $CakBS->SEGMEN;
                array_push($CakBSValue3, $CakBS->JUMLAH);
           }
            $n++;
        }

        sort($CakBSValue1);
        sort($CakBSBulan1);

        sort($CakBSValue2);
        sort($CakBSBulan2);
        
        sort($CakBSValue3);
        sort($CakBSBulan3);
       
        $naik1 = 0; $naik2=0;$naik3=0;
        $tumbuh1=[];$tumbuh2=[];$tumbuh3=[];

        $t = 0;

        for ($x = 0; $x < (count($CakBSValue1)); $x++) {
            if (isset($CakBSValue1[$x])) {
                if ($x <> 0) {
                    $naik1 = (($CakBSValue1[$x] - $CakBSValue1[$x - 1]) / $CakBSValue1[$x - 1]) * 100;
                } else {
                    $naik1 = 0;
                }
                array_push($tumbuh1, $naik1);

            }
        };

        for ($x = 0; $x < (count($CakBSValue2)); $x++) {
            if (isset($CakBSValue2[$x])) {
                if ($x <> 0) {
                    $naik2= (($CakBSValue2[$x] - $CakBSValue2[$x - 1]) / $CakBSValue2[$x - 1]) * 100;
                } else {
                    $naik2 = 0;
                }
                array_push($tumbuh2, $naik2);

            }
        };

        for ($x = 0; $x < (count($CakBSValue3)); $x++) {
            if (isset($CakBSValue3[$x])) {
                if ($x <> 0) {
                    $naik3 = (($CakBSValue3[$x] - $CakBSValue3[$x - 1]) / $CakBSValue3[$x - 1]) * 100;
                } else {
                    $naik3 = 0;
                }                
                array_push($tumbuh3, $naik1);
            }
        };

        if (count($tumbuh1 == 1)) {
            array_unshift($tumbuh1, 0);
        }

        if (count($tumbuh2 == 1)) {
            array_unshift($tumbuh2, 0);
        }

        if (count($tumbuh3 == 1)) {
            array_unshift($tumbuh3, 0);
        }

        if (count($CakBSValue1 == 1)) {
            array_unshift($CakBSValue1, 0);
        }

        if (count($CakBSValue2 == 1)) {
            array_unshift($CakBSValue2, 0);
        }

         if (count($CakBSValue3 == 1)) {
            array_unshift($CakBSValue3, 0);
        }



        $kep03 = Charts::multi('bar-lin', 'highcharts')
            ->title('Pertumbuhan Jumlah Pendapatan Per Segmen Per Program')
            ->elementLabel("Target")
            ->legend(true)
            ->dimensions(430, 400)
            ->pointPadding(['0.1', '0.2'])
            //  ->responsive(true)
            ->colors(['rgb(0, 191, 255)', 'rgb(255, 87, 51)'])
            ->template('orange-matrial')
            ->program($segmen)
            ->labels($CakBSBulan1)
            ->types(['column', 'spline'])

            ->dataset('Aktual', $CakBSValue1)
            ->dataset('PPU', $tumbuh1)
            ->dataset('BPU', $tumbuh2)
            ->dataset('JAKON', $tumbuh3);
            

        // dd($kep03);
        return $kep03;


    }  

static function PendapatanBySegmen($program,$bulan,$tahun,$kanwil=null,$cabang=null){
   // $bulan=2;$tahun=2017;$program='All';
    $iuranLabel=[];$iuranValue=[];$iurantarget=[];
    $iuran = pendapatan::PendapatanBysegmenprogram($program,$bulan,$tahun,$kanwil,$cabang);
    //DD($iuran);
 //   $iuran=sort($iuran);
   
        foreach($iuran as $iur) {

               array_push($iuranLabel,$iur->SEGMEN);
               array_push($iuranValue,$iur->JUMLAH);
               array_push($iurantarget,$iur->JUMLAH + ((20/100) * $iur->JUMLAH));
        }
       

$pendapatan01=Charts::multi('bar-over', 'highcharts')
            ->title('Perbandingan Aktual dengan Target Penerimaan Iuran Per Segmen')
            ->elementLabel("Target")
            ->legend(true)
            ->dimensions(430, 400) 
           // ->format('point.y:,.2f')
            ->pointPadding(['0.2','0.1'])
            //  ->responsive(true)
            ->colors(['rgb(255, 0, 0)', 'rgb(255, 245, 230,0.8)'])
            ->labels($iuranLabel)

            
            
            ->dataset('Aktual',$iuranValue )
            ->dataset('Target', $iurantarget);
          //  dd($pendapatan01);
            
     return $pendapatan01;
}

static function TumbuhPendapatanBySegmen($program,$bulan,$tahun,$kanwil=null,$cabang=null){
   // $bulan=2;$tahun=2017;$program='All';
    $iuranLabel=[];$iuranValue=[];$iurantarget=[];$iuranBulan=[];
    $iuranValuebpu=[];$iuranValueppu=[];$iuranValuejakon=[];$wulan=[];
   // dd($program);

    $iuran = pendapatan::TumbuhPendapatanBysegmenprogram($program,$bulan,$tahun,$kanwil,$cabang);
 
       // echo count($iuran);
         for ($x = 0; $x < count($iuran); $x++) {
             if ($iuran[$x]->ID_SEGMEN==2){
                    array_push($iuranValuebpu,$iuran[$x]->JUMLAH);
            }  
             if ($iuran[$x]->ID_SEGMEN==1){
                    array_push($iuranValueppu,$iuran[$x]->JUMLAH);
            }  
             if ($iuran[$x]->ID_SEGMEN==3){
                    array_push($iuranValuejakon,$iuran[$x]->JUMLAH);
            }  
            $monthNum  = 3;
            
            
                    $swulan=$iuran[$x]->BULAN;
                  //  $dateObj   = DateTime::createFromFormat('!m', $swulan);
                    $swulan = date("M", mktime(0, 0, 0, $swulan, 10));
                 //   $swulan = $dateObj->format('F'); // March
                    array_push($wulan,$swulan);
         //   array_pluck()
           
         }
 //dd($iuran);   
 $bulan=array_unique($wulan);

$pendapatan01=Charts::multi('bar', 'highcharts')
            ->title('Pertumbuhan Penerimaan Iuran Bulanan Per Segmen')
            ->elementLabel("Target")
            ->legend(true)
            ->dimensions(430, 400) 
            ->labels($bulan)
            ->dataset('PPU',$iuranValueppu)
            ->dataset('BPU',$iuranValuebpu)
            ->dataset('JAKON',$iuranValuejakon);

     return $pendapatan01;
}

}
