<?php

namespace App\Models\sismonev\charts;

use Illuminate\Database\Eloquent\Model;
use Charts;
use App\Models\sismonev\data\piutang;


class PiutangCharts extends Model
{
    static function PiutangBySegmen($program,$bulan,$tahun,$kanwil=null,$cabang=null){
        // $bulan=2;$tahun=2017;$program='All';
         $iuranLabel=[];$iurantarget=[];
         $iuranValue1=[];$iuranValue2=[];$iuranValue3=[];
         $iuran = piutang::PiutangBysegmenprogram($program,$bulan,$tahun,$kanwil,$cabang);
       // DD($iuran);
      //   $iuran=sort($iuran);
        
             foreach($iuran as $iur) {
     
                array_push($iuranLabel,$iur->SEGMEN);

                if(!empty($iur->LANCAR)){
                   array_push($iuranValue1,$iur->LANCAR);
                } else {
                    array_push($iuranValue1,0);
                }
                if(!empty($iur->KURANGLANGCAR)){
                    array_push($iuranValue2,$iur->KURANGLANGCAR);
                 } else {
                    array_push($iuranValue2,0);
                 }
                 if(!empty($iur->MACET)){
                    array_push($iuranValue3,$iur->MACET);
                 } else {
                    array_push($iuranValue3,0);
                 }

                  
                   
                  //  array_push($iurantarget,$iur->JUMLAH + ((20/100) * $iur->JUMLAH));
             }
          /*  
             $iuranValue1=sort($iuranValue1);
             $iuranValue2=sort($iuranValue2);
             $iuranValue3=sort($iuranValue3);
             $iuranLabel=sort($iuranLabel);
         */
         print_r($iuranValue2);

     //    DD($iuran);
     $pendapatan01=Charts::multi('bar', 'highcharts')
                 ->title('Piutang Iuran Berdasarkan Kategori Piutang')
                 ->elementLabel("Target")
                 ->legend(true)
                 ->dimensions(430, 400)
         ->template("orange-material")
                 //  ->responsive(true)
                // ->colors(['rgb(255, 0, 0)', 'rgb(255, 245, 230,0.8)'])
                 ->labels($iuranLabel)

                 ->dataset('LANCAR',$iuranValue1 )
                 ->dataset('KURANG LANCAR', $iuranValue2)
                 ->dataset('MACET', $iuranValue3);
          return $pendapatan01;
     }   


}
