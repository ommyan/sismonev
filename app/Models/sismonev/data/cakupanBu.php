<?php

namespace App\Models\sismonev\data;

use Illuminate\Database\Eloquent\Model;
use DB;

class cakupanBu extends Model
{
    //
    protected $table = "CAKUPAN_BU";
    protected $fillable = [
        'BULAN' ,
        'TAHUN',          
        'ID_KANWIL',
        'ID_CABANG',
        'ID_PROGRAM' ,
        'ID_SEGMEN',
        'ID_SKALA',
        'ID_JENIS_BU',
        'JUMLAH' 
    ];

    static function getBUProgram($program,$bulan,$tahun,$kanwil,$cabang){


       $data=DB::table('CAKUPAN_BU')
            ->select('ID_PROGRAM',DB::raw('SUM(JUMLAH) as JUMLAH' ),DB::raw('SUM(JUMLAH_BU) as JUMLAHBU') )
            ->where('ID_PROGRAM',$program)
            ->where('BULAN',$bulan)
            ->where('TAHUN',$tahun)
            ->groupBy('ID_PROGRAM')->get();
      //  $data = json_decode(($data), True);
        return $data;

    }

    static function getBUSkala($program,$skala,$bulan,$tahun,$kanwil,$cabang){

        $data = DB::table('CAKUPAN_BU')
            ->select('ID_PROGRAM',DB::raw('SUM(JUMLAH) as JUMLAH' ),DB::raw('SUM(JUMLAH_BU) as JUMLAHBU' ))
            ->where('ID_PROGRAM',$program)
            ->whereAnd('ID_SKALA',$skala)
            ->where('BULAN',$bulan)
            ->where('TAHUN',$tahun)
            ->get();

        return $data;

    }
    static function getBUSkalaAll($program,$bulan,$tahun,$kanwil,$cabang){
        DB::enableQueryLog();
        $data = DB::table('CAKUPAN_BU')
            ->select('ID_PROGRAM',DB::raw('SUM(JUMLAH) as JUMLAH' ),DB::raw('SUM(JUMLAH_BU) as JUMLAHBU' ))
            ->where('ID_PROGRAM',$program)
            ->where('BULAN',$bulan)
            ->where('TAHUN',$tahun)
            ->groupBy('CAKUPAN_BU.ID_SKALA')
            ->get();
          //  dd(DB::getQueryLog()); 
           //     dd ($data);
        return $data;

    }

    static function getBUjenis($program,$bulan,$tahun,$kanwil,$cabang){
        
        if(!empty($kanwil)){
            if(!empty($cabang)){
                $data = DB::table('CAKUPAN_BU')
                    -> join('JENIS_BU', 'CAKUPAN_BU.ID_JENIS_BU', '=', 'JENIS_BU.ID_JENIS_BU')
                    ->select('CAKUPAN_BU.ID_JENIS_BU as ID_JENIS_BU','JENIS_BU.JENIS_BU', DB::raw('SUM(JUMLAH_BU) as JUMLAH' ))
                    ->where('ID_PROGRAM',$program)
                    ->where('ID_KANWIL',$kanwil)
                    ->where('ID_CABANG',$cabang)
                    ->where('BULAN',$bulan)
                    ->where('TAHUN',$tahun)
                    ->groupBy('CAKUPAN_BU.ID_JENIS_BU')
                    ->get();
            }else {
                $data = DB::table('CAKUPAN_BU')
                    -> join('JENIS_BU', 'CAKUPAN_BU.ID_JENIS_BU', '=', 'JENIS_BU.ID_JENIS_BU')
                    ->select('CAKUPAN_BU.ID_JENIS_BU as ID_JENIS_BU','JENIS_BU.JENIS_BU', DB::raw('SUM(JUMLAH_BU) as JUMLAH' ))
                    ->where('ID_PROGRAM',$program)
                    ->where('ID_KANWIL',$kanwil)
                    ->where('BULAN',$bulan)
                    ->where('TAHUN',$tahun)
                    ->groupBy('CAKUPAN_BU.ID_JENIS_BU')
                    ->get();
            }
        } else {
            $data = DB::table('CAKUPAN_BU')
                -> join('JENIS_BU', 'CAKUPAN_BU.ID_JENIS_BU', '=', 'JENIS_BU.ID_JENIS_BU')
                ->select('CAKUPAN_BU.ID_JENIS_BU as ID_JENIS_BU','JENIS_BU.JENIS_BU', DB::raw('SUM(JUMLAH_BU) as JUMLAH' ))
                ->where('CAKUPAN_BU.ID_PROGRAM',$program)
                ->where('CAKUPAN_BU.BULAN',$bulan)
                ->where('CAKUPAN_BU.TAHUN',$tahun)
                ->orderBy('CAKUPAN_BU.ID_JENIS_BU', 'asc')
                ->groupBy('CAKUPAN_BU.ID_JENIS_BU')
                ->get();
        }
     //   dd($program,$bulan,$tahun,$kanwil,$cabang);
        return $data;

    }
    static function getBUbyprogram($program,$bulan,$tahun,$kanwil,$cabang){
       // dd($program,$bulan,$tahun,$kanwil,$cabang);
 DB::enableQueryLog();
       // dd($program,$bulan,$tahun,$kanwil,$cabang);
        if(!empty($kanwil)){
            if(!empty($cabang)){
                $data = DB::table('CAKUPAN_BU')
                    ->join('PROGRAM','CAKUPAN_BU.ID_PROGRAM','=','PROGRAM.ID_PROGRAM')
                    ->select('CAKUPAN_BU.ID_PROGRAM as ID_PROGRAM','PROGRAM.PROGRAM', DB::raw('SUM(JUMLAH_BU) as JUMLAH' ))
                    ->where('ID_KANWIL',$kanwil)
                    ->where('ID_CABANG',$cabang)
                    ->where('BULAN',$bulan)
                    ->where('TAHUN',$tahun)
                    ->groupBy('CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM')
                    ->get();
            }else {
                $data = DB::table('CAKUPAN_BU')
                    ->join('PROGRAM','CAKUPAN_BU.ID_PROGRAM','=','PROGRAM.ID_PROGRAM')
                    ->select('CAKUPAN_BU.ID_PROGRAM as ID_PROGRAM','PROGRAM.PROGRAM', DB::raw('SUM(JUMLAH_BU) as JUMLAH' ))
                    ->where('ID_KANWIL',$kanwil)
                    ->where('BULAN',$bulan)
                    ->where('TAHUN',$tahun)
                    ->groupBy('CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM')
                    ->get();
            }
        } else {
            $data = DB::table('CAKUPAN_BU')
                ->join('PROGRAM','CAKUPAN_BU.ID_PROGRAM','=','PROGRAM.ID_PROGRAM')
                ->select('CAKUPAN_BU.ID_PROGRAM as ID_PROGRAM','PROGRAM.PROGRAM', DB::raw('SUM(JUMLAH_BU) as JUMLAH' ))
                ->where('CAKUPAN_BU.BULAN',$bulan)
                ->where('CAKUPAN_BU.TAHUN',$tahun)
                ->groupBy('CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM')
                ->get();
        }
       // print_r($data);
      //  dd(DB::getQueryLog()); 
     //   dd($data);
        return $data;

    }


}

