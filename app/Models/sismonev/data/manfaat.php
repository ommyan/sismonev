<?php

namespace App\Models\sismonev\data;

use Illuminate\Database\Eloquent\Model;

use DB;

class manfaat extends Model
{
    protected $table='MANFAAT';

    static function PembayaranBysegmen($program,$bulan,$tahun,$kanwil=null,$cabang=null){
        /*
        mengakses table MANFAAT,segmen untuk mendapatkan jumlah badan usaha per skala usaha difilter
        berdasarkan bulan,tahun,program,kanwil,cabang
        filter berdasarkan program dan waktu.
        use link 'data/bu/program/1/3/2017''
        */
        //print 'seg'.$segmen .'bul '. $bulan .'ta '. $tahun .'kan ' . $kanwil .'cab '. $cabang;
        DB::enableQueryLog();
                    if ($program=='All'){    
                    $data = DB::table('MANFAAT')
                                        -> join('SEGMEN', 'MANFAAT.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                        ->select('MANFAAT.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'),DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS'))
                                         ->where('MANFAAT.BULAN','<=',$bulan)
                                        ->where('MANFAAT.TAHUN',$tahun)
                                        ->orderBy('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM', 'asc')
                                        ->groupBy('MANFAAT.ID_SEGMEN','SEGMEN.SEGMEN')
                                        ->get();
                  } elseif (is_null($cabang) and is_null($kanwil)) {
        
                    $data = DB::table('MANFAAT')   
                                        -> join('SEGMEN', 'MANFAAT.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                        ->select('MANFAAT.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'),DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS' ))
                                        ->where('MANFAAT.ID_PROGRAM',$program)
                                        ->where('MANFAAT.BULAN','<=',$bulan)
                                        ->where('MANFAAT.TAHUN',$tahun)
                                       ->orderBy('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM', 'asc')
                                        ->groupBy('MANFAAT.ID_SEGMEN','SEGMEN.SEGMEN')
                                        ->get();
                                      
                    }elseif (is_null($cabang)) {
        
                    $data = DB::table('MANFAAT')  
                                        -> join('SEGMEN', 'MANFAAT.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                        ->select('MANFAAT.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'),DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS' ))
                                        ->where('MANFAAT.ID_PROGRAM',$program)
                                        ->where('MANFAAT.BULAN','<=',$bulan)
                                        ->where('MANFAAT.TAHUN',$tahun)
                                        ->where('MANFAAT.ID_KANWIL',$kanwil)
                                       ->orderBy('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM', 'asc')
                                        ->groupBy('MANFAAT.ID_SEGMEN','SEGMEN.SEGMEN')
                                        ->get();
                    }  else {
                    $data = DB::table('MANFAAT')
                                       -> join('SEGMEN', 'MANFAAT.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                       ->select('MANFAAT.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'),DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS' ))
                                        ->where('MANFAAT.ID_PROGRAM',$program)
                                        ->where('MANFAAT.BULAN','<=',$bulan)
                                        ->where('MANFAAT.TAHUN',$tahun)
                                        ->where('MANFAAT.ID_KANWIL',$kanwil)
                                        ->where('MANFAAT.ID_CABANG',$cabang)
                                       ->orderBy('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM', 'asc')
                                        ->groupBy('MANFAAT.ID_SEGMEN','SEGMEN.SEGMEN')
                                        ->get();
               }       
               // dd($data);
              //  dd(DB::getQueryLog());         
        return $data;  
               
        } 
        static function PembayaranByprogram($program,$bulan,$tahun,$kanwil=null,$cabang=null){
            /*
            mengakses table MANFAAT,segmen untuk mendapatkan jumlah badan usaha per skala usaha difilter
            berdasarkan bulan,tahun,program,kanwil,cabang
            filter berdasarkan program dan waktu.
            use link 'data/bu/program/1/3/2017''
            */
            //print 'seg'.$segmen .'bul '. $bulan .'ta '. $tahun .'kan ' . $kanwil .'cab '. $cabang;
            DB::enableQueryLog();
                        if ($program=='All'){    
                        $data = DB::table('MANFAAT')
                                            -> join('SEGMEN', 'MANFAAT.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                            ->select('MANFAAT.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'),DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS'))
                                            ->where('MANFAAT.BULAN','<=',$bulan)
                                            ->where('MANFAAT.TAHUN',$tahun)
                                            ->orderBy('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM', 'asc')
                                            ->groupBy('MANFAAT.ID_SEGMEN','SEGMEN.SEGMEN')
                                            ->get();
                      } elseif (is_null($cabang) and is_null($kanwil)) {
            
                        $data = DB::table('MANFAAT')   
                                            -> join('SEGMEN', 'MANFAAT.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                            ->select('MANFAAT.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'),DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS' ))
                                            ->where('MANFAAT.ID_PROGRAM',$program)
                                            ->where('MANFAAT.BULAN','<=',$bulan)
                                            ->where('MANFAAT.TAHUN',$tahun)
                                           ->orderBy('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM', 'asc')
                                            ->groupBy('MANFAAT.ID_SEGMEN','SEGMEN.SEGMEN')
                                            ->get();
                                          
                        }elseif (is_null($cabang)) {
            
                        $data = DB::table('MANFAAT')  
                                            -> join('SEGMEN', 'MANFAAT.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                            ->select('MANFAAT.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'),DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS' ))
                                            ->where('MANFAAT.ID_PROGRAM',$program)
                                            ->where('MANFAAT.BULAN','<=',$bulan)
                                            ->where('MANFAAT.TAHUN',$tahun)
                                            ->where('MANFAAT.ID_KANWIL',$kanwil)
                                           ->orderBy('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM', 'asc')
                                            ->groupBy('MANFAAT.ID_SEGMEN','SEGMEN.SEGMEN')
                                            ->get();
                        }  else {
                        $data = DB::table('MANFAAT')
                                           -> join('SEGMEN', 'MANFAAT.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                           ->select('MANFAAT.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'),DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS' ))
                                            ->where('MANFAAT.ID_PROGRAM',$program)
                                            ->where('MANFAAT.BULAN','<=',$bulan)
                                            ->where('MANFAAT.TAHUN',$tahun)
                                            ->where('MANFAAT.ID_KANWIL',$kanwil)
                                            ->where('MANFAAT.ID_CABANG',$cabang)
                                           ->orderBy('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM', 'asc')
                                            ->groupBy('MANFAAT.ID_SEGMEN','SEGMEN.SEGMEN')
                                            ->get();
                   }       
                   // dd($data);
                  //  dd(DB::getQueryLog());         
            return $data;  
                   
            }

    static function PembayaranByJenisKlaim($program, $bulan, $tahun, $kanwil = null, $cabang = null)
    {
        /*
        mengakses table MANFAAT,segmen untuk mendapatkan jumlah badan usaha per skala usaha difilter
        berdasarkan bulan,tahun,program,kanwil,cabang
        filter berdasarkan program dan waktu.
        use link 'data/bu/program/1/3/2017''
        */
        //print 'seg'.$segmen .'bul '. $bulan .'ta '. $tahun .'kan ' . $kanwil .'cab '. $cabang;
        DB::enableQueryLog();

        if ($program == 'All') {
            //        echo 'All';
            if ($kanwil) {
                //           echo 'kanwil  ' . $kanwil;
                if ($cabang) {
                    //               echo 'Cabang' . $cabang;
                    $data = DB::table('MANFAAT')
                        ->join('KLAIM', 'MANFAAT.ID_KLAIM', '=', 'KLAIM.ID_KLAIM')
                        ->select('MANFAAT.ID_KLAIM', 'KLAIM.KLAIM', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'), DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS'))
                        ->where('MANFAAT.BULAN','<=', $bulan)
                        ->where('MANFAAT.TAHUN', $tahun)
                        ->where('MANFAAT.ID_KANWIL', $kanwil)
                        ->where('MANFAAT.ID_CABANG', $cabang)
                        ->orderBy('MANFAAT.ID_KLAIM', 'KLAIM.KLAIM', 'asc')
                        ->groupBy('MANFAAT.ID_KLAIM', 'KLAIM.KLAIM')
                        ->get();

                } else {
                    //               echo 'Cabang Kosong ' . $cabang;
                    $data = DB::table('MANFAAT')
                        ->join('KLAIM', 'MANFAAT.ID_KLAIM', '=', 'KLAIM.ID_KLAIM')
                        ->select('MANFAAT.ID_KLAIM', 'KLAIM.KLAIM', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'), DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS'))
                        ->where('MANFAAT.BULAN','<=', $bulan)
                        ->where('MANFAAT.TAHUN', $tahun)
                        ->where('MANFAAT.ID_KANWIL', $kanwil)
                        ->orderBy('MANFAAT.ID_KLAIM', 'KLAIM.KLAIM', 'asc')
                        ->groupBy('MANFAAT.ID_KLAIM', 'KLAIM.KLAIM')
                        ->get();
                }
            } else {
                //           echo 'Kanwilnya adalah ' . $kanwil;
                $data = DB::table('MANFAAT')
                    ->join('KLAIM', 'MANFAAT.ID_KLAIM', '=', 'KLAIM.ID_KLAIM')
                    ->select('MANFAAT.ID_KLAIM', 'KLAIM.KLAIM', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'), DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS'))
                    ->where('MANFAAT.BULAN','<=', $bulan)
                    ->where('MANFAAT.TAHUN', $tahun)
                    ->orderBy('MANFAAT.ID_KLAIM', 'KLAIM.KLAIM', 'asc')
                    ->groupBy('MANFAAT.ID_KLAIM', 'KLAIM.KLAIM')
                    ->get();
            }
        } else {
            //       echo 'Program ' . $program;
            if ($kanwil) {
                //           echo 'kanwil ' . $kanwil;
                if ($cabang) {
                    //               echo 'Cabang' . $cabang;
                    $data = DB::table('MANFAAT')
                        ->join('KLAIM', 'MANFAAT.ID_KLAIM', '=', 'KLAIM.ID_KLAIM')
                        ->select('MANFAAT.ID_KLAIM', 'KLAIM.KLAIM', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'), DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS'))
                        ->where('MANFAAT.ID_PROGRAM', $program)
                        ->where('MANFAAT.BULAN','<=', $bulan)
                        ->where('MANFAAT.TAHUN', $tahun)
                        ->where('MANFAAT.ID_KANWIL', $kanwil)
                        ->where('MANFAAT.ID_CABANG', $cabang)
                        ->orderBy('MANFAAT.ID_KLAIM', 'KLAIM.KLAIM', 'asc')
                        ->groupBy('MANFAAT.ID_KLAIM', 'KLAIM.KLAIM')
                        ->get();
                } else {
                    //               echo 'Cabang Kosong ' . $cabang;
                    $data = DB::table('MANFAAT')
                        ->join('KLAIM', 'MANFAAT.ID_KLAIM', '=', 'KLAIM.ID_KLAIM')
                        ->select('MANFAAT.ID_KLAIM', 'KLAIM.KLAIM', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'), DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS'))
                        ->where('MANFAAT.ID_PROGRAM', $program)
                        ->where('MANFAAT.BULAN','<=', $bulan)
                        ->where('MANFAAT.TAHUN', $tahun)
                        ->where('MANFAAT.ID_KANWIL', $kanwil)
                        ->orderBy('MANFAAT.ID_KLAIM', 'KLAIM.KLAIM', 'asc')
                        ->groupBy('MANFAAT.ID_KLAIM', 'KLAIM.KLAIM')
                        ->get();
                }
            } else {
                //           echo 'Kanwilnya adalah ' . $kanwil;
                $data = DB::table('MANFAAT')
                    ->join('KLAIM', 'MANFAAT.ID_KLAIM', '=', 'KLAIM.ID_KLAIM')
                    ->select('MANFAAT.ID_KLAIM', 'KLAIM.KLAIM', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'), DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS'))
                    ->where('MANFAAT.ID_PROGRAM', $program)
                    ->where('MANFAAT.BULAN','<=', $bulan)
                    ->where('MANFAAT.TAHUN', $tahun)
                    ->orderBy('MANFAAT.ID_KLAIM', 'KLAIM.KLAIM', 'asc')
                    ->groupBy('MANFAAT.ID_KLAIM', 'KLAIM.KLAIM')
                    ->get();
            }
        }

       // dd($data);
        //  dd(DB::getQueryLog());
        return $data;

    }
    

        static function PembayaranTotalBysegmen($program,$bulan,$tahun,$kanwil=null,$cabang=null){
            /*
            mengakses table MANFAAT,segmen untuk mendapatkan jumlah badan usaha per skala usaha difilter
            berdasarkan bulan,tahun,program,kanwil,cabang
            filter berdasarkan program dan waktu.
            use link 'data/bu/program/1/3/2017''
            */
            //print 'seg'.$segmen .'bul '. $bulan .'ta '. $tahun .'kan ' . $kanwil .'cab '. $cabang;
            DB::enableQueryLog();
                        if ($program=='All'){    
                        $data = DB::table('MANFAAT')
                                            -> join('SEGMEN', 'MANFAAT.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                            -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'),DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS' ))
                                            ->where('MANFAAT.BULAN','<=',$bulan)
                                            ->where('MANFAAT.TAHUN',$tahun)
                                            ->orderBy('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM', 'asc')
                                            ->groupBy('MANFAAT.ID_SEGMEN','SEGMEN.SEGMEN','MANFAAT.ID_PROGRAM')
                                            ->get();
                      } elseif (is_null($cabang) and is_null($kanwil)) {
            
                        $data = DB::table('MANFAAT')   
                                            -> join('SEGMEN', 'MANFAAT.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                            -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'),DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS' ))
                                            ->where('MANFAAT.ID_PROGRAM',$program)
                                            ->where('MANFAAT.BULAN','<=',$bulan)
                                            ->where('MANFAAT.TAHUN',$tahun)
                                           ->orderBy('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM', 'asc')
                                            ->groupBy('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN','SEGMEN.SEGMEN')
                                            ->get();
                                          
                        }elseif (is_null($cabang)) {
            
                        $data = DB::table('MANFAAT')  
                                            -> join('SEGMEN', 'MANFAAT.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                            -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'),DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS' ))
                                            ->where('MANFAAT.ID_PROGRAM',$program)
                                            ->where('MANFAAT.BULAN','<=',$bulan)
                                            ->where('MANFAAT.TAHUN',$tahun)
                                            ->where('MANFAAT.ID_KANWIL',$kanwil)
                                           ->orderBy('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM', 'asc')
                                            ->groupBy('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN','SEGMEN.SEGMEN')
                                            ->get();
                        }  else {
                        $data = DB::table('MANFAAT')
                                           -> join('SEGMEN', 'MANFAAT.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                            -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'),DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS' ))
                                            ->where('MANFAAT.ID_PROGRAM',$program)
                                            ->where('MANFAAT.BULAN','<=',$bulan)
                                            ->where('MANFAAT.TAHUN',$tahun)
                                            ->where('MANFAAT.ID_KANWIL',$kanwil)
                                            ->where('MANFAAT.ID_CABANG',$cabang)
                                           ->orderBy('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM', 'asc')
                                            ->groupBy('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN','SEGMEN.SEGMEN')
                                            ->get();
                   }
            $data = json_decode(json_encode($data), True);
                  //  dd(DB::getQueryLog());         
            return $data;  
                   
            }         
}
