<?php
/*
Coding ini di dedikasikan kepada Negara Republik Indonesia
untuk orang-orang yang peduli pada jaminan sosial ketenagakerjaan,
peduli pada tenaga kerja, buruh dan seluruh pekerja
- ditulis : Yan Kusyanto
- Tanggal : 02 Februari 2017
*/
namespace App\Models\sismonev\data;

use Illuminate\Database\Eloquent\Model;
use DB;
use stdClass;

class peserta extends Model
{
protected $table="CAKUPAN";
protected $primaryKey = 'ID';


static function getTarget($program,$segmen){
  //$data=DB::table('TARGET')->where('username', $username)->pluck('groupName');
  $data = DB::table('TARGET')
          ->select('TARGET')
          ->where('TARGET.ID_PROGRAM',$program)
          ->where('TARGET.ID_SEGMEN',$segmen)
          ->pluck('TARGET');
 // $data = json_decode(json_encode($data), True);
  //  echo $program . " - " . $segmen;
 //   dd($data) ;
  return $data;        
}
static function getBulanTahun(){
    $data=DB::table('CAKUPAN')
            ->SELECT('BULAN','TAHUN')
            ->groupBy('BULAN','TAHUN')
            ->get();
    return $data;        
}
static function getMaxBulanTahun($tablenya){
    $data1 = DB::table($tablenya)->max('BULAN');
    $data2 = DB::table($tablenya)->max('TAHUN');
    $data=$data1 . '/' . $data2;
    return $data;        
}
static function getMaxBulan($tablenya){
        $data = DB::table($tablenya)->max('BULAN');
        return $data;
    }
    static function getMaxTahun($tablenya){
        $data = DB::table($tablenya)->max('TAHUN');
        return $data;
    }

static function getRKATSegmen($segmen,$tahun){
  $data=DB::table('TARGET')
            ->SELECT('ID_SEGMEN as ID_TARGET','TARGET')
            ->where('TAHUN','=',$tahun)
            ->where('ID_SEGMEN','=',$segmen)
            ->get();
  //dd($data);
  return $data;
}
static function getRKATSkala($segmen,$tahun){
  $data=DB::table('TARGET')
            ->SELECT('ID_SKALA as ID_SKALA','TARGET')
            ->where('TAHUN','=',$tahun)
            ->where('ID_SKALA','=',$segmen)
            ->get();
  //dd($data);
  return $data;
}

static function AjaxListPesertaKanwil($program,$bulan,$tahun){
    DB::enableQueryLog();
        $data=DB::table('CAKUPAN')
            ->join('KANWIL','CAKUPAN.ID_KANWIL','=','KANWIL.ID_KANWIL')
            ->select(DB::raw("CAKUPAN.ID_KANWIL as ID,KANWIL.KANWIL,
            FORMAT(SUM(CASE WHEN ( ID_SEGMEN=1) THEN JUMLAH_PESERTA END),0) AS PPUJKK,
            FORMAT(SUM(CASE WHEN ( ID_SEGMEN=2) THEN JUMLAH_PESERTA END),0) AS BPUJKK,
            FORMAT(SUM(CASE WHEN ( ID_SEGMEN=3) THEN JUMLAH_PESERTA END),0) AS JAKONJKK"))
            ->where('CAKUPAN.ID_PROGRAM',$program)
            ->where('CAKUPAN.BULAN',$bulan)
            ->where('CAKUPAN.TAHUN',$tahun)
            ->orderBy('CAKUPAN.ID_KANWIL','KANWIL.KANWIL','asc') 
            ->groupBy('CAKUPAN.ID_KANWIL','KANWIL.KANWIL')    
            ->get();
       //$data = json_decode(json_encode($data), True);
      //     dd(DB::getQueryLog()); 
      //  dd($data);

        return $data;

}
static function AjaxListPesertaCabang($program,$kanwil,$bulan,$tahun){
    DB::enableQueryLog();
        $data=DB::table('CAKUPAN')
            ->join('CABANG','CAKUPAN.ID_CABANG','=','CABANG.ID_CABANG')
            ->select(DB::raw("CAKUPAN.ID_CABANG as ID_CABANG,CABANG.CABANG,TYPE AS TIPE,
            FORMAT(SUM(CASE WHEN ( ID_SEGMEN=1) THEN JUMLAH_PESERTA END),0) AS PPUJKK,FORMAT(SUM(CASE WHEN ( ID_SEGMEN=2) THEN JUMLAH_PESERTA END),0) AS BPUJKK,FORMAT(SUM(CASE WHEN ( ID_SEGMEN=3) THEN JUMLAH_PESERTA END),0) AS JAKONJKK"))
            ->where('CAKUPAN.ID_PROGRAM',$program)
            ->where('CAKUPAN.BULAN',$bulan)
            ->where('CAKUPAN.TAHUN',$tahun)
            ->where('CAKUPAN.ID_KANWIL',$kanwil)
            ->orderBy('CAKUPAN.ID_CABANG','CABANG.CABANG','asc') 
            ->groupBy('CAKUPAN.ID_CABANG','CABANG.CABANG')    
            ->get();
       //$data = json_decode(json_encode($data), True);
      //    dd(DB::getQueryLog()); 
       // dd($data);

        return $data;

}

static function jmlpesertaBysegmen($segmen,$bulan,$tahun,$kanwil=null,$cabang=null){
/*
mengakses table CAKUPAN untuk mendapatkan jumlah peserta per segmen difilter
berdasarkan bulan,tahun,program,kanwil,cabang
filter berdasarkan program dan waktu.
use link 'data/peserta/segmen/1/3/2017''
*/
DB::enableQueryLog();
            if ($segmen=='All'){    
            $data = DB::table('CAKUPAN')
                             -> join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('CAKUPAN.ID_SEGMEN as ID_SEGMEN','CAKUPAN.ID_PROGRAM as ID_PROGRAM','PROGRAM.PROGRAM as PROGRAM', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH' ))
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_SEGMEN','CAKUPAN.ID_PROGRAM','PROGRAM.PROGRAM')
                                 ->get();
                } elseif (is_null($cabang) and is_null($kanwil)) {

            $data = DB::table('CAKUPAN')       
                                -> join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('CAKUPAN.ID_SEGMEN as ID_SEGMEN','CAKUPAN.ID_PROGRAM as ID_PROGRAM','PROGRAM.PROGRAM as PROGRAM', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH' ))
                                ->where('CAKUPAN.ID_SEGMEN',$segmen)
                                ->where('CAKUPAN.BULAN',$bulan)
                                ->where('CAKUPAN.TAHUN',$tahun)
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_SEGMEN','CAKUPAN.ID_PROGRAM','PROGRAM.PROGRAM')
                                 ->get();
                              
            } elseif (is_null($kanwil)) {
             $data = DB::table('CAKUPAN')       
                                -> join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('CAKUPAN.ID_SEGMEN as ID_SEGMEN','CAKUPAN.ID_PROGRAM as ID_PROGRAM','PROGRAM.PROGRAM as PROGRAM', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH' ))
                                ->where('CAKUPAN.ID_SEGMEN',$segmen)
                                ->where('CAKUPAN.BULAN',$bulan)
                                ->where('CAKUPAN.TAHUN',$tahun)
                                ->where('CAKUPAN.ID_CABANG',$cabang)
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_SEGMEN','CAKUPAN.ID_PROGRAM','PROGRAM.PROGRAM')
                                 ->get();
    

            } elseif (is_null($cabang)) {

            $data = DB::table('CAKUPAN')       
                                 -> join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('CAKUPAN.ID_SEGMEN as ID_SEGMEN','CAKUPAN.ID_PROGRAM as ID_PROGRAM','PROGRAM.PROGRAM as PROGRAM', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH' ))
                                ->where('CAKUPAN.ID_SEGMEN',$segmen)
                                ->where('CAKUPAN.BULAN',$bulan)
                                ->where('CAKUPAN.TAHUN',$tahun)
                                ->where('CAKUPAN.ID_KANWIL',$kanwil)
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_SEGMEN','CAKUPAN.ID_PROGRAM','PROGRAM.PROGRAM')
                                 ->get();

            }  else {
            $data = DB::table('CAKUPAN')
                                -> join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('CAKUPAN.ID_SEGMEN as ID_SEGMEN','CAKUPAN.ID_PROGRAM as ID_PROGRAM','PROGRAM.PROGRAM as PROGRAM', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH' ))
                                ->where('CAKUPAN.ID_SEGMEN',$segmen)
                                ->where('CAKUPAN.BULAN',$bulan)
                                ->where('CAKUPAN.TAHUN',$tahun)
                                ->where('CAKUPAN.ID_KANWIL',$kanwil)
                                ->where('CAKUPAN.ID_CABANG',$cabang)
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_SEGMEN','CAKUPAN.ID_PROGRAM','PROGRAM.PROGRAM')
                                 ->get();
          }       
      //  dd(DB::getQueryLog()); 
$array= array();
foreach ($array as $idx => $dat) {
    //Id is not exists in json,create new data 
    if( !array_key_exists($dat['ID_SEGMEN'], $data) ){//Id is unique in db,use Id for index ,you can find it easily
        $json[$data['ID_SEGMEN']]=array(
            'ID_PROGRAM'  =>$data['ID_PROGRAM'],
            'PROGRAM'=>$data['PROGRAM'],
            'Value'=>array(
                    $data['JUMLAH']=>$data['JUMLAH']
                )
        );
        continue;
    }

    //Id is exists in json, append value
    $json[$data['Id']]['Value'][$data['Date']] =$data['Value'];
}
$json = json_encode($data);

return $json;  
       
} // end of function jmlpesertaBysegmen

static function jmlbuBysegmen($program,$bulan,$tahun,$kanwil=null,$cabang=null){
/*
mengakses table CAKUPAN_BU,SKALA_BU untuk mendapatkan jumlah badan usaha per skala usaha difilter
berdasarkan bulan,tahun,program,kanwil,cabang
filter berdasarkan program dan waktu.
use link 'data/bu/program/1/3/2017''
*/
DB::enableQueryLog();
            if ($program=='All'){    
            $data = DB::table('CAKUPAN_BU')
                                -> join('SKALA_BU', 'CAKUPAN_BU.ID_SKALA', '=', 'SKALA_BU.ID_SKALA')
                                 -> join('PROGRAM', 'CAKUPAN_BU.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('PROGRAM.PROGRAM AS PROGRAM','SKALA_BU.SKALA as SKALA', DB::raw('SUM(JUMLAH) as JUMLAH' ))
                                ->orderBy('PROGRAM.PROGRAM','SKALA_BU.SKALA', 'asc')
                                ->groupBy('PROGRAM.PROGRAM','SKALA_BU.SKALA')
                                 ->get();
          } elseif (is_null($cabang) and is_null($kanwil)) {

            $data = DB::table('CAKUPAN_BU')       
                                -> join('SKALA_BU', 'CAKUPAN_BU.ID_SKALA', '=', 'SKALA_BU.ID_SKALA')
                                ->select('CAKUPAN_BU.ID_PROGRAM as ID_PROGRAM','CAKUPAN_BU.ID_SKALA as ID_SKALA','SKALA_BU.SKALA as SKALA', DB::raw('SUM(JUMLAH) as JUMLAH' ))
                                ->where('CAKUPAN_BU.ID_PROGRAM',$program)
                                ->where('CAKUPAN_BU.BULAN',$bulan)
                                ->where('CAKUPAN_BU.TAHUN',$tahun)
                                ->orderBy('CAKUPAN_BU.ID_SKALA', 'asc')
                                ->groupBy('CAKUPAN_BU.ID_PROGRAM','CAKUPAN_BU.ID_SKALA','SKALA_BU.SKALA')
                                 ->get();
                              
            }elseif (is_null($cabang)) {

            $data = DB::table('CAKUPAN_BU')       
                                -> join('SKALA_BU', 'CAKUPAN_BU.ID_SKALA', '=', 'SKALA_BU.ID_SKALA')
                                ->select('CAKUPAN_BU.ID_PROGRAM as ID_PROGRAM','CAKUPAN_BU.ID_SKALA as ID_SKALA','SKALA_BU.SKALA as SKALA', DB::raw('SUM(JUMLAH) as JUMLAH' ))
                                ->where('CAKUPAN_BU.ID_PROGRAM',$program)
                                ->where('CAKUPAN_BU.BULAN',$bulan)
                                ->where('CAKUPAN_BU.TAHUN',$tahun)
                                ->where('CAKUPAN_BU.ID_KANWIL',$kanwil)
                                ->orderBy('CAKUPAN_BU.ID_SKALA', 'asc')
                                ->groupBy('CAKUPAN_BU.ID_PROGRAM','CAKUPAN_BU.ID_SKALA','SKALA_BU.SKALA')
                                 ->get();

            }  else {
            $data = DB::table('CAKUPAN_BU')
                                 -> join('SKALA_BU', 'CAKUPAN_BU.ID_SKALA', '=', 'SKALA_BU.ID_SKALA')
                                ->select('CAKUPAN_BU.ID_PROGRAM as ID_PROGRAM','CAKUPAN_BU.ID_SKALA as ID_SKALA','SKALA_BU.SKALA as SKALA', DB::raw('SUM(JUMLAH) as JUMLAH' ))
                                ->where('CAKUPAN_BU.ID_PROGRAM',$program)
                                ->where('CAKUPAN_BU.BULAN',$bulan)
                                ->where('CAKUPAN_BU.TAHUN',$tahun)
                                ->where('CAKUPAN.ID_KANWIL',$kanwil)
                                 ->where('CAKUPAN_BU.ID_CABANG',$cabang)
                                ->orderBy('CAKUPAN_BU.ID_SKALA', 'asc')
                                ->groupBy('CAKUPAN_BU.ID_PROGRAM','CAKUPAN_BU.ID_SKALA','SKALA_BU.SKALA')
                                 ->get();
       }       
    //    dd(DB::getQueryLog());         
return $data;  
       
}
static  function getKanwil(){
    $kanwul= DB::table('KANWIL')->pluck('KANWIL','ID_KANWIL');
    return $kanwul;
}
static function CakupanTotal($program,$bulan,$tahun,$kanwil,$cabang){

    DB::enableQueryLog();
    if ($program == 'All') {
   //     echo 'All';
        if ($kanwil) {
   //         echo 'kanwil  ' . $kanwil;
            if ($cabang) {
   //             echo 'Cabang' . $cabang;

                $data = DB::table('CAKUPAN_TOTAL')
                    ->join('SEGMEN', 'CAKUPAN_TOTAL.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                    ->join('TARGET as TS',function($join){
                        $join->on('CAKUPAN_TOTAL.ID_PROGRAM','=','TS.ID_PROGRAM')
                            ->on('CAKUPAN_TOTAL.ID_SEGMEN','=','TS.ID_SEGMEN');
                    })
                    ->select('CAKUPAN_TOTAL.BULAN','CAKUPAN_TOTAL.TAHUN','CAKUPAN_TOTAL.ID_PROGRAM','CAKUPAN_TOTAL.ID_SEGMEN','SEGMEN.SEGMEN',DB::RAW('SUM(JUMLAH) AS JUMLAH'),'TS.TARGET as TARGET')
                    ->where('CAKUPAN_TOTAL.ID_KANWIL',$kanwil)
                    ->where('CAKUPAN_TOTAL.ID_CABANG',$cabang)
                    ->where('CAKUPAN_TOTAL.BULAN',$bulan)
                    ->where('CAKUPAN_TOTAL.TAHUN',$tahun)
                    ->where('TS.KODE_TARGET',1)
                    ->orderBy('CAKUPAN_TOTAL.ID_PROGRAM','CAKUPAN_TOTAL.ID_SEGMEN', 'asc')
                    ->groupBy('CAKUPAN_TOTAL.BULAN','CAKUPAN_TOTAL.TAHUN','CAKUPAN_TOTAL.ID_PROGRAM','CAKUPAN_TOTAL.ID_SEGMEN','SEGMEN.SEGMEN')

                    ->get();

            } else {
   //             echo 'Cabang Kosong ' . $cabang;

                $data = DB::table('CAKUPAN_TOTAL')
                    ->join('SEGMEN', 'CAKUPAN_TOTAL.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                    ->join('TARGET as TS',function($join){
                        $join->on('CAKUPAN_TOTAL.ID_PROGRAM','=','TS.ID_PROGRAM')
                            ->on('CAKUPAN_TOTAL.ID_SEGMEN','=','TS.ID_SEGMEN');
                    })
                    ->select('CAKUPAN_TOTAL.BULAN','CAKUPAN_TOTAL.TAHUN','CAKUPAN_TOTAL.ID_PROGRAM','CAKUPAN_TOTAL.ID_SEGMEN','SEGMEN.SEGMEN',DB::RAW('SUM(JUMLAH) AS JUMLAH'),'TS.TARGET as TARGET')
                    ->where('CAKUPAN_TOTAL.ID_KANWIL',$kanwil)
                    ->where('CAKUPAN_TOTAL.BULAN',$bulan)
                    ->where('CAKUPAN_TOTAL.TAHUN',$tahun)
                    ->where('TS.KODE_TARGET',1)
                    ->orderBy('CAKUPAN_TOTAL.ID_PROGRAM','CAKUPAN_TOTAL.ID_SEGMEN', 'asc')
                    ->groupBy('CAKUPAN_TOTAL.BULAN','CAKUPAN_TOTAL.TAHUN','CAKUPAN_TOTAL.ID_PROGRAM','CAKUPAN_TOTAL.ID_SEGMEN','SEGMEN.SEGMEN')

                    ->get();


            }
        } else {
   //         echo 'Kanwilnya adalah ' . $kanwil;
            $data = DB::table('CAKUPAN_TOTAL')
                ->join('SEGMEN', 'CAKUPAN_TOTAL.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                ->join('TARGET as TS',function($join){
                    $join->on('CAKUPAN_TOTAL.ID_PROGRAM','=','TS.ID_PROGRAM')
                        ->on('CAKUPAN_TOTAL.ID_SEGMEN','=','TS.ID_SEGMEN');
                })
                ->select('CAKUPAN_TOTAL.BULAN','CAKUPAN_TOTAL.TAHUN','CAKUPAN_TOTAL.ID_PROGRAM','CAKUPAN_TOTAL.ID_SEGMEN','SEGMEN.SEGMEN',DB::RAW('SUM(JUMLAH) AS JUMLAH'),'TS.TARGET as TARGET')
                ->where('CAKUPAN_TOTAL.ID_CABANG',$cabang)
                ->where('CAKUPAN_TOTAL.BULAN',$bulan)
                ->where('CAKUPAN_TOTAL.TAHUN',$tahun)
                ->where('TS.KODE_TARGET',1)
                ->orderBy('CAKUPAN_TOTAL.ID_PROGRAM','CAKUPAN_TOTAL.ID_SEGMEN', 'asc')
                ->groupBy('CAKUPAN_TOTAL.BULAN','CAKUPAN_TOTAL.TAHUN','CAKUPAN_TOTAL.ID_PROGRAM','CAKUPAN_TOTAL.ID_SEGMEN','SEGMEN.SEGMEN')

                ->get();
        }
    } else {
   //     echo 'Program ' . $program;
        if ($kanwil) {
    //        echo 'kanwil ' . $kanwil;
            if ($cabang) {
     //           echo 'Cabang' . $cabang;
                $data = DB::table('CAKUPAN_TOTAL')
                    ->join('SEGMEN', 'CAKUPAN_TOTAL.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                    ->join('TARGET as TS',function($join){
                        $join->on('CAKUPAN_TOTAL.ID_PROGRAM','=','TS.ID_PROGRAM')
                            ->on('CAKUPAN_TOTAL.ID_SEGMEN','=','TS.ID_SEGMEN');
                    })
                    ->select('CAKUPAN_TOTAL.BULAN','CAKUPAN_TOTAL.TAHUN','CAKUPAN_TOTAL.ID_PROGRAM','CAKUPAN_TOTAL.ID_SEGMEN','SEGMEN.SEGMEN',DB::RAW('SUM(JUMLAH) AS JUMLAH'),'TS.TARGET as TARGET')
                    ->where('CAKUPAN_TOTAL.ID_PROGRAM',$program)
                    ->where('CAKUPAN_TOTAL.ID_KANWIL',$kanwil)
                    ->where('CAKUPAN_TOTAL.ID_CABANG',$cabang)
                    ->where('CAKUPAN_TOTAL.BULAN',$bulan)
                    ->where('CAKUPAN_TOTAL.TAHUN',$tahun)
                    ->where('TS.KODE_TARGET',1)
                    ->orderBy('CAKUPAN_TOTAL.ID_PROGRAM','CAKUPAN_TOTAL.ID_SEGMEN', 'asc')
                    ->groupBy('CAKUPAN_TOTAL.BULAN','CAKUPAN_TOTAL.TAHUN','CAKUPAN_TOTAL.ID_PROGRAM','CAKUPAN_TOTAL.ID_SEGMEN','SEGMEN.SEGMEN')

                    ->get();

            } else {
     //           echo 'Cabang Kosong ' . $cabang;
                $data = DB::table('CAKUPAN_TOTAL')
                    ->join('SEGMEN', 'CAKUPAN_TOTAL.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                    ->join('TARGET as TS',function($join){
                        $join->on('CAKUPAN_TOTAL.ID_PROGRAM','=','TS.ID_PROGRAM')
                            ->on('CAKUPAN_TOTAL.ID_SEGMEN','=','TS.ID_SEGMEN');
                    })
                    ->select('CAKUPAN_TOTAL.BULAN','CAKUPAN_TOTAL.TAHUN','CAKUPAN_TOTAL.ID_PROGRAM','CAKUPAN_TOTAL.ID_SEGMEN','SEGMEN.SEGMEN',DB::RAW('SUM(JUMLAH) as JUMLAH'),'TS.TARGET as TARGET')
                    ->where('CAKUPAN_TOTAL.ID_PROGRAM',$program)
                    ->where('CAKUPAN_TOTAL.ID_KANWIL',$kanwil)
                    ->where('CAKUPAN_TOTAL.BULAN',$bulan)
                    ->where('CAKUPAN_TOTAL.TAHUN',$tahun)
                    ->where('TS.KODE_TARGET',1)
                    ->orderBy('CAKUPAN_TOTAL.ID_PROGRAM','CAKUPAN_TOTAL.ID_SEGMEN', 'asc')
                    ->groupBy('CAKUPAN_TOTAL.BULAN','CAKUPAN_TOTAL.TAHUN','CAKUPAN_TOTAL.ID_PROGRAM','CAKUPAN_TOTAL.ID_SEGMEN','SEGMEN.SEGMEN')
                    ->get();

            }
        } else {
     //       echo 'Kanwilnya adalah ' . $kanwil;
            $data = DB::table('CAKUPAN_TOTAL')
                ->join('SEGMEN', 'CAKUPAN_TOTAL.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                ->join('TARGET as TS',function($join){
                    $join->on('CAKUPAN_TOTAL.ID_PROGRAM','=','TS.ID_PROGRAM')
                        ->on('CAKUPAN_TOTAL.ID_SEGMEN','=','TS.ID_SEGMEN');
                })
                ->select('CAKUPAN_TOTAL.BULAN','CAKUPAN_TOTAL.TAHUN','CAKUPAN_TOTAL.ID_PROGRAM','CAKUPAN_TOTAL.ID_SEGMEN','SEGMEN.SEGMEN',DB::RAW('SUM(JUMLAH) AS JUMLAH'),'TS.TARGET as TARGET')
                ->where('CAKUPAN_TOTAL.ID_PROGRAM',$program)
                ->where('CAKUPAN_TOTAL.BULAN',$bulan)
                ->where('CAKUPAN_TOTAL.TAHUN',$tahun)
                ->where('TS.KODE_TARGET',1)
                ->orderBy('CAKUPAN_TOTAL.ID_PROGRAM','CAKUPAN_TOTAL.ID_SEGMEN', 'asc')
                ->groupBy('CAKUPAN_TOTAL.BULAN','CAKUPAN_TOTAL.TAHUN','CAKUPAN_TOTAL.ID_PROGRAM','CAKUPAN_TOTAL.ID_SEGMEN','SEGMEN.SEGMEN')

                ->get();

        }
    }
  //  dd(DB::getQueryLog());
    $data = json_decode(json_encode($data), True);
  //  dd($data);
    return $data;

}
static function resumeCakupan($program,$bulan,$tahun,$kanwil,$cabang){
/*
mengakses table CAKUPAN_BU,SKALA_BU untuk mendapatkan jumlah badan usaha per skala usaha difilter
berdasarkan bulan,tahun,program,kanwil,cabang
filter berdasarkan program dan waktu.
use link 'data/bu/program/1/3/2017''
*/
//dd($kanwil,$cabang);
    DB::enableQueryLog();
    if ($program == 'All') {
    //    echo 'All';
        if ($kanwil) {
     //      echo 'kanwil  ' . $kanwil;
            if ($cabang) {
    //            echo 'Cabang' . $cabang;
                $data = DB::table('CAKUPAN')
                    ->join('SEGMEN', 'CAKUPAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                    ->join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                    ->select('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM as ID_PROGRAM', 'CAKUPAN.ID_SEGMEN as ID_SEGMEN', 'SEGMEN.SEGMEN as SEGMEN', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH'))
                    ->where('CAKUPAN.BULAN', $bulan)
                    ->where('CAKUPAN.TAHUN', $tahun)
                    ->where('CAKUPAN.ID_KANWIL', $kanwil)
                    ->where('CAKUPAN.ID_CABANG', $cabang)
                    ->orderBy('CAKUPAN.ID_PROGRAM', 'CAKUPAN.ID_SEGMEN', 'ASC')
                    ->groupBy('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM', 'CAKUPAN.ID_SEGMEN', 'SEGMEN.SEGMEN')
                    ->get();

            } else {
     //           echo 'Cabang Kosong ' . $cabang;
                $data = DB::table('CAKUPAN')
                    ->join('SEGMEN', 'CAKUPAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                    ->join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                    ->select('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM as ID_PROGRAM', 'CAKUPAN.ID_SEGMEN as ID_SEGMEN', 'SEGMEN.SEGMEN as SEGMEN', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH'))
                    ->where('CAKUPAN.BULAN', $bulan)
                    ->where('CAKUPAN.TAHUN', $tahun)
                    ->where('CAKUPAN.ID_KANWIL', $kanwil)
                    ->orderBy('CAKUPAN.ID_PROGRAM', 'CAKUPAN.ID_SEGMEN', 'ASC')
                    ->groupBy('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM', 'CAKUPAN.ID_SEGMEN', 'SEGMEN.SEGMEN')
                    ->get();

            }
        } else {
     //       echo 'Kanwilnya adalah ' . $kanwil;
            $data = DB::table('CAKUPAN')
                ->join('SEGMEN', 'CAKUPAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                ->join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                ->select('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM as ID_PROGRAM', 'CAKUPAN.ID_SEGMEN as ID_SEGMEN', 'SEGMEN.SEGMEN as SEGMEN', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH'))
                ->where('CAKUPAN.BULAN', $bulan)
                ->where('CAKUPAN.TAHUN', $tahun)
                ->orderBy('CAKUPAN.ID_PROGRAM', 'CAKUPAN.ID_SEGMEN', 'ASC')
                ->groupBy('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM', 'CAKUPAN.ID_SEGMEN', 'SEGMEN.SEGMEN')
                ->get();


        }
    } else {
     //   echo 'Program ' . $program;
        if ($kanwil) {
     //       echo 'kanwil ' . $kanwil;
            if ($cabang) {
     //           echo 'Cabang' . $cabang;
                $data = DB::table('CAKUPAN')
                    ->join('SEGMEN', 'CAKUPAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                    ->join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                    ->select('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM as ID_PROGRAM', 'CAKUPAN.ID_SEGMEN as ID_SEGMEN', 'SEGMEN.SEGMEN as SEGMEN', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH'))
                    ->where('CAKUPAN.BULAN', $bulan)
                    ->where('CAKUPAN.TAHUN', $tahun)
                    ->where('CAKUPAN.ID_KANWIL', $kanwil)
                    ->where('CAKUPAN.ID_CABANG', $cabang)
                    ->where('CAKUPAN.ID_PROGRAM', $program)
                    ->orderBy('CAKUPAN.ID_PROGRAM', 'CAKUPAN.ID_SEGMEN', 'ASC')
                    ->groupBy('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM', 'CAKUPAN.ID_SEGMEN', 'SEGMEN.SEGMEN')
                    ->get();

            } else {
     //           echo 'Cabang Kosong ' . $cabang;
                $data = DB::table('CAKUPAN')
                    ->join('SEGMEN', 'CAKUPAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                    ->join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                    ->select('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM as ID_PROGRAM', 'CAKUPAN.ID_SEGMEN as ID_SEGMEN', 'SEGMEN.SEGMEN as SEGMEN', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH'))
                    ->where('CAKUPAN.BULAN', $bulan)
                    ->where('CAKUPAN.TAHUN', $tahun)
                    ->where('CAKUPAN.ID_KANWIL', $kanwil)
                    ->where('CAKUPAN.ID_PROGRAM', $program)
                    ->orderBy('CAKUPAN.ID_PROGRAM', 'CAKUPAN.ID_SEGMEN', 'ASC')
                    ->groupBy('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM', 'CAKUPAN.ID_SEGMEN', 'SEGMEN.SEGMEN')
                    ->get();

            }
        } else {
     //       echo 'Kanwilnya adalah ' . $kanwil;
            $data = DB::table('CAKUPAN')
                ->join('SEGMEN', 'CAKUPAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                ->join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                ->select('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM as ID_PROGRAM', 'CAKUPAN.ID_SEGMEN as ID_SEGMEN', 'SEGMEN.SEGMEN as SEGMEN', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH'))
                ->where('CAKUPAN.BULAN', $bulan)
                ->where('CAKUPAN.TAHUN', $tahun)
                ->where('CAKUPAN.ID_PROGRAM', $program)
                ->orderBy('CAKUPAN.ID_PROGRAM', 'CAKUPAN.ID_SEGMEN', 'ASC')
                ->groupBy('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM', 'CAKUPAN.ID_SEGMEN', 'SEGMEN.SEGMEN')
                ->get();

        }
    }

//DB::enableQueryLog();
    //dd ($data);
    //  dd(DB::getQueryLog());
       $data = json_decode(json_encode($data), True);

return $data;  
       
} 
static function resumeCakupanProgram($program,$bulan,$tahun,$kanwil,$cabang){
/*
mengakses table CAKUPAN_BU,SKALA_BU untuk mendapatkan jumlah badan usaha per skala usaha difilter
berdasarkan bulan,tahun,program,kanwil,cabang
filter berdasarkan program dan waktu.
use link 'data/bu/program/1/3/2017''
*/
    if ($program == 'All') {
     //   echo 'All';
        if ($kanwil) {
     //       echo 'kanwil  ' . $kanwil;
            if ($cabang) {
                $data = DB::table('CAKUPAN')
                ->join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                    ->select('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM as ID_PROGRAM',  DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH'))
                    ->where('CAKUPAN.BULAN', $bulan)
                    ->where('CAKUPAN.TAHUN', $tahun)
                    ->where('CAKUPAN.ID_KANWIL', $kanwil)
                    ->where('CAKUPAN.ID_CABANG', $cabang)
                    ->orderBy('CAKUPAN.ID_PROGRAM', 'CAKUPAN.ID_SEGMEN', 'asc')
                    ->groupBy('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM')
                    ->get();

            } else {
     //           echo 'Cabang Kosong ' . $cabang;
                $data = DB::table('CAKUPAN')
                    ->join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                    ->select('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM as ID_PROGRAM',  DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH'))
                    ->where('CAKUPAN.BULAN', $bulan)
                    ->where('CAKUPAN.TAHUN', $tahun)
                    ->where('CAKUPAN.ID_KANWIL', $kanwil)
                    ->orderBy('CAKUPAN.ID_PROGRAM', 'CAKUPAN.ID_SEGMEN', 'asc')
                    ->groupBy('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM')
                    ->get();

            }
        } else {
     //       echo 'Kanwilnya adalah ' . $kanwil;
            $data = DB::table('CAKUPAN')
                ->join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                ->select('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM as ID_PROGRAM',  DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH'))
                ->where('CAKUPAN.BULAN', $bulan)
                ->where('CAKUPAN.TAHUN', $tahun)
                ->orderBy('CAKUPAN.ID_PROGRAM', 'CAKUPAN.ID_SEGMEN', 'asc')
                ->groupBy('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM')
                ->get();

        }
    } else {
     //   echo 'Program ' . $program;
        if ($kanwil) {
     //       echo 'kanwil ' . $kanwil;
            if ($cabang) {
               $data = DB::table('CAKUPAN')
                ->join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                    ->select('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM as ID_PROGRAM',  DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH'))
                    ->where('CAKUPAN.ID_PROGRAM', $program)
                    ->where('CAKUPAN.BULAN', $bulan)
                    ->where('CAKUPAN.TAHUN', $tahun)
                    ->where('CAKUPAN.ID_KANWIL', $kanwil)
                    ->where('CAKUPAN.ID_CABANG', $cabang)
                    ->orderBy('CAKUPAN.ID_PROGRAM', 'CAKUPAN.ID_SEGMEN', 'asc')
                    ->groupBy('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM')
                    ->get();

            } else {
               $data = DB::table('CAKUPAN')
                ->join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                    ->select('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM as ID_PROGRAM',  DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH'))
                    ->where('CAKUPAN.ID_PROGRAM', $program)
                    ->where('CAKUPAN.BULAN', $bulan)
                    ->where('CAKUPAN.TAHUN', $tahun)
                    ->where('CAKUPAN.ID_KANWIL', $kanwil)
                    ->orderBy('CAKUPAN.ID_PROGRAM', 'CAKUPAN.ID_SEGMEN', 'asc')
                    ->groupBy('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM')
                    ->get();

            }
        } else {
     //       echo 'Kanwilnya adalah ' . $kanwil;
            $data = DB::table('CAKUPAN')
                ->join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                ->select('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM as ID_PROGRAM',  DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH'))
                ->where('CAKUPAN.ID_PROGRAM', $program)
                ->where('CAKUPAN.BULAN', $bulan)
                ->where('CAKUPAN.TAHUN', $tahun)
                ->orderBy('CAKUPAN.ID_PROGRAM', 'CAKUPAN.ID_SEGMEN', 'asc')
                ->groupBy('CAKUPAN.BULAN', 'CAKUPAN.TAHUN', 'CAKUPAN.ID_PROGRAM')
                ->get();

        }
    }


 // dd(DB::getQueryLog());
     //  $data = json_decode(json_encode($data), True);
   //   dd ($data);
return $data;  
       
} 
static function jmlpesertaByprogram($segmen,$bulan,$tahun,$kanwil=null,$cabang=null){
/*
mengakses table CAKUPAN untuk mendapatkan jumlah peserta per program
filter berdasarkan segmen dan waktu

*/

DB::enableQueryLog();
            if ($segmen=='All'){    
            $data = DB::table('CAKUPAN')
                                -> join('TARGET AS T','CAKUPAN.ID_PROGRAM','=','T.ID_PROGRAM')
                                -> join('TARGET AS S','CAKUPAN.ID_SEGMEN','=','S.ID_SEGMEN')
                                -> join('SEGMEN','CAKUPAN.ID_SEGMEN','=','SEGMEN.ID_SEGMEN')
                                -> join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select(DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH'),DB::raw('SUM(S.TARGET) as TARGET'),DB::raw(getTarget('CAKUPAN.ID_PROGRAM','CAKUPAN.ID_SEGMEN')),'CAKUPAN.ID_SEGMEN as ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN')
                                ->where('CAKUPAN.ID_PROGRAM',$segmen)
                                ->where('CAKUPAN.BULAN',$bulan)
                                ->where('CAKUPAN.TAHUN',$tahun)
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_PROGRAM','CAKUPAN.ID_SEGMEN','SEGMEN.SEGMEN')
                                ->get();
            } else {
            if (is_null($kanwil) and is_null($cabang)){
            $data = DB::table('CAKUPAN')
                                -> join('TARGET AS T','CAKUPAN.ID_PROGRAM','=','T.ID_PROGRAM')
                                -> join('TARGET AS S','CAKUPAN.ID_SEGMEN','=','S.ID_SEGMEN')
                                -> join('SEGMEN','CAKUPAN.ID_SEGMEN','=','SEGMEN.ID_SEGMEN')
                                -> join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select(DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH'),DB::raw('SUM(S.TARGET) as TARGET'),'CAKUPAN.ID_PROGRAM as ID_PROGRAM','CAKUPAN.ID_SEGMEN as ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN')
                                ->where('CAKUPAN.ID_PROGRAM',$segmen)
                                ->where('CAKUPAN.BULAN',$bulan)
                                ->where('CAKUPAN.TAHUN',$tahun)
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_PROGRAM','CAKUPAN.ID_SEGMEN','SEGMEN.SEGMEN')
                                ->get();

            } elseif (is_null($cabang)) {
              $data = DB::table('CAKUPAN')
              -> join('TARGET AS T','CAKUPAN.ID_PROGRAM','=','T.ID_PROGRAM')
              -> join('TARGET AS S','CAKUPAN.ID_SEGMEN','=','S.ID_SEGMEN')
              -> join('SEGMEN','CAKUPAN.ID_SEGMEN','=','SEGMEN.ID_SEGMEN')
              -> join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
              ->select(DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH'),DB::raw('SUM(S.TARGET) as TARGET'),'CAKUPAN.ID_PROGRAM as ID_PROGRAM','CAKUPAN.ID_SEGMEN as ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN')
              ->where('CAKUPAN.ID_PROGRAM',$segmen)
                                ->where('CAKUPAN.BULAN',$bulan)
                                ->where('CAKUPAN.TAHUN',$tahun)
                                ->where('CAKUPAN.ID_KANWIL',$kanwil) 
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_PROGRAM','CAKUPAN.ID_SEGMEN','SEGMEN.SEGMEN')
                                ->get();

            } elseif (is_null($kanwil)) {
              $data = DB::table('CAKUPAN')
              -> join('TARGET AS T','CAKUPAN.ID_PROGRAM','=','T.ID_PROGRAM')
              -> join('TARGET AS S','CAKUPAN.ID_SEGMEN','=','S.ID_SEGMEN')
              -> join('SEGMEN','CAKUPAN.ID_SEGMEN','=','SEGMEN.ID_SEGMEN')
              -> join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
              ->select(DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH'),DB::raw('SUM(S.TARGET) as TARGET'),'CAKUPAN.ID_PROGRAM as ID_PROGRAM','CAKUPAN.ID_SEGMEN as ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN')
              ->where('CAKUPAN.ID_PROGRAM',$segmen)
                                ->where('CAKUPAN.BULAN',$bulan)
                                ->where('CAKUPAN.TAHUN',$tahun)
                                ->where('CAKUPAN.ID_CABANG',$cabang) 
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_PROGRAM','CAKUPAN.ID_SEGMEN','SEGMEN.SEGMEN')
                                ->get();
           } else {  
            $data = DB::table('CAKUPAN')
            -> join('TARGET AS T','CAKUPAN.ID_PROGRAM','=','T.ID_PROGRAM')
            -> join('TARGET AS S','CAKUPAN.ID_SEGMEN','=','S.ID_SEGMEN')
            -> join('SEGMEN','CAKUPAN.ID_SEGMEN','=','SEGMEN.ID_SEGMEN')
            -> join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
            ->select(DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH'),DB::raw('SUM(S.TARGET) as TARGET'),'CAKUPAN.ID_PROGRAM as ID_PROGRAM','SEGMEN.SEGMEN AS SEGMEN')
            ->where('CAKUPAN.ID_PROGRAM',$segmen)
                                ->where('CAKUPAN.BULAN',$bulan)
                                ->where('CAKUPAN.TAHUN',$tahun)
                                ->where('CAKUPAN.ID_KANWIL',$kanwil) 
                                ->where('CAKUPAN.ID_CABANG',$cabang) 
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_PROGRAM','CAKUPAN.ID_SEGMEN','SEGMEN.SEGMEN')
                                ->get();
             }                    
            }   
 //dd(DB::getQueryLog());                                
 //         DD($data);
 return $data;  
} // end of function jmlpesertaBysegmen

static function jmlBUskalaByprogram($segmen,$bulan,$tahun,$kanwil=null,$cabang=null){
/*
mengakses table CAKUPAN_BU,SKALA_BU,PROGRAM untuk mendapatkan jumlah BU per program per skala usaha
filter berdasarkan kanwil,dan waktu
*/

DB::enableQueryLog();
            if ($segmen=='All'){    
            $data = DB::table('CAKUPAN_BU')
                                -> join('PROGRAM', 'CAKUPAN_BU.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                -> join('SKALA_BU', 'CAKUPAN_BU.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                
                                ->select('CAKUPAN.ID_SEGMEN as ID_SEGMEN','CAKUPAN.ID_PROGRAM as ID_PROGRAM','PROGRAM.PROGRAM as PROGRAM', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH' ))
                                ->where('CAKUPAN.BULAN',$bulan)
                                ->where('CAKUPAN.TAHUN',$tahun)
                                ->orderBy('CAKUPAN.ID_PROGRAM', 'asc')
                                ->groupBy('CAKUPAN.ID_SEGMEN','CAKUPAN.ID_PROGRAM','PROGRAM.PROGRAM')
                                ->get();
            } else {
            if (is_null($kanwil) and is_null($cabang)){
            $data = DB::table('CAKUPAN')
                                  -> join('SEGMEN', 'CAKUPAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                ->select('CAKUPAN.ID_PROGRAM as ID_PROGRAM','CAKUPAN.ID_SEGMEN as ID_SEGMEN','SEGMEN.SEGMEN as SEGMEN', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH' ))
                                ->where('CAKUPAN.ID_PROGRAM',$segmen)
                                ->where('CAKUPAN.BULAN',$bulan)
                                ->where('CAKUPAN.TAHUN',$tahun)
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_PROGRAM','CAKUPAN.ID_SEGMEN','SEGMEN.SEGMEN')
                                ->get();

            } elseif (is_null($cabang)) {
              $data = DB::table('CAKUPAN')
                                     -> join('SEGMEN', 'CAKUPAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                ->select('CAKUPAN.ID_PROGRAM as ID_PROGRAM','CAKUPAN.ID_SEGMEN as ID_SEGMEN','SEGMEN.SEGMEN as SEGMEN', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH' ))
                                ->where('CAKUPAN.ID_PROGRAM',$segmen)
                                ->where('CAKUPAN.BULAN',$bulan)
                                ->where('CAKUPAN.TAHUN',$tahun)
                                ->where('CAKUPAN.ID_KANWIL',$kanwil)
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_PROGRAM','CAKUPAN.ID_SEGMEN','SEGMEN.SEGMEN')
                                ->get();

            } elseif (is_null($kanwil)) {
              $data = DB::table('CAKUPAN')
                                  -> join('SEGMEN', 'CAKUPAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                ->select('CAKUPAN.ID_PROGRAM as ID_PROGRAM','CAKUPAN.ID_SEGMEN as ID_SEGMEN','SEGMEN.SEGMEN as SEGMEN', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH' ))
                                ->where('CAKUPAN.ID_PROGRAM',$segmen)
                                ->where('CAKUPAN.BULAN',$bulan)
                                ->where('CAKUPAN.TAHUN',$tahun)
                               ->where('CAKUPAN.ID_CABANG',$cabang)   
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_PROGRAM','CAKUPAN.ID_SEGMEN','SEGMEN.SEGMEN')
                                ->get();
           } else {  
            $data = DB::table('CAKUPAN')
                                  -> join('SEGMEN', 'CAKUPAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                ->select('CAKUPAN.ID_PROGRAM as ID_PROGRAM','CAKUPAN.ID_SEGMEN as ID_SEGMEN','SEGMEN.SEGMEN as SEGMEN', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH' ))
                                ->where('CAKUPAN.ID_PROGRAM',$segmen)
                                ->where('CAKUPAN.BULAN',$bulan)
                                ->where('CAKUPAN.TAHUN',$tahun)
                                ->where('CAKUPAN.ID_KANWIL',$kanwil)
                               ->where('CAKUPAN.ID_CABANG',$cabang)   
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_PROGRAM','CAKUPAN.ID_SEGMEN','SEGMEN.SEGMEN')
                                ->get();
              }                    
            }   
// dd(DB::getQueryLog());                                
return $data;  
} // end of function jmlpesertaBysegmen
//BUbyprogramSkala
static function BUbyprogramSkala($program,$bulan,$tahun,$kanwil=null,$cabang=null){
/*
mengakses table CAKUPAN_BU,SKALA_BU untuk mendapatkan jumlah badan usaha per skala usaha difilter
berdasarkan bulan,tahun,program,kanwil,cabang
filter berdasarkan program dan waktu.
use link 'data/bu/program/1/3/2017''
*/
DB::enableQueryLog();
            if ($program=='All'){    
            $data = DB::table('CAKUPAN_BU')
                                -> join('SKALA_BU', 'CAKUPAN_BU.ID_SKALA', '=', 'SKALA_BU.ID_SKALA')
                                -> join('PROGRAM', 'CAKUPAN_BU.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('SKALA_BU.SKALA as SKALA','CAKUPAN_BU.ID_SKALA','CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM AS PROGRAM', DB::raw('SUM(JUMLAH) as JUMLAH' ))
                                ->orderBy('CAKUPAN_BU.ID_SKALA','CAKUPAN_BU.ID_PROGRAM', 'asc')
                                ->groupBy('CAKUPAN_BU.ID_SKALA','SKALA_BU.SKALA','CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM')
                                 ->get();
          } elseif (is_null($cabang) and is_null($kanwil)) {

            $data = DB::table('CAKUPAN_BU')       
 
                                -> join('SKALA_BU', 'CAKUPAN_BU.ID_SKALA', '=', 'SKALA_BU.ID_SKALA')
                                -> join('PROGRAM', 'CAKUPAN_BU.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('SKALA_BU.SKALA as SKALA','PROGRAM.PROGRAM AS PROGRAM', DB::raw('SUM(JUMLAH) as JUMLAH' ))
                                 ->where('CAKUPAN_BU.ID_PROGRAM',$program)
                                ->where('CAKUPAN_BU.BULAN',$bulan)
                                ->where('CAKUPAN_BU.TAHUN',$tahun)
                                ->orderBy('SKALA_BU.SKALA','PROGRAM.PROGRAM', 'asc')
                                ->groupBy('SKALA_BU.SKALA','PROGRAM.PROGRAM')
                                 ->get();
                              
            }elseif (is_null($cabang)) {

            $data = DB::table('CAKUPAN_BU')       
                               -> join('SKALA_BU', 'CAKUPAN_BU.ID_SKALA', '=', 'SKALA_BU.ID_SKALA')
                                -> join('PROGRAM', 'CAKUPAN_BU.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('SKALA_BU.SKALA as SKALA','PROGRAM.PROGRAM AS PROGRAM', DB::raw('SUM(JUMLAH) as JUMLAH' ))
                                 ->where('CAKUPAN_BU.ID_PROGRAM',$program)
                                ->where('CAKUPAN_BU.BULAN',$bulan)
                                ->where('CAKUPAN_BU.TAHUN',$tahun)
                                ->where('CAKUPAN_BU.ID_KANWIL',$kanwil)
                                ->orderBy('SKALA_BU.SKALA','PROGRAM.PROGRAM', 'asc')
                                ->groupBy('SKALA_BU.SKALA','PROGRAM.PROGRAM')
                                 ->get();

            }  else {
            $data = DB::table('CAKUPAN_BU')
                              -> join('SKALA_BU', 'CAKUPAN_BU.ID_SKALA', '=', 'SKALA_BU.ID_SKALA')
                                -> join('PROGRAM', 'CAKUPAN_BU.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('SKALA_BU.SKALA as SKALA','PROGRAM.PROGRAM AS PROGRAM', DB::raw('SUM(JUMLAH) as JUMLAH' ))
                                 ->where('CAKUPAN_BU.ID_PROGRAM',$program)
                                ->where('CAKUPAN_BU.BULAN',$bulan)
                                ->where('CAKUPAN_BU.TAHUN',$tahun)
                                ->where('CAKUPAN_BU.ID_KANWIL',$kanwil)
                                ->where('CAKUPAN_BU.ID_CABANG',$cabang)
                                ->orderBy('SKALA_BU.ID_SKALA','PROGRAM.ID_PROGRAM', 'asc')
                                ->groupBy('SKALA_BU.SKALA','PROGRAM.PROGRAM')
                                 ->get()-toArray();
         }       
//  dd(DB::getQueryLog());     



return $data;  
       
} 
static function BUbysegmenSkala($program,$bulan,$tahun,$kanwil,$cabang){
/*
mengakses table CAKUPAN_BU,SKALA_BU untuk mendapatkan jumlah badan usaha per skala usaha difilter
berdasarkan bulan,tahun,program,kanwil,cabang
filter berdasarkan program dan waktu.
use link 'data/bu/program/1/3/2017''

    $data = DB::table(' CAKUPAN_TOTAL_BU')
        ->join('TARGET as TS',function($join){
            $join->on(' CAKUPAN_TOTAL_BU.ID_PROGRAM','=','TS.ID_PROGRAM')
                ->on(' CAKUPAN_TOTAL_BU.ID_SEGMEN','=','TS.ID_SEGMEN');
        })
        ->select(' CAKUPAN_TOTAL_BU.BULAN',' CAKUPAN_TOTAL_BU.TAHUN',' CAKUPAN_TOTAL_BU.ID_PROGRAM',' CAKUPAN_TOTAL_BU.ID_SKALA',DB::RAW('SUM(JUMLAHP) AS JUMLAH'),DB::RAW('SUM(JUMLAHBU) AS JUMLAHBU'),'TS.TARGET as TARGET')
        ->where(' CAKUPAN_TOTAL_BU.ID_PROGRAM',$program)
        ->where(' CAKUPAN_TOTAL_BU.BULAN',$bulan)
        ->where(' CAKUPAN_TOTAL_BU.TAHUN',$tahun)
        ->where('TS.KODE_TARGET',1)
        ->orderBy(' CAKUPAN_TOTAL_BU.ID_PROGRAM',' CAKUPAN_TOTAL_BU.ID_SEGMEN', 'asc')
        ->groupBy(' CAKUPAN_TOTAL_BU.BULAN',' CAKUPAN_TOTAL_BU.TAHUN',' CAKUPAN_TOTAL_BU.ID_PROGRAM',' CAKUPAN_TOTAL_BU.ID_SKALA')

        ->get();
*/
DB::enableQueryLog();
    if ($program == 'All') {
  //      echo 'All';
        if ($kanwil) {
  //          echo 'kanwil  ' . $kanwil;
            if ($cabang) {
  //              echo 'Cabang' . $cabang;
                $data = DB::table('CAKUPAN_BU_TOTAL')
                    ->JOIN('SKALA_BU', 'CAKUPAN_BU_TOTAL.ID_SKALA','=','SKALA_BU.ID_SKALA')
                    ->join('TARGET as TS',function($join){
                        $join->on('CAKUPAN_BU_TOTAL.ID_PROGRAM','=','TS.ID_PROGRAM')
                            ->on('CAKUPAN_BU_TOTAL.ID_SKALA','=','TS.ID_SKALA');
                    })
                    ->select('CAKUPAN_BU_TOTAL.BULAN','CAKUPAN_BU_TOTAL.TAHUN','CAKUPAN_BU_TOTAL.ID_PROGRAM','CAKUPAN_BU_TOTAL.ID_SKALA','SKALA_BU.SKALA',DB::RAW('SUM(JUMLAHP) AS JUMLAH'),DB::RAW('SUM(JUMLAHBU) AS JUMLAHBU'),'TS.TARGET as TARGET')
                    ->where('CAKUPAN_BU_TOTAL.BULAN',$bulan)
                    ->where('CAKUPAN_BU_TOTAL.TAHUN',$tahun)
                    ->where('CAKUPAN_BU_TOTAL.ID_KANWIL',$kanwil)
                    ->where('CAKUPAN_BU_TOTAL.ID_CABANG',$cabang)
                    ->where('TS.KODE_TARGET',2)
                    ->orderBy('CAKUPAN_BU_TOTAL.ID_PROGRAM','CAKUPAN_BU_TOTAL.ID_SKALA', 'asc')
                    ->groupBy('CAKUPAN_BU_TOTAL.BULAN','CAKUPAN_BU_TOTAL.TAHUN','CAKUPAN_BU_TOTAL.ID_PROGRAM','CAKUPAN_BU_TOTAL.ID_SKALA')

                    ->get();

            } else {
    //            echo 'Cabang Kosong ' . $cabang;
                $data = DB::table('CAKUPAN_BU_TOTAL')
                    ->JOIN('SKALA_BU', 'CAKUPAN_BU_TOTAL.ID_SKALA','=','SKALA_BU.ID_SKALA')
                    ->join('TARGET as TS',function($join){
                        $join->on('CAKUPAN_BU_TOTAL.ID_PROGRAM','=','TS.ID_PROGRAM')
                            ->on('CAKUPAN_BU_TOTAL.ID_SKALA','=','TS.ID_SKALA');
                    })
                    ->select('CAKUPAN_BU_TOTAL.BULAN','CAKUPAN_BU_TOTAL.TAHUN','CAKUPAN_BU_TOTAL.ID_PROGRAM','CAKUPAN_BU_TOTAL.ID_SKALA','SKALA_BU.SKALA',DB::RAW('SUM(JUMLAHP) AS JUMLAH'),DB::RAW('SUM(JUMLAHBU) AS JUMLAHBU'),'TS.TARGET as TARGET')
                    ->where('CAKUPAN_BU_TOTAL.BULAN',$bulan)
                    ->where('CAKUPAN_BU_TOTAL.TAHUN',$tahun)
                    ->where('CAKUPAN_BU_TOTAL.ID_KANWIL',$kanwil)
                    ->where('TS.KODE_TARGET',2)
                    ->orderBy('CAKUPAN_BU_TOTAL.ID_PROGRAM','CAKUPAN_BU_TOTAL.ID_SKALA', 'asc')
                    ->groupBy('CAKUPAN_BU_TOTAL.BULAN','CAKUPAN_BU_TOTAL.TAHUN','CAKUPAN_BU_TOTAL.ID_PROGRAM','CAKUPAN_BU_TOTAL.ID_SKALA')

                    ->get();

            }
        } else {
     //       echo 'Kanwilnya adalah ' . $kanwil;
            $data = DB::table('CAKUPAN_BU_TOTAL')
                ->JOIN('SKALA_BU', 'CAKUPAN_BU_TOTAL.ID_SKALA','=','SKALA_BU.ID_SKALA')
                ->join('TARGET as TS',function($join){
                    $join->on('CAKUPAN_BU_TOTAL.ID_PROGRAM','=','TS.ID_PROGRAM')
                        ->on('CAKUPAN_BU_TOTAL.ID_SKALA','=','TS.ID_SKALA');
                })
                ->select('CAKUPAN_BU_TOTAL.BULAN','CAKUPAN_BU_TOTAL.TAHUN','CAKUPAN_BU_TOTAL.ID_PROGRAM','CAKUPAN_BU_TOTAL.ID_SKALA','SKALA_BU.SKALA',DB::RAW('SUM(JUMLAHP) AS JUMLAH'),DB::RAW('SUM(JUMLAHBU) AS JUMLAHBU'),'TS.TARGET as TARGET')
                ->where('CAKUPAN_BU_TOTAL.BULAN',$bulan)
                ->where('CAKUPAN_BU_TOTAL.TAHUN',$tahun)
                ->where('TS.KODE_TARGET',2)
                ->orderBy('CAKUPAN_BU_TOTAL.ID_PROGRAM','CAKUPAN_BU_TOTAL.ID_SKALA', 'asc')
                ->groupBy('CAKUPAN_BU_TOTAL.BULAN','CAKUPAN_BU_TOTAL.TAHUN','CAKUPAN_BU_TOTAL.ID_PROGRAM','CAKUPAN_BU_TOTAL.ID_SKALA')

                ->get();
        }
    } else {
     //   echo 'Program ' . $program;
        if ($kanwil) {
       //     echo 'kanwil ' . $kanwil;
            if ($cabang) {
           //     echo 'Cabang' . $cabang;
                $data = DB::table('CAKUPAN_BU_TOTAL')
                    ->JOIN('SKALA_BU', 'CAKUPAN_BU_TOTAL.ID_SKALA','=','SKALA_BU.ID_SKALA')
                    ->join('TARGET as TS',function($join){
                        $join->on('CAKUPAN_BU_TOTAL.ID_PROGRAM','=','TS.ID_PROGRAM')
                            ->on('CAKUPAN_BU_TOTAL.ID_SKALA','=','TS.ID_SKALA');
                    })
                    ->select('CAKUPAN_BU_TOTAL.BULAN','CAKUPAN_BU_TOTAL.TAHUN','CAKUPAN_BU_TOTAL.ID_PROGRAM','CAKUPAN_BU_TOTAL.ID_SKALA','SKALA_BU.SKALA',DB::RAW('SUM(JUMLAHP) AS JUMLAH'),DB::RAW('SUM(JUMLAHBU) AS JUMLAHBU'),'TS.TARGET as TARGET')
                    ->where('CAKUPAN_BU_TOTAL.ID_PROGRAM',$program)
                    ->where('CAKUPAN_BU_TOTAL.BULAN',$bulan)
                    ->where('CAKUPAN_BU_TOTAL.TAHUN',$tahun)
                    ->where('CAKUPAN_BU_TOTAL.ID_KANWIL',$kanwil)
                    ->where('CAKUPAN_BU_TOTAL.ID_CABANG',$cabang)
                    ->where('TS.KODE_TARGET',2)
                    ->orderBy('CAKUPAN_BU_TOTAL.ID_PROGRAM','CAKUPAN_BU_TOTAL.ID_SKALA', 'asc')
                    ->groupBy('CAKUPAN_BU_TOTAL.BULAN','CAKUPAN_BU_TOTAL.TAHUN','CAKUPAN_BU_TOTAL.ID_PROGRAM','CAKUPAN_BU_TOTAL.ID_SKALA')

                    ->get();
            } else {
         //       echo 'Cabang Kosong ' . $cabang;
                $data = DB::table('CAKUPAN_BU_TOTAL')
                    ->JOIN('SKALA_BU', 'CAKUPAN_BU_TOTAL.ID_SKALA','=','SKALA_BU.ID_SKALA')
                    ->join('TARGET as TS',function($join){
                        $join->on('CAKUPAN_BU_TOTAL.ID_PROGRAM','=','TS.ID_PROGRAM')
                            ->on('CAKUPAN_BU_TOTAL.ID_SKALA','=','TS.ID_SKALA');
                    })
                    ->select('CAKUPAN_BU_TOTAL.BULAN','CAKUPAN_BU_TOTAL.TAHUN','CAKUPAN_BU_TOTAL.ID_PROGRAM','CAKUPAN_BU_TOTAL.ID_SKALA','SKALA_BU.SKALA',DB::RAW('SUM(JUMLAHP) AS JUMLAH'),DB::RAW('SUM(JUMLAHBU) AS JUMLAHBU'),'TS.TARGET as TARGET')
                    ->where('CAKUPAN_BU_TOTAL.ID_PROGRAM',$program)
                    ->where('CAKUPAN_BU_TOTAL.BULAN',$bulan)
                    ->where('CAKUPAN_BU_TOTAL.TAHUN',$tahun)
                    ->where('CAKUPAN_BU_TOTAL.ID_KANWIL',$kanwil)
                    ->where('TS.KODE_TARGET',2)
                    ->orderBy('CAKUPAN_BU_TOTAL.ID_PROGRAM','CAKUPAN_BU_TOTAL.ID_SKALA', 'asc')
                    ->groupBy('CAKUPAN_BU_TOTAL.BULAN','CAKUPAN_BU_TOTAL.TAHUN','CAKUPAN_BU_TOTAL.ID_PROGRAM','CAKUPAN_BU_TOTAL.ID_SKALA')

                    ->get();

            }
        } else {
    //        echo 'Kanwilnya adalah ' . $kanwil;
            $data = DB::table('CAKUPAN_BU_TOTAL')
                ->JOIN('SKALA_BU', 'CAKUPAN_BU_TOTAL.ID_SKALA','=','SKALA_BU.ID_SKALA')
                ->join('TARGET as TS',function($join){
                    $join->on('CAKUPAN_BU_TOTAL.ID_PROGRAM','=','TS.ID_PROGRAM')
                        ->on('CAKUPAN_BU_TOTAL.ID_SKALA','=','TS.ID_SKALA');
                })
                ->select('CAKUPAN_BU_TOTAL.BULAN','CAKUPAN_BU_TOTAL.TAHUN','CAKUPAN_BU_TOTAL.ID_PROGRAM','CAKUPAN_BU_TOTAL.ID_SKALA','SKALA_BU.SKALA',DB::RAW('SUM(JUMLAHP) AS JUMLAH'),DB::RAW('SUM(JUMLAHBU) AS JUMLAHBU'),'TS.TARGET as TARGET')
                ->where('CAKUPAN_BU_TOTAL.ID_PROGRAM',$program)
                ->where('CAKUPAN_BU_TOTAL.BULAN',$bulan)
                ->where('CAKUPAN_BU_TOTAL.TAHUN',$tahun)
                ->where('TS.KODE_TARGET',2)
                ->orderBy('CAKUPAN_BU_TOTAL.ID_PROGRAM','CAKUPAN_BU_TOTAL.ID_SEGMEN', 'asc')
                ->groupBy('CAKUPAN_BU_TOTAL.BULAN','CAKUPAN_BU_TOTAL.TAHUN','CAKUPAN_BU_TOTAL.ID_PROGRAM','CAKUPAN_BU_TOTAL.ID_SKALA')

                ->get();

        }
    }


      //   dd(DB::getQueryLog());     

return $data;  
       
} 
static function BUbyprogram($program,$bulan,$tahun,$kanwil=null,$cabang=null){
/*
mengakses table CAKUPAN_BU,SKALA_BU untuk mendapatkan jumlah badan usaha per skala usaha difilter
berdasarkan bulan,tahun,program,kanwil,cabang
filter berdasarkan program dan waktu.
use link 'data/bu/program/1/3/2017''
*/
DB::enableQueryLog();
            if ($program=='All'){    
            $data = DB::table('CAKUPAN_BU')
                                -> join('PROGRAM', 'CAKUPAN_BU.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('CAKUPAN_BU.ID_SKALA','CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM AS PROGRAM', DB::raw('SUM(JUMLAH) as JUMLAH' ))
                                ->where('CAKUPAN_BU.BULAN',$bulan)
                                ->where('CAKUPAN_BU.TAHUN',$tahun)
                                ->orderBy('CAKUPAN_BU.ID_SKALA','CAKUPAN_BU.ID_PROGRAM', 'DESC')
                                ->groupBy('CAKUPAN_BU.ID_SKALA','CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM')
                                 ->get();
          } elseif (is_null($cabang) and is_null($kanwil)) {

            $data = DB::table('CAKUPAN_BU')       
                                 -> join('PROGRAM', 'CAKUPAN_BU.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('CAKUPAN_BU.ID_SKALA','CAKUPAN_BU.ID_SKALA','CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM AS PROGRAM', DB::raw('SUM(JUMLAH) as JUMLAH' ))
                                ->where('CAKUPAN_BU.ID_PROGRAM',$program)
                                ->where('CAKUPAN_BU.BULAN',$bulan)
                                ->where('CAKUPAN_BU.TAHUN',$tahun)
                                ->orderBy('CAKUPAN_BU.ID_SKALA','CAKUPAN_BU.ID_PROGRAM', 'DESC')
                                ->groupBy('CAKUPAN_BU.ID_SKALA','CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM')
                                 ->get();
                              
            }elseif (is_null($cabang)) {

            $data = DB::table('CAKUPAN_BU')       
                                 -> join('PROGRAM', 'CAKUPAN_BU.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('CAKUPAN_BU.ID_SKALA','CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM AS PROGRAM', DB::raw('SUM(JUMLAH) as JUMLAH' ))
                                ->where('CAKUPAN_BU.ID_PROGRAM',$program)
                                ->where('CAKUPAN_BU.BULAN',$bulan)
                                ->where('CAKUPAN_BU.TAHUN',$tahun)
                                 ->where('CAKUPAN_BU.ID_KANWIL',$kanwil)
                                ->orderBy('CAKUPAN_BU.ID_SKALA','CAKUPAN_BU.ID_PROGRAM', 'DESC')
                                ->groupBy('CAKUPAN_BU.ID_SKALA','CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM')
                                 ->get();

            }  else {
            $data = DB::table('CAKUPAN_BU')
                                 -> join('PROGRAM', 'CAKUPAN_BU.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('CAKUPAN_BU.ID_SKALA','CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM AS PROGRAM', DB::raw('SUM(JUMLAH) as JUMLAH' ))
                                ->where('CAKUPAN_BU.ID_PROGRAM',$program)
                                ->where('CAKUPAN_BU.BULAN',$bulan)
                                ->where('CAKUPAN_BU.TAHUN',$tahun)
                                ->where('CAKUPAN_BU.ID_KANWIL',$kanwil)
                                ->where('CAKUPAN_BU.ID_CABANG',$cabang) 
                                ->orderBy('CAKUPAN_BU.ID_SKALA','CAKUPAN_BU.ID_PROGRAM', 'DESC')
                                ->groupBy('CAKUPAN_BU.ID_SKALA','CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM')

                                 ->get();
         }       
//  dd(DB::getQueryLog());     
//dd($data);


return $data;  
       
} 

static function BUbyprogramtumbuh($program,$bulan,$tahun,$kanwil=null,$cabang=null){
  /*
  mengakses table CAKUPAN_BU,SKALA_BU untuk mendapatkan jumlah badan usaha per skala usaha difilter
  berdasarkan bulan,tahun,program,kanwil,cabang
  filter berdasarkan program dan waktu.
  use link 'data/bu/program/1/3/2017''
  */
  //  echo 'Cek var ' . $program . 'tahun' . $tahun . 'bulan' .$bulan . 'k' .$kanwil . 'c' .$cabang . '<BR>';
//  DB::enableQueryLog();

    if ($program == 'All') {
      //  echo 'All';
        if ($kanwil) {
       //     echo 'kanwil ' . $kanwil;
            if ($cabang) {
        //        echo 'Cabang' . $cabang;
                $data = DB::table('CAKUPAN_BU')
                    -> join('PROGRAM', 'CAKUPAN_BU.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                    ->select('CAKUPAN_BU.TAHUN','CAKUPAN_BU.BULAN','CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM AS PROGRAM', DB::raw('SUM(JUMLAH) as JUMLAH' ))
                    ->where('CAKUPAN_BU.BULAN','<=',$bulan)
                    ->where('CAKUPAN_BU.TAHUN','<=',$tahun)
                    ->where('CAKUPAN_BU.ID_KANWIL',$kanwil)
                    ->where('CAKUPAN_BU.ID_CABANG',$cabang)
                    ->orderBy('CAKUPAN_BU.BULAN','CAKUPAN_BU.ID_PROGRAM', 'asc')
                    ->groupBy('CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM','CAKUPAN_BU.TAHUN','CAKUPAN_BU.BULAN')
                    ->get();

            } else {
            //    echo 'Cabang Kosong ' . $cabang;
                $data = DB::table('CAKUPAN_BU')
                    -> join('PROGRAM', 'CAKUPAN_BU.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                    ->select('CAKUPAN_BU.TAHUN','CAKUPAN_BU.BULAN','CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM AS PROGRAM', DB::raw('SUM(JUMLAH) as JUMLAH' ))
                    ->where('CAKUPAN_BU.BULAN','<=',$bulan)
                    ->where('CAKUPAN_BU.TAHUN','<=',$tahun)
                    ->where('CAKUPAN_BU.ID_KANWIL',$kanwil)
                    ->orderBy('CAKUPAN_BU.BULAN','CAKUPAN_BU.ID_PROGRAM', 'asc')
                    ->groupBy('CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM','CAKUPAN_BU.TAHUN','CAKUPAN_BU.BULAN')
                    ->get();


            }
        } else {
         //   echo 'Kanwilnya adalah ' . $kanwil;
            $data = DB::table('CAKUPAN_BU')
                -> join('PROGRAM', 'CAKUPAN_BU.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                ->select('CAKUPAN_BU.TAHUN','CAKUPAN_BU.BULAN','CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM AS PROGRAM', DB::raw('SUM(JUMLAH) as JUMLAH' ))
                ->where('CAKUPAN_BU.BULAN','<=',$bulan)
                ->where('CAKUPAN_BU.TAHUN','<=',$tahun)
                ->orderBy('CAKUPAN_BU.BULAN','CAKUPAN_BU.ID_PROGRAM', 'asc')
                ->groupBy('CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM','CAKUPAN_BU.TAHUN','CAKUPAN_BU.BULAN')
                ->get();

        }
    } else {
    //    echo 'Program ' . $program;
        if ($kanwil) {
         //   echo 'kanwil ' . $kanwil;
            if ($cabang) {
             //   echo 'Cabang' . $cabang;
                $data = DB::table('CAKUPAN_BU')
                    -> join('PROGRAM', 'CAKUPAN_BU.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                    ->select('CAKUPAN_BU.TAHUN','CAKUPAN_BU.BULAN','CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM AS PROGRAM', DB::raw('SUM(JUMLAH) as JUMLAH' ))
                    ->where('CAKUPAN_BU.ID_PROGRAM',$program)
                    ->where('CAKUPAN_BU.BULAN','<=',$bulan)
                    ->where('CAKUPAN_BU.TAHUN',$tahun)
                    ->where('CAKUPAN_BU.ID_KANWIL',$kanwil)
                    ->where('CAKUPAN_BU.ID_CABANG',$cabang)
                    ->orderBy('CAKUPAN_BU.BULAN','CAKUPAN_BU.ID_PROGRAM', 'asc')
                    ->groupBy('CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM','CAKUPAN_BU.TAHUN','CAKUPAN_BU.BULAN')
                    ->get();

            } else {

                $data = DB::table('CAKUPAN_BU')
                    -> join('PROGRAM', 'CAKUPAN_BU.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                    ->select('CAKUPAN_BU.TAHUN','CAKUPAN_BU.BULAN','CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM AS PROGRAM', DB::raw('SUM(JUMLAH) as JUMLAH' ))
                    ->where('CAKUPAN_BU.ID_PROGRAM',$program)
                    ->where('CAKUPAN_BU.BULAN','<=',$bulan)
                    ->where('CAKUPAN_BU.TAHUN',$tahun)
                    ->where('CAKUPAN_BU.ID_KANWIL',$kanwil)
                    ->orderBy('CAKUPAN_BU.BULAN','CAKUPAN_BU.ID_PROGRAM', 'asc')
                    ->groupBy('CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM','CAKUPAN_BU.TAHUN','CAKUPAN_BU.BULAN')
                    ->get();

            }
        } else {

            $data = DB::table('CAKUPAN_BU')
                -> join('PROGRAM', 'CAKUPAN_BU.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                ->select('CAKUPAN_BU.TAHUN','CAKUPAN_BU.BULAN','CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM AS PROGRAM', DB::raw('SUM(JUMLAH) as JUMLAH' ))
                ->where('CAKUPAN_BU.ID_PROGRAM',$program)
                ->where('CAKUPAN_BU.BULAN','<=',$bulan)
                ->where('CAKUPAN_BU.TAHUN',$tahun)
                ->orderBy('CAKUPAN_BU.BULAN','CAKUPAN_BU.ID_PROGRAM', 'asc')
                ->groupBy('CAKUPAN_BU.ID_PROGRAM','PROGRAM.PROGRAM','CAKUPAN_BU.TAHUN','CAKUPAN_BU.BULAN')
                ->get();
        }
    }
  //  DD($data);
  return $data;  
         
  } 


} // end of class
