<?php

namespace App\Models\sismonev\data;

use Illuminate\Database\Eloquent\Model;
use DB;

class pendapatan extends Model
{
    static function AjaxListIuranKanwil($program,$bulan,$tahun){
        DB::enableQueryLog();
            $data=DB::table('IURAN')
                ->join('KANWIL','IURAN.ID_KANWIL','=','KANWIL.ID_KANWIL')
                ->select(DB::raw("IURAN.ID_KANWIL as ID,KANWIL.KANWIL,FORMAT(SUM(CASE WHEN ( ID_SEGMEN=1) THEN PENDAPATAN_IURAN END),0) AS PPUJKK,FORMAT(SUM(CASE WHEN ( ID_SEGMEN=2) THEN PENDAPATAN_IURAN END),0) AS BPUJKK,FORMAT(SUM(CASE WHEN ( ID_SEGMEN=3) THEN PENDAPATAN_IURAN END),0) AS JAKONJKK"))
                ->where('IURAN.ID_PROGRAM',$program)
                ->where('IURAN.BULAN',$bulan)
                ->where('IURAN.TAHUN',$tahun)
                ->orderBy('IURAN.ID_KANWIL','KANWIL.KANWIL','asc') 
                ->groupBy('IURAN.ID_KANWIL','KANWIL.KANWIL')    
                ->get();
           //$data = json_decode(json_encode($data), True);
          //     dd(DB::getQueryLog()); 
          //  dd($data);
    
            return $data;
    
    }
    static function AjaxListIuranCabang($program,$kanwil,$bulan,$tahun){
        DB::enableQueryLog();
            $data=DB::table('IURAN')
                ->join('CABANG','IURAN.ID_CABANG','=','CABANG.ID_CABANG')
                ->select(DB::raw("IURAN.ID_CABANG as ID_CABANG,CABANG.CABANG,CABANG.TYPE as TIPE,FORMAT(SUM(CASE WHEN ID_SEGMEN=1 THEN PENDAPATAN_IURAN END),0) AS PPUJKK,FORMAT(SUM(CASE WHEN ID_SEGMEN=2 THEN PENDAPATAN_IURAN END),0) AS BPUJKK,FORMAT(SUM(CASE WHEN ID_SEGMEN=3 THEN PENDAPATAN_IURAN END),0) AS JAKONJKK"))
                ->where('IURAN.ID_PROGRAM',$program)
                ->where('IURAN.BULAN',$bulan)
                ->where('IURAN.TAHUN',$tahun)
                ->where('IURAN.ID_KANWIL',$kanwil)
                ->orderBy('IURAN.ID_CABANG','CABANG.CABANG','asc') 
                ->groupBy('IURAN.ID_CABANG','CABANG.CABANG')    
                ->get();
           //$data = json_decode(json_encode($data), True);
          //    dd(DB::getQueryLog()); 
           // dd($data);
    
            return $data;
    
    }
    static function resumeIuranProgram($program,$bulan,$tahun,$kanwil=null,$cabang=null){
        /*
        mengakses table IURAN_BU,SKALA_BU untuk mendapatkan jumlah badan usaha per skala usaha difilter
        berdasarkan bulan,tahun,program,kanwil,cabang
        filter berdasarkan program dan waktu.
        use link 'data/bu/program/1/3/2017''
        */
        //print 'seg'.$segmen .'bul '. $bulan .'ta '. $tahun .'kan ' . $kanwil .'cab '. $cabang;
        //dd($cabang);
        DB::enableQueryLog();
                    if ($program=='All'){  
        
                    $data = DB::table('IURAN')
                                        -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                        ->select('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_PROGRAM as ID_PROGRAM', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                                        ->where('IURAN.BULAN',$bulan)
                                        ->where('IURAN.TAHUN',$tahun)
                                        ->orderBy('IURAN.ID_PROGRAM','IURAN.ID_SEGMEN', 'asc')
                                        ->groupBy('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_PROGRAM')
                                         ->get();
                  } elseif (is_null($cabang) and is_null($kanwil)) {
        
                    $data = DB::table('IURAN')  
                                        -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                        ->select('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_PROGRAM as ID_PROGRAM', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                                        ->where('IURAN.ID_PROGRAM',$program)
                                        ->where('IURAN.BULAN',$bulan)
                                        ->where('IURAN.TAHUN',$tahun)
                                        ->orderBy('IURAN.ID_PROGRAM','IURAN.ID_SEGMEN', 'asc')
                                        ->groupBy('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_PROGRAM')
                                         ->get();
              
                    }elseif (is_null($cabang)) {
        
                    $data = DB::table('IURAN')  
                                        -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                        ->select('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_PROGRAM as ID_PROGRAM', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                                        ->where('IURAN.ID_PROGRAM',$program)
                                        ->where('IURAN.BULAN',$bulan)
                                        ->where('IURAN.TAHUN',$tahun)
                                        ->where('IURAN.ID_KANWIL',$kanwil)
                                        ->orderBy('IURAN.ID_PROGRAM','IURAN.ID_SEGMEN', 'asc')
                                        ->groupBy('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_PROGRAM')
                                         ->get();
                    }  else {
                    $data = DB::table('IURAN')
                                        -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                        ->select('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_PROGRAM as ID_PROGRAM','IURAN.ID_KANWIL', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                                        ->where('IURAN.ID_PROGRAM',$program)
                                        ->where('IURAN.BULAN',$bulan)
                                        ->where('IURAN.TAHUN',$tahun)
                                        ->where('IURAN.ID_KANWIL',$kanwil)
                                        ->where('IURAN.ID_CABANG',$cabang)
                                        ->orderBy('IURAN.ID_PROGRAM','IURAN.ID_SEGMEN', 'asc')
                                        ->groupBy('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_PROGRAM','IURAN.ID_KANWIL')
                                        ->get();
        
               }       
               // dd(DB::getQueryLog());         
               $data = json_decode(json_encode($data), True);
            // dd ($data);
        return $data;  
               
        } 

        static function resumeIuranProgramSegmen($program,$bulan,$tahun,$kanwil=null,$cabang=null){
            /*
            mengakses table IURAN_BU,SKALA_BU untuk mendapatkan jumlah badan usaha per skala usaha difilter
            berdasarkan bulan,tahun,program,kanwil,cabang
            filter berdasarkan program dan waktu.
            use link 'data/bu/program/1/3/2017''
            */
            //print 'seg'.$segmen .'bul '. $bulan .'ta '. $tahun .'kan ' . $kanwil .'cab '. $cabang;
            //dd($cabang);
            DB::enableQueryLog();
                        if ($program=='All'){  
            
                        $data = DB::table('IURAN')
                                            -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_SEGMEN','IURAN.ID_PROGRAM as ID_PROGRAM', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                                            ->where('IURAN.BULAN',$bulan)
                                            ->where('IURAN.TAHUN',$tahun)
                                            ->orderBy('IURAN.ID_PROGRAM','IURAN.ID_SEGMEN', 'asc')
                                            ->groupBy('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_PROGRAM','IURAN.ID_SEGMEN')
                                             ->get();
                      } elseif (is_null($cabang) and is_null($kanwil)) {
            
                        $data = DB::table('IURAN')  
                                            -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_SEGMEN','IURAN.ID_PROGRAM as ID_PROGRAM', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                                            ->where('IURAN.ID_PROGRAM',$program)
                                            ->where('IURAN.BULAN',$bulan)
                                            ->where('IURAN.TAHUN',$tahun)
                                            ->orderBy('IURAN.ID_PROGRAM','IURAN.ID_SEGMEN', 'asc')
                                            ->groupBy('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_PROGRAM','IURAN.ID_SEGMEN')
                                             ->get();
                  
                        }elseif (is_null($cabang)) {
            
                        $data = DB::table('IURAN')  
                                            -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_SEGMEN','IURAN.ID_PROGRAM as ID_PROGRAM', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                                            ->where('IURAN.ID_PROGRAM',$program)
                                            ->where('IURAN.BULAN',$bulan)
                                            ->where('IURAN.TAHUN',$tahun)
                                            ->where('IURAN.ID_KANWIL',$kanwil)
                                            ->orderBy('IURAN.ID_PROGRAM','IURAN.ID_SEGMEN', 'asc')
                                            ->groupBy('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_PROGRAM','IURAN.ID_SEGMEN')
                                             ->get();
                        }  else {
                        $data = DB::table('IURAN')
                                            -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_SEGMEN','IURAN.ID_PROGRAM as ID_PROGRAM','IURAN.ID_KANWIL', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                                            ->where('IURAN.ID_PROGRAM',$program)
                                            ->where('IURAN.BULAN',$bulan)
                                            ->where('IURAN.TAHUN',$tahun)
                                            ->where('IURAN.ID_KANWIL',$kanwil)
                                            ->where('IURAN.ID_CABANG',$cabang)
                                            ->orderBy('IURAN.ID_PROGRAM','IURAN.ID_SEGMEN', 'asc')
                                            ->groupBy('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_PROGRAM','IURAN.ID_SEGMEN')
                                            ->get();
            
                   }       
                 //   dd(DB::getQueryLog());         
                   $data = json_decode(json_encode($data), True);
                // dd ($data);
            return $data;  
                   
            }         

 static function resumeIuranProgramSegmenTotal($program,$bulan,$tahun,$kanwil=null,$cabang=null){
            /*
            mengakses table IURAN_BU,SKALA_BU untuk mendapatkan jumlah badan usaha per skala usaha difilter
            berdasarkan bulan,tahun,program,kanwil,cabang
            filter berdasarkan program dan waktu.
            use link 'data/bu/program/1/3/2017''
            */
            //print 'seg'.$segmen .'bul '. $bulan .'ta '. $tahun .'kan ' . $kanwil .'cab '. $cabang;
            //dd($cabang);
            DB::enableQueryLog();
                        if ($program=='All'){  
            
                        $data = DB::table('IURAN')
                                            -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN)/1000 as JUMLAH' ))
                                            ->where('IURAN.BULAN',$bulan)
                                            ->where('IURAN.TAHUN',$tahun)
                                            ->orderBy('IURAN.ID_PROGRAM','IURAN.ID_SEGMEN', 'asc')
                                            ->groupBy('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_SEGMEN')
                                             ->get();
                      } elseif (is_null($cabang) and is_null($kanwil)) {
            
                        $data = DB::table('IURAN')  
                                            -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN)/1000 as JUMLAH' ))
                                            ->where('IURAN.ID_PROGRAM',$program)
                                            ->where('IURAN.BULAN',$bulan)
                                            ->where('IURAN.TAHUN',$tahun)
                                            ->orderBy('IURAN.ID_PROGRAM','IURAN.ID_SEGMEN', 'asc')
                                            ->groupBy('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_SEGMEN')
                                             ->get();
                  
                        }elseif (is_null($cabang)) {
            
                        $data = DB::table('IURAN')  
                                            -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN)/1000 as JUMLAH' ))
                                            ->where('IURAN.ID_PROGRAM',$program)
                                            ->where('IURAN.BULAN',$bulan)
                                            ->where('IURAN.TAHUN',$tahun)
                                            ->where('IURAN.ID_KANWIL',$kanwil)
                                            ->orderBy('IURAN.ID_PROGRAM','IURAN.ID_SEGMEN', 'asc')
                                            ->groupBy('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_SEGMEN')
                                             ->get();
                        }  else {
                        $data = DB::table('IURAN')
                                            -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_SEGMEN','IURAN.ID_KANWIL', DB::raw('SUM(PENDAPATAN_IURAN)/1000 as JUMLAH' ))
                                            ->where('IURAN.ID_PROGRAM',$program)
                                            ->where('IURAN.BULAN',$bulan)
                                            ->where('IURAN.TAHUN',$tahun)
                                            ->where('IURAN.ID_KANWIL',$kanwil)
                                            ->where('IURAN.ID_CABANG',$cabang)
                                            ->orderBy('IURAN.ID_PROGRAM','IURAN.ID_SEGMEN', 'asc')
                                            ->groupBy('IURAN.BULAN','IURAN.TAHUN','IURAN.ID_SEGMEN')
                                            ->get();
            
                   }       
                 //   dd(DB::getQueryLog());         
                //   $data = json_decode(json_encode($data), True);
                // dd ($data);
            return $data;  
                   
            }        

    static function PendapatanBysegmen($program,$bulan,$tahun,$kanwil=null,$cabang=null){
/*
mengakses table Iuran,segmen untuk mendapatkan jumlah badan usaha per skala usaha difilter
berdasarkan bulan,tahun,program,kanwil,cabang
filter berdasarkan program dan waktu.
use link 'data/bu/program/1/3/2017''
*/
//print 'seg'.$segmen .'bul '. $bulan .'ta '. $tahun .'kan ' . $kanwil .'cab '. $cabang;
DB::enableQueryLog();
            if ($program=='All'){    
            $data = DB::table('IURAN')
                                -> join('SEGMEN', 'IURAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('IURAN.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                                ->orderBy('IURAN.ID_SEGMEN', 'asc')
                                ->groupBy('IURAN.ID_SEGMEN','SEGMEN.SEGMEN')
                                ->get();
          } elseif (is_null($cabang) and is_null($kanwil)) {

            $data = DB::table('IURAN')   
                                -> join('SEGMEN', 'IURAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('IURAN.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                                ->where('IURAN.ID_PROGRAM',$program)
                                ->where('IURAN.BULAN',$bulan)
                                ->where('IURAN.TAHUN',$tahun)
                               ->orderBy('IURAN.ID_SEGMEN', 'asc')
                                ->groupBy('IURAN.ID_SEGMEN','SEGMEN.SEGMEN')
                                ->get();
                              
            }elseif (is_null($cabang)) {

            $data = DB::table('IURAN')  
                                -> join('SEGMEN', 'IURAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('IURAN.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                                ->where('IURAN.ID_PROGRAM',$program)
                                ->where('IURAN.BULAN',$bulan)
                                ->where('IURAN.TAHUN',$tahun)
                                ->where('IURAN.ID_KANWIL',$kanwil)
                               ->orderBy('IURAN.ID_SEGMEN', 'asc')
                                ->groupBy('IURAN.ID_SEGMEN','SEGMEN.SEGMEN')
                                ->get();
            }  else {
            $data = DB::table('IURAN')
                               -> join('SEGMEN', 'IURAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('IURAN.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                                ->where('IURAN.ID_PROGRAM',$program)
                                ->where('IURAN.BULAN',$bulan)
                                ->where('IURAN.TAHUN',$tahun)
                                ->where('IURAN.ID_KANWIL',$kanwil)
                                ->where('IURAN.ID_CABANG',$cabang)
                               ->orderBy('IURAN.ID_SEGMEN', 'asc')
                                ->groupBy('IURAN.ID_SEGMEN','SEGMEN.SEGMEN')
                                ->get();
       }
        $data = json_decode(json_encode($data), True);
     //  dd($data);
      //  dd(DB::getQueryLog());         
return $data;  
       
} 

static function PendapatanByprogram($program,$bulan,$tahun,$kanwil=null,$cabang=null){
/*
mengakses table Iuran,segmen untuk mendapatkan jumlah badan usaha per skala usaha difilter
berdasarkan bulan,tahun,program,kanwil,cabang
filter berdasarkan program dan waktu.
use link 'data/bu/program/1/3/2017''
*/
//print 'seg'.$segmen .'bul '. $bulan .'ta '. $tahun .'kan ' . $kanwil .'cab '. $cabang;
DB::enableQueryLog();
            if ($program=='All'){    
            $data = DB::table('IURAN')
                                -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('IURAN.ID_PROGRAM AS ID_PROGRAM','PROGRAM AS PROGRAM', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                                ->orderBy('IURAN.ID_PROGRAM', 'asc')
                                ->groupBy('IURAN.ID_PROGRAM','PROGRAM.PROGRAM')
                                ->get();
          } elseif (is_null($cabang) and is_null($kanwil)) {

            $data = DB::table('IURAN')   
                                -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('IURAN.ID_PROGRAM AS ID_PROGRAM','PROGRAM AS PROGRAM', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                               ->where('IURAN.ID_PROGRAM',$program)
                                ->where('IURAN.BULAN',$bulan)
                                ->where('IURAN.TAHUN',$tahun)
                                ->orderBy('IURAN.ID_PROGRAM', 'asc')
                                ->groupBy('IURAN.ID_PROGRAM','PROGRAM.PROGRAM')                                ->get();
                              
            }elseif (is_null($cabang)) {

            $data = DB::table('IURAN')  
                                -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('IURAN.ID_PROGRAM AS ID_PROGRAM','PROGRAM AS PROGRAM', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                              ->where('IURAN.ID_PROGRAM',$program)
                                ->where('IURAN.BULAN',$bulan)
                                ->where('IURAN.TAHUN',$tahun)
                                ->where('IURAN.ID_KANWIL',$kanwil)
                                ->orderBy('IURAN.ID_PROGRAM', 'asc')
                                ->groupBy('IURAN.ID_PROGRAM','PROGRAM.PROGRAM')                                ->get();
            }  else {
            $data = DB::table('IURAN')
                                -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('IURAN.ID_PROGRAM AS ID_PROGRAM','PROGRAM AS PROGRAM', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                               ->where('IURAN.ID_PROGRAM',$program)
                                ->where('IURAN.BULAN',$bulan)
                                ->where('IURAN.TAHUN',$tahun)
                                ->where('IURAN.ID_KANWIL',$kanwil)
                                ->where('IURAN.ID_CABANG',$cabang)
                                ->orderBy('IURAN.ID_PROGRAM', 'asc')
                                ->groupBy('IURAN.ID_PROGRAM','PROGRAM.PROGRAM')                                
                                ->get();
       }       
    //    dd(DB::getQueryLog());         
return $data;  
       
} 
    static function PendapatanBysegmenprogram($program,$bulan,$tahun,$kanwil,$cabang){
/*
mengakses table Iuran,segmen untuk mendapatkan jumlah badan usaha per skala usaha difilter
berdasarkan bulan,tahun,program,kanwil,cabang
filter berdasarkan program dan waktu.
use link 'data/bu/program/1/3/2017''
*/
//print 'seg'.$program .'bul '. $bulan .'ta '. $tahun .'kan ' . $kanwil .'cab '. $cabang;
//dd($program);
        if ($program == 'All') {
    //        echo 'All';
            if ($kanwil) {
    //            echo 'kanwil  ' . $kanwil;
                if ($cabang) {
    //                echo 'Cabang' . $cabang;
                    $data = DB::table('IURAN')
                        -> join('SEGMEN', 'IURAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                        -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                        ->select('IURAN.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                        ->where('IURAN.BULAN',$bulan)
                        ->where('IURAN.TAHUN',$tahun)
                        ->where('IURAN.ID_KANWIL',$kanwil)
                        ->where('IURAN.ID_CABANG',$cabang)
                        ->orderBy('IURAN.ID_SEGMEN','IURAN.ID_PROGRAM', 'asc')
                        ->groupBy('IURAN.ID_SEGMEN','SEGMEN.SEGMEN')
                        ->get();

                } else {
    //                echo 'Cabang Kosong ' . $cabang;
                    $data = DB::table('IURAN')
                        -> join('SEGMEN', 'IURAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                        -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                        ->select('IURAN.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                        ->where('IURAN.BULAN',$bulan)
                        ->where('IURAN.TAHUN',$tahun)
                        ->where('IURAN.ID_KANWIL',$kanwil)
                        ->orderBy('IURAN.ID_SEGMEN','IURAN.ID_PROGRAM', 'asc')
                        ->groupBy('IURAN.ID_SEGMEN','SEGMEN.SEGMEN')
                        ->get();

                }
            } else {
    //            echo 'Kanwilnya adalah ' . $kanwil;
                echo 'Cabang Kosong ' . $cabang;
                $data = DB::table('IURAN')
                    -> join('SEGMEN', 'IURAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                    -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                    ->select('IURAN.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                    ->where('IURAN.BULAN',$bulan)
                    ->where('IURAN.TAHUN',$tahun)
                    ->orderBy('IURAN.ID_SEGMEN','IURAN.ID_PROGRAM', 'asc')
                    ->groupBy('IURAN.ID_SEGMEN','SEGMEN.SEGMEN')
                    ->get();

            }
        } else {
     //       echo 'Program ' . $program;
            if ($kanwil) {
     //           echo 'kanwil ' . $kanwil;
                if ($cabang) {
     //               echo 'Cabang' . $cabang;
                    $data = DB::table('IURAN')
                        -> join('SEGMEN', 'IURAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                        -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                        ->select('IURAN.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                        ->where('IURAN.ID_PROGRAM',$program)
                        ->where('IURAN.BULAN',$bulan)
                        ->where('IURAN.TAHUN',$tahun)
                        ->where('IURAN.ID_KANWIL',$kanwil)
                        ->where('IURAN.ID_CABANG',$cabang)
                        ->orderBy('IURAN.ID_SEGMEN','IURAN.ID_PROGRAM', 'asc')
                        ->groupBy('IURAN.ID_SEGMEN','SEGMEN.SEGMEN')
                        ->get();

                } else {
      //              echo 'Cabang Kosong ' . $cabang;
                    $data = DB::table('IURAN')
                        -> join('SEGMEN', 'IURAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                        -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                        ->select('IURAN.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                        ->where('IURAN.ID_PROGRAM',$program)
                        ->where('IURAN.BULAN',$bulan)
                        ->where('IURAN.TAHUN',$tahun)
                        ->where('IURAN.ID_KANWIL',$kanwil)
                        ->orderBy('IURAN.ID_SEGMEN','IURAN.ID_PROGRAM', 'asc')
                        ->groupBy('IURAN.ID_SEGMEN','SEGMEN.SEGMEN')
                        ->get();

                }
            } else {
    //            echo 'Kanwilnya adalah ' . $kanwil;
                $data = DB::table('IURAN')
                    -> join('SEGMEN', 'IURAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                    -> join('PROGRAM', 'IURAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                    ->select('IURAN.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                    ->where('IURAN.ID_PROGRAM',$program)
                    ->where('IURAN.BULAN',$bulan)
                    ->where('IURAN.TAHUN',$tahun)
                    ->orderBy('IURAN.ID_SEGMEN','IURAN.ID_PROGRAM', 'asc')
                    ->groupBy('IURAN.ID_SEGMEN','SEGMEN.SEGMEN')
                    ->get();

            }
        }
DB::enableQueryLog();

      // $data = collect($data)->map(function($x){ return (array) $x; })->toArray(); 
    //dd(DB::getQueryLog());   
    // dd($data);      
return $data;  
       
} 

    static function TumbuhPendapatanBysegmenprogram($program,$bulan,$tahun,$kanwil=null,$cabang=null){
/*
mengakses table Iuran,segmen untuk mendapatkan jumlah badan usaha per skala usaha difilter
berdasarkan bulan,tahun,program,kanwil,cabang
filter berdasarkan program dan waktu.
use link 'data/bu/program/1/3/2017''
*/
//print 'seg'.$segmen .'bul '. $bulan .'ta '. $tahun .'kan ' . $kanwil .'cab '. $cabang;
// dd($program);
DB::enableQueryLog();

        if ($program == 'All') {
     //       echo 'All';
            if ($kanwil) {
     //           echo 'kanwil  ' . $kanwil;
                if ($cabang) {
     //               echo 'Cabang' . $cabang;
                    $data = DB::table('IURAN')
                        -> join('SEGMEN', 'IURAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                        ->select('BULAN','IURAN.ID_SEGMEN AS ID_SEGMEN','IURAN.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                        ->where('IURAN.BULAN','<=',$bulan)
                        ->where('IURAN.TAHUN','=',$tahun)
                        ->where('IURAN.ID_KANWIL',$kanwil)
                        ->where('IURAN.ID_CABANG',$cabang)
                        ->orderBy('TAHUN','BULAN','IURAN.ID_SEGMEN','IURAN.ID_PROGRAM', 'asc')
                        ->groupBy('TAHUN','BULAN','IURAN.ID_SEGMEN','IURAN.ID_PROGRAM','SEGMEN.SEGMEN')
                        ->get();

                } else {
     //               echo 'Cabang Kosong ' . $cabang;
                    $data = DB::table('IURAN')
                        -> join('SEGMEN', 'IURAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                        ->select('BULAN','IURAN.ID_SEGMEN AS ID_SEGMEN','IURAN.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                        ->where('IURAN.BULAN','<=',$bulan)
                        ->where('IURAN.TAHUN','=',$tahun)
                        ->where('IURAN.ID_KANWIL',$kanwil)
                        ->orderBy('TAHUN','BULAN','IURAN.ID_SEGMEN','IURAN.ID_PROGRAM', 'asc')
                        ->groupBy('TAHUN','BULAN','IURAN.ID_SEGMEN','IURAN.ID_PROGRAM','SEGMEN.SEGMEN')
                        ->get();


                }
            } else {
      //          echo 'Kanwilnya adalah ' . $kanwil;
                $data = DB::table('IURAN')
                    -> join('SEGMEN', 'IURAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                    ->select('BULAN','IURAN.ID_SEGMEN AS ID_SEGMEN','IURAN.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                    ->where('IURAN.BULAN','<=',$bulan)
                    ->where('IURAN.TAHUN','=',$tahun)
                    ->orderBy('TAHUN','BULAN','IURAN.ID_SEGMEN','IURAN.ID_PROGRAM', 'asc')
                    ->groupBy('TAHUN','BULAN','IURAN.ID_SEGMEN','IURAN.ID_PROGRAM','SEGMEN.SEGMEN')
                    ->get();


            }
        } else {
      //      echo 'Program ' . $program;
            if ($kanwil) {
      //          echo 'kanwil ' . $kanwil;
                if ($cabang) {
       //             echo 'Cabang' . $cabang;
                    $data = DB::table('IURAN')
                        -> join('SEGMEN', 'IURAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                        ->select('BULAN','IURAN.ID_SEGMEN AS ID_SEGMEN','IURAN.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                        ->where('IURAN.ID_PROGRAM',$program)
                        ->where('IURAN.BULAN','<=',$bulan)
                        ->where('IURAN.TAHUN','=',$tahun)
                        ->where('IURAN.ID_KANWIL',$kanwil)
                        ->where('IURAN.ID_CABANG',$cabang)
                        ->orderBy('TAHUN','BULAN','IURAN.ID_SEGMEN','IURAN.ID_PROGRAM', 'asc')
                        ->groupBy('TAHUN','BULAN','IURAN.ID_SEGMEN','IURAN.ID_PROGRAM','SEGMEN.SEGMEN')
                        ->get();
                } else {
       //             echo 'Cabang Kosong ' . $cabang;
                    $data = DB::table('IURAN')
                        -> join('SEGMEN', 'IURAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                        ->select('BULAN','IURAN.ID_SEGMEN AS ID_SEGMEN','IURAN.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                        ->where('IURAN.ID_PROGRAM',$program)
                        ->where('IURAN.BULAN','<=',$bulan)
                        ->where('IURAN.TAHUN','=',$tahun)
                        ->where('IURAN.ID_KANWIL',$kanwil)
                        ->orderBy('TAHUN','BULAN','IURAN.ID_SEGMEN','IURAN.ID_PROGRAM', 'asc')
                        ->groupBy('TAHUN','BULAN','IURAN.ID_SEGMEN','IURAN.ID_PROGRAM','SEGMEN.SEGMEN')
                        ->get();

                }
            } else {
        //        echo 'Kanwilnya adalah ' . $kanwil;
                $data = DB::table('IURAN')
                    -> join('SEGMEN', 'IURAN.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                    ->select('BULAN','IURAN.ID_SEGMEN AS ID_SEGMEN','IURAN.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', DB::raw('SUM(PENDAPATAN_IURAN) as JUMLAH' ))
                    ->where('IURAN.ID_PROGRAM',$program)
                    ->where('IURAN.BULAN','<=',$bulan)
                    ->where('IURAN.TAHUN','=',$tahun)
                    ->orderBy('TAHUN','BULAN','IURAN.ID_SEGMEN','IURAN.ID_PROGRAM', 'asc')
                    ->groupBy('TAHUN','BULAN','IURAN.ID_SEGMEN','IURAN.ID_PROGRAM','SEGMEN.SEGMEN')
                    ->get();

            }
        }

// dd($data);
return $data;  
       
} 

}
