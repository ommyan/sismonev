<?php

namespace App\Models\sismonev\data;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use DB;

class organisasi extends Model
{
    protected $table="ORGANISASI";

    public static function getJumlahkaryawankanwil($bulan,$tahun){
       // echo $kanwils . $cabangs;
        DB::enableQueryLog();
        $data = collect(DB::select(DB::raw("select A.ID_KANWIL as ID,C.KANWIL as KANWIL,COUNT(A.ID_CABANG) AS JMLCABANG,SUM(JUMLAH_KARYAWAN) AS JMLKARYAWAN,COALESCE( B.JML_PESERTA, 0 ) AS JMLPESERTA,SUM(JUMLAH_KARYAWAN)/COALESCE( B.JML_PESERTA, 0 ) as RASIO FROM ORGANISASI A inner join KANWIL C on A.ID_KANWIL=C.ID_KANWIL LEFT JOIN (SELECT ID_KANWIL,SUM(JUMLAH_PESERTA) AS JML_PESERTA FROM CAKUPAN WHERE BULAN=$bulan AND TAHUN=$tahun AND ID_PROGRAM=1 GROUP BY ID_KANWIL,ID_PROGRAM) B ON A.ID_KANWIL=B.ID_KANWIL WHERE A.BULAN=$bulan AND A.TAHUN=$tahun GROUP BY A.ID_KANWIL, A.BULAN, A.TAHUN")));
        $data->all();
        $data = json_decode(json_encode($data), True);
       // dd(DB::getQueryLog());
        //    dd($data);
            return $data;

    }

    public static function getJumlahkaryawancabang($bulan,$tahun,$idc){
        // echo $kanwils . $cabangs;
        DB::enableQueryLog();
                $data=collect(DB::select(DB::raw("select A.ID_CABANG as ID,C.CABANG,SUM(JUMLAH_KARYAWAN) as JMLKARYAWAN,COALESCE( B.JML_PESERTA, 0 ) as JMLPESERTA FROM ORGANISASI A inner join CABANG C on A.ID_CABANG=C.ID_CABANG LEFT JOIN (SELECT ID_CABANG,SUM(JUMLAH_PESERTA) as JML_PESERTA FROM CAKUPAN WHERE BULAN=$bulan AND TAHUN=$tahun AND ID_PROGRAM=1 GROUP BY ID_CABANG,ID_PROGRAM) B ON A.ID_CABANG=B.ID_CABANG WHERE A.BULAN=$bulan AND A.TAHUN=$tahun and A.ID_KANWIL=$idc GROUP BY A.ID_CABANG, A.BULAN, A.TAHUN")));
        $data->all();
        $data = json_decode(json_encode($data), True);

        return $data;

    }
}
