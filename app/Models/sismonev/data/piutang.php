<?php

namespace App\Models\sismonev\data;

use Illuminate\Database\Eloquent\Model;
use DB;
class piutang extends Model

{
    static function PiutangBysegmenprogram($program,$bulan,$tahun,$kanwil=null,$cabang=null){
        /*
        mengakses table PIUTANG,segmen untuk mendapatkan jumlah badan usaha per skala usaha difilter
        berdasarkan bulan,tahun,program,kanwil,cabang
        filter berdasarkan program dan waktu.
        use link 'data/bu/program/1/3/2017''
        */
        //print 'seg'.$segmen .'bul '. $bulan .'ta '. $tahun .'kan ' . $kanwil .'cab '. $cabang;
        //dd($program);
        DB::enableQueryLog();
                    if ($program=='All'){    
                    $data = DB::table('PIUTANG')
                                        -> join('SEGMEN', 'PIUTANG.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                        -> join('PROGRAM', 'PIUTANG.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                        ->select('PIUTANG.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', 
                                        DB::raw('SUM(PIUTANG_L) as LANCAR'),
                                        DB::raw('SUM(PIUTANG_KL) as KURANGLANGCAR'),
                                        DB::raw('SUM(PIUTANG_M) as MACET'))
                                        ->orderBy('PIUTANG.ID_SEGMEN','PIUTANG.ID_PROGRAM', 'asc')
                                        ->groupBy('PIUTANG.ID_SEGMEN','SEGMEN.SEGMEN')
                                        ->get();
                  } elseif (is_null($cabang) and is_null($kanwil)) {
        
                    $data = DB::table('PIUTANG')   
                                        -> join('SEGMEN', 'PIUTANG.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                        -> join('PROGRAM', 'PIUTANG.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                        ->select('PIUTANG.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', 
                                        DB::raw('SUM(PIUTANG_L) as LANCAR'),
                                        DB::raw('SUM(PIUTANG_KL) as KURANGLANGCAR'),
                                        DB::raw('SUM(PIUTANG_M) as MACET')) 
                                        ->where('PIUTANG.ID_PROGRAM',$program)
                                        ->where('PIUTANG.BULAN',$bulan)
                                        ->where('PIUTANG.TAHUN',$tahun)
                                       ->orderBy('PIUTANG.ID_SEGMEN','PIUTANG.ID_PROGRAM', 'asc')
                                        ->groupBy('PIUTANG.ID_SEGMEN','SEGMEN.SEGMEN')
                                        ->get();
                                      
                    }elseif (is_null($cabang)) {
        
                    $data = DB::table('PIUTANG')  
                                        -> join('SEGMEN', 'PIUTANG.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                        -> join('PROGRAM', 'PIUTANG.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                        ->select('PIUTANG.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', 
                                        DB::raw('SUM(PIUTANG_L) as LANCAR'),
                                        DB::raw('SUM(PIUTANG_KL) as KURANGLANGCAR'),
                                        DB::raw('SUM(PIUTANG_M) as MACET'))
                                        ->where('PIUTANG.ID_PROGRAM',$program)
                                        ->where('PIUTANG.BULAN',$bulan)
                                        ->where('PIUTANG.TAHUN',$tahun)
                                        ->where('PIUTANG.ID_KANWIL',$kanwil)
                                       ->orderBy('PIUTANG.ID_SEGMEN','PIUTANG.ID_PROGRAM', 'asc')
                                        ->groupBy('PIUTANG.ID_SEGMEN','SEGMEN.SEGMEN')
                                        ->get();
                    }  else {
                    $data = DB::table('PIUTANG')
                                       -> join('SEGMEN', 'PIUTANG.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                        -> join('PROGRAM', 'PIUTANG.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                        ->select('PIUTANG.ID_SEGMEN AS ID_SEGMEN','SEGMEN.SEGMEN AS SEGMEN', 
                                        DB::raw('SUM(PIUTANG_L) as LANCAR'),
                                        DB::raw('SUM(PIUTANG_KL) as KURANGLANGCAR'),
                                        DB::raw('SUM(PIUTANG_M) as MACET'))
                                        ->where('PIUTANG.ID_PROGRAM',$program)
                                        ->where('PIUTANG.BULAN',$bulan)
                                        ->where('PIUTANG.TAHUN',$tahun)
                                        ->where('PIUTANG.ID_KANWIL',$kanwil)
                                        ->where('PIUTANG.ID_CABANG',$cabang)
                                       ->orderBy('PIUTANG.ID_SEGMEN','PIUTANG.ID_PROGRAM', 'asc')
                                        ->groupBy('PIUTANG.ID_SEGMEN','SEGMEN.SEGMEN')
                                        ->get();
               }       
              // $data = collect($data)->map(function($x){ return (array) $x; })->toArray(); 
            //dd(DB::getQueryLog());   
            // dd($data);      
        return $data;  
               
        } 
}
