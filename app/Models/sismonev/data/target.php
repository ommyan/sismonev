<?php

namespace App\Models\sismonev\data;

use Illuminate\Database\Eloquent\Model;

class target extends Model
{
    protected  $table="TARGET";

    public static function getTarget($tahun){
        $cabang = DB::table('TARGET')->select('ID_SEGMEN','ID_PROGRAM', 'TARTGET')
            ->where('TAHUN', '=', $tahun)->get();
        return $cabang;
    }
}
