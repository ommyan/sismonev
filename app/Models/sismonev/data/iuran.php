<?php

namespace App\Models\sismonev\data;

use Illuminate\Database\Eloquent\Model;

class iuran extends Model
{
    protected $table = "IURAN";
    protected $fillable = [
        'BULAN' ,
        'TAHUN',          
        'ID_KANWIL',
        'ID_CABANG',
        'ID_PROGRAM' ,
        'ID_SEGMEN',
        'PENDAPATAN_IURAN'];
};
