<?php

namespace App\Models\sismonev\data;

use Illuminate\Database\Eloquent\Model;
use DB;

class Potensi extends Model
{
    //
    protected  $table="potensi";

    public static function getPotensi($tahun){
        $cabang = DB::table('POTENSI')->select('ID_SEGMEN', 'POTENSI')
            ->where('TAHUN', '=', $tahun)->get();
        //$cabang = json_decode(json_encode($cabang), True);
        return $cabang;
    }

}
