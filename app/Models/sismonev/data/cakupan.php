<?php

namespace App\Models\sismonev\data;

use Illuminate\Database\Eloquent\Model;
use DB;

class cakupan extends Model
{
    protected $table = "CAKUPAN";
    protected $fillable = [
        'BULAN',
        'TAHUN',
        'ID_KANWIL',
        'ID_CABANG',
        'ID_PROGRAM',
        'ID_SEGMEN',
        'JUMLAH_KARTU_TERBIT',
        'JUMLAH_PTNIK',
        'JUMLAH_TKA',
        'JUMLAH_PESERTA'
    ];

    public static function getPTNIK($program, $bulan, $tahun, $kanwil, $cabang)
    {
        echo $program;

        if ($program == 'All') {
            //     echo 'All';
            if ($kanwil) {
                if ($cabang) {
                    $data = DB::table('CAKUPAN')
                        ->select('CAKUPAN.ID_PROGRAM',DB::raw('(SUM(JUMLAH_PTNIK)/SUM(JUMLAH_PESERTA))*100 as JTNIK'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN', $bulan)
                        ->where('TAHUN', $tahun)
                        ->where('ID_KANWIL', $kanwil)
                        ->where('ID_CABANG', $cabang)
                        ->groupBy('ID_PROGRAM')
                        ->get();
                } else {
                    $data = DB::table('CAKUPAN')
                        ->select('CAKUPAN.ID_PROGRAM',DB::raw('SUM(JUMLAH_PTNIK)/SUM(JUMLAH_PESERTA)*100 as JTNIK'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN', $bulan)
                        ->where('TAHUN', $tahun)
                        ->where('ID_KANWIL', $kanwil)
                        ->groupBy('ID_PROGRAM')
                        ->get();
                }
            } else {
                $data = DB::table('CAKUPAN')
                    ->select('CAKUPAN.ID_PROGRAM',DB::raw('SUM(JUMLAH_PTNIK)/SUM(JUMLAH_PESERTA)*100 as JTNIK'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                    ->where('BULAN', $bulan)
                    ->where('TAHUN', $tahun)
                    ->groupBy('ID_PROGRAM')
                    ->get();
            }
        } else {
            //        echo 'Program ' . $program;
            if ($kanwil) {
                echo 'kanwil ' . $kanwil;
                if ($cabang) {

                    $data = DB::table('CAKUPAN')
                        ->select('CAKUPAN.ID_PROGRAM',DB::raw('SUM(JUMLAH_PTNIK)/SUM(JUMLAH_PESERTA)*100 as JTNIK'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN', $bulan)
                        ->where('TAHUN', $tahun)
                        ->where('ID_PROGRAM', $program)
                        ->where('ID_KANWIL', $kanwil)
                        ->where('ID_CABANG', $cabang)
                        ->groupBy('ID_PROGRAM')
                        ->get();
                } else {
                    //                echo 'Cabang Kosong ' . $cabang;
                    $data = DB::table('CAKUPAN')
                        ->select('CAKUPAN.ID_PROGRAM',DB::raw('SUM(JUMLAH_PTNIK)/SUM(JUMLAH_PESERTA)*100 as JTNIK'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN', $bulan)
                        ->where('TAHUN', $tahun)
                        ->where('ID_PROGRAM', $program)
                        ->where('ID_KANWIL', $kanwil)
                        ->groupBy('ID_PROGRAM')
                        ->get();
                }
            } else {
                //         echo 'Kanwilnya adalah ' . $kanwil;
                $data = DB::table('CAKUPAN')
                    ->select('CAKUPAN.ID_PROGRAM',DB::raw('SUM(JUMLAH_PTNIK)/SUM(JUMLAH_PESERTA)*100 as JTNIK'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                    ->where('BULAN', $bulan)
                    ->where('TAHUN', $tahun)
                    ->groupBy('ID_PROGRAM')
                    ->get();
            }
        }
        $data = json_decode(json_encode($data), True);
        return $data;

    }
    public static function getKartuTerbit($program, $bulan, $tahun, $kanwil, $cabang)
    {
        if ($program == 'All') {
            //     echo 'All';
            if ($kanwil) {
                //         echo 'kanwil ' . $kanwil;
                if ($cabang) {
                    //             echo 'Cabang' . $cabang;
                    $data = DB::table('CAKUPAN')
                        ->select('ID_PROGRAM',DB::raw('SUM(JUMLAH_KARTU_TERBIT)/SUM(JUMLAH_PESERTA)*100 as JKT'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN', $bulan)
                        ->where('TAHUN', $tahun)
                        ->where('ID_KANWIL', $kanwil)
                        ->where('ID_CABANG', $cabang)
                        ->groupBy('ID_PROGRAM')
                        ->get();
                } else {
                    //              echo 'Cabang Kosong ' . $cabang;
                    $data = DB::table('CAKUPAN')
                        ->select('ID_PROGRAM',DB::raw('SUM(JUMLAH_KARTU_TERBIT)/SUM(JUMLAH_PESERTA)*100 as JKT'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN', $bulan)
                        ->where('TAHUN', $tahun)
                        ->where('ID_KANWIL', $kanwil)
                        ->groupBy('ID_PROGRAM')
                        ->get();
                }
            } else {
                //      echo 'Kanwilnya adalah ' . $kanwil;
                $data = DB::table('CAKUPAN')
                    ->select('ID_PROGRAM',DB::raw('SUM(JUMLAH_KARTU_TERBIT)/SUM(JUMLAH_PESERTA)*100 as JKT'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                    ->where('BULAN', $bulan)
                    ->where('TAHUN', $tahun)
                    ->groupBy('ID_PROGRAM')
                    ->get();
            }
        } else {
            //        echo 'Program ' . $program;
            if ($kanwil) {
                echo 'kanwil ' . $kanwil;
                if ($cabang) {
                    $data = DB::table('CAKUPAN')
                        ->select('ID_PROGRAM',DB::raw('SUM(JUMLAH_KARTU_TERBIT)/SUM(JUMLAH_PESERTA)*100 as JKT'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN', $bulan)
                        ->where('TAHUN', $tahun)
                        ->where('ID_PROGRAM', $program)
                        ->where('ID_KANWIL', $kanwil)
                        ->where('ID_CABANG', $cabang)
                        ->groupBy('ID_PROGRAM')
                        ->get();
                } else {
                    $data = DB::table('CAKUPAN')
                        ->select('ID_PROGRAM',DB::raw('SUM(JUMLAH_KARTU_TERBIT)/SUM(JUMLAH_PESERTA)*100 as JKT'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN', $bulan)
                        ->where('TAHUN', $tahun)
                        ->where('ID_PROGRAM', $program)
                        ->where('ID_KANWIL', $kanwil)
                        ->groupBy('ID_PROGRAM')
                        ->get();
                }
            } else {
                $data = DB::table('CAKUPAN')
                    ->select('ID_PROGRAM',DB::raw('SUM(JUMLAH_KARTU_TERBIT)/SUM(JUMLAH_PESERTA)*100 as JKT'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                    ->where('BULAN', $bulan)
                    ->where('TAHUN', $tahun)
                    ->groupBy('ID_PROGRAM')
                    ->get();
            }
        }
        $data = json_decode(json_encode($data), True);
        return $data;

    }
    public static function getTKA($program, $bulan, $tahun, $kanwil, $cabang)
    {
        if ($program == 'All') {
            if ($kanwil) {
                if ($cabang) {
                    //             echo 'Cabang' . $cabang;
                    $data = DB::table('CAKUPAN')
                        ->select('ID_PROGRAM',DB::raw('SUM(JUMLAH_TKA)/SUM(JUMLAH_PESERTA)*100 as JTKA'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN', $bulan)
                        ->where('TAHUN', $tahun)
                        ->where('ID_KANWIL', $kanwil)
                        ->where('ID_CABANG', $cabang)
                        ->groupBy('ID_PROGRAM')
                        ->get();
                } else {
                    $data = DB::table('CAKUPAN')
                        ->select('ID_PROGRAM',DB::raw('SUM(JUMLAH_TKA)/SUM(JUMLAH_PESERTA)*100 as JTKA'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN', $bulan)
                        ->where('TAHUN', $tahun)
                        ->where('ID_KANWIL', $kanwil)
                        ->groupBy('ID_PROGRAM')
                        ->get();
                }
            } else {
                $data = DB::table('CAKUPAN')
                    ->select('ID_PROGRAM',DB::raw('SUM(JUMLAH_TKA)/SUM(JUMLAH_PESERTA) *100 as JTKA'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                    ->where('BULAN', $bulan)
                    ->where('TAHUN', $tahun)
                    ->groupBy('ID_PROGRAM')
                    ->get();
            }
        } else {
            //        echo 'Program ' . $program;
            if ($kanwil) {
                echo 'kanwil ' . $kanwil;
                if ($cabang) {
                    $data = DB::table('CAKUPAN')
                        ->select('ID_PROGRAM',DB::raw('SUM(JUMLAH_TKA)/SUM(JUMLAH_PESERTA)*100 as JTKA'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN', $bulan)
                        ->where('TAHUN', $tahun)
                        ->where('ID_PROGRAM', $program)
                        ->where('ID_KANWIL', $kanwil)
                        ->where('ID_CABANG', $cabang)
                        ->groupBy('ID_PROGRAM')
                        ->get();
                } else {
                    //                echo 'Cabang Kosong ' . $cabang;
                    $data = DB::table('CAKUPAN')
                        ->select('ID_PROGRAM',DB::raw('SUM(JUMLAH_TKA)/SUM(JUMLAH_PESERTA)*100 as JTKA'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN', $bulan)
                        ->where('TAHUN', $tahun)
                        ->where('ID_PROGRAM', $program)
                        ->where('ID_KANWIL', $kanwil)
                        ->groupBy('ID_PROGRAM')
                        ->get();
                }
            } else {
                //         echo 'Kanwilnya adalah ' . $kanwil;
                $data = DB::table('CAKUPAN')
                    ->select('ID_PROGRAM',DB::raw('SUM(JUMLAH_TKA)/SUM(JUMLAH_PESERTA)*100 as JTKA'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                    ->where('BULAN', $bulan)
                    ->where('TAHUN', $tahun)
                    ->groupBy('ID_PROGRAM')
                    ->get();
            }
        }
        $data = json_decode(json_encode($data), True);
        return $data;

    }
}