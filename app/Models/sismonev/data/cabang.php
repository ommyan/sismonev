<?php

namespace App\Models\sismonev\data;

use Illuminate\Database\Eloquent\Model;

class cabang extends Model
{
    protected $table='CABANG';
    protected $primaryKey = 'ID_CABANG';
}
