<?php

namespace App\Models\sismonev\data;

use Illuminate\Database\Eloquent\Model;
use DB;

class pembayaran extends Model
{
    static function AjaxListManfaatKanwil($program,$bulan,$tahun){
        DB::enableQueryLog();
            $data=DB::table('MANFAAT')
                ->join('KANWIL','MANFAAT.ID_KANWIL','=','KANWIL.ID_KANWIL')
                ->select(DB::raw("MANFAAT.ID_KANWIL as ID,KANWIL.KANWIL,FORMAT(SUM(CASE WHEN ( ID_SEGMEN=1) THEN JUMLAH_PEMBAYARAN END),0) AS PPUJKK,FORMAT(SUM(CASE WHEN ( ID_SEGMEN=2) THEN JUMLAH_PEMBAYARAN END),0) AS BPUJKK,FORMAT(SUM(CASE WHEN ( ID_SEGMEN=3) THEN JUMLAH_PEMBAYARAN END),0) AS JAKONJKK"))
                ->where('MANFAAT.ID_PROGRAM',$program)
                ->where('MANFAAT.BULAN',$bulan)
                ->where('MANFAAT.TAHUN',$tahun)
                ->orderBy('MANFAAT.ID_KANWIL','KANWIL.KANWIL','asc') 
                ->groupBy('MANFAAT.ID_KANWIL','KANWIL.KANWIL')    
                ->get();
           //$data = json_decode(json_encode($data), True);
          //     dd(DB::getQueryLog()); 
          //  dd($data);
    
            return $data;
    
    }
    static function AjaxListManfaatCabang($program,$kanwil,$bulan,$tahun){
        DB::enableQueryLog();
            $data=DB::table('MANFAAT')
                ->join('CABANG','MANFAAT.ID_CABANG','=','CABANG.ID_CABANG')
                ->select(DB::raw("MANFAAT.ID_CABANG as ID_CABANG,CABANG.CABANG,FORMAT(SUM(CASE WHEN ID_SEGMEN=1 THEN JUMLAH_PEMBAYARAN END),0) AS PPUJKK,FORMAT(SUM(CASE WHEN ID_SEGMEN=2 THEN JUMLAH_PEMBAYARAN END),0) AS BPUJKK,FORMAT(SUM(CASE WHEN ID_SEGMEN=3 THEN JUMLAH_PEMBAYARAN END),0) AS JAKONJKK"))
                ->where('MANFAAT.ID_PROGRAM',$program)
                ->where('MANFAAT.BULAN',$bulan)
                ->where('MANFAAT.TAHUN',$tahun)
                ->where('MANFAAT.ID_KANWIL',$kanwil)
                ->orderBy('MANFAAT.ID_CABANG','CABANG.CABANG','asc') 
                ->groupBy('MANFAAT.ID_CABANG','CABANG.CABANG')    
                ->get();
           //$data = json_decode(json_encode($data), True);
          //    dd(DB::getQueryLog()); 
           // dd($data);
    
            return $data;
    
    }
    static function resumeManfaatProgram($program,$bulan,$tahun,$kanwil=null,$cabang=null){
        /*
        mengakses table MANFAAT_BU,SKALA_BU untuk mendapatkan jumlah badan usaha per skala usaha difilter
        berdasarkan bulan,tahun,program,kanwil,cabang
        filter berdasarkan program dan waktu.
        use link 'data/bu/program/1/3/2017''
        */
        //print 'seg'.$segmen .'bul '. $bulan .'ta '. $tahun .'kan ' . $kanwil .'cab '. $cabang;
        //dd($bulan);
        DB::enableQueryLog();
                    if ($program=='All'){  
        
                    $data = DB::table('MANFAAT')
                                        -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                        ->select('MANFAAT.ID_PROGRAM as ID_PROGRAM','MANFAAT.ID_SEGMEN', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH' ))
                                        ->where('MANFAAT.BULAN','<=',$bulan)
                                        ->where('MANFAAT.TAHUN',$tahun)
                                        ->orderBy('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN', 'asc')
                                        ->groupBy('MANFAAT.ID_PROGRAM')
                                         ->get();
                  } elseif (is_null($cabang) and is_null($kanwil)) {
        
                    $data = DB::table('MANFAAT')  
                                        -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                        ->select('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM as ID_PROGRAM', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH' ))
                                        ->where('MANFAAT.ID_PROGRAM',$program)
                                        ->where('MANFAAT.BULAN','<=',$bulan)
                                        ->where('MANFAAT.TAHUN',$tahun)
                                        ->orderBy('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN', 'asc')
                                        ->groupBy('MANFAAT.ID_PROGRAM')
                                         ->get();
              
                    }elseif (is_null($cabang)) {
        
                    $data = DB::table('MANFAAT')  
                                        -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                        ->select('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM as ID_PROGRAM', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH' ))
                                        ->where('MANFAAT.ID_PROGRAM',$program)
                                        ->where('MANFAAT.BULAN','<=',$bulan)
                                        ->where('MANFAAT.TAHUN',$tahun)
                                        ->where('MANFAAT.ID_KANWIL',$kanwil)
                                        ->orderBy('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN', 'asc')
                                        ->groupBy('MANFAAT.ID_PROGRAM')
                                         ->get();
                    }  else {
                    $data = DB::table('MANFAAT')
                                        -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                        ->select('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM as ID_PROGRAM','MANFAAT.ID_KANWIL', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH' ))
                                        ->where('MANFAAT.ID_PROGRAM',$program)
                                        ->where('MANFAAT.BULAN','<=',$bulan)
                                        ->where('MANFAAT.TAHUN',$tahun)
                                        ->where('MANFAAT.ID_KANWIL',$kanwil)
                                        ->where('MANFAAT.ID_CABANG',$cabang)
                                        ->orderBy('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN', 'asc')
                                        ->groupBy('MANFAAT.ID_PROGRAM')
                                        ->get();
        
               }       
            //    dd(DB::getQueryLog());         
               $data = json_decode(json_encode($data), True);
           //  dd ($data);
        return $data;  
               
        } 

        static function resumeManfaatPerProgram($program,$bulan,$tahun,$kanwil=null,$cabang=null){
        /*
        mengakses table MANFAAT_BU,SKALA_BU untuk mendapatkan jumlah badan usaha per skala usaha difilter
        berdasarkan bulan,tahun,program,kanwil,cabang
        filter berdasarkan program dan waktu.
        use link 'data/bu/program/1/3/2017''
        */
        //print 'seg'.$segmen .'bul '. $bulan .'ta '. $tahun .'kan ' . $kanwil .'cab '. $cabang;
        //dd($bulan);
        DB::enableQueryLog();
                    if ($program=='All'){  
        
                    $data = DB::table('MANFAAT')
                                        -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                        ->select('MANFAAT.ID_PROGRAM as ID_PROGRAM','MANFAAT.ID_PROGRAM', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH'),DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS'))
                                        ->where('MANFAAT.BULAN','<=',$bulan)
                                        ->where('MANFAAT.TAHUN',$tahun)
                                        ->orderBy('MANFAAT.ID_PROGRAM', 'asc')
                                        ->groupBy('MANFAAT.ID_PROGRAM')
                                         ->get();
                  } elseif (is_null($cabang) and is_null($kanwil)) {
        
                    $data = DB::table('MANFAAT')  
                                        -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                        ->select('MANFAAT.ID_PROGRAM as ID_PROGRAM', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH' ),DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS'))
                                        ->where('MANFAAT.ID_PROGRAM',$program)
                                        ->where('MANFAAT.BULAN','<=',$bulan)
                                        ->where('MANFAAT.TAHUN',$tahun)
                                        ->orderBy('MANFAAT.ID_PROGRAM', 'asc')
                                        ->groupBy('MANFAAT.ID_PROGRAM')
                                         ->get();
              
                    }elseif (is_null($cabang)) {
        
                    $data = DB::table('MANFAAT')  
                                        -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                        ->select('MANFAAT.ID_PROGRAM as ID_PROGRAM', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH' ),DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS'))
                                        ->where('MANFAAT.ID_PROGRAM',$program)
                                        ->where('MANFAAT.BULAN','<=',$bulan)
                                        ->where('MANFAAT.TAHUN',$tahun)
                                        ->where('MANFAAT.ID_KANWIL',$kanwil)
                                        ->orderBy('MANFAAT.ID_PROGRAM', 'asc')
                                        ->groupBy('MANFAAT.ID_PROGRAM')
                                         ->get();
                    }  else {
                    $data = DB::table('MANFAAT')
                                        -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                        ->select('MANFAAT.ID_PROGRAM as ID_PROGRAM','MANFAAT.ID_KANWIL', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH' ),DB::raw('SUM(JUMLAH_KASUS) as JUMLAHKASUS'))
                                        ->where('MANFAAT.ID_PROGRAM',$program)
                                        ->where('MANFAAT.BULAN','<=',$bulan)
                                        ->where('MANFAAT.TAHUN',$tahun)
                                        ->where('MANFAAT.ID_KANWIL',$kanwil)
                                        ->where('MANFAAT.ID_CABANG',$cabang)
                                        ->orderBy('MANFAAT.ID_PROGRAM', 'asc')
                                        ->groupBy('MANFAAT.ID_PROGRAM')
                                        ->get();
        
               }       
            //    dd(DB::getQueryLog());         
               $data = json_decode(json_encode($data), True);
           //  dd ($data);
        return $data;  
               
        } 

        static function resumeManfaatProgramSegmen($program,$bulan,$tahun,$kanwil=null,$cabang=null){
            /*
            mengakses table MANFAAT_BU,SKALA_BU untuk mendapatkan jumlah badan usaha per skala usaha difilter
            berdasarkan bulan,tahun,program,kanwil,cabang
            filter berdasarkan program dan waktu.
            use link 'data/bu/program/1/3/2017''
            */
            //print 'seg'.$segmen .'bul '. $bulan .'ta '. $tahun .'kan ' . $kanwil .'cab '. $cabang;
            //dd($cabang);

            DB::enableQueryLog();
                        if ($program=='All'){  
            
                        $data = DB::table('MANFAAT')
                                            -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM as ID_PROGRAM', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH' ), DB::raw('SUM(JUMLAH_KASUS) as KASUS' ))
                                            ->where('MANFAAT.BULAN','<=',$bulan)
                                            ->where('MANFAAT.TAHUN',$tahun)
                                            ->orderBy('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN', 'asc')
                                            ->groupBy('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN')
                                             ->get();
                      } elseif (is_null($cabang) and is_null($kanwil)) {
            
                        $data = DB::table('MANFAAT')  
                                            -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM as ID_PROGRAM', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH' ), DB::raw('SUM(JUMLAH_KASUS) as KASUS' ))
                                            ->where('MANFAAT.ID_PROGRAM',$program)
                                            ->where('MANFAAT.BULAN','<=',$bulan)
                                            ->where('MANFAAT.TAHUN',$tahun)
                                            ->orderBy('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN', 'asc')
                                            ->groupBy('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN')
                                             ->get();
                  
                        } elseif (is_null($cabang)) {
            
                        $data = DB::table('MANFAAT')  
                                            -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM as ID_PROGRAM', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH' ), DB::raw('SUM(JUMLAH_KASUS) as KASUS' ))
                                            ->where('MANFAAT.ID_PROGRAM',$program)
                                            ->where('MANFAAT.BULAN','<=',$bulan)
                                            ->where('MANFAAT.TAHUN',$tahun)
                                            ->where('MANFAAT.ID_KANWIL',$kanwil)
                                            ->orderBy('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN', 'asc')
                                            ->groupBy('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN')
                                             ->get();
                        }  else {
                        $data = DB::table('MANFAAT')
                                            -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('MANFAAT.ID_SEGMEN','MANFAAT.ID_PROGRAM as ID_PROGRAM','MANFAAT.ID_KANWIL', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH' ), DB::raw('SUM(JUMLAH_KASUS) as KASUS' ))
                                            ->where('MANFAAT.ID_PROGRAM',$program)
                                            ->where('MANFAAT.BULAN','<=',$bulan)
                                            ->where('MANFAAT.TAHUN',$tahun)
                                            ->where('MANFAAT.ID_KANWIL',$kanwil)
                                            ->where('MANFAAT.ID_CABANG',$cabang)
                                            ->orderBy('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN', 'asc')
                                            ->groupBy('MANFAAT.ID_PROGRAM','MANFAAT.ID_SEGMEN')
                                            ->get();
            
                   }       
                 //   dd(DB::getQueryLog());         
                   $data = json_decode(json_encode($data), True);
             //    dd ($data);
            return $data;  
                   
            } 

    static function resumeManfaatProgramSegmenTotal($program,$bulan,$tahun,$kanwil=null,$cabang=null){
            /*
            mengakses table MANFAAT_BU,SKALA_BU untuk mendapatkan jumlah badan usaha per skala usaha difilter
            berdasarkan bulan,tahun,program,kanwil,cabang
            filter berdasarkan program dan waktu.
            use link 'data/bu/program/1/3/2017''
            */
            //print 'seg'.$segmen .'bul '. $bulan .'ta '. $tahun .'kan ' . $kanwil .'cab '. $cabang;
            //dd($cabang);
         //   dd($bulan ,  $tahun);
            DB::enableQueryLog();
                        if ($program=='All'){  
            
                        $data = DB::table('MANFAAT')
                                            -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('MANFAAT.ID_SEGMEN', DB::raw('SUM(JUMLAH_PEMBAYARAN)/1000 as JUMLAH' ))
                                            ->where('MANFAAT.BULAN','<=',$bulan)
                                            ->where('MANFAAT.TAHUN',$tahun)
                                            ->orderBy('MANFAAT.ID_SEGMEN', 'asc')
                                            ->groupBy('MANFAAT.ID_SEGMEN')
                                             ->get();
                      } elseif (is_null($cabang) and is_null($kanwil)) {
            
                        $data = DB::table('MANFAAT')  
                                            -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('MANFAAT.ID_SEGMEN', DB::raw('SUM(JUMLAH_PEMBAYARAN)/1000 as JUMLAH' ))
                                            ->where('MANFAAT.ID_PROGRAM',$program)
                                            ->where('MANFAAT.BULAN','<=',$bulan)
                                            ->where('MANFAAT.TAHUN',$tahun)
                                            ->orderBy('MANFAAT.ID_SEGMEN', 'asc')
                                            ->groupBy('MANFAAT.ID_SEGMEN')
                                             ->get();
                  
                        }elseif (is_null($cabang)) {
            
                        $data = DB::table('MANFAAT')  
                                            -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('MANFAAT.ID_SEGMEN', DB::raw('SUM(JUMLAH_PEMBAYARAN)/1000 as JUMLAH' ))
                                            ->where('MANFAAT.ID_PROGRAM',$program)
                                            ->where('MANFAAT.BULAN','<=',$bulan)
                                            ->where('MANFAAT.TAHUN',$tahun)
                                            ->where('MANFAAT.ID_KANWIL',$kanwil)
                                            ->orderBy('MANFAAT.ID_SEGMEN', 'asc')
                                            ->groupBy('MANFAAT.ID_SEGMEN')
                                             ->get();
                        }  else {
                        $data = DB::table('MANFAAT')
                                            -> join('PROGRAM', 'MANFAAT.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                            ->select('MANFAAT.ID_SEGMEN', DB::raw('SUM(JUMLAH_PEMBAYARAN)/1000 as JUMLAH' ))
                                            ->where('MANFAAT.ID_PROGRAM',$program)
                                            ->where('MANFAAT.BULAN','<=',$bulan)
                                            ->where('MANFAAT.TAHUN',$tahun)
                                            ->where('MANFAAT.ID_KANWIL',$kanwil)
                                            ->where('MANFAAT.ID_CABANG',$cabang)
                                            ->orderBy('MANFAAT.ID_SEGMEN', 'asc')
                                            ->groupBy('MANFAAT.ID_SEGMEN')
                                            ->get();
            
                   }       
                   // dd(DB::getQueryLog());         
                 //  $data = json_decode(json_encode($data), True);
              //   dd ($data);
            return $data;  
                   
            }            

    static function payClaimBySegmen($segmen,$bulan,$tahun,$kanwil=null,$cabang=null){

            if ($segmen=='All'){    
            $data = DB::table('MANFAAT')
                             -> join('SEGMEN', 'MANFAAT.ID_SEGMEN', '=', 'SEGMEN.ID_SEGMEN')
                                ->select('MANFAAT.ID_SEGMEN as ID_SEGMEN','SEGMEN.SEGMEN as SEGMEN', DB::raw('SUM(JUMLAH_PEMBAYARAN) as JUMLAH' ))
                                ->orderBy('MANFAAT.ID_SEGMEN', 'asc')
                                ->groupBy('MANFAAT.ID_SEGMEN','SEGMEN.SEGMEN')
                                 ->get();
                } elseif (is_null($cabang) and is_null($kanwil)) {

            $data = DB::table('MANFAAT')       
                                -> join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('CAKUPAN.ID_SEGMEN as ID_SEGMEN','CAKUPAN.ID_PROGRAM as ID_PROGRAM','PROGRAM.PROGRAM as PROGRAM', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH' ))
                                ->where('CAKUPAN.ID_SEGMEN',$segmen)
                                ->where('CAKUPAN.BULAN',$bulan)
                                ->where('CAKUPAN.TAHUN',$tahun)
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_SEGMEN','CAKUPAN.ID_PROGRAM','PROGRAM.PROGRAM')
                                 ->get();
                              
            } elseif (is_null($kanwil)) {
             $data = DB::table('CAKUPAN')       
                                -> join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('CAKUPAN.ID_SEGMEN as ID_SEGMEN','CAKUPAN.ID_PROGRAM as ID_PROGRAM','PROGRAM.PROGRAM as PROGRAM', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH' ))
                                ->where('CAKUPAN.ID_SEGMEN',$segmen)
                                ->where('CAKUPAN.BULAN',$bulan)
                                ->where('CAKUPAN.TAHUN',$tahun)
                                ->where('CAKUPAN.ID_CABANG',$cabang)
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_SEGMEN','CAKUPAN.ID_PROGRAM','PROGRAM.PROGRAM')
                                 ->get();
    

            } elseif (is_null($cabang)) {

            $data = DB::table('CAKUPAN')       
                                 -> join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('CAKUPAN.ID_SEGMEN as ID_SEGMEN','CAKUPAN.ID_PROGRAM as ID_PROGRAM','PROGRAM.PROGRAM as PROGRAM', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH' ))
                                ->where('CAKUPAN.ID_SEGMEN',$segmen)
                                ->where('CAKUPAN.BULAN',$bulan)
                                ->where('CAKUPAN.TAHUN',$tahun)
                                ->where('CAKUPAN.ID_KANWIL',$kanwil)
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_SEGMEN','CAKUPAN.ID_PROGRAM','PROGRAM.PROGRAM')
                                 ->get();

            }  else {
            $data = DB::table('CAKUPAN')
                                -> join('PROGRAM', 'CAKUPAN.ID_PROGRAM', '=', 'PROGRAM.ID_PROGRAM')
                                ->select('CAKUPAN.ID_SEGMEN as ID_SEGMEN','CAKUPAN.ID_PROGRAM as ID_PROGRAM','PROGRAM.PROGRAM as PROGRAM', DB::raw('SUM(JUMLAH_PESERTA) as JUMLAH' ))
                                ->where('CAKUPAN.ID_SEGMEN',$segmen)
                                ->where('CAKUPAN.BULAN',$bulan)
                                ->where('CAKUPAN.TAHUN',$tahun)
                                ->where('CAKUPAN.ID_KANWIL',$kanwil)
                                ->where('CAKUPAN.ID_CABANG',$cabang)
                                ->orderBy('CAKUPAN.ID_SEGMEN', 'asc')
                                ->groupBy('CAKUPAN.ID_SEGMEN','CAKUPAN.ID_PROGRAM','PROGRAM.PROGRAM')
                                 ->get();
          }   

          return $data;



    }
}
