<?php

namespace App\Models\sismonev;

use Illuminate\Database\Eloquent\Model;
use ChartJs;
use Chart;
use Charts;
use DB;
use phpDocumentor\Reflection\Types\Null_;

class dashboard extends Model
{
    //


    static function Higchart()
    {

        $Higchart = [

            'chart' => ['type' => 'column'],
            'title' => ['text' => 'fruit Consumption'],
            'xAxis' => [
                'categories' => ['apples', 'bananas'],
            ],
            'yAxis' => [
                'title' => [
                    'text' => 'Fruit Eaten'
                ]
            ],
            'series' => [
                [
                    'name' => 'Reza',
                    'data' => [1, 2]
                ],
                [
                    'name' => 'Kika',
                    'data' => [2, 4]
                ],
            ]
        ];

        return $Higchart;


    }

    static function ChartsConsole()
    {
        $ChartsConsole = Charts::multi('bar-over', 'highcharts')
            ->title('Persentase Peserta Tenaga Kerja Per Program')
            ->elementLabel("Cakupan Peserta")
            ->legend(true)
            ->format('{point.y:.2f}' . '%')
            ->colors(['#cceeff', '#ffccff'])
            ->labels(['PPU', 'BPU', 'JASKON'])
            ->pointPadding(['0', '0.1']);

        $ChartsConsole->dataset('Target', [30, 30, 30]);
        $ChartsConsole->dataset('Aktual', [10, 20, 25]);

        return $ChartsConsole;

    }

    static function klaimrasio()
    {
        $klaimrasio = Charts::create('gauge', 'canvas-gauges')
            ->title('Rasio Klaim')
            ->elementLabel('Klaim')
            ->values([28, 0, 100])
            ->responsive(true)
            ->height(200)
            ->width(0);

        return $klaimrasio;
    }

    static function gaugemeter($program,$bulan,$tahun,$kanwil,$cabang)
    {
        DB::enableQueryLog();
  //      echo $program . 'tahun' . $tahun . 'bulan' .$bulan . 'k' .$kanwil . 'c' .$cabang . '<BR>';
        $data = DB::table('CAKUPAN')
            ->select(DB::raw('SUM(JUMLAH_PTNIK) as JKT'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
            ->where('BULAN', $bulan)
            ->where('TAHUN', $tahun);


        if ($program == 'All') {
       //     echo 'All';
            if ($kanwil) {
       //         echo 'kanwil ' . $kanwil;
                if ($cabang) {
       //             echo 'Cabang' . $cabang;
                    $data = DB::table('CAKUPAN')
                        ->select(DB::raw('SUM(JUMLAH_PTNIK) as JKT'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN', $bulan)
                        ->where('TAHUN', $tahun)
                        ->where('ID_KANWIL', $kanwil)
                        ->where('ID_CABANG', $cabang)
                        ->get();
                } else {
      //              echo 'Cabang Kosong ' . $cabang;
                    $data = DB::table('CAKUPAN')
                        ->select(DB::raw('SUM(JUMLAH_PTNIK) as JKT'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN', $bulan)
                        ->where('TAHUN', $tahun)
                        ->where('ID_KANWIL', $kanwil)
                        ->get();
                }
            } else {
          //      echo 'Kanwilnya adalah ' . $kanwil;
                $data = DB::table('CAKUPAN')
                    ->select(DB::raw('SUM(JUMLAH_PTNIK) as JKT'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                    ->where('BULAN', $bulan)
                    ->where('TAHUN', $tahun)
                    ->get();
            }
        } else {
    //        echo 'Program ' . $program;
            if ($kanwil) {
                echo 'kanwil ' . $kanwil;
                if ($cabang) {
                    $data = DB::table('CAKUPAN')
                        ->select(DB::raw('SUM(JUMLAH_PTNIK) as JKT'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN', $bulan)
                        ->where('TAHUN', $tahun)
                        ->where('ID_PROGRAM', $program)
                        ->where('ID_KANWIL', $kanwil)
                        ->where('ID_CABANG', $cabang)
                        ->get();
                } else {
    //                echo 'Cabang Kosong ' . $cabang;
                    $data = DB::table('CAKUPAN')
                        ->select(DB::raw('SUM(JUMLAH_PTNIK) as JKT'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN', $bulan)
                        ->where('TAHUN', $tahun)
                        ->where('ID_PROGRAM', $program)
                        ->where('ID_KANWIL', $kanwil)
                        ->get();
                }
            } else {
       //         echo 'Kanwilnya adalah ' . $kanwil;
                $data = DB::table('CAKUPAN')
                    ->select(DB::raw('SUM(JUMLAH_PTNIK) as JKT'), DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                    ->where('BULAN', $bulan)
                    ->where('TAHUN', $tahun)
                    ->get();
            }
        }
      //  dd(DB::getQueryLog());
      //  $data = json_decode(json_encode($data), True);
      //  dd($data);
        if ($data[0]->JKT>=1) {
            $percent = ($data[0]->JKT / $data[0]->JP) * 100;
        } else {
            $percent=0;
        }

        $gaugemeter = Charts::create('gauge', 'justgage')
            ->title('% Peserta Tanpa NIK')
            ->elementLabel('% Peserta tanpa NIK')
            ->values([$percent, 0, 100])
            ->responsive(true);
      //  dd($gaugemeter);
        return $gaugemeter;
    }

static function persenkartuterbit($program,$bulan,$tahun,$kanwil,$cabang)
    {
        if ($program == 'All') {
      //      echo 'All';
            if ($kanwil) {
       //         echo 'kanwil  ' . $kanwil;
                if ($cabang) {
        //            echo 'Cabang' . $cabang;
                    $data = DB::table('CAKUPAN')
                        ->select(DB::raw('SUM(JUMLAH_KARTU_TERBIT) as JKT'),DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN',$bulan)
                        ->where('TAHUN',$tahun)
                        ->where('ID_KANWIL',$kanwil)
                        ->where('ID_CABANG',$cabang)
                        ->get();
                } else {
      //              echo 'Cabang Kosong ' . $cabang;
                    $data = DB::table('CAKUPAN')
                        ->select(DB::raw('SUM(JUMLAH_KARTU_TERBIT) as JKT'),DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN',$bulan)
                        ->where('TAHUN',$tahun)
                        ->where('ID_KANWIL',$kanwil)
                        ->get();
                }
            } else {
     //           echo 'Kanwilnya adalah ' . $kanwil;
                $data = DB::table('CAKUPAN')
                    ->select(DB::raw('SUM(JUMLAH_KARTU_TERBIT) as JKT'),DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                    ->where('BULAN',$bulan)
                    ->where('TAHUN',$tahun)
                    ->get();
            }
        } else {
      //      echo 'Program ' . $program;
            if ($kanwil) {
                echo 'kanwil ' . $kanwil;
                if ($cabang) {
                    $data = DB::table('CAKUPAN')
                        ->select(DB::raw('SUM(JUMLAH_KARTU_TERBIT) as JKT'),DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('ID_PROGRAM',$program)
                        ->where('BULAN',$bulan)
                        ->where('TAHUN',$tahun)
                        ->where('ID_KANWIL',$kanwil)
                        ->where('ID_CABANG',$cabang)
                        ->get();

                } else {
       //             echo 'Cabang Kosong ' . $cabang;
                    $data = DB::table('CAKUPAN')
                        ->select(DB::raw('SUM(JUMLAH_KARTU_TERBIT) as JKT'),DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('ID_PROGRAM',$program)
                        ->where('BULAN',$bulan)
                        ->where('TAHUN',$tahun)
                        ->where('ID_KANWIL',$kanwil)
                        ->get();

                }
            } else {
      //          echo 'Kanwilnya adalah ' . $kanwil;
                $data = DB::table('CAKUPAN')
                    ->select(DB::raw('SUM(JUMLAH_KARTU_TERBIT) as JKT'),DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                    ->where('ID_PROGRAM',$program)
                    ->where('BULAN',$bulan)
                    ->where('TAHUN',$tahun)
                    ->get();
            }
        }


        if ($data[0]->JKT>=1) {
            $percent = ($data[0]->JKT / $data[0]->JP) * 100;
        } else {
            $percent=0;
        }
        $gaugemeter = Charts::create('percentage', 'justgage')
            ->title('% Kartu Terbit')
            ->elementLabel('Persentase kartu terbit')
            ->values([$percent, 0, 100])
            ->responsive(true)
            ->template("google");

        return $gaugemeter;
}

    static function gaugemeteraa()
    {
        $gaugemeter = Charts::create('percentage', 'justgage')
            ->title('% Tanpa NIK')
            ->elementLabel('Peserta tanpa NIK')
            ->values([0.05, 0, 100])
            ->responsive(true);


        return $gaugemeter;
    }

    static function gmtka($program,$bulan,$tahun,$kanwil,$cabang)
    {

        if ($program == 'All') {
      //      echo 'All';
            if ($kanwil) {
      //          echo 'kanwil  ' . $kanwil;
                if ($cabang) {
         //           echo 'Cabang' . $cabang;
                    $data = DB::table('CAKUPAN')
                        ->select(DB::raw('SUM(JUMLAH_TKA) as JKT'),DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN',$bulan)
                        ->where('TAHUN',$tahun)
                        ->where('ID_KANWIL',$kanwil)
                        ->where('ID_CABANG',$cabang)
                        ->get();

                } else {
        //            echo 'Cabang Kosong ' . $cabang;
                    $data = DB::table('CAKUPAN')
                        ->select(DB::raw('SUM(JUMLAH_TKA) as JKT'),DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN',$bulan)
                        ->where('TAHUN',$tahun)
                        ->where('ID_KANWIL',$kanwil)
                        ->get();

                }
            } else {
         //       echo 'Kanwilnya adalah ' . $kanwil;
                $data = DB::table('CAKUPAN')
                    ->select(DB::raw('SUM(JUMLAH_TKA) as JKT'),DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                    ->where('BULAN',$bulan)
                    ->where('TAHUN',$tahun)
                    ->get();
            }
        } else {
        //    echo 'Program ' . $program;
            if ($kanwil) {
                echo 'kanwil ' . $kanwil;
                if ($cabang) {
                    $data = DB::table('CAKUPAN')
                        ->select(DB::raw('SUM(JUMLAH_TKA) as JKT'),DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN',$bulan)
                        ->where('TAHUN',$tahun)
                        ->where('ID_KANWIL',$kanwil)
                        ->where('ID_CABANG',$cabang)
                        ->where('ID_PROGRAM',$program)
                        ->get();

                } else {
          //          echo 'Cabang Kosong ' . $cabang;
                    $data = DB::table('CAKUPAN')
                        ->select(DB::raw('SUM(JUMLAH_TKA) as JKT'),DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                        ->where('BULAN',$bulan)
                        ->where('TAHUN',$tahun)
                        ->where('ID_KANWIL',$kanwil)
                        ->where('ID_PROGRAM',$program)
                        ->get();

                }
            } else {
        //        echo 'Kanwilnya adalah ' . $kanwil;
                $data = DB::table('CAKUPAN')
                    ->select(DB::raw('SUM(JUMLAH_TKA) as JKT'),DB::raw('SUM(JUMLAH_PESERTA) as JP'))
                    ->where('BULAN',$bulan)
                    ->where('TAHUN',$tahun)
                    ->where('ID_PROGRAM',$program)
                    ->get();
            }
        }

        if ($data[0]->JKT>=1) {
            $percent = ($data[0]->JKT / $data[0]->JP) * 100;
        } else {
            $percent=0;
        }

        $gmtka = Charts::create('percentage', 'justgage')
            ->title('TKA Peserta')
            ->elementLabel('Peserta Tenaga Kerja Asing')
            ->values([$percent, 0, 100])
            ->responsive(true)
            ->template("material");

        return $gmtka;
    }

    static function BU_JumlahSkala()
    {
        $BU_JumlahSkala = Charts::multi('bar', 'highcharts')
            ->title("Peserta Badan Usaha Berdasarkan Skala Usaha")
            ->dimensions(0, 400)// Width x Height
            ->legend(true)
            //->dataLabels(true)
            ->template("material")
            ->labels(['Skala Besar', 'Skala Menengah', 'Skala Kecil', 'Skala Mikro'])
            ->dataset('JKK', [7470195, 5426002, 1355924, 384219])
            ->dataset('JKM', [7470195, 5426002, 1355924, 384219])
            ->dataset('JHT', [7405512, 5068952, 1112165, 243951])
            ->dataset('JP', [6425810, 2974253, 253164, 24474]);


        return $BU_JumlahSkala;


    }

    static function PM_segmen_perprogram()
    {
        $PM_segmen_perprogram = Charts::multi('bar', 'highcharts')
            ->title('Penerimaan Iuran Per Segmen')
            ->elementLabel('Penerimaan Iuran')
            ->labels(['PBPU', 'PPU', 'Jakon'])
            ->legend(true)
            ->values([53021271100, 20792127687555, 117290543728])
            ->dimensions(1000, 500)
            ->responsive(true)
            ->dataset('Aktual', [53021271100, 20792127687555, 117290543728])
            ->dataset('Target', [54021271100, 21792127687555, 127290543728]);
        return $PM_segmen_perprogram;
    }

    static function manfaatbyprogram()
    {
        $manfaatbyprogram = Charts::create('pie', 'highcharts')
            ->title('Pembayaran Manfaat Program JKK')
            ->labels([
                'Cacat Fungsi',
                'Cacat Sebagian',
                'Cacat Total Tetap',
                'COB',
                'Masih Pengobatan',
                'Meninggal',
                'RTW',
                'Sembuh Tanpa Cacat'])
            ->values([
                '8017040052.825',
                '4506006077.46',
                '133056755.49',
                '0',
                '22850373357.39',
                '34558901125.88',
                '0',
                '14068956493.67'
            ])
            ->legend(true)
            ->responsive(false);

        return $manfaatbyprogram;
    }


    static function manfaatbyprogramjht()
    {
        $manfaatbyprogramjht = Charts::create('bar', 'highcharts')
            ->title('Pembayaran Manfaat Program JHT')
            ->labels([
                '5 Tahun 1 Bulan',
                'Cacat Total Tetap',
                'Kepesertaan 10 tahun, pengambilan maksimal 10%',
                'Kepesertaan 10 tahun, pengambilan maksimal 30% (Perumahan)',
                'Mengundurkan Diri',
                'Meninggal dunia dalam hubungan kerja (Kecelakaan)',
                'Meninggal Dunia dalam masa perlindungan 6 bulan',
                'Meninggal dunia karena Penyakit Akibat Kerja (PAK)',
                'Meninggal dunia pada saat kepesertaan aktif',
                'Meninggal dunia pada saat kepesertaan tidak aktif',
                'Meninggalkan NKRI',
                'Perubahan status menjadi Pegawai Negeri Sipil/Anggota ABRI',
                'PHK',
                'Usia Pensiun'
            ])
            ->values([
                84051054,
                139176844,
                16444567894,
                195300973,
                1130645315956,
                1475776763,
                60124878,
                46544826,
                32662426198,
                4623076460,
                20764659720,
                12884722,
                231864669136,
                176375544848
            ])
            ->legend(true)
            ->responsive(false);

        return $manfaatbyprogramjht;
    }


    static function manfaatbyprogramjkm()
    {
        $manfaatbyprogramjkm = Charts::create('pie', 'highcharts')
            ->title('Pembayaran Manfaat Program JKM')
            ->labels([
                'Meninggal Dunia dalam masa perlindungan 6 bulan',
                'Meninggal Dunia dalam masa perlindungan 6 bulan, JHT sudah dibayarkan',
                'Meninggal Dunia pada saat kepesertaan aktif, dengan Beasiswa',
                'Meninggal Dunia pada saat kepesertaan aktif, tanpa Beasiswa'

            ])
            ->values([
                '187800000',
                '0',
                '14472000000',
                '30144000000',
            ])
            ->legend(true)
            ->responsive(false);

        return $manfaatbyprogramjkm;
    }

    static function manfaatbyprogramjp()
    {
        $manfaatbyprogramjp = Charts::create('pie', 'highcharts')
            ->title('Pembayaran Manfaat Program JKM')
            ->labels([
                'Lump Sum - Peserta cacat total tetap tidak memenuhi syarat manfaat pasti',
                'Lump Sum - Peserta mencapai usia pensiun sebelum memiliki masa iur 15 tahun',
                'Lump Sum - Peserta meninggal dunia tidak memenuhi persyaratan manfaat pasti',
                'Lump Sum - Peserta meninggalkan NKRI',
                'Pensiun Anak',
                'Pensiun Cacat',
                'Pensiun Janda/Duda',
                'Pensiun Normal (Hari Tua)',
                'Pensiun Orang Tua'
            ])
            ->values([
                '0',
                '3668410070',
                '334325790',
                '31805640',
                '76903609.5',
                '2848086.8',
                '2805367622.66',
                '0',
                '334987850'
            ])
            ->legend(true)
            ->responsive(false);

        return $manfaatbyprogramjp;
    }

    static function pertumbuhanTK()
    {

        $pertumbuhanTK = Charts::multi('spline', 'highcharts')
            ->title('Pertumbuhan Peserta 2017')
            ->colors(['#ff5733', '#ffbd33', '#33ffbd', '#dbff33'])
            ->labels(['Februari', 'Maret', 'April', 'Mei'])
            ->dataset('JKK', ['22165550', '22454299', '22473584', '22623375'])
            ->dataset('JKM', ['22165550', '22454299', '22473584', '15527854'])
            ->dataset('JHT', ['13722627', '13885315', '14005880', '13939315'])
            ->dataset('JP', ['0', '9462752', '9623536', '9677701']);

        return $pertumbuhanTK;

    }

    static function klaim($program)
    {
        $klaim = Charts::multi('line', 'highcharts')
            ->title('Klaim per Jenis Klaim Program ' . $program)
            ->colors(['#1E90FF', '#00BFFF', '#6495ED', '#87CEEB', '#87CEFA', '#B0E0E6', '#ADD8E6', '#B0C4DE'])
            ->labels(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])
            ->dimensions(500, 600)//         
            ->responsive(false);

        if ($program == 'JKK') {
            $klaim->dataset('Cacat Fungsi', ['0', '08410070837', '7499750665', '5684161754', '8017040052']);
            $klaim->dataset('Cacat Sebagian', ['0', '78484699.83', '6165639480.07', '3933291413.3', '4506006077.46']);
            $klaim->dataset('Cacat Total Tetap', ['0', '4630275472.22', '183072324', '59436486.4', '133056755.49']);
            $klaim->dataset('COB dengan Asuransi Lain', ['0', '32936584686.01', '36481115180.47', '22050701883.5', '34558901125.88']);
            $klaim->dataset('Masih Pengobatan', ['0', '14886992685.55', '16287487575.69', '12411102369.5', '14068956493.67']);
            $klaim->dataset('Meninggal', ['0', '0', '0', '0', '0',]);
            $klaim->dataset('RTW', ['0', '0', '0', '0', '0',]);
            $klaim->dataset('Sembuh Tanpa Cacat', ['0', '18419551056.9', '23604943818.16', '20044496232.07', '22850373357.39']);
        } elseif ($program == 'JKM') {
            $klaim->dataset('Meninggal Dunia dalam masa perlindungan 6 bulan', ['0', '19980000000', '15576000000', '13077000000', '14472000000']);
            $klaim->dataset('Meninggal Dunia dalam masa perlindungan 6 bulan, JHT sudah dibayarkan', ['0', '26314451600', '33696000000', '25488000000', '30144000000']);
            $klaim->dataset('Meninggal Dunia pada saat kepesertaan aktif, dengan Beasiswa', ['0', '335100000', '294600000', '369600000', '187800000']);
            $klaim->dataset('Meninggal Dunia pada saat kepesertaan aktif, tanpa Beasiswa', ['0', '21000000', '21000000', '0', '0']);

        } elseif ($program == 'JHT') {
            $klaim->dataset('5 Tahun 1 Bulan', ['0', '1071446687017.6', '1243379994152.29', '961977671192.22', '1130645315956.58']);
            $klaim->dataset('Cacat Total Tetap', ['0', '237037269041.05', '245292843550.96', '182718819883.75', '231864669136.03']);
            $klaim->dataset('Kepesertaan 10 tahun, pengambilan maksimal 10%', ['0', '14477157284.7', '18548139500.3', '16260214082.95', '16444567894.31']);
            $klaim->dataset('Kepesertaan 10 tahun, pengambilan maksimal 30% (Perumahan)', ['0', '82581569.78', '140456346.52', '240342982.27', '195300973.31']);
            $klaim->dataset('Mengundurkan Diri', ['0', '110759179.86', '147908875.67', '34042110.06', '84051054.66']);
            $klaim->dataset('Meninggal dunia dalam hubungan kerja (Kecelakaan)', ['0', '193140514840.2', '202752087063.98', '171825017909.37', '176375544848.5']);
            $klaim->dataset('Meninggal Dunia dalam masa perlindungan 6 bulan', ['0', '17078565.92', '5732421.21', '145100566.99', '139176844.08']);
            $klaim->dataset('Meninggal dunia karena Penyakit Akibat Kerja (PAK)', ['0', '18731991754.71', '20775961325.47', '14246527848.02', '20764659720.49']);
            $klaim->dataset('Meninggal dunia pada saat kepesertaan aktif', ['0', '2028070003.38', '2289727887.54', '1252462834.24', '1475776763.18']);
            $klaim->dataset('Meninggal dunia pada saat kepesertaan tidak aktif', ['0', '260625700.98', '90478033.86', '70327050.09', '60124878.23']);
            $klaim->dataset('Meninggalkan NKRI', ['0', '16442466.03', '1757369.79', '0', '46544826.53']);
            $klaim->dataset('Perubahan status menjadi Pegawai Negeri Sipil/Anggota ABRI', ['0', '30048981262.94', '34112940664.97', '27418474533.7', '32662426197.72']);
            $klaim->dataset('PHK', ['0', '3784879735.68', '9804343221.72', '4179640377.15', '4623076460.2']);
            $klaim->dataset('Usia Pensiun', ['0', '12839284.82', '32935034.16', '12361522.98', '12884722.16']);

        } else {
            $klaim->dataset('Lump Sum - Peserta cacat total tetap tidak memenuhi syarat manfaat pasti', ['0', '0', '0', '0', '0']);
            $klaim->dataset('Lump Sum - Peserta mencapai usia pensiun sebelum memiliki masa iur 15 tahun', ['0', '3313780.2', '4272130.2', '2528636.8', '2848086.8']);
            $klaim->dataset('Lump Sum - Peserta meninggal dunia tidak memenuhi persyaratan manfaat pasti', ['0', '3593200865.56', '6064709338.12', '1864119180.4', '2805367622.66']);
            $klaim->dataset('Lump Sum - Peserta meninggalkan NKRI', ['0', '59450624.31', '146882583.79', '86270330.75', '76903609.5']);
            $klaim->dataset('Pensiun Anak', ['0', '437481300', '869588950', '250117700', '334987850']);
            $klaim->dataset('Pensiun Cacat', ['0', '2270330', '0', '0', '0']);
            $klaim->dataset('Pensiun Janda/Duda', ['0', '2714836490', '3096069930', '3035357260', '3668410070']);
            $klaim->dataset('Pensiun Normal (Hari Tua)', ['0', '301416920', '400352140', '298452670', '334325790']);
            $klaim->dataset('Pensiun Orang Tua', ['0', '24746820', '56715870', '19756360', '31805640']);
        }
        return $klaim;
    }

}
