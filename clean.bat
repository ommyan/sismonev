php artisan cache:clear
php artisan view:clear
php artisan optimize --force
php artisan config:cache
php artisan route:cache
composer dump-autoload