 <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">

     {!!  Form::open(['route' => 'filter.data']) !!}
      <input type=hidden name=alamat value="{{Route::current()->getName()}}">
      <div class="form-group">
          <div class="input-group">
              <div class="input-group-addon input-sm">
                  <span class="glyphicon glyphicon-search"></span>
              </div>

              {{--<input id="datepicker" class="form-control input-sm" type="text" placeholder="Bulan">--}}
              {!! Form::text('bulan', Null, ['id'=>'bulan','class'=>'input-sm','placeholder'=>'Bulan']) !!}
          </div>
      </div>

      <div class="form-group">
          <div class="input-group">
              <div class="input-group-addon input-sm">
                  <span class="glyphicon glyphicon-search"></span>
              </div>
              {!!     Form::select('kanwil',$kanwul, '0',['id'=>'kanwil','class'=>'input-sm','placeholder'=>'Kanwil']) !!}
          </div>
      </div>
      <div class="form-group">
          <div class="input-group">
              <div class="input-group-addon input-sm">
                  <span class="glyphicon glyphicon-search"></span>
              </div>
           {!!  Form::select('cabang',['Cabang'],null,['id'=>'cabang','class'=>'input-sm','placeholder'=>'Cabang']) !!}
          </div>
      </div>

      <div class="form-group">
          <div class="input-group">
              <div class="input-group-addon input-sm">
                  {{--<button onclick="generateurl()">Tampilkan</button>--}}
                  {!! Form::submit('Tampilkan') !!}
              </div>

          </div>
      </div>

      <input type="hidden" id="namakanwil" name="namakanwil" >
      <input type="hidden" id="namacabang" name="namacabang" >

      {!! Form::close() !!}
  </aside>


<script type="application/javascript">
    $(document).ready(function() {
        var options={
            autoclose: true,
            format: 'mm/yyyy',
            startView: "months",
            minViewMode: "months"
        };
      $("#bulan").datepicker(options);
  })

    $('#kanwil').on('change',function(e){
        var id_kanwil = e.target.value;

        var nk = $.trim($("#kanwil :selected").text());
        $("#namakanwil").val(nk);

        $('#cabang').empty();
      //  $('#cabang').attr('disabled',false);
        $.get('/ajax-cabang?id_kanwil='+id_kanwil,function(data){
            $.each(data, function(index,subcatObj){
                console.log(subcatObj.ID_CABANG);
                console.log(id_kanwil);
                console.log(nk);
                $('#cabang').append('<option value="' + subcatObj.ID_CABANG+ '">' +subcatObj.CABANG+ '</option>');
            })

        });

    });

    $('#cabang').on('change',function(e){
        var e =document.getElementById("cabang").value;
        var id_cabang =$('#cabang').val();
        console.log(id_cabang);
        var nc = $.trim($("#cabang :selected").text());
        $('#namacabang').val(nc);
         console.log(nc);
    })

    function generateurl() {
        var id_bulan = document.getElementById("bulan").value;
        var id_kanwil=document.getElementById("kanwil").value;



        var arr = document.location.href;
        var parser = document.createElement('a');
        parser.href = arr;
        var host=parser.host;
        var pro=parser.protocol;
        var pat=parser.pathname
        link=pat.substring(1,5);

        switch(link) {
            case 'home':
                var url=pro+'//'+host+'/home/All/'+id_bulan+'/' + id_kanwil+'/'+id_kacab;
                break;
            case 'pese':
                var url=pro+'//'+host+'/peserta/All/'+id_bulan+'/' + id_kanwil+'/'+id_kacab;
                break;
            case 'pend':
                var url=pro+'//'+host+'/pendapatan/All/'+id_bulan+'/' + id_kanwil+'/'+id_kacab;
                break;
            case 'pemb':
                var url=pro+'//'+host+'/pembayaran/All/'+id_bulan+'/' + id_kanwil+'/'+id_kacab;
                break;
            default:
                var url=pro+'//'+host+'/home/'+id_bulan+'/' + id_kanwil+'/'+id_kacab;
        }


        window.location = url;
    }
</script>

