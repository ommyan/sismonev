
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('/bower_components/AdminLTE/dist/img/user-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Administrator</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">PROGRAM</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="/jkk/1/{{$bln}}"><i class="icon-jkk fa-2x" style=" vertical-align: middle;"></i> <span>Jaminan Kecelakaan Kerja</span></a></li>
        <li class="active"><a href="/jkm/2/{{$bln}}"><i class="icon-jkm fa-2x" style=" vertical-align: middle;"></i> <span>Jaminan Kematian</span></a></li>
        <li class="active"><a href="/jht/3/{{$bln}}"><i class="icon-jht fa-2x" style=" vertical-align: middle;"></i> <span>Jaminan Hari Tua</span></a></li>
        <li class="active"><a href="/jp/4/{{$bln}}"><i class="icon-jp fa-2x" style=" vertical-align: middle;"></i> <span>Jaminan Pensiun</span></a></li>
      </ul>

      <ul class="sidebar-menu">
        <li class="header">STATISTIK PENGUNJUNG</li>
        <li class="active"><a href=""><span>  Pengunjung Hari ini :</span></a></li>
        <li class="active"><a href=""><span>  Pengunjung Bulan ini :</span></a></li>
        <li class="active"><a href=""><span>  Total Pengunjung</span></a></li>    
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

