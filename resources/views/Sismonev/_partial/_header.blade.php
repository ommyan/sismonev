 <header class="main-header">

    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">S<b>i</b>S</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">Monev<b>Jamsos</b>TK</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar left  Menu -->
         <ul class="nav navbar-nav">
                <li><a href="/home">Home</a></li>
                <li><a href="/peserta/All/{{$bln}}"><i class="fa fa-cubes"></i>Kepesertaan</a></li>
                <li><a href="/pendapatan/All/{{$bln}}"><i class="fa fa-cubes"></i>Iuran</a></li>
                <li><a href="/pembayaran/All/{{$bln}}"><i class="fa fa-cubes"></i>Manfaat</a></li>
                {{-- <li><a href="/keuangan/All/{{$bln}}"><i class="fa fa-cubes"></i>Keuangan & Investasi</a></li> --}}
                <li><a href="/organisasi/All/{{$bln}}"><i class="fa fa-cubes"></i>Organisasi</a></li>
          </ul>
      <!-- Navbar left  Menu -->
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        
          <li>

            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"> <span>: Filter Data  </span> </i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>