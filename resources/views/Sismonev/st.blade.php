@extends('start');
@section('page_title', 'Dashboard')
@section('content_header')    
@stop

@section('content')
   <section class="content-header">
      <h1>
      Ringkasan Indikator Monev
      </h1>
    
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          <!-- The time line -->
          <ul class="timeline">
            <!-- timeline time label -->
           @include('sismonev.aspek.resume.kepesertaan')
             @include('sismonev.aspek.resume.pendapatan')
               @include('sismonev.aspek.resume.pembayaran')
                 @include('sismonev.aspek.resume.organisasi')
                   @include('sismonev.aspek.resume.keuangan')

          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>



   
@stop