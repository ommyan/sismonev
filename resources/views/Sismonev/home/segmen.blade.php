 <script>
    $(function () {
        $('.chart').easyPieChart({
            //your options goes here
            lineWidth: 10,
            barColor: function (percent) {
                return (percent < 35 ? '#f45042' : percent < 70 ? '#f0ad4e' : '#35d32a');
            },

            onStep: function (from, to, percent) {
                $(this.el).find('.percent').text(Math.round(percent));
            }

        });
    });
</script>
<div class="row">
 <div class="col-md-6">
                        <div class="box">
                            
                        <div class="box-body">
                                <div class="row">
                            <div class="col-md-4 col-xs-6">
                                <div class="description-block border-right">
                         <span class="chart" data-percent="25"
                         data-scale-color="#ffb400">
						<span class="percent">{{ 25 }}</span>
					     </span>

                                    <h5 class="description-header">25.222.234 </h5>
                                    <span class="description-text">Penerima Upah</span>
                                </div>
                                <!-- /.description-block -->
                            </div>

                            <div class="col-sm-4 col-xs-6">
                                <div class="description-block border-right">
                     <span <i class="icon-bpu fa-2x "></i></span>
                                    <h5 class="description-header">35000 </h5>
                                    <span class="description-text">Bukan Penerima Upah</span>
                                </div>
                                <!-- /.description-block -->
                            </div>

                            <div class="col-sm-4 col-xs-6">
                                <div class="description-block border-right">
                   <span <i class="icon-jakon fa-2x "></i></span>
                                    <h5 class="description-header">35555</h5>
                                    <span class="description-text">Jasa Konstruksi</span>
                                </div>
                                <!-- /.description-block -->
                            </div>

                        </div>

                        </div>
                    </div>
 </div>

 <div class="col-md-6">
                        <div class="box">
                            
                        <div class="box-body">
                                <div class="row">
                            <div class="col-md-4 col-xs-6">
                                <div class="description-block border-right">
                            
                                    <h5 class="description-header">252222 </h5>
                                    <span class="description-text">Penerima Upah</span>
                                </div>
                                <!-- /.description-block -->
                            </div>

                            <div class="col-sm-4 col-xs-6">
                                <div class="description-block border-right">
                    
                                    <h5 class="description-header">35000 </h5>
                                    <span class="description-text">Bukan Penerima Upah</span>
                                </div>
                                <!-- /.description-block -->
                            </div>

                            <div class="col-sm-4 col-xs-6">
                                <div class="description-block border-right">
                  
                                    <h5 class="description-header">35555</h5>
                                    <span class="description-text">Jasa Konstruksi</span>
                                </div>
                                <!-- /.description-block -->
                            </div>

                        </div>

                        </div>
                    </div>
 </div>
</div>
<div class="row">

 <div class="col-md-6">
                        <div class="box">
                            
                        <div class="box-body">
                                <div class="row">
                            <div class="col-md-4 col-xs-6">
                                <div class="description-block border-right">
                            
                                    <h5 class="description-header">252222 </h5>
                                    <span class="description-text">Penerima Upah</span>
                                </div>
                                <!-- /.description-block -->
                            </div>

                            <div class="col-sm-4 col-xs-6">
                                <div class="description-block border-right">
                    
                                    <h5 class="description-header">35000 </h5>
                                    <span class="description-text">Bukan Penerima Upah</span>
                                </div>
                                <!-- /.description-block -->
                            </div>

                            <div class="col-sm-4 col-xs-6">
                                <div class="description-block border-right">
                  
                                    <h5 class="description-header">35555</h5>
                                    <span class="description-text">Jasa Konstruksi</span>
                                </div>
                                <!-- /.description-block -->
                            </div>

                        </div>

                        </div>
                    </div>
 </div>

 <div class="col-md-6">
                        <div class="box">
                            
                        <div class="box-body">
                                <div class="row">
                            <div class="col-md-4 col-xs-6">
                                <div class="description-block border-right">
                                    
                                    <h5 class="description-header">252222 </h5>
                                    <span class="description-text">Penerima Upah</span>
                                </div>
                                <!-- /.description-block -->
                            </div>

                            <div class="col-sm-4 col-xs-6">
                                <div class="description-block border-right">
                    
                                    <h5 class="description-header">35000 </h5>
                                    <span class="description-text">Bukan Penerima Upah</span>
                                </div>
                                <!-- /.description-block -->
                            </div>

                            <div class="col-sm-4 col-xs-6">
                                <div class="description-block border-right">
                  
                                    <h5 class="description-header">35555</h5>
                                    <span class="description-text">Jasa Konstruksi</span>
                                </div>
                                <!-- /.description-block -->
                            </div>

                        </div>

                        </div>
                    </div>
 </div>