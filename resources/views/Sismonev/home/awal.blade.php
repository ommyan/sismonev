
                    <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">
                                    <small>Cakupan Peserta</small>
                                    | Kepesertaan
                                </h3>
                                <!-- /.box-header -->
                                <div class="box-body">
                                     @include('Sismonev.home.program.kepesertaan')
                                </div>
                            </div>
                            </div>
                            <br>
                          <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">
                                    <small>Penerimaan Iuran dan Pembayaran Manfaat</small>
                                    | Iuran & Manfaat
                                </h3>
                                <!-- /.box-header -->
                                <div class="box-body">
                                     @include('Sismonev.home.program.iuran')
                                </div>

                            </div>
                            </div>
<br>
                            <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">
                                    <small>Organisasi</small>
                                    | Organisasi
                                </h3>
                                <!-- /.box-header -->
                                <div class="box-body">
                                     @include('Sismonev.aspek.organisasi.detail1')
                                </div>
                                
                            </div>
                            </div>



