@foreach($iuran as $iur)

      @if($iur->ID_SEGMEN==2)
      @php $bpu=$iur->JUMLAH @endphp
      @elseif ($iur->ID_SEGMEN==1)
      @php $ppu=$iur->JUMLAH @endphp
      @else
       @php $jakon=$iur->JUMLAH @endphp
      @endif
@endforeach
@foreach($iuranprogram as $iurp)

      @if($iurp->ID_PROGRAM==1)
            @php $jkk=$iurp->JUMLAH @endphp
      @elseif ($iurp->ID_PROGRAM==2)
            @php $jkm=$iurp->JUMLAH @endphp
      @elseif ($iurp->ID_PROGRAM==3)
            @php $jht=$iurp->JUMLAH @endphp
      @elseif ($iurp->ID_PROGRAM==4)
            @php $jp=$iurp->JUMLAH @endphp
      @endif
@endforeach
@php $jp=0 @endphp

 <div class="row">
      <br>
      <br>
         
                        <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="info-box bg-red">
                        <span class="info-box-icon"><i class="icon-bpu"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">Bukan Penerima Upah</span>
                      
                        <span class="info-box-number"> {{ number_format($bpu) }} </span>
                      
                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>

                <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="info-box bg-aqua">
                        <span class="info-box-icon"><i class="icon-ppu"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">Penerima Upah</span>
                       
                        <span class="info-box-number"> {{ number_format($ppu) }}</span>
                        

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                       
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>

                  
                  <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="info-box bg-yellow">
                        <span class="info-box-icon"><i class="icon-jakon"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">Jasa Konstruksi</span>
                       
                        <span class="info-box-number">0 </span>
                       
                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                


                 </div>

                    <div class="row">
               
           
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Orange">
                        <span class="info-box-icon"><i class="icon-jkk"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JHT</span>
                        <span class="info-box-number"><small>{{ number_format($jht) }}</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                      
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                 </div>