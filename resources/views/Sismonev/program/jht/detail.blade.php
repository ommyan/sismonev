<div class="row">

    <div class="col-md-12">
        <!-- Custom Tabs (Pulled to the right) -->
        <div class="nav-pills">
            <ul class="nav nav-pills pull-center">

                <li class="active"><a href="#tab_5" data-toggle="tab">Kepesertaan</a></li>
                <li><a href="#tab_4" data-toggle="tab">Iuran</a></li>
                <li><a href="#tab_3" data-toggle="tab">Manfaat</a></li>
                <li><a href="#tab_2" data-toggle="tab">Keuangan</a></li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane" id="tab_5">
                    @include('Sismonev.program.jht.detail.kepesertaan')
                </div>
                <div class="tab-pane" id="tab_4">
                    @include('Sismonev.program.jht.detail.iuran')
                </div>
                <div class="tab-pane" id="tab_3">
                    @include('Sismonev.program.jht.detail.pembayaran')
                </div><!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                    @include('Sismonev.program.jht.detail.keuangan')
                </div><!-- /.tab-pane -->

            </div>  <!-- /.tab-content -->
        </div>  <!-- nav-tabs-custom -->
    </div>

</div>