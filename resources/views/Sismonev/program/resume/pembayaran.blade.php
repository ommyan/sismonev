@php $bpu=0 @endphp
@php $ppu=0 @endphp
@php $jakon=0 @endphp

@php $jkk=0 @endphp
@php $jkm=0 @endphp
@php $jht=0 @endphp
@php $jp=0 @endphp

 @if(count($manfaat)==3)
     @php $ppu=$manfaat[0]['JUMLAH']; @endphp
     @php   $bpu=$manfaat[1]['JUMLAH']; @endphp
     @php   $jakon=$manfaat[2]['JUMLAH']; @endphp
 @elseif(count($manfaat)==2)
     @php $ppu=$manfaat[0]['JUMLAH']; @endphp
     @php   $bpu=$manfaat[1]['JUMLAH']; @endphp
     @php   $jakon=0; @endphp
 @elseif(count($manfaat)==1)
     @php $ppu=$manfaat[0]['JUMLAH']; @endphp
     @php   $bpu=0; @endphp
     @php   $jakon=0; @endphp
 @endif

 @php $totalmanfaat=$ppu+$bpu+$jakon @endphp


<div class="row">
    <div class="col-md-6">

        <div class="small-box bg-green">
            <div class="inner">

                <h3>{{ number_format($totalmanfaat) }} <sup style="font-size: 20px"></sup></h3>

                <p>Total Penerimaan Iuran Program {{$program}}</p>
            </div>
            <div class="icon">
                <i class="ion ion-people"></i>
            </div>
            <a href="#" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-md-6">

        <div class="small-box bg-green">
            <div class="inner">
                @foreach( $buprogram as $row)
                    <h3>{{ number_format(0) }}<sup style="font-size: 20px"></sup></h3>
                @endforeach
                <p>Total Badan Usaha Terdaftar program {{$program}}</p>
            </div>
            <div class="icon">
                <i class="ion ion-people"></i>
            </div>
            <a href="#" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>


</div>
 <div class="row">
      <br>
         
                        <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="info-box bg-red">
                        <span class="info-box-icon"><i class="icon-bpu"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">Penerima Upah</span>
                      
                        <span class="info-box-number"> {{ number_format($ppu) }} </span>
                      
                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>

                <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="info-box bg-aqua">
                        <span class="info-box-icon"><i class="icon-ppu"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">Bukan  Penerima Upah</span>
                       
                        <span class="info-box-number"> {{ number_format($bpu) }}</span>
                        

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                       
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>

                  
                  <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="info-box bg-yellow">
                        <span class="info-box-icon"><i class="icon-jakon"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">Jasa Konstruksi</span>
                       
                        <span class="info-box-number">{{ number_format($jakon) }} </span>
                       
                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                


                 </div>

