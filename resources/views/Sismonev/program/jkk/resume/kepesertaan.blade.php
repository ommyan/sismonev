
<div class="row">
    <br>
</div>
<div class="row">
    <div class="col-md-6">

        <div class="small-box bg-green">
            <div class="inner">

                <h3>{{ number_format($resumeCakupan[0]['JUMLAH'] + $resumeCakupan[1]['JUMLAH'] + $resumeCakupan[2]['JUMLAH']) }} <sup style="font-size: 20px"></sup></h3>

                <p>Total Peserta Program {{$program}}</p>
            </div>
            <div class="icon">
                <i class="ion ion-people"></i>
            </div>
            <a href="#" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-md-6">

        <div class="small-box bg-green">
            <div class="inner">
                @foreach( $buprogram as $row)
                <h3>{{ number_format($row->JUMLAHBU) }}<sup style="font-size: 20px"></sup></h3>
                @endforeach
                <p>Total Badan Usaha Terdaftar program {{$program}}</p>
            </div>
            <div class="icon">
                <i class="ion ion-people"></i>
            </div>
            <a href="#" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>


</div>
<div class="row">


    <div class="col-md-4 col-sm-4 col-xs-4">
        <div class="info-box bg-red">
            <span class="info-box-icon"><i class="icon-bpu"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Bukan Penerima Upah</span>
                <span class="info-box-number">{{ number_format($resumeCakupan[1]['JUMLAH'])}}</span>

                <div class="progress">
                    <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">

                                                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>

    <div class="col-md-4 col-sm-4 col-xs-4">
        <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="icon-ppu"></i></span>


            <div class="info-box-content">
                <span class="info-box-text">Penerima Upah</span>
                <span class="info-box-number">{{ number_format($resumeCakupan[0]['JUMLAH'])}}</span>

                <div class="progress">
                    <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">

                                          </span>
            </div> <!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div>


    <div class="col-md-4 col-sm-4 col-xs-4">
        <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="icon-jakon"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Jasa Konstruksi</span>
                <span class="info-box-number">{{ number_format($resumeCakupan[2]['JUMLAH'])}}</span>

                <div class="progress">
                    <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">

                                    </span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div><!-- col -->
</div><!-- row -->
<div class="row">

</div>  <!--row-->