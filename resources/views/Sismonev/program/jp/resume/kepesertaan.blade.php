
<div class="row">
    <br>
</div>
<div class="row">
    <div class="col-md-4">

        <div class="small-box bg-green">
            <div class="inner">

                <h3>{{ number_format($resumeCakupan[0]['JUMLAH'])  }} <sup style="font-size: 20px"></sup></h3>

                <p>Total pekerja terdaftar, mengacu kepada peserta program JKK</p>
            </div>
            <div class="icon">
                <i class="ion ion-people"></i>
            </div>
            <a href="#" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-md-4">

        <div class="small-box bg-green">
            <div class="inner">
                <h3>395.684 <sup style="font-size: 20px"></sup></h3>

                <p>Total Badan Usaha Terdaftar program Jaminan Sosial ketenagakerjaan</p>
            </div>
            <div class="icon">
                <i class="ion ion-people"></i>
            </div>
            <a href="#" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-md-4">

        <div class="small-box bg-red">
            <div class="inner">
                <h3> 15.5% <sup style="font-size: 20px"></sup></h3>

                <p>Pekerja terdaftar dibandingkan dengan penduduk bekerja</p>
            </div>
            <div class="icon">
                <i class="ion ion-stats-bars"></i>

            </div>
            <a href="#" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>


</div>
<div class="row">


    <div class="col-md-4 col-sm-4 col-xs-4">
        <div class="info-box bg-red">
            <span class="info-box-icon"><i class="icon-bpu"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Bukan Penerima Upah</span>
                <span class="info-box-number">0</span>

                <div class="progress">
                    <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">

                                                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>

    <div class="col-md-4 col-sm-4 col-xs-4">
        <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="icon-ppu"></i></span>


            <div class="info-box-content">
                <span class="info-box-text">Penerima Upah</span>
                <span class="info-box-number">{{ number_format($resumeCakupan[0]['JUMLAH'])}}</span>

                <div class="progress">
                    <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">

                                          </span>
            </div> <!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div>


    <div class="col-md-4 col-sm-4 col-xs-4">
        <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="icon-jakon"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Jasa Konstruksi</span>
                <span class="info-box-number">0</span>

                <div class="progress">
                    <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">

                                    </span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div><!-- col -->
</div><!-- row -->
<div class="row">


    <div class="col-md-3 col-sm-3 col-xs-3">
        <div class="info-box bg-Orange">
            <span class="info-box-icon"> <i class="icon-jkk"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">JP</span>
                <span class="info-box-number"><small>{{ number_format($resumeCakupan[0]['JUMLAH'] )  }}</small></span>

                <div class="progress">
                    <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">

                        </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>



</div>  <!--row-->