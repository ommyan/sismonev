
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-right">
       <li class="active"><a href="#tab_121" data-toggle="tab">Seluruh Program</a></li>
    <li class="pull-left header"><i class="fa fa-th"></i> Resume </li>
    </ul>

<div class="tab-content">
    <div class="tab-pane active" id="tab_121">
    <div  id="" style="width: 300 margin: 0 auto"></div>
    @include('sismonev.aspek.pendapatan.resume')
    </div>
</div>
</div>

<div class="row">
    <div class="col-md-6">

                        <div class="nav-tabs-custom">
                              <ul class="nav nav-tabs pull-right">
                                  <li><a href="#tab_411" data-toggle="tab">JP</a></li>
                                  <li><a href="#tab_311" data-toggle="tab">JHT</a></li>
                                  <li><a href="#tab_211" data-toggle="tab">JKM</a></li>
                                  <li class="active"><a href="#tab_111" data-toggle="tab">JKK</a></li>





                              <li class="pull-left header"><i class="fa fa-th"></i> Penerimaan Iuran </li>
                              </ul>
                              <div class="tab-content">
                                    <div class="tab-pane active" id="tab_111">
                                    <div  id="" style="width: 300 margin: 0 auto"></div>
                                        {!! $chartiuranjkk->render() !!}
                                    </div>
                                    <!-- /.tab-pane -->
                                    <div class="tab-pane " id="tab_211">
                                    <div  id="" style="width: 300 margin: 0 auto"></div>
                                    {!! $chartiuranjkm->render() !!}
                                    </div>
                                    <!-- /.tab-pane -->
                                    <div class="tab-pane" id="tab_311">
                                    <div  id="" style="width: 300 margin: 0 auto"></div>
                                    {!! $chartiuranjht->render() !!}
                                    </div>
                                    
                                    <div class="tab-pane" id="tab_411">
                                    <div  id="" style="width: 300 margin: 0 auto"></div>
                                    {!! $chartiuranjp->render() !!}
                                    </div><!-- /.tab-pane -->
                              </div> <!-- /.tab-content -->
                       </div><!-- nav-tabs-custom -->
</div>     

 <div class="col-md-6">
        <!-- Custom Tabs (Pulled to the right) -->
                 <div class="nav-tabs-custom">
                              <ul class="nav nav-tabs pull-right">
                                  <li><a href="#tab_4A" data-toggle="tab">JP</a></li>
                                  <li><a href="#tab_3A" data-toggle="tab">JHT</a></li>
                                  <li><a href="#tab_2A" data-toggle="tab">JKM</a></li>
                              <li class="active"><a href="#tab_1A" data-toggle="tab">JKK</a></li>

                              <li class="pull-left header"><i class="fa fa-th"></i> Penerimaan Iuran </li>
                              </ul>

                         <div class="tab-content">
                              <div class="tab-pane active" id="tab_1A">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                               {!! $chartTumbuhjkk->render() !!}
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane " id="tab_2A">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                              {!! $chartTumbuhjkm->render() !!}
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="tab_3A">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                              {!! $chartTumbuhjht->render() !!}
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="tab_4A">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                              {!! $chartTumbuhjp->render() !!}
                              </div>       
                              <!-- /.tab-pane -->
                         </div>
                              <!-- /.tab-content -->
                   </div>
                              <!-- nav-tabs-custom -->
</div>                                                          
</div>    

<div class="row">
<div class="col-md-6">
        <!-- Custom Tabs (Pulled to the right) -->
                 <div class="nav-tabs-custom">
                              <ul class="nav nav-tabs pull-right">
                                  <li><a href="#tab_41" data-toggle="tab">JP</a></li>
                                  <li><a href="#tab_31" data-toggle="tab">JHT</a></li>
                                  <li><a href="#tab_21" data-toggle="tab">JKM</a></li>
                                  <li class="active"><a href="#tab_11" data-toggle="tab">JKK</a></li>



                              <li class="pull-left header"><i class="fa fa-th"></i> Piutang Iuran </li>
                              </ul>

                         <div class="tab-content">
                              <div class="tab-pane active" id="tab_11">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                              
                              {!! $piutangAgjkk->render() !!}
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane " id="tab_21">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                              {!! $piutangAgjkm->render() !!}
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="tab_31">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                              {!! $piutangAgjht->render() !!}
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="tab_41">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                              {!! $piutangAgjp->render() !!}
                              </div>       
                              <!-- /.tab-pane -->
                         </div>
                              <!-- /.tab-content -->
                   </div>
                              <!-- nav-tabs-custom -->
</div>   

{{-- <div class="col-md-6">
        <!-- Custom Tabs (Pulled to the right) -->
                 <div class="nav-tabs-custom">
                              <ul class="nav nav-tabs pull-right">
                                  <li><a href="#tab_42" data-toggle="tab">JP</a></li>
                                  <li><a href="#tab_32" data-toggle="tab">JHT</a></li>
                                  <li><a href="#tab_22" data-toggle="tab">JKM</a></li>
                                  <li class="active"><a href="#tab_12" data-toggle="tab">JKK</a></li>



                              <li class="pull-left header"><i class="fa fa-th"></i> Penerimaan Iuran </li>
                              </ul>

                         <div class="tab-content">
                              <div class="tab-pane active" id="tab_12">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                             
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane " id="tab_22">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                             jkm
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="tab_32">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                             jht
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="tab_42">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                            jp
                              </div>       
                              <!-- /.tab-pane -->
                         </div>
                              <!-- /.tab-content -->
                   </div>
                              <!-- nav-tabs-custom -->
</div>    --}}




</div>


