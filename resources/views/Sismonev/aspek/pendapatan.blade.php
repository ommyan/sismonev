@extends('start');
@section('page_title', 'Dashboard')
@section('content_header')    
@stop

@section('content')
{!! Charts::assets() !!}
   <section class="content-header">
      <h1>
       Iuran
        <small>Detail Indikator Penerimaan Iuran</small>
      </h1>
          <h5> {{$sub_page_title}} </h5>
       <h5> {{$nk . ': ' . $nc}} </h5>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Pendapatan</a></li>
        <li class="active">Detail</li>
      </ol>
    </section>
    <section class="content">
    @include('sismonev.aspek.pendapatan.detail')
           
    </section>



   
@stop