<div class="row">
    @include('sismonev.aspek.pembayaran.awal')
</div> 


<div class="row">
    <div class="col-md-6">

                        <div class="nav-tabs-custom">
                              <ul class="nav nav-tabs pull-right">
                                  <li><a href="#tab_4111" data-toggle="tab">JP</a></li>
                                  <li><a href="#tab_3111" data-toggle="tab">JHT</a></li>
                                  <li><a href="#tab_2111" data-toggle="tab">JKM</a></li>
                                  <li class="active"><a href="#tab_1111" data-toggle="tab">JKK</a></li>

                              <li class="pull-left header"><i class="fa fa-th"></i> Pembayaran Manfaat per Program </li>
                              </ul>
                              <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1111">
                                    <div  id="" style="width: 300 margin: 0 auto"></div>
                                    {!! $ManfaatBySegmen1->render() !!} 
                                    </div>
                                    <!-- /.tab-pane -->
                                    <div class="tab-pane " id="tab_2111">
                                    <div  id="" style="width: 300 margin: 0 auto"></div>
                                    {!! $ManfaatBySegmen2->render() !!} 
                                    </div>
                                    <!-- /.tab-pane -->
                                    <div class="tab-pane" id="tab_3111">
                                    <div  id="" style="width: 300 margin: 0 auto"></div>
                                    {!! $ManfaatBySegmen3->render() !!} 
                                    </div>
                                    
                                    <div class="tab-pane" id="tab_4111">
                                    <div  id="" style="width: 300 margin: 0 auto"></div>
                                    {!! $ManfaatBySegmen4->render() !!} 
                                    </div><!-- /.tab-pane -->
                              </div> <!-- /.tab-content -->
                       </div><!-- nav-tabs-custom -->
</div> 

<div class="col-md-6">

                        <div class="nav-tabs-custom">
                              <ul class="nav nav-tabs pull-right">
                                  <li><a href="#tab_41115" data-toggle="tab">JP</a></li>
                                  <li><a href="#tab_31115" data-toggle="tab">JHT</a></li>
                                  <li><a href="#tab_21115" data-toggle="tab">JKM</a></li>
                              <li class="active"><a href="#tab_11115" data-toggle="tab">JKK</a></li>
                              <li class="pull-left header"><i class="fa fa-th"></i> Pembayaran Manfaat per Program </li>
                              </ul>
                              <div class="tab-content">
                                    <div class="tab-pane active" id="tab_11115">
                                    <div  id="" style="width: 300 margin: 0 auto"></div>
                                    {!! $ManfaatBysegmenkasus1->render() !!} 
                                    </div>
                                    <!-- /.tab-pane -->
                                    <div class="tab-pane " id="tab_21115">
                                    <div  id="" style="width: 300 margin: 0 auto"></div>
                                    {!! $ManfaatBysegmenkasus2->render() !!} 
                                    </div>
                                    <!-- /.tab-pane -->
                                    <div class="tab-pane" id="tab_31115">
                                    <div  id="" style="width: 300 margin: 0 auto"></div>
                                    {!! $ManfaatBysegmenkasus3->render() !!} 
                                    </div>
                                    
                                    <div class="tab-pane" id="tab_41115">
                                    <div  id="" style="width: 300 margin: 0 auto"></div>
                                    {!! $ManfaatBysegmenkasus4->render() !!} 
                                    </div><!-- /.tab-pane -->
                              </div> <!-- /.tab-content -->
                       </div><!-- nav-tabs-custom -->
</div>      

                                                          
</div>    

<div class="row">
<div class="col-md-6">
        <!-- Custom Tabs (Pulled to the right) -->
                 <div class="nav-tabs-custom">
                              <ul class="nav nav-tabs pull-right">
                                  <li><a href="#tab_4A" data-toggle="tab">JP</a></li>
                                  <li><a href="#tab_3A" data-toggle="tab">JHT</a></li>
                                  <li><a href="#tab_2A" data-toggle="tab">JKM</a></li>
                              <li class="active"><a href="#tab_1A" data-toggle="tab">JKK</a></li>



                              <li class="pull-left header"><i class="fa fa-th"></i> Pembayaran Manfaat </li>
                              </ul>

                         <div class="tab-content">
                              <div class="tab-pane active" id="tab_1A">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                              {!! $ManfaatByJK1->render() !!} 
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane " id="tab_2A">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                              {!! $ManfaatByJK2->render() !!} 
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="tab_3A">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                              {!! $ManfaatByJK3->render() !!} 
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="tab_4A">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                              {!! $ManfaatByJK4->render() !!} 
                              </div>       
                              <!-- /.tab-pane -->
                         </div>
                              <!-- /.tab-content -->
                   </div>
                              <!-- nav-tabs-custom -->
</div> 
 

<div class="col-md-6">
        <!-- Custom Tabs (Pulled to the right) -->
                 <div class="nav-tabs-custom">
                              <ul class="nav nav-tabs pull-right">
                                  <li><a href="#tab_42" data-toggle="tab">JP</a></li>
                                  <li><a href="#tab_32" data-toggle="tab">JHT</a></li>
                                  <li><a href="#tab_22" data-toggle="tab">JKM</a></li>
                              <li class="active"><a href="#tab_12" data-toggle="tab">JKK</a></li>



                              <li class="pull-left header"><i class="fa fa-th"></i> Pembayaran Manfaat </li>
                              </ul>

                         <div class="tab-content">
                              <div class="tab-pane active" id="tab_12">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                               {!! $ManfaatByJKasus1->render() !!} 
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane " id="tab_22">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                              {!! $ManfaatByJKasus2->render() !!} 
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="tab_32">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                              {!! $ManfaatByJKasus3->render() !!} 
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="tab_42">
                              <div  id="" style="width: 300 margin: 0 auto"></div>
                             {!! $ManfaatByJKasus4->render() !!} 
                              </div>       
                              <!-- /.tab-pane -->
                         </div>
                              <!-- /.tab-content -->
                   </div>
                              <!-- nav-tabs-custom -->
</div>   




</div>


