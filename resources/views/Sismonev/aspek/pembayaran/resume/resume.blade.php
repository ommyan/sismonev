<div class="table-responsive">
									<table class="table table-xlg text-nowrap">
										<tbody>
											<tr>
												<td class="col-md-4">
													<div class="media-left media-middle">
														<div id="tickets-status"></div>
													</div>

													<div class="media-left">
														<h5 class="text-semibold no-margin">14,327 <small class="text-success text-size-base"><i class="icon-arrow-up12"></i> (+2.9%)</small></h5>
														<span class="text-muted"><span class="status-mark border-success position-left"></span> Jun 16, 10:00 am</span>
													</div>
												</td>

												<td class="col-md-3">
													<div class="media-left media-middle">
														<a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-alarm-add"></i></a>
													</div>

													<div class="media-left">
														<h5 class="text-semibold no-margin">
															1,132 <small class="display-block no-margin">total tickets</small>
														</h5>
													</div>
												</td>

												<td class="col-md-3">
													<div class="media-left media-middle">
														<a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-spinner11"></i></a>
													</div>

													<div class="media-left">
														<h5 class="text-semibold no-margin">
															06:25:00 <small class="display-block no-margin">response time</small>
														</h5>
													</div>
												</td>

												<td class="text-right col-md-2">
													<a href="#" class="btn bg-teal-400"><i class="icon-statistics position-left"></i> Report</a>
												</td>
											</tr>
										</tbody>
									</table>	
								</div>