@php $jkkbpu=0 @endphp
@php $jkkppu=0 @endphp
@php $jkkjakon=0 @endphp

@php $jkmbpu=0 @endphp
@php $jkmppu=0 @endphp
@php $jkmjakon=0 @endphp

@php $jhtbpu=0 @endphp
@php $jhtppu=0 @endphp
@php $jhtjakon=0 @endphp

@php $jpbpu=0 @endphp
@php $jpppu=0 @endphp
@php $jpjakon=0 @endphp

@php $jkk=0 @endphp
@php $jht=0 @endphp
@php $jkm=0 @endphp
@php $jp=0 @endphp


@php $bpu=0 @endphp
@php $ppu=0 @endphp
@php $jakon=0 @endphp


@foreach($resumeProgramSegmen as $iur)

@if($iur['ID_PROGRAM']==1 && $iur['ID_SEGMEN']==2)
@php $jkkbpu=$iur['JUMLAH']/1000 @endphp
@elseif ($iur['ID_PROGRAM']==1 && $iur['ID_SEGMEN']==1)
@php $jkkppu=$iur['JUMLAH']/1000 @endphp
@elseif ($iur['ID_PROGRAM']==1 && $iur['ID_SEGMEN']==3)
@php $jkkjakon= $iur['JUMLAH']/1000 @endphp
@endif

@if($iur['ID_PROGRAM']==2 && $iur['ID_SEGMEN']==2)
@php $jkmbpu=$iur['JUMLAH']/1000 @endphp
@elseif ($iur['ID_PROGRAM']==2 && $iur['ID_SEGMEN']==1)
@php $jkmppu=$iur['JUMLAH']/1000 @endphp
@elseif ($iur['ID_PROGRAM']==2 && $iur['ID_SEGMEN']==3)
@php $jkmjakon= $iur['JUMLAH']/1000 @endphp
@endif

@if($iur['ID_PROGRAM']==3 && $iur['ID_SEGMEN']==2)
@php $jhtbpu=$iur['JUMLAH']/1000 @endphp
@elseif ($iur['ID_PROGRAM']==3 && $iur['ID_SEGMEN']==1)
@php $jhtppu=$iur['JUMLAH']/1000 @endphp
@elseif ($iur['ID_PROGRAM']==3 && $iur['ID_SEGMEN']==3)
@php $jhtjakon= $iur['JUMLAH']/1000 @endphp
@endif

@if($iur['ID_PROGRAM']==4 && $iur['ID_SEGMEN']==2)
@php $jpbpu=$iur['JUMLAH']/1000 @endphp
@elseif ($iur['ID_PROGRAM']==4 && $iur['ID_SEGMEN']==1)
@php $jpppu=$iur['JUMLAH']/1000 @endphp
@elseif ($iur['ID_PROGRAM']==4 && $iur['ID_SEGMEN']==3)
@php $jpjakon= $iur['JUMLAH']/0000 @endphp
@endif


@if($iur['ID_SEGMEN']==1)
@php $ppu=$iur['JUMLAH']/1000 @endphp
@elseif($iur['ID_SEGMEN']==2)
@php $bpu=$iur['JUMLAH']/10000 @endphp
@else
@php $jakon=$iur['JUMLAH']/1000 @endphp
@endif

@endforeach


@foreach($resume as $iurp)
@if($iurp['ID_PROGRAM']==1)
@php $jkk=$iurp['JUMLAH']/1000 @endphp
@elseif($iurp['ID_PROGRAM']==2)
@php $jkm=$iurp['JUMLAH']/1000 @endphp
@elseif ($iurp['ID_PROGRAM']==3)
@php $jht=$iurp['JUMLAH']/1000 @endphp
@elseif ($iurp['ID_PROGRAM']==4)
@php $jp=$iurp['JUMLAH']/1000 @endphp
@endif
@endforeach



<div class="row">
    <div class="col-md-6">
                            
        <div class="small-box bg-green">
               <div class="inner">
                                                <h5>Jaminan Kecelakaan Kerja </h5>
                                                <h3>{{number_format($jkk,0,'.','.')}} <sup style="font-size: 20px">Rp.</sup></h3>

                                                <p>Pembayaran Manfaat program Jaminan Kecelakaan Kerja  </p>
                                                <hr>
                                                <div class="row">
                                                <div class="col-md-4">
                                                <span class="info-box-text"><i class="icon-ppu fa-2x "></i>PPU</span>
                                                <span class="info-box-number">{{number_format($jkkppu,0,'.','.')}}</span>
                                                </div>
                                                <div class="col-md-4">
                                                <span class="info-box-text"><i class="icon-bpu fa-2x"></i>BPU</span>
                                                <span class="info-box-number">{{number_format($jkkbpu,0,'.','.')}}</span>
                                                </div>
                                                <div class="col-md-4">
                                                <span class="info-box-text"><i class="icon-jakon fa-2x"></i>JAKON</span>
                                                <span class="info-box-number">{{number_format($jkkjakon,0,'.','.')}}</span>                                                
                                                </div>
                                                
                                                </div>
                                                
                                                    <div class="icon">
                                                    <i class="icon-jkk"></i>
                                                    </div>
                                            <a href="/w/manfaat/1/{{$rbulan}}/{{$rtahun}}" class="small-box-footer">
                                            More info <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                </div>
        </div>
    </div>

     <div class="col-md-6">
                            
        <div class="small-box bg-Orange">
               <div class="inner">
                                                <h5>Jaminan Kematian </h5>
                                                <h3>{{number_format($jkm,0,'.','.')}} <sup style="font-size: 20px">Rp.</sup></h3>

                                                <p>Pembayaran Manfaat  program Jaminan Kematian </p>
                                                 <hr>
                                                <div class="row">
                                                <div class="col-md-4">
                                                <span class="info-box-text"><i class="icon-ppu fa-2x"></i>PPU</span>
                                                <span class="info-box-number">{{number_format($jkmppu,0,'.','.')}}</span>
                                                </div>
                                                <div class="col-md-4">
                                                <span class="info-box-text"><i class="icon-bpu fa-2x"></i>BPU</span>
                                                <span class="info-box-number">{{number_format($jkmbpu,0,'.','.')}}</span>
                                                </div>
                                                <div class="col-md-4">
                                                <span class="info-box-text"><i class="icon-jakon fa-2x"></i>JAKON</span>
                                                <span class="info-box-number">{{number_format($jkmjakon,0,'.','.')}}</span>                                                
                                                </div>
                                                </div>

                                                    <div class="icon">
                                                    <i class="icon-jkm"></i>
                                                    </div>
                                            <a href="/w/manfaat/2/{{$rbulan}}/{{$rtahun}}" class="small-box-footer">
                                            More info <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
                            
        <div class="small-box bg-Teal">
               <div class="inner">
                                                <h5>Jaminan Hari Tua </h5>
                                                <h3>{{number_format($jht,0,'.','.')}} <sup style="font-size: 20px">Rp.</sup></h3>

                                                <p>Pembayaran Manfaat  program Jaminan Hari Tua </p>
                                               <hr>
                                              <div class="row">
                                                <div class="col-md-4">
                                                <span class="info-box-text"><i class="icon-ppu fa-2x"></i>PPU</span>
                                                <span class="info-box-number">{{number_format($jhtppu,0,'.','.')}}</span>
                                                </div>
                                                <div class="col-md-4">
                                                <span class="info-box-text"><i class="icon-bpu fa-2x"></i>BPU</span>
                                                <span class="info-box-number">{{number_format($jhtbpu,0,'.','.')}}</span>
                                                </div>
                                                <div class="col-md-4">
                                                <span class="info-box-text"><i class="icon-jakon fa-2x"></i>JAKON</span>
                                                <span class="info-box-number">{{number_format($jhtjakon,0,'.','.')}}</span>                                                
                                                </div>
                                                </div>

                                                    <div class="icon">
                                                    <i class="icon-jht"></i>
                                                    </div>
                                            <a href="/w/manfaat/3/{{$rbulan}}/{{$rtahun}}" class="small-box-footer">
                                            More info <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                </div>
        </div>
    </div>

     <div class="col-md-6">
                            
        <div class="small-box bg-Purple">
               <div class="inner">
                                                <h5>Jaminan Pensiun </h5>
                                                <h3>{{number_format($jp,0,'.','.')}} <sup style="font-size: 20px">Rp.</sup></h3>

                                                <p>Pembayaran Manfaat  program aminan Pensiun </p>
                                               <hr>
                                              <div class="row">
                                                <div class="col-md-4">
                                                <span class="info-box-text"><i class="icon-ppu fa-2x"></i>PPU</span>
                                                <span class="info-box-number">{{number_format($jpppu,0,'.','.')}}</span>
                                                </div>
                                                <div class="col-md-4">
                                                <span class="info-box-text"><i class="icon-bpu fa-2x"></i>BPU</span>
                                                <span class="info-box-number">{{number_format($jpbpu,0,'.','.')}}</span>
                                                </div>
                                                <div class="col-md-4">
                                                <span class="info-box-text"><i class="icon-jakon fa-2x"></i>JAKON</span>
                                                <span class="info-box-number">{{number_format($jpjakon,0,'.','.')}}</span>                                                
                                                </div>
                                                </div>

                                                    <div class="icon">
                                                    <i class="icon-jp"></i>
                                                    </div>
                                            <a href="/w/manfaat/4/{{$rbulan}}/{{$rtahun}}" class="small-box-footer">
                                            More info <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                </div>
        </div>
    </div>
</div>
             
 
   

         