@foreach($ptnik1 as $bu)
    @if($bu['ID_PROGRAM']==1)
        @php $ptnikjkk=$bu['JTNIK']/1000 @endphp
    @elseif($bu['ID_PROGRAM']==2)
        @php $ptnikjkm=$bu['JTNIK']/1000 @endphp
    @elseif($bu['ID_PROGRAM']==3)
        @php $ptnikjht=$bu['JTNIK']/1000 @endphp
    @elseif($bu['ID_PROGRAM']==4)
        @php $ptnikjp=$bu['JTNIK']/1000 @endphp
    @endif

@endforeach
<script>
    $(function() {
        $(".dial").knob(
            {
                'readOnly': true,
                'draw': function () {
                    $(this.i).val(this.cv + '%');
                }
            }
        );
    });
</script>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-right">
        <li><a href="#tab_4211" data-toggle="tab">JP</a></li>
        <li><a href="#tab_3211" data-toggle="tab">JHT</a></li>
        <li><a href="#tab_2211" data-toggle="tab">JKM</a></li>
        <li class="active"><a href="#tab_121" data-toggle="tab">JKK</a></li>
        <li class="pull-left header"><i class="fa fa-th"></i> Kepesertaan </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="tab_1211">
        @include('Sismonev.aspek.kepesertaan.knob.jkk')
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane " id="tab_2211">
            @include('Sismonev.aspek.kepesertaan.knob.jkm')

        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_3211">
            @include('Sismonev.aspek.kepesertaan.knob.jht')
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_4211">
            @include('Sismonev.aspek.kepesertaan.knob.jp')
        </div>
        <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
</div>
