<div class="row">
    @include('sismonev.aspek.kepesertaan.awal')
</div>
<div class="row">
    {{--@include('sismonev.aspek.kepesertaan.knob')--}}
</div> 
<div class="row">

    <div class="col-md-12">
        @include('sismonev.aspek.kepesertaan.knob.kepesertaan')
    </div>
                   
</div>

 <br>             
<div class="row">
          <div class="col-md-6">
                  <!-- Custom Tabs (Pulled to the right) -->
                          <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs pull-right">
                                            <li><a href="#tab_42" data-toggle="tab">JP</a></li>
                                            <li><a href="#tab_32" data-toggle="tab">JHT</a></li>
                                            <li><a href="#tab_22" data-toggle="tab">JKM</a></li>
                                            <li class="active"><a href="#tab_12" data-toggle="tab">JKK</a></li>
                                        <li class="pull-left header"><i class="fa fa-th"></i> Peserta Tenaga Kerja </li>
                                        </ul>

                                  <div class="tab-content">
                                        <div class="tab-pane active" id="tab_12">
                                        <div  id="" style="width: 300 margin: 0 auto"></div>
                                        {!! $kep1->render() !!} 
                                        </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane " id="tab_22">
                                        <div  id="" style="width: 300 margin: 0 auto"></div>
                                        {!! $kep2->render() !!} 
                                        </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane" id="tab_32">
                                        <div  id="" style="width: 300 margin: 0 auto"></div>
                                        {!! $kep3->render() !!} 
                                        </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane" id="tab_42">
                                        <div  id="" style="width: 300 margin: 0 auto"></div>
                                        {!! $kep4->render() !!} 
                                        </div>       
                                        <!-- /.tab-pane -->
                                  </div>
                                        <!-- /.tab-content -->
                            </div>
                                        <!-- nav-tabs-custom -->
          </div>    

          <div class="col-md-6">
                  <!-- Custom Tabs (Pulled to the right) -->
                          <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs pull-right">
                                            <li><a href="#tab_422" data-toggle="tab">JP</a></li>
                                            <li><a href="#tab_322" data-toggle="tab">JHT</a></li>
                                            <li><a href="#tab_222" data-toggle="tab">JKM</a></li>
                                            <li class="active"><a href="#tab_122" data-toggle="tab">JKK</a></li>



                                        <li class="pull-left header"><i class="fa fa-th"></i> Peserta Badan Usaha </li>
                                        </ul>

                                  <div class="tab-content">
                                        <div class="tab-pane active" id="tab_122">
                                        <div  id="" style="width: 300 margin: 0 auto"></div>
                                        {!! $kepbu1->render() !!}
                                        </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane " id="tab_222">
                                        <div  id="" style="width: 300 margin: 0 auto"></div>
                                        {!! $kepbu2->render() !!}
                                        </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane" id="tab_322">
                                        <div  id="" style="width: 300 margin: 0 auto"></div>
                                        {!! $kepbu3->render() !!}
                                        </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane" id="tab_422">
                                        <div  id="" style="width: 300 margin: 0 auto"></div>
                                        {!! $kepbu4->render() !!}
                                        </div>       
                                        <!-- /.tab-pane -->
                                  </div>
                                        <!-- /.tab-content -->
                            </div>
                                        <!-- nav-tabs-custom -->
          </div>  
</div>
   
  
<div class="row">

<div class="col-md-6">
                  <!-- Custom Tabs (Pulled to the right) -->
                          <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs pull-right">
                                            <li><a href="#tab_4221" data-toggle="tab">JP</a></li>
                                            <li><a href="#tab_3221" data-toggle="tab">JHT</a></li>
                                            <li><a href="#tab_2221" data-toggle="tab">JKM</a></li>
                                            <li class="active"><a href="#tab_1221" data-toggle="tab">JKK</a></li>



                                        <li class="pull-left header"><i class="fa fa-th"></i> Pertumbuhan Badan Usaha </li>
                                        </ul>

                                  <div class="tab-content">
                                        <div class="tab-pane active" id="tab_1221">
                                        <div  id="" style="width: 300 margin: 0 auto"></div>
                                        {!! $kep031->render() !!}
                                        </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane " id="tab_2221">
                                        <div  id="" style="width: 300 margin: 0 auto"></div>
                                        {!! $kep032->render() !!}
                                        </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane" id="tab_3221">
                                        <div  id="" style="width: 300 margin: 0 auto"></div>
                                        {!! $kep033->render() !!}
                                        </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane" id="tab_4221">
                                        <div  id="" style="width: 300 margin: 0 auto"></div>
                                        {!! $kep034->render() !!}
                                        </div>       
                                        <!-- /.tab-pane -->
                                  </div>
                                        <!-- /.tab-content -->
                            </div>
                                        <!-- nav-tabs-custom -->
          </div>  
                           <div class='col-md-6'>
                        <!-- Box -->
                              <div class="box box-primary">
                                    <div class="box-header with-border">
                                     <i class="fa fa-th"> Badan Usaha </i>
                                    </div>
                                        <div class="box-body">
                                        {!! $kepbu04->render() !!}
                                        </div><!-- /.box-body -->
                                    <div class="box-footer">

                                    </div><!-- /.box-footer-->
                              </div><!-- /.box -->
                        </div><!-- /.col -->
   
</div>


