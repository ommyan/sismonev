<script type="text/javascript">

    function format ( d ) {
        // `d` is the original data object for the row
        return '<div class="slider">'+
            '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
            '<tr>'+
            '<td>Full name:</td>'+
            '<td>'+d.name+'</td>'+
            '</tr>'+
            '<tr>'+
            '<td>Extension number:</td>'+
            '<td>'+d.extn+'</td>'+
            '</tr>'+
            '<tr>'+
            '<td>Extra info:</td>'+
            '<td>And any further details here (images etc)...</td>'+
            '</tr>'+
            '</table>'+
            '</div>';
    }

    $(document).ready(function() {


        var bulan={{$rbulan}};
        var tahun={{$rtahun}};

        var template = Handlebars.compile($("#details-template").html());
        var table =  $('#organisasi').DataTable({
            "columnDefs": [
                { className: "text-right", "targets": [3,4] },
                {
                    "targets": [ 1 ],
                    "visible": false
                }
            ],
            processing: true,
            serverSide: true,
            responsive: true,

            ajax: {
                "url": "{!! url('organisasi.data') !!}",
                "type": "POST",
                "data": function ( d ) {

                    d.bulan= bulan;
                    d.tahun= tahun;
                }
            },
            columns: [
                {
                    "className":      'details-control',
                    "orderable":      false,
                    "searchable":     false,
                    "data":           null,
                    "defaultContent": ''
                },
                { data: 'ID' , name: 'ID',width:100 },
                { data: 'KANWIL', name: 'KANWIL' },
                { data: 'JMLCABANG', name: 'JMLCABANG' },
                { data: 'JMLKARYAWAN', name: 'JMLKARYAWAN' },
                { data: 'JMLPESERTA', name: 'JMLPESERTA' }
            ],
            "order": [[1, 'asc']]
        });

        $('#organisasi tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );
            var tableId = 'posts-' + row.data().ID;


            //alert(tableId);
            if ( row.child.isShown() ) {
                // This row is already open - close it
                //$('div.slider', row.child()).slideUp( function () {
                row.child.hide();
                tr.removeClass('shown');

                //}
                // );
            }
            else {
                // Open this row
                // row.child(template(row.data())).show();
                row.child(template(row.data())).show();
                initTable(tableId, row.data(),row.data().ID);
                tr.addClass('shown');
                tr.next().find('td').addClass('no-padding bg-gray');
            }
        } );


    });
    function initTable(tableId, data,rows) {

        var bulan={{$rbulan}};
        var tahun={{$rtahun}};

        $('#' + tableId).DataTable({
            "columnDefs": [
                { className: "text-right", "targets": [2,3,4] },
                {
                    "targets": [ 0 ],
                    "visible": false
                }
            ],
            processing: true,
            serverSide: true,
            ajax: {
                "url": "{!! url('manfaat.detail') !!}",
                "type": "POST",
                "data": function ( d ) {

                    d.idc =  rows;
                    d.bulan= bulan;
                    d.tahun= tahun;
                }
            },

            columns: [
                { data: 'ID_CABANG' , name: 'ID_CABANG',width:100 },
                { data: 'CABANG', name: 'CABANG' },
                { data: 'PPUJKK', name: 'PPUJKK' },
                { data: 'BPUJKK', name: 'BPUJKK' },
                { data: 'JAKONJKK', name: 'JAKONJKK' }
            ]

        })

    }


</script>


<script id="details-template" type="text/x-handlebars-template">

    <table class="table details-table" id="posts-@{{ID}}">
        <thead>
        <tr>
            <th>ID</th>
            <th>Cabang</th>
            <th>PPU</th>
            <th>BPU</th>
            <th>Jakon</th>

        </tr>
        </thead>
    </table>
</script>
<style>


    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }
</style>
<style type="text/css">
    .progress_result {
        height: 100%;
        float: left;
        box-shadow: inset 0 -1px 0 rgba(0,0,0,0.15);
        background: #eb6e1a;
    }
    .progress_bar_element {
        background-color: #f5f5f5;
        border-radius: 4px;
        width: 100%;
        height: 6px;
        -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
        box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
        margin-top: 16px;
        float: left;
        overflow: hidden;
        cursor: pointer;
    }
</style>
<div class="container-fluid">

    <div class="table-responsive">
        <table class="table table-striped table-bordered" width="100%" id="organisasi">
            <thead>
            <tr>
                <th></th>
                <th>Kantor</th>
                <th>Jumlah Cabang</th>
                <th>Jumlah Karyawan</th>
                <th>Jumlah Peserta</th>
                <th>Rasio</th>
            </tr>
            </thead>
        </table>
    </div>
</div>