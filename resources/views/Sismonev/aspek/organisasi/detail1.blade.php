{{--
@php $jmlcabang=array_sum(array_column($orgall, 'JMLCABANG')) @endphp
@php $jmlkaryawan=array_sum(array_column($orgall, 'JMLKARYAWAN')) @endphp
--}}
@php $jmlcabang=325 @endphp
@php $jmlkaryawan=4163 @endphp


<script>
    $(function() {
        $(".dial").knob(
            {
                'readOnly': true,
                'draw': function () {
                    $(this.i).val(this.cv + '%');
                }
            }
        );
    });
</script>
<script>
    $(function() {
        $('.chart').easyPieChart({
            //your options goes here
        });
    });
</script>
<div class="row">
    <div class="col-sm-4 col-md-4 col-xs-6">
    <div class="info-box bg-green">
        <span class="info-box-icon" style="padding-top: 10px">
        <input type="text" data-fgColor="red" value="{{number_format(11/34*100)}}" data-width="70"  data-height="70" class="dial">
        </span>

            <div class="info-box-content">
            <span class="info-box-text"><i class="fa fa-building-o" style="padding-right: 15px"></i>Kantor Kedeputian</span>
            <span class="info-box-number">11</span>

            <div class="progress">
                <div class="progress-bar" style="width: {{number_format(11/34*100)}}%"></div>
            </div>
            <span class="progress-description">
                    dari total propinsi
                  </span>
        </div>
        <!-- /.info-box-content -->
    </div>
    </div>
    <!-- /.info-box -->
    <div class="col-sm-4 col-md-4 col-xs-6">
    <div class="info-box bg-red">
        <span class="info-box-icon" style="padding-top: 10px">
        <input type="text" data-fgColor="orange" value="{{number_format($jmlcabang/450*100)}}" data-width="70"  data-height="70" class="dial">
        </span>
        <div class="info-box-content">
            <span class="info-box-text"><i class="fa fa-building-o" style="padding-right: 15px"></i>Kantor Cabang</span>
            <span class="info-box-number">{{$jmlcabang}}</span>

            <div class="progress">
                <div class="progress-bar" style="width: {{number_format($jmlcabang/450*100)}}%"></div>
            </div>
            <span class="progress-description">
                    dari total kabupaten
                  </span>
        </div>
        <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
    </div>
    <div class="col-sm-4 col-md-4 col-xs-6">
    <div class="info-box bg-aqua">
          <span class="info-box-icon" style="padding-top: 10px">
        <input type="text" data-fgColor="red" value="{{number_format($jmlkaryawan)}}" data-width="70"  data-height="70" class="dial">
        </span>
        <div class="info-box-content">
            <span class="info-box-text"><i class="fa fa-users" style="padding-right: 15px"></i>Jumlah Karyawan</span>
            <span class="info-box-number">{{$jmlkaryawan}}</span>

            <div class="progress">
                <div class="progress-bar" style="width: {{number_format($jmlkaryawan/450*100)}}%"></div>
            </div>
            <span class="progress-description">
                    dari total peserta
                  </span>
        </div>
        <!-- /.info-box-content -->
    </div>
    </div>

</div>

<br>


</div>

{{--@include('Sismonev.aspek.organisasi.list.organisasi')--}}