  <li class="time-label">
                  <span class="bg-red">
                   Iuran
                  </span>
            </li>
            <!-- /.timeline-label -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-comments bg-yellow"></i>

              <div class="timeline-item">
                <h3 class="timeline-header">Penerimaan Iuran</h3>
                <div class="timeline-body">
                <div class="row">
         
                        <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="info-box bg-red">
                        <span class="info-box-icon"><i class="fa fa-balance-scale"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">Pekerja Bukan Penerima Upah</span>
                        <span class="info-box-number">54.412.090.085</span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        70% Increase in 30 Days
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>

                <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="info-box bg-aqua">
                        <span class="info-box-icon"><i class="fa fa-odnoklassniki"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">Pekerja Penerima Upah</span>
                        <span class="info-box-number">20.766.830.450.217</span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        70% Increase in 30 Days
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>

                  
                  <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="info-box bg-yellow">
                        <span class="info-box-icon"><i class="fa   fa-users"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">Pekerja Jasa Konstruksi</span>
                        <span class="info-box-number">117.290.543.728</span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        70% Increase in 30 Days
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                


                 </div>

                 <div class="row">
               
           
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Orange">
                        <span class="info-box-icon"><i class="fa  fa-legal"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JKK</span>
                        <span class="info-box-number"><small>1.709.787.515.247</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Kecelakaan Kerja
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                  
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Teal">
                        <span class="info-box-icon"><i class="fa  fa-ship"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JKM</span>
                        <span class="info-box-number"><small>773.796.596.797</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Kematian
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                  
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Purple">
                        <span class="info-box-icon"><i class="fa fa-street-view"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JHT</span>
                        <span class="info-box-number"><small>13.969.463.758.802</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Hari Tua
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                  
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Maroon">
                        <span class="info-box-icon"><i class="fa fa-user-secret"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JP</span>
                        <span class="info-box-number"><small>4.485.485.213.183</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Pensiun
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                 </div>
                </div>
                <div class="timeline-footer">
                  
                </div>
              </div>
            </li>
            <!-- END timeline item -->
            <!-- timeline time label -->