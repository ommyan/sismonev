@php $jkkbpu=0 @endphp
@php $jkkppu=0 @endphp
@php $jkkjakon=0 @endphp

@php $jkk=0 @endphp
@php $jht=0 @endphp
@php $jkm=0 @endphp
@php $jp=0 @endphp


@php $bpu=0 @endphp
@php $ppu=0 @endphp
@php $jakon=0 @endphp


@foreach($resume as $iur)

@if($iur['ID_PROGRAM']==1 && $iur['ID_SEGMEN']==2)
@php $jkkbpu=$iur['JUMLAH'] @endphp
@elseif ($iur['ID_PROGRAM']==1 && $iur['ID_SEGMEN']==1)
@php $jkkppu=$iur['JUMLAH'] @endphp
@else
@php $jkkjakon= $iur['JUMLAH'] @endphp
@endif

@if($iur['ID_SEGMEN']==1)
@php $ppu=$iur['JUMLAH'] @endphp
@elseif($iur['ID_SEGMEN']==2)
@php $bpu=$iur['JUMLAH'] @endphp
@else
@php $jakon=$iur['JUMLAH'] @endphp
@endif



@endforeach

@foreach($resumeprogram as $iurp)
@if($iurp->ID_PROGRAM==1)
@php $jkk=$iurp->JUMLAH @endphp
@elseif($iurp->ID_PROGRAM==2)
@php $jkm=$iurp->JUMLAH @endphp
@elseif ($iurp->ID_PROGRAM==3)
@php $jht=$iurp->JUMLAH @endphp
@elseif ($iurp->ID_PROGRAM==4)
@php $jp=$iurp->JUMLAH @endphp
@endif
@endforeach



<li class="time-label">
                  <span class="bg-red">
                   Kepesertaan
                  </span>
</li>
            <!-- /.timeline-label -->
            <!-- timeline item -->
<li>
              <i class="fa fa-comments bg-yellow"></i>

  <div class="timeline-item">
               
    <h3 class="timeline-header">Kepesertaan</h3>
		

         <div class="timeline-body">
               

                 <div class="row">
                                    <div class="col-md-6">
                            
                                            <div class="small-box bg-green">
                                                <div class="inner">
                                                            <h3>{{number_format($jkk)}} <sup style="font-size: 20px"></sup></h3>

                                                            <p>Total pekerja terdaftar, mengacu kepada peserta program JKK</p>
                                                </div>
                                                    <div class="icon">
                                                    <i class="ion ios-people"></i>
                                                    </div>
                                            <a href="#" class="small-box-footer">
                                            More info <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                                            </div>
                                </div>
                                <div class="col-md-6">
                                
                                            <div class="small-box bg-red">
                                                    <div class="inner">
                                                    <h3> 15.5% <sup style="font-size: 20px"></sup></h3>

                                                    <p>Pekerja terdaftar dibandingkan dengan penduduk bekerja</p>
                                                    </div>
                                                            <div class="icon">
                                                            <i class="ion ion-stats-bars"></i>
                                                            
                                                            </div>
                                            <a href="#" class="small-box-footer">
                                            More info <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                                            </div>
                                    </div>   
                                
             </div> <!--row-->
               
            <div class="row">
     
                
                        <div class="col-md-4 col-sm-4 col-xs-4">
                              <div class="info-box bg-red">
                                    <span class="info-box-icon">
                                        <i class="icon-bpu"></i>
                                    </span>

                                    <div class="info-box-content">
                                                <span class="info-box-text">Pekerja Bukan Penerima Upah</span>
                                                <span class="info-box-number">{{number_format($bpu)}}</span>

                                                <div class="progress">
                                                <div class="progress-bar" style="width: 70%"></div>
                                                </div>
                                                <span class="progress-description">
                                               
                                                </span>
                                    </div>
                              <!-- /.info-box-content -->
                              </div>
                        <!-- /.info-box -->
                        </div> <!-- /.icol -->

                        <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="info-box bg-aqua">
                                        <span class="info-box-icon">
                                            <i class="icon-ppu"></i>
                                        </span>

                                     <div class="info-box-content">
                                        <span class="info-box-text">Pekerja Penerima Upah</span>
                                        <span class="info-box-number">{{number_format($ppu)}}</span>

                                        <div class="progress">
                                        <div class="progress-bar" style="width: 70%"></div>
                                        </div>
                                    <span class="progress-description">
                                    </span>
                                    </div><!-- /.info-box-content -->
                                </div><!-- /.info-box -->
                        </div><!-- /.col -->

                  
                
                  
                        <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="info-box bg-yellow">
                                    <span class="info-box-icon"><i class="icon-jakon"></i></span>

                                    <div class="info-box-content">
                                            <span class="info-box-text">Pekerja Jasa Konstruksi</span>
                                            <span class="info-box-number">{{number_format($jakon)}}</span>

                                            <div class="progress">
                                                <div class="progress-bar" style="width: 70%"></div>
                                            </div>
                                                <span class="progress-description">
                                                </span>
                                    </div> <!-- /.info-box-content -->
                                </div> <!-- /.info-box -->
                        </div>  <!--col-->
         </div> <!--row-->

                 <div class="row">
               
           
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Orange">
                        <span class="info-box-icon"><i class="icon-jkk"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JKK</span>
                        <span class="info-box-number"><small>{{number_format($jkk)}}</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Kecelakaan Kerja
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                  
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Teal">
                        <span class="info-box-icon"><i class="icon-jkm"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JKM</span>
                        <span class="info-box-number"><small>{{number_format($jkm)}}</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Kematian
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                  
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Purple">
                        <span class="info-box-icon"><i class="icon-jht"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JHT</span>
                        <span class="info-box-number"><small>{{number_format($jht)}}</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Hari Tua
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                  
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Maroon">
                        <span class="info-box-icon"><i class="icon-jp"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JP</span>
                        <span class="info-box-number"><small>{{number_format($jp)}}</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Pensiun
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
  </div>  <!--row-->         

</div>
</li>

         