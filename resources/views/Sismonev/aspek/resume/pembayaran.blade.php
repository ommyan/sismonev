  <li class="time-label">
                  <span class="bg-red">
                   Pembayaran
                  </span>
            </li>
            <!-- /.timeline-label -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-comments bg-yellow"></i>

              <div class="timeline-item">
               



                <h3 class="timeline-header">Pembayaran Klaim (Rupiah)</h3>
		

                <div class="timeline-body">
               

                 <div class="row">
               
           
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Orange">
                        <span class="info-box-icon"><i class="fa  fa-legal"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JKK</span>
                        <span class="info-box-number"><small>84.134.333.862</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Kecelakaan Kerja
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                  
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Teal">
                        <span class="info-box-icon"><i class="fa  fa-ship"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JKM</span>
                        <span class="info-box-number"><small>44.803.800.000</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Kematian
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                  
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Purple">
                        <span class="info-box-icon"><i class="fa fa-street-view"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JHT</span>
                        <span class="info-box-number"><small>1.615.394.120.275</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Hari Tua
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                  
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Maroon">
                        <span class="info-box-icon"><i class="fa fa-user-secret"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JP</span>
                        <span class="info-box-number"><small> 7.254.648.668</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Pensiun
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                 </div>
                </div>


                 <h3 class="timeline-header">Pembayaran Klaim (Kasus)</h3>
		

                <div class="timeline-body">
               

                 <div class="row">
           
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Orange">
                        <span class="info-box-icon"><i class="fa  fa-legal"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JKK</span>
                        <span class="info-box-number"><small>10.237</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Kecelakaan Kerja
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                  
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Teal">
                        <span class="info-box-icon"><i class="fa  fa-ship"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JKM</span>
                        <span class="info-box-number"><small>1.890</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Kematian
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                  
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Purple">
                        <span class="info-box-icon"><i class="fa fa-street-view"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JHT</span>
                        <span class="info-box-number"><small>163.413</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Hari Tua
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                  
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Maroon">
                        <span class="info-box-icon"><i class="fa fa-user-secret"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JP</span>
                        <span class="info-box-number"><small>5.175</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Pensiun
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                 </div>
                </div>
                <div class="timeline-footer">
                  
                </div>


              </div>
            </li>
            <!-- END timeline item -->
            <!-- timeline time label -->