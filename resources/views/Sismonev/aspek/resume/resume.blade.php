<style>
/* Style the tab */



/* Style the tab content */
.tabcontent {
    float: left;
    padding: 0px 12px;
    border: 1px solid #ccc;
    width: 70%;
    border-left: none;
    height: 300px;
}

</style>
  <div class="row">
      <br>             
</div>
<div class="row">
                    <div class="col-md-4">
                   
                                <div class="small-box bg-green">
                                    <div class="inner">
                                                <h3>22.234.456 <sup style="font-size: 20px"></sup></h3>

                                                <p>Total pekerja terdaftar, mengacu kepada peserta program JKK</p>
                                    </div>
                                          <div class="icon">
                                          <i class="ion ios-people"></i>
                                          </div>
                                <a href="#" class="small-box-footer">
                                More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                                </div>
                    </div>
                  <div class="col-md-4">
                   
                                <div class="small-box bg-green">
                                    <div class="inner">
                                                <h3>395.684 <sup style="font-size: 20px"></sup></h3>

                                                <p>Total Badan Usaha Terdaftar program Jaminan Sosial ketenagakerjaan</p>
                                    </div>
                                          <div class="icon">
                                          <i class="ion ios-people"></i>
                                          </div>
                                <a href="#" class="small-box-footer">
                                More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                                </div>
                    </div>
                    <div class="col-md-4">
                     
                                <div class="small-box bg-red">
                                          <div class="inner">
                                          <h3> 15.5% <sup style="font-size: 20px"></sup></h3>

                                          <p>Pekerja terdaftar dibandingkan dengan penduduk bekerja</p>
                                          </div>
                                                <div class="icon">
                                                <i class="ion ion-stats-bars"></i>
                                                
                                                </div>
                                <a href="#" class="small-box-footer">
                                More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                                </div>
                    </div>           
                  

 </div>
 <div class="row">
     
                
                        <div class="col-md-4 col-sm-4 col-xs-4">
                              <div class="info-box bg-red">
                              <span class="info-box-icon"><i class="icon-bpu"></i></span>

                                    <div class="info-box-content">
                                                <span class="info-box-text">Bukan Penerima Upah</span>
                                                <span class="info-box-number">1.891.763</span>

                                                <div class="progress">
                                                <div class="progress-bar" style="width: 70%"></div>
                                                </div>
                                                <span class="progress-description">
                                                
                                                </span>
                                    </div>
                              <!-- /.info-box-content -->
                              </div>
                        <!-- /.info-box -->
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4">
                              <div class="info-box bg-aqua">
                              <span class="info-box-icon"><i class="icon-ppu"></i></span>
   

                                    <div class="info-box-content">
                                          <span class="info-box-text">Penerima Upah</span>
                                          <span class="info-box-number">14.721.081</span>

                                          <div class="progress">
                                                <div class="progress-bar" style="width: 70%"></div>
                                          </div>
                                          <span class="progress-description">
                                          
                                          </span>
                                    </div> <!-- /.info-box-content -->
                              </div><!-- /.info-box -->
                        </div>

   
                  <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="info-box bg-yellow">
                              <span class="info-box-icon"><i class="icon-jakon"></i></span>

                              <div class="info-box-content">
                                    <span class="info-box-text">Jasa Konstruksi</span>
                                    <span class="info-box-number">7.095.521</span>

                                    <div class="progress">
                                    <div class="progress-bar" style="width: 70%"></div>
                                    </div>
                                    <span class="progress-description">
                                    
                                    </span>
                              </div><!-- /.info-box-content -->
                        </div><!-- /.info-box -->
                  </div><!-- col -->
</div><!-- row -->
 <div class="row">
               
           
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Orange">
                        <span class="info-box-icon"> <i class="icon-jkk"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JKK</span>
                        <span class="info-box-number"><small>22.623.375</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Kecelakaan Kerja
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                  
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Teal">
                        <span class="info-box-icon"> <i class="icon-jkm"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JKM</span>
                        <span class="info-box-number"><small>22.623.375</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Kematian
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                  
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Purple">
                        <span class="info-box-icon"> <i class="icon-jht"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JHT</span>
                        <span class="info-box-number"><small>13.939.315</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Hari Tua
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
                  
                  <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="info-box bg-Maroon">
                        <span class="info-box-icon"> <i class="icon-jp"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text">JP</span>
                        <span class="info-box-number"><small>9.677.701</small></span>

                        <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                        Jaminan Pensiun
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        </div>
                
  </div>  <!--row-->  
<div class="row">
<div class="box box-default">
   <div class="box-body">
                  <div class="col-md-4">
                      <div id="" style="min-width: 100px; height:200px; margin: 0 auto">
                        {!! $gaugemeter->render() !!}
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div id="" style="min-width: 100px; height:200px; margin: 0 auto">
                        {!! $persenkartuterbit->render() !!}
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div id="" style="min-width: 100px; height:200px; margin: 0 auto">
                         {!! $gmtka->render() !!}
                      </div>
                  </div>
                   
</div>
</div>
</div>

              
<div class="row">
    
 <div class="col-md-6">

    
      <div id="tabs1" class="c-tabs no-js">
        <div class="c-tabs-nav ">
          <a href="#" class="c-tabs-nav__link is-active">
           
             <i class="icon-jkk"></i>
            <span>JKK</span>
          </a>
          <a href="#" class="c-tabs-nav__link">
            <i class="icon-jkm"></i>
            <span>JKM</span>
          </a>
          <a href="#" class="c-tabs-nav__link">
           <i class="icon-jht"></i>
            <span>JHT</span>
          </a>
          <a href="#" class="c-tabs-nav__link">
           <i class="icon-jp"></i>
            <span>JP </span>
          </a>
          
        </div>

        <div class="c-tab is-active">
          <div class="c-tab__content">
             {!! $kep1->render() !!} 
            </div>
        </div>
        <div class="c-tab">
          <div class="c-tab__content">
            {!! $kep2->render() !!} 
          </div>
        </div>
        <div class="c-tab">
          <div class="c-tab__content">
            {!! $kep3->render() !!} 
          </div>
        </div>
        <div class="c-tab">
          <div class="c-tab__content">
            {!! $kep4->render() !!}           </div>
        </div>
        
      </div>
    </div>

 <div class="col-md-6">

    
      <div id="tabs2" class="c-tabs no-js">
        <div class="c-tabs-nav ">
          <a href="#" class="c-tabs-nav__link is-active">
            <i class="icon-jkk"></i>
            <span>JKK</span>
          </a>
          <a href="#" class="c-tabs-nav__link">
            <i class="icon-jkm"></i>
            <span>JKM</span>
          </a>
          <a href="#" class="c-tabs-nav__link">
           <i class="icon-jht"></i>
            <span>JHT</span>
          </a>
          <a href="#" class="c-tabs-nav__link">
           <i class="icon-jp"></i>
            <span>JP </span>
          </a>
          
        </div>

        <div class="c-tab is-active">
          <div class="c-tab__content">
             {!! $kepbu1->render() !!}
            </div>
        </div>
        <div class="c-tab">
          <div class="c-tab__content">
            {!! $kepbu2->render() !!} 
          </div>
        </div>
        <div class="c-tab">
          <div class="c-tab__content">
            {!! $kepbu3->render() !!} 
          </div>
        </div>
        <div class="c-tab">
          <div class="c-tab__content">
            {!! $kepbu4->render() !!}           
          </div>
        </div>
        
      </div>
    </div>

</div>     
     
 <hr>
<div class="row">
      <div class="col-md-6">

    
                <div id="tabs3" class="c-tabs no-js">
                  <div class="c-tabs-nav ">
                    <a href="#" class="c-tabs-nav__link is-active">
                      <i class="icon-jkk"></i>
                      <span>JKK</span>
                    </a>
                    <a href="#" class="c-tabs-nav__link">
                      <i class="icon-jkm"></i>
                      <span>JKM</span>
                    </a>
                    <a href="#" class="c-tabs-nav__link">
                    <i class="icon-jht"></i>
                      <span>JHT</span>
                    </a>
                    <a href="#" class="c-tabs-nav__link">
                    <i class="icon-jp"></i>
                      <span>JP </span>
                    </a> 
                  </div>

                  <div class="c-tab is-active">
                    <div class="c-tab__content">
                      {!! $kep031->render() !!}
                      </div>
                  </div>

                  <div class="c-tab">
                    <div class="c-tab__content">
                      {!! $kep032->render() !!}
                    </div>
                  </div>

                  <div class="c-tab">
                    <div class="c-tab__content">
                      {!! $kep033->render() !!}
                    </div>
                  </div>

                  <div class="c-tab">
                    <div class="c-tab__content">
                     {!! $kep034->render() !!}        
                    </div>
                  </div>

                  
                </div>
              </div>
      </div>

</div> 
</div>  
<br>
<div class="row">
                        <div class='col-md-6'>
                        <!-- Box -->
                              <div class="box box-primary">
                                    <div class="box-header with-border">

                                    </div>
                                    <div class="box-body">
                                    {!! $BU_JumlahSkala->render() !!}
                                    </div><!-- /.box-body -->
                                    <div class="box-footer">

                                    </div><!-- /.box-footer-->
                              </div><!-- /.box -->
                        </div><!-- /.col -->
                  
                      <div class="col-md-6">
                              <!-- Custom Tabs (Pulled to the right) -->
                              <div class="nav-tabs-custom">
                              <ul class="nav nav-tabs pull-right">
                              <li class="active"><a href="#tab_6" data-toggle="tab">JKK</a></li>
                              <li><a href="#tab_7" data-toggle="tab">JKM</a></li>
                              <li><a href="#tab_8" data-toggle="tab">JHT</a></li>
                              <li><a href="#tab_9" data-toggle="tab">JP</a></li>


                              <li class="pull-left header"><i class="fa fa-th"></i> Pertumbuhan Peserta [3]</li>
                              </ul>
                              <div class="panel-body">
                              <div class="tab-content">
                              <div class="tab-pane active " id="tab_6">
                              <div  id="tumbuhpesertajkk" style=" margin: 0 auto"></div>
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="tab_7">
                              <div  id="tumbuhpesertajkm" style=" margin: 0 auto"></div>
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="tab_8">
                              <div  id="tumbuhpesertajht" style=" margin: 0 auto"></div>
                              </div>
                              <div class="tab-pane" id="tab_9">
                              <div  id="tumbuhpesertajp" style=" margin: 0 auto"></div>
                              </div>
                              <!-- /.tab-pane -->
                              </div>
                              <!-- /.tab-content -->
                              </div> 
                              </div>
                              <!-- nav-tabs-custom -->
                      </div>
                
</div>



<div class="row">
  <div class="col-md-6">

                                    <div class="box box-primary">
                                    <div class="box-header with-border">
                                    <h3 class="box-title">Kepesertaan [5]</h3>

                                    <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                    </div>
                                    <div class="box-body">
                                    <div class="chart">
                                    <div  id="cakpendudukbekerja" style=" margin: 0 auto">
                                     
                                    </div>
                                    </div>
                                    </div>
                                    <!-- /.box-body -->
                                    </div>
                                
                               </div>

                               <div class="col-md-6">

                                    <div class="box box-primary">
                                    <div class="box-header with-border">
                                    <h3 class="box-title">Kepesertaan [6]</h3>

                                    <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                    </div>
                                    <div class="box-body">
                                    <div class="chart">
                                    <div  id="jhtaktif" style=" margin: 0 auto">
                                     
                                    </div>
                                    </div>
                                    </div>
                                    <!-- /.box-body -->
                                    </div>
                                
                               </div>

</div>


<script src="{{ asset('js/src/tabs.js') }}"></script>
<script>
  var myTabs1 = tabs({
    el: '#tabs1',
    tabNavigationLinks: '.c-tabs-nav__link',
    tabContentContainers: '.c-tab'
  });
  var myTabs2 = tabs({
    el: '#tabs2',
    tabNavigationLinks: '.c-tabs-nav__link',
    tabContentContainers: '.c-tab'
  });

  var myTabs3 = tabs({
    el: '#tabs3',
    tabNavigationLinks: '.c-tabs-nav__link',
    tabContentContainers: '.c-tab'
  });

  myTabs1.init();
  myTabs2.init();
  myTabs3.init();
  
</script>

