@extends('start');
@section('page_title', 'Dashboard')
@section('content_header')    
@stop
 
@section('content')
{!! Charts::assets() !!}
   <section class="content-header">
      <h1>
       Kepesertaan
        <small>Detail Indikator Kepesertaan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Kepesertaan</a></li>
        <li class="active">Detail</li>
      </ol>
    </section>
    <section class="content">

      
    @include('sismonev.aspek.kepesertaan.detail')
           
    </section>



   
@stop