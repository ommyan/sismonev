@extends('start');
@section('page_title', 'Dashboard')
@section('content_header')    
@stop

@section('content')
{!! Charts::assets() !!}
   <section class="content-header">
      <h1>
       Pembayaran Manfaat
        <small>Detail Indikator Pembayaran Manfaat</small>
      </h1>
          <h5> {{$sub_page_title}} </h5>
       <h5> {{$nk . ': ' . $nc}} </h5>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Pembayaran</a></li>
        <li class="active">Detail</li>
      </ol>
    </section>
    <section class="content">
    @include('sismonev.aspek.pembayaran.detail')
           
    </section>



   
@stop