@extends('start');
@section('content_header')    
@stop

@section('content')
   <section class="content-header">
   
      <h1>
        Resume
        <small>Ringkasan Indikator Monev</small>
      </h1>
      <h5> {{$sub_page_title}} </h5>
     @empty($nk)
        <h5> Nasional </h5>
      @else 
         <h5> {{'KANWIL'  . $nk . ' ' . $nc}} </h5>
      @endempty
      
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          <!-- The time line -->
          <ul class="timeline">
            <!-- timeline time label -->
           @include('sismonev.home.awal')
            {{-- @include('sismonev.aspek.pendapatan.resume')
               @include('sismonev.aspek.pembayaran.resume')
                 @include('sismonev.aspek.resume.organisasi')
                   @include('sismonev.aspek.resume.keuangan')--}}

          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
  
@stop
