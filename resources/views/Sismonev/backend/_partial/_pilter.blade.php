{!! Form::open() !!}

    <div class="form-group">
      {!! Form::select('kanwil',[''=>'--- Kanwil ---']+$kanwil,null,['class'=>'form-control']) !!}
    </div>

    <div class="form-group">
      <label>Select State:</label>
    </div>

    <div class="form-group">
      <button class="btn btn-success" type="submit">Go</button>
    </div>

    {!! Form::close() !!}

    <script type="text/javascript">
  $("select[name='id_kanwil']").change(function(){
      var id_kanwil = $(this).val();
      var token = $("input[name='_token']").val();
      $.ajax({
          url: "<?php echo route('select-ajax') ?>",
          method: 'POST',
          data: {id_kanwil:id_kacab, _token:token},
          success: function(data) {
            $("select[name='id_kacab'").html('');
            $("select[name='id_kacab'").html(data.options);
          }
      });
  });
</script>