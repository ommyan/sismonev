
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('/bower_components/AdminLTE/dist/img/user-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Thanh Nguyen</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      
      <!-- /.search form -->

      <!-- Sidebar Menu -->
       {{--  @if (Auth::check())  --}}
      <ul class="sidebar-menu">
        <li class="header">PROGRAM</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="/peserta/upload"> <span>Tenaga Kerja</span></a></li>
        <li class="active"><a href="#"> <span>Badan Usaha</span></a></li>
        <li class="active"><a href="#"> <span>Iuran</span></a></li>
        <li class="active"><a href="#"> <span>Manfaat</span></a></li>
        <li class="active"><a href="#"></i> <span>Keuangan</span></a></li>
        <li class="active"><a href="#"></i> <span>Organisasi</span></a></li>
        <li class="active"><a href="#"></i> <span>Referensi</span></a></li>   
      </ul>
      
      
      {{--  @endif  --}}
    
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

