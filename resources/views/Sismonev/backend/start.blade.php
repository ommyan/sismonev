<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>{{ $page_title or "AdminLTE Dashboard" }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
     <link rel="stylesheet" href="{{ asset('/bower_components/blueimp/css/style.css')}}">
    <!-- Bootstrap 3.3.6 -->
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('/bower_components/AdminLTE/dist/css/AdminLTE.css')}}">
    <!-- Material Design -->
    <link rel="stylesheet" href="{{ asset('/bower_components/responsive-tabs/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/bower_components/responsive-tabs/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('/bower_components/AdminLTE/dist/css/bootstrap-material-design.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/bower_components/AdminLTE/dist/css/ripples.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/bower_components/AdminLTE/dist/css/MaterialAdminLTE.css')}}">
   

    <link rel="stylesheet" href="{{ asset('/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}"/>
   

    <link rel="stylesheet" href="{{ asset('/bower_components/blueimp/css/jquery.fileupload.css')}}">

    <link rel="stylesheet" href="{{ asset('css/style.icomo.css') }}">

    <!-- MaterialAdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="{{ asset('/bower_components/AdminLTE/dist/css/skins/skin-red.css')}}">
    <link rel="stylesheet" href="{{ asset('/bower_components/AdminLTE/bootstrap/css/bootstrap.css')}}">
    {{--  <link rel="stylesheet" href="{{ asset('/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.css')}}">   --}}
    <link rel="stylesheet" href="{{ asset('/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css')}}"> 
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.0.2/css/responsive/2.0.2/css/responsive.bootstrap.min.css">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{{ asset('/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
    <script src="{{ asset('/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js')}}"></script>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-red sidebar-mini" style="margin-top: -25px;">
<div class="wrapper">

    <!-- Main Header -->
@include('Sismonev._partial._header')
@include('Sismonev._partial._logo')

<!-- Sidebar -->
@include('Sismonev._partial._sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title or "Page Title" }}
                <small>{{ $page_description or null }}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
        @yield('content')
        <!-- Your Page Content Here -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
@include('Sismonev._partial._footer')


<!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
    @include('Sismonev._partial._rightbar')
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->

<!-- Bootstrap 3.3.6 -->

<!-- Material Design -->
<script src="{{ asset('/bower_components/AdminLTE/dist/js/material.min.js')}}"></script>
<script src="{{ asset('/bower_components/AdminLTE/dist/js/ripples.min.js')}}"></script>
<script src="{{ asset('/bower_components/AdminLTE/dist/js/url.js')}}"></script>

<script type="text/javascript" src="{{ asset('/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{ asset('/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{ asset('/bower_components/blueimp/js/vendor/jquery.ui.widget.js')}}"></script>
<script src="{{ asset('/bower_components/blueimp/js/jquery.iframe-transport.js')}}"></script>
<script src="{{ asset('/bower_components/blueimp/js/jquery.fileupload.js')}}"></script>
<script src="{{ asset('/bower_components/blueimp/js/jquery.fileupload-process.js')}}"></script>
<script src="{{ asset('/bower_components/blueimp/js/jquery.fileupload-validate.js')}}"></script>
<script src="{{ asset('/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="https://datatables.yajrabox.com/js/handlebars.js"></script>


<script>
    $.material.init();
</script>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker7').datetimepicker({
            viewMode: 'years',
            format: 'MM/YYYY'
        });
    });
</script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<!-- AdminLTE App -->
<script src="{{ asset('/bower_components/AdminLTE/dist/js/app.min.js')}}"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->


</body>
</html>
