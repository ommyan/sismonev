@extends('Sismonev.backend.layouts.start');
@section('page_title', 'Dashboard')
@section('content_header')    
@stop
  {!! Charts::assets() !!}
@section('content')


    <div class="panel panel-primary">
 <div class="panel-heading">Managemen Data - Import Data dari Excel</div>
 @if (Session::has('message'))
   <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
  <div class="panel-body"> 
    

       {!! Form::open(array('route' => 'pesertaimport','method'=>'POST','files'=>'true')) !!}
        <div class="row">
           <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    {!! Form::label('sample_file','Select File to Import:',['class'=>'col-md-3']) !!}
                    <div class="col-md-9">
                    {!! Form::file('sample_file', array('class' => 'form-control')) !!}
                    {!! $errors->first('sample_file', '<p class="alert alert-danger">:message</p>') !!}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            {!! Form::submit('Upload',['class'=>'btn btn-primary']) !!}
            </div>
        </div>
       {!! Form::close() !!}
 </div>
</div>
@stop