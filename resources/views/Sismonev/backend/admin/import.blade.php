@extends('start');
@section('page_title', 'Dashboard')
@section('content_header')    
@stop
  {!! Charts::assets() !!}
@section('content')
   <section class="content-header">
      <h1>
       Import Data Kepesertaan
        <small>Administrasi Data Kepesertaan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Kepesertaan</a></li>
        <li class="active">Import</li>
      </ol>
    </section>
    <section class="content">
    @include('sismonev.aspek.kepesertaan.import')
           
    </section>



   
@stop