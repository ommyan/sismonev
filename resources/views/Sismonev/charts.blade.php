@extends('start');
@section('page_title', 'Dashboard')
@section('content_header')    
@stop

@section('content')
{!! Charts::assets() !!}
   <section class="content-header">
      <h1>
       Kepesertaan
        <small>Detail Indikator Kepesertaan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Kepesertaan</a></li>
        <li class="active">Detail</li>
      </ol>
    </section>
    <section class="content">
    <div class="col-md-6">
                  <!-- Custom Tabs (Pulled to the right) -->
                          <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs pull-right">
                                        <li class="active"><a href="#tab_1221" data-toggle="tab">JKK</a></li>
                                        <li><a href="#tab_2221" data-toggle="tab">JKM</a></li>
                                        <li><a href="#tab_3221" data-toggle="tab">JHT</a></li>
                                        <li><a href="#tab_4221" data-toggle="tab">JP</a></li>
                                        <li class="pull-left header"><i class="fa fa-th"></i> Pertumbuhan Badan Usaha </li>
                                        </ul>

                                  <div class="tab-content">
                                        <div class="tab-pane active" id="tab_1221">
                                        <div  id="" style="width: 300 margin: 0 auto"></div>
                                        {!! $kep031->render() !!}
                                        </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane " id="tab_2221">
                                        <div  id="" style="width: 300 margin: 0 auto"></div>
                                        {!! $kep032->render() !!}
                                        </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane" id="tab_3221">
                                        <div  id="" style="width: 300 margin: 0 auto"></div>
                                        {!! $kep033->render() !!}
                                    </div>
                                        <!-- /.tab-pane -->
                                        <div class="tab-pane" id="tab_4221">
                                        <div  id="" style="width: 300 margin: 0 auto"></div>
                                        {!! $kep034->render() !!}
                                    </div>       
                                        <!-- /.tab-pane -->
                                  </div>
                                        <!-- /.tab-content -->
                            </div>
                                        <!-- nav-tabs-custom -->
          </div>  





           
    </section>



   
@stop