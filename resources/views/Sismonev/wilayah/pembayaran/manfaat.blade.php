@extends('start');
@section('page_title', 'Dashboard')
@section('content_header')    
@stop
 
@section('content')
{!! Charts::assets() !!}
   <section class="content-header">
      <h1>
       Pembayaran Manfaat Program {{$mprogram}}
        <small>Table Pembayaran Manfaat</small>
      </h1>
      <h5>
         <h5> {{$sub_page_title}} </h5>
       <h5> {{$nk . ': ' . $nc}} </h5>
      </h5>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Iuran</a></li>
        <li class="active">Detail</li>
      </ol>
    </section>
    <section class="content">
          @include('Sismonev.wilayah.pembayaran.data');           
    </section>
@stop

@section('scripts')

@stop