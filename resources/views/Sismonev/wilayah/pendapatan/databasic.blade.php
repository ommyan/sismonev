<style>


td.details-control {
    background: url('../resources/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.details td.details-control {
    background: url('../resources/details_close.png') no-repeat center center;
}
</style>

<div class="container-fluid">
	
	<div class="table-responsive">
		<table class="table table-striped table-bordered" width="100%" id="peserta">
			<thead>
				<tr>
				<th></th>	
				<th>ID Kanwil</th>
				<th>Kedeputian</th>
				<th>PPU</th>
				<th>BPU</th>
				<th>JAKON</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<script type="text/javascript">
//'ID_KANWIL','ID_CABANG','JUMLAH_PESERTA','JUMLAH_PTNIK','JUMLAH_KARTU_TERBIT'

function format ( d ) {
    // `d` is the original data object for the row
    return '<div class="slider">'+
        '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
            '<tr>'+
                '<td>Full name:</td>'+
                '<td>'+d.name+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td>Extension number:</td>'+
                '<td>'+d.extn+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td>Extra info:</td>'+
                '<td>And any further details here (images etc)...</td>'+
            '</tr>'+
        '</table>'+
    '</div>';
}

$(document).ready(function() {

  var table =  $('#peserta').DataTable({
    	"columnDefs": [
            { className: "text-right", "targets": [3,4,5] }],
        processing: true,
        serverSide: true,
    	responsive: true,
		
        ajax: '{!! url('peserta.data/3/2017') !!}',
        columns: [
        	{
                "className":      'details-control',
                "orderable":      false,
                "searchable":     false,
                "data":           null,
                "defaultContent": ''
            },
            { data: 'ID' , name: 'ID',width:100 },
            { data: 'KANWIL', name: 'KANWIL' },
			{ data: 'PPUJKK', name: 'PPUJKK' },
			{ data: 'BPUJKK', name: 'BPUJKK' },
			{ data: 'JAKONJKK', name: 'JAKONJKK' }

			
        ],
         "order": [[1, 'asc']]
    });

 $('#peserta tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            $('div.slider', row.child()).slideUp( function () {
                row.child.hide();
                tr.removeClass('shown');
            } );
        }
        else {
            // Open this row
            row.child( format(row.data()), 'no-padding' ).show();
            tr.addClass('shown');
 
            $('div.slider', row.child()).slideDown();
        }
    } );


});

</script>