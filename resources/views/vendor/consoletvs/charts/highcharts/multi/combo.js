Highcharts.chart('container', {
    title: {
        text: 'Cakupan Peserta Per Program'
    },
    xAxis: {
        categories: ['JKK', 'JKM', 'JHT', 'JP']
    },
    labels: {
        items: [{
            html: '% Cakupan Peserta',
            style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            }
        }]
    },
    series: [{
        type: 'column',
        name: 'BPU',
        data: [3, 2, 1, 3]
    }, {
        type: 'column',
        name: 'PPU',
        data: [2, 3, 5, 7]
    }, {
        type: 'column',
        name: 'JASKON',
        data: [4, 3, 3, 9]
    },
        {
        type: 'line',
        name: 'Target',
        data: [3, 2.67, 3, 6.33],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        },
        
    }]
});

