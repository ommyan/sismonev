<script type="text/javascript">

    $(function () {
        	Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
    		return Highcharts.Color(color)
    			.setOpacity(0.5)
    			.get('rgba');
    	});
      
        var {{ $model->id }} =; new Highcharts.Chart({
            chart: {
                renderTo:  "{{ $model->id }}",
                @include('charts::_partials.dimension.js2')
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'column'
            },
            @if($model->title)
                title: {
                    text:  "{!! $model->title !!}"
                },
            @endif
            @if(!$model->credits)
                credits: {
                    enabled: false
                },
            @endif
            plotOptions: {
               column: {
                   grouping: false,
                   shadow: false,
                   borderWidth: 0,
                   dataLabels: {
                   enabled: true,
                   format: '{!! $model->format !!}'
                  }
               }
           },
           xAxis: [{
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        crosshair: true
                 }],
            
        yAxis: [{ // Primary yAxis
        labels: {
            format: '',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },
        title: {
            text: 'Jumlah Peserta',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        }
    }, { // Secondary yAxis
        title: {
            text: 'Pertumbuhan',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        labels: {
            format: '{value}',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        opposite: true
    }],
            legend: {
                @if(!$model->legend)
                    enabled: false,
                @endif
            },
            series: [
               
                
                @for ($i = 0; $i < count($model->datasets); $i++)
                    {
                        
                        name:  "{!! $model->datasets[$i]['label'] !!}", 
                        type: "{{ $model->types[$i] }}",
                        @if($i==1)
                        yAxis: 1,
                        @endif
                        @if($model->colors && count($model->colors) > $i)
                            color: "{{ $model->colors[$i] }}",
                        @endif
                       
                        data: [
                            @foreach($model->datasets[$i]['values'] as $dta)
                                {{ $dta }},
                            @endforeach
                        ]    
                             
                    },
                    
                @endfor
            
        ]
        })
    });
</script>

@if(!$model->customId)
    @include('charts::_partials.container.div')
@endif

