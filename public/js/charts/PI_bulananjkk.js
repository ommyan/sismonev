$(document).ready(function () {
    Highcharts.setOptions({
        lang: {
            decimalPoint: ',',
            thousandsSep: '.'
        }
    });
    Highcharts.chart('pijkk', {
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Pembayaran Klaim Program JKK'
        },
        subtitle: {
            text: 'Menurut Jenis Klaim Tahun 2017'
        },
        xAxis: [{
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'Penerimaan Iuran',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: 'Pertumbuhan',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value} %',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        plotOptions: {
            column: {
                grouping: false,
                shadow: false,
                borderWidth: 0,
                dataLabels: {
                    enabled: true
                },
            }

        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 0,
            verticalAlign: 'top',
            y: 0,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [
            {
                name: 'JKK',
                type: 'column',
                color: 'rgb(255, 64, 0)',
                data: [0, 550727575955.76, 956367780167.137, 1324216570513.47, 1709787515247.71, 0, 0, 0, 0, 0, 0, 0],
                tooltip: {
                    valueSuffix: ''
                }
            },

            {
                name: 'Pertumbuhan',
                type: 'spline',
                yAxis: 1,
                color: 'rgb(102, 102, 255)',
                data: [0, 0, 73.65, 38.46, 29.11, 0, 0, 0, 0, 0, 0, 0],


                tooltip: {
                    valueSuffix: ' %'
                }

            }]
    })
});