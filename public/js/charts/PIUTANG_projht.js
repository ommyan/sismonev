Highcharts.setOptions({
    lang: {
        decimalPoint: ',',
        thousandsSep: '.'
    }
});
Highcharts.chart('piutangJHT', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'PIUTANG IURAN PROGRAM JHT'
    },
    xAxis: {
        categories: ['Lancar', 'Kurang Lancar', 'Macet']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Piutang Iuran berdasarkan Umur Piutang'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            dataLabels: {
                enabled: true
            }
        },

    },
    series: [{
        name: 'JHT',
        data: [166620338074.56, 141439536144.18, 150849775503.99]
    }]
});