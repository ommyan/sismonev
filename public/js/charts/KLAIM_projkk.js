Highcharts.setOptions({
    lang: {
        decimalPoint: ',',
        thousandsSep: '.'
    }
});
Highcharts.chart('klaimjkk', {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: 'Pembayaran Klaim Program JKK'
    },
    subtitle: {
        text: 'Menurut Jenis Klaim Tahun 2017'
    },
    xAxis: [{
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        crosshair: true
    }],
    yAxis: [{ // Primary yAxis
        labels: {
            format: '',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },
        title: {
            text: 'Penerimaan Iuran',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        }
    }, { // Secondary yAxis
        title: {
            text: 'Pertumbuhan',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        labels: {
            format: '{value} %',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        opposite: true
    }],
    tooltip: {
        shared: true
    },
    plotOptions: {
        column: {
            grouping: false,
            shadow: false,
            borderWidth: 0,
            dataLabels: {
                enabled: true
            },
        }

    },
    legend: {
        layout: 'horizontal',
        align: 'right',
        x: 0,
        verticalAlign: 'bottom',
        y: 0,
        floating: true,
        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
    },
    series: [
        {
            name: 'Cacat Fungsi',
            type: 'spline',
            yAxis: 1,
            color: 'rgb(102, 102, 255)',
            data: [0, 0, 8410070837, 7499750665, 5684161754, 8017040052, 0, 0, 0, 0, 0, 0, 0],
        }, {
            name: 'Cacat Sebagian',
            type: 'spline',
            yAxis: 1,
            color: 'rgb(102, 102, 255)',
            data: [0, 0, 78484699.83, 6165639480.07, 3933291413.3, 4506006077.46, 0, 0, 0, 0, 0, 0, 0],
        }, {
            name: 'Cacat Total Tetap',
            type: 'spline',
            yAxis: 1,
            color: 'rgb(102, 102, 255)',
            data: [0, 0, 4630275472.22, 183072324, 59436486.4, 133056755.49, 0, 0, 0, 0, 0, 0, 0],
        },
        {
            name: 'COB dengan Asuransi Lain',
            type: 'spline',
            yAxis: 1,
            color: 'rgb(102, 102, 255)',
            data: [0, 0, 32936584686.01, 36481115180.47, 22050701883.5, 34558901125.88, 0, 0, 0, 0, 0, 0, 0],
        },
        {
            name: 'Masih Pengobatan',
            type: 'spline',
            yAxis: 1,
            color: 'rgb(102, 102, 255)',
            data: [0, 0, 14886992685.55, 16287487575.69, 12411102369.5, 14068956493.67, 0, 0, 0, 0, 0, 0, 0],
        }, {
            name: 'Meninggal',
            type: 'spline',
            yAxis: 1,
            color: 'rgb(102, 102, 255)',
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        }, {
            name: 'RTW',
            type: 'spline',
            yAxis: 1,
            color: 'rgb(102, 102, 255)',
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        }, {
            name: 'Sembuh Tanpa Cacat',
            type: 'spline',
            yAxis: 1,
            color: 'rgb(102, 102, 255)',
            data: [0, 0, 18419551056.99, 23604943818.16, 20044496232.07, 22850373357.39, 0, 0, 0, 0, 0, 0, 0],

            tooltip: {
                valueSuffix: ' %'
            }

        }]
});