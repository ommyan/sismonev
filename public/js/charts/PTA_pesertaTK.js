var config = {
    type: 'bar',
    data: {
        labels: ["BPU", "PPU", "Jakon"],
        datasets: [{
            type: 'bubble',
            pointPlacement: -0.2,
            label: 'Potensi',
            data: [56, 85, 40],
            borderColor: 'black',
            fill: false
        }, {
            type: 'bar',
            label: 'Target',
            backgroundColor: "red",
            data: [65, 0, 80],
        }, {
            type: 'bar',
            label: 'Aktual',
            backgroundColor: "blue",
            data: [60, 5, 85]
        }]
    },
    options: {
        scales: {
            xAxes: [{
                stacked: true,

            }],
            yAxes: [{
                stacked: true,
                barPercentage: 0.2
            }]
        }
    }
};

var ctx = document.getElementById("myChart").getContext("2d");
new Chart(ctx, config);