Highcharts.setOptions({
    lang: {
        decimalPoint: ',',
        thousandsSep: '.'
    }
});
Highcharts.chart('manfaatjp', {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: 'Pembayaran Jaminan Kecelakaan Kerja'
    },
    subtitle: {
        text: 'Tahun 2017'
    },
    xAxis: [{
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        crosshair: true
    }],
    yAxis: [{ // Primary yAxis
        labels: {
            format: '',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },
        title: {
            text: 'Pembayaran',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        }
    }, { // Secondary yAxis
        title: {
            text: 'Pertumbuhan',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        labels: {
            format: '{value} %',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        opposite: true
    }],
    tooltip: {
        shared: true
    },
    plotOptions: {
        column: {
            grouping: false,
            shadow: false,
            borderWidth: 0,
            dataLabels: {
                enabled: true
            },
        }

    },
    legend: {
        layout: 'vertical',
        align: 'left',
        x: 0,
        verticalAlign: 'top',
        y: 0,
        floating: true,
        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
    },
    series: [
        {
            name: 'Jaminan Kecelakaan Kerja',
            type: 'column',
            color: 'rgb(255, 153, 0)',
            data: [0, 7136717130.07, 10638590942.11, 5556602137.95, 7254648668.96, 0, 0, 0, 0, 0, 0, 0],
            tooltip: {
                valueSuffix: ''
            }
        },

        {
            name: 'Pertumbuhan',
            type: 'spline',
            yAxis: 1,
            color: 'rgb(102, 102, 255)',
            data: [0, 0, 49.06, -47.76, 30.55, 0, 0, 0, 0, 0, 0, 0, 0, 0],


            tooltip: {
                valueSuffix: ' %'
            }

        }]
});