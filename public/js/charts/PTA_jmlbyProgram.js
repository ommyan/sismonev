$(document).ready(function () {


    var url = 'data/peserta/program/2/3/2017';
    $.getJSON(url, function (result) {
            var item = '';
            var data = [];
            var labels = [];

            for (i = 0; i < result.length;) {

                data.push(result[i]['JUMLAH']);
                labels.push(result[i]['SEGMEN']);
                i++;
            }


            /*
            var myData=[891514,14636340,7095521];
            var dati=[];
            var mySeries = [];
                for (var i = 0; i < myData.length; i++) {
                    mySeries.push(myData[i], myData[i + 1]);
                    i++
                }
            */

            Highcharts.setOptions({
                lang: {
                    decimalPoint: ',',
                    thousandsSep: '.'
                }
            });

            var chart = new Highcharts.chart('jlmpesertasegmen', {


                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Jumlah Peserta Per Segmen Program JKK Mei 2017'
                },
                xAxis: {
                    categories: labels // ['PBPU','PPU','JAKON']
                },
                yAxis: [{
                    min: 0,
                    title: {
                        text: 'Jumlah'
                    }
                }, {
                    title: {
                        text: 'Segmen'
                    },
                    opposite: true
                }],
                legend: {
                    shadow: false
                },
                tooltip: {
                    shared: true
                },
                plotOptions: {
                    column: {
                        grouping: false,
                        shadow: false,
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true

                        }
                    },


                },


                series: [
                    {
                        type: 'column',
                        name: 'Potensi',
                        color: 'rgb(204, 0, 0)',
                        data: [1891514, 24636340, 9095521],
                        pointWidth: 35,
                        pointPadding: 0.3,
                        pointPlacement: -0.2,

                    },
                    {
                        name: 'Target',
                        color: 'rgb(255, 0, 0)',
                        data: [991514, 17636340, 9095521],
                        pointPadding: 0.5,
                        pointPlacement: -0.2,
                        pointWidth: 15
                    },
                    {
                        name: 'Aktual',
                        color: 'rgb(255, 245, 230,0.3)',
                        data: data, // [891514,14636340,7095521],
                        pointPadding: 0.4,
                        pointPlacement: -0.2,
                        pointWidth: 25
                    },
                ]

            });
            console.log(data);
            console.log(labels);

        }
    );
});