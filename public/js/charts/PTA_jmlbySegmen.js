$(function (program) {
    $(document).ready(function () {

        var url = 'data/peserta/program/All/3/2017';
        var program = 1;
        $.getJSON(url, function (json) {
            // alasql('SELECT * INTO XLSX("' + my_filename + '",{headers:true}) FROM;
            var data = alasql('select SEGMEN,JUMLAH from ? ', [json]);
            var segmen = [];
            var jumlah = [];
            // console.log(data);
            $.each(data, function (key, value) {
                segmen.push(value.SEGMEN);
                jumlah.push(value.JUMLAH);
            });
            //  console.log(segmen);
            //  console.log(jumlah);

            Highcharts.setOptions({
                lang: {
                    decimalPoint: ',',
                    thousandsSep: '.'
                }
            });

            Highcharts.chart('jmlpesertasegmen', {


                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Jumlah Peserta Per Segmen Program JKK Mei 2017'
                },
                xAxis: {
                    categories: segmen //['PBPU','PPU','JAKON']
                },
                yAxis: [{
                    min: 0,
                    title: {
                        text: 'Jumlah'
                    }
                }, {
                    title: {
                        text: 'Segmen'
                    },
                    opposite: true
                }],
                legend: {
                    shadow: false
                },
                tooltip: {
                    shared: true
                },
                plotOptions: {
                    column: {
                        grouping: false,
                        shadow: false,
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true

                        }
                    },


                },


                series: [

                    {
                        name: 'Target',
                        color: 'rgb(255, 0, 0)',
                        data: [991514, 17636340, 9095521],
                        pointPadding: 0.5,
                        //  pointPlacement: -0.2,
                        pointWidth: 15
                    },
                    {
                        name: 'Aktual',
                        color: 'rgb(255, 245, 230,0.8)',
                        data: jumlah, //[891514,14636340,7095521],
                        pointPadding: 0.4,
                        //  pointPlacement: -0.2,
                        pointWidth: 25
                    },
                ]

            })
        })
    })

}); //doc ready