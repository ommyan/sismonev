$(document).ready(function () {
    Highcharts.setOptions({
        lang: {
            decimalPoint: ',',
            thousandsSep: '.'
        }
    });
    Highcharts.chart('piutangJKK', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'PIUTANG IURAN PROGRAM JKK'
        },
        xAxis: {
            categories: ['Lancar', 'Kurang Lancar', 'Macet']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Piutang Iuran berdasarkan Umur Piutang'
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            },

        },
        series: [{
            name: 'JKK',
            data: [20565461140.09, 24054702813.33, 27301045699.91]
        }]
    })
});