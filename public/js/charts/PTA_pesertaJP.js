Highcharts.setOptions({
    lang: {
        decimalPoint: ',',
        thousandsSep: '.'
    }
});


Highcharts.chart('containerjp', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Jumlah Peserta Per Segmen Program JP'
    },
    xAxis: {
        categories: [
            'PBPU',
            'PPU',
            'JAKON'
        ]
    },
    yAxis: [{
        min: 0,
        title: {
            text: 'Jumlah'
        }
    }, {
        title: {
            text: 'Segmen'
        },
        opposite: true
    }],
    legend: {
        shadow: false
    },
    tooltip: {
        shared: true
    },
    plotOptions: {
        column: {
            grouping: false,
            shadow: false,
            borderWidth: 0,
            dataLabels: {
                enabled: true
            }
        }
    },
    series: [{
        name: 'Aktual',
        color: 'rgb(153, 230, 255)',
        data: [0, 9677701, 0],
        pointPadding: 0.3,
        pointPlacement: -0.2,
        pointWidth: 25
    }, {
        name: 'Target',
        color: 'rgb(153, 153, 255)',
        data: [0, 0, 0],
        pointPadding: 0.4,
        pointPlacement: -0.2,
        pointWidth: 15
    }, {
        type: 'bubble',
        pointPlacement: -0.2,
        name: 'Potensi',
        data: [0, 0, 0],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        }
    }
    ]
});