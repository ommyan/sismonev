Highcharts.setOptions({
    lang: {
        decimalPoint: ',',
        thousandsSep: '.'
    }
});
Highcharts.chart('piutangJKM', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'PIUTANG IURAN PROGRAM JKM'
    },
    xAxis: {
        categories: ['Lancar', 'Kurang Lancar', 'Macet']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Piutang Iuran berdasarkan Umur Piutang'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            dataLabels: {
                enabled: true
            }
        },

    },
    series: [{
        name: 'JKM',
        data: [10546873965.73, 9816777411.10, 10160007185.35]
    }]
});