Highcharts.chart('tumbuhpesertajkk', {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: 'Pertumbuhan Peserta Program Jaminan Kecelakaan Kerja'
    },
    subtitle: {
        text: 'Tahun 2017'
    },
    xAxis: [{
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        crosshair: true
    }],
    yAxis: [{ // Primary yAxis
        labels: {
            format: '',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },
        title: {
            text: 'Jumlah Peserta',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        }
    }, { // Secondary yAxis
        title: {
            text: 'Pertumbuhan',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        labels: {
            format: '{value} %',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        opposite: true
    }],
    tooltip: {
        shared: true
    },
    plotOptions: {
        column: {
            grouping: false,
            shadow: false,
            borderWidth: 0,
            dataLabels: {
                enabled: true
            },
        }

    },
    legend: {
        layout: 'vertical',
        align: 'left',
        x: 0,
        verticalAlign: 'bottom',
        y: 0,
        floating: true,
        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
    },
    series: [
        {
            name: 'JKK',
            type: 'column',
            color: 'rgb(0, 191, 255)',
            data: [0, 22165550, 22454299, 22473584, 22623375, 0, 0, 0, 0, 0, 0, 0],
            tooltip: {
                valueSuffix: ''
            }
        },

        {
            name: 'Pertumbuhan',
            type: 'spline',
            yAxis: 1,
            color: 'rgb(255, 87, 51)',
            data: [0, 0, 1.30, 0.08, 0.66, 0, 0, 0, 0, 0, 0, 0],


            tooltip: {
                valueSuffix: ' %'
            }

        }]
});