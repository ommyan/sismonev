$(document).ready(function () {
    Highcharts.setOptions({
        lang: {
            decimalPoint: ',',
            thousandsSep: '.'
        }
    });
    var myData = [891514, 14636340, 7095521];
    var dati = [];
    var mySeries = [];
    for (var i = 0; i < myData.length; i++) {
        mySeries.push(myData[i], myData[i + 1]);
        i++
    }


//console.log(mySeries);
    var chart = new Highcharts.chart('containerjkk', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Jumlah Peserta Per Segmen Program JKK Mei 2017'
        },
        xAxis: {
            categories: [
                'PBPU',
                'PPU',
                'JAKON'
            ]
        },
        yAxis: [
            {
                plotLines: [
                    {
                        value: [40000000],
                        color: 'red',
                        dashStyle: 'shortdash',
                        width: 2,
                        label: {
                            text: 'Potensi Pekerja Penerima Upah'
                        }
                    },
                    {
                        value: [24636340],
                        color: 'green',
                        dashStyle: 'shortdash',
                        width: 2,
                        label: {
                            text: 'Potensi Pekerja formal'
                        }
                    },
                    {
                        value: [9095521],
                        color: 'black',
                        dashStyle: 'shortdash',
                        width: 2,
                        label: {
                            text: 'Potensi Pekerja Jakon'
                        }
                    }

                ]
            },


            {
                min: 0,
                title: {
                    text: 'Jumlah'
                }
            }, {
                title: {
                    text: 'Segmen'
                },
                opposite: true
            }],
        legend: {
            shadow: false
        },
        tooltip: {
            shared: true
        },
        plotOptions: {
            column: {
                grouping: false,
                shadow: false,
                borderWidth: 0,
                dataLabels: {
                    enabled: true

                }
            },


        },

        series: [
            /*
            {
            type: 'bubble',
            name: 'Potensi',
            color: 'rgb(204, 0, 0)',
            data: [1891514,24636340,9095521],
            pointWidth: 35,
            pointPadding: 0.3,
            pointPlacement: -0.2,
            marker: {
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[3],
                fillColor: 'white'
            }

        },
        */
            {
                name: 'Target',
                color: 'rgb(255, 0, 0)',
                data: [991514, 17636340, 9095521],
                pointPadding: 0.5,
                pointPlacement: -0.2,
                pointWidth: 15
            },
            {
                name: 'Aktual',
                color: 'rgb(255, 214, 204,.8)',
                //      data: mySeries,
                pointPadding: 0.4,
                pointPlacement: -0.2,
                pointWidth: 25
            },
        ]
    })
});