Highcharts.setOptions({
    lang: {
        decimalPoint: ',',
        thousandsSep: '.'
    }
});


Highcharts.chart('containerjht', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Jumlah Peserta Per Segmen Program JHT'
    },
    xAxis: {
        categories: [
            'PBPU',
            'PPU',
            'JAKON'
        ]
    },
    yAxis: [{
        min: 0,
        title: {
            text: 'Jumlah'
        }
    }, {
        title: {
            text: 'Segmen'
        },
        opposite: true
    }],
    legend: {
        shadow: false
    },
    tooltip: {
        shared: true
    },
    plotOptions: {
        column: {
            grouping: false,
            shadow: false,
            borderWidth: 0,
            dataLabels: {
                enabled: true
            },
        }

    },
    series: [{
        name: 'Aktual',
        color: 'rgb(255, 255, 153)',
        data: [108735, 13830580, 0],
        pointPadding: 0.3,
        pointPlacement: -0.2,
        pointWidth: 25
    }, {
        name: 'Target',
        color: 'rgb(255, 204, 153)',
        data: [0, 0, 0],
        pointPadding: 0.4,
        pointPlacement: -0.2,
        pointWidth: 15
    }, {
        type: 'spline',
        name: 'Potensi',
        data: [0, 0, 0],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        }
    }
    ]
});