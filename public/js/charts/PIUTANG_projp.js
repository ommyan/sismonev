Highcharts.setOptions({
    lang: {
        decimalPoint: ',',
        thousandsSep: '.'
    }
});
Highcharts.chart('piutangJP', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'PIUTANG IURAN PROGRAM JP'
    },
    xAxis: {
        categories: ['Lancar', 'Kurang Lancar', 'Macet']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Piutang Iuran berdasarkan Umur Piutang'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            dataLabels: {
                enabled: true
            }
        },

    },
    series: [{
        name: 'JP',
        data: [43578610465.41, 36814956005.27, 25906402881.89]
    }]
});