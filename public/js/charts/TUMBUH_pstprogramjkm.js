Highcharts.chart('tumbuhpesertajkm', {


    chart: {
        zoomType: 'xy'
    },

    title: {
        text: 'Pertumbuhan Peserta Program Jaminan Kematian'
    },
    subtitle: {
        text: 'Tahun 2017'
    },

    xAxis: [{
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        crosshair: true
    }],
    yAxis: [{ // Primary yAxis
        labels: {
            format: '',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },
        title: {
            text: 'Jumlah Peserta',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        }
    }, { // Secondary yAxis
        title: {
            text: 'Pertumbuhan',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        labels: {
            format: '{value} %',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        opposite: true
    }],
    tooltip: {
        shared: true
    },
    plotOptions: {
        column: {
            grouping: false,
            shadow: false,
            borderWidth: 0,
            dataLabels: {
                enabled: true
            },
        }

    },
    legend: {
        layout: 'vertical',
        align: 'left',
        x: 0,
        verticalAlign: 'top',
        y: 0,
        floating: true,
        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
    },


    series: [
        {
            name: 'Jaminan Kematian',
            type: 'column',
            color: 'rgb(0, 191, 255)',
            data: [0, 22165550, 22454299, 22473584, 15527854, 0, 0, 0, 0, 0, 0, 0],
            tooltip: {
                valueSuffix: ''
            }
        },

        {
            name: 'Pertumbuhan',
            type: 'spline',
            yAxis: 1,
            color: 'rgb(255, 87, 51)',
            data: [0, 1.3, 1.30, 0.08, -30.90, 0, 0, 0, 0, 0, 0, 0, 0],


            tooltip: {
                valueSuffix: ' %'
            }

        }]

});