Highcharts.setOptions({
    lang: {
        decimalPoint: ',',
        thousandsSep: '.'
    }
});


Highcharts.chart('containerbujkm', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Jumlah Badan Usaha Per Skala Usaha Program JHT'
    },
    xAxis: {
        categories: [
            'Skala Besar',
            'Skala Menengah',
            'Skala Kecil',
            'Skala Micro'
        ]
    },
    yAxis: [{
        min: 0,
        title: {
            text: 'Jumlah'
        }
    }, {
        title: {
            text: 'Segmen'
        },
        opposite: true
    }],
    legend: {
        shadow: false
    },
    tooltip: {
        shared: true
    },
    plotOptions: {
        column: {
            grouping: false,
            shadow: false,
            borderWidth: 0,
            dataLabels: {
                enabled: true
            },
        }

    },
    series: [{
        name: 'Aktual',
        color: 'rgb(255, 128, 0)',
        data: [9328, 125031, 79426, 181899],
        pointPadding: 0.3,
        pointPlacement: -0.2,
        pointWidth: 25
    }, {
        name: 'Target',
        color: 'rgb(255, 204, 153)',
        data: [11193, 150037, 95311, 218278],
        pointPadding: 0.4,
        pointPlacement: -0.2,
        pointWidth: 15
    }, {
        type: 'bubble',
        name: 'Potensi',
        data: [0, 0, 0],
        pointPlacement: -0.2,
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        }
    }
    ]
});