$(function () {
    var categories = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var data = [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4];
    var colors = ['#7cb5ec', '#434348', '#90ed7d', '#f7a35c', '#8085e9', '#f15c80', '#e4d354', '#8085e8', '#8d4653', '#91e8e1'];
    var seriesData = [];

    data.forEach(function (val, i) {
        seriesData.push({
            name: categories[i],
            color: colors[(i % colors.length)],
            data: [{
                x: i,
                y: val
            }]
        });
    });

    $('#container').highcharts({
        chart: {
            type: 'bar'
        },
        xAxis: {
            categories: categories
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        tooltip: {
            headerFormat: ''
        },
        series: seriesData
    });
});