Highcharts.setOptions({
    lang: {
        decimalPoint: ',',
        thousandsSep: '.'
    }
});


var options = new Highcharts.chart('containerbujkk', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Jumlah Badan Usaha Per Skala Usaha Program JKK'
    },
    xAxis: {
        categories: [
            'Skala Besar',
            'Skala Menengah',
            'Skala Kecil',
            'Skala Micro'
        ]
    },
    yAxis: [
        {
            plotLines: [
                {
                    value: [11328],
                    color: 'red',
                    dashStyle: 'shortdash',
                    width: 2,
                    label: {
                        text: 'Potensi Badan Usaha Skala Besar'
                    }
                },
                {
                    value: [145031],
                    color: 'green',
                    dashStyle: 'shortdash',
                    width: 2,
                    label: {
                        text: 'Potensi Badan Usaha Skala Menengah'
                    }
                },
                {
                    value: [99426],
                    color: 'blue',
                    dashStyle: 'shortdash',
                    width: 2,
                    label: {
                        text: 'Potensi Badan Usaha Skala Kecil'
                    }
                },
                {
                    value: [191899],
                    color: 'black',
                    dashStyle: 'shortdash',
                    width: 2,
                    label: {
                        text: 'Potensi Badan Usaha Skala Menengah'
                    }
                }
            ]
        },

        {
            min: 0,
            title: {
                text: 'Jumlah'
            }
        }, {
            title: {
                text: 'Segmen'
            },
            opposite: true,

        },
    ],


    legend: {
        shadow: false
    },
    tooltip: {
        shared: true
    },
    plotOptions: {
        column: {
            grouping: false,
            shadow: false,
            borderWidth: 0,
            dataLabels: {
                enabled: true
            },
        }

    },


    series: [
        {
            name: 'Target',
            color: 'rgb(0, 191, 255)',
            data: [11193, 150037, 95311, 218278],
            pointPadding: 0.4,
            pointPlacement: -0.2,
            pointWidth: 15
        },

        {
            name: 'Aktual',
            color: 'rgb(5, 128, 255,0.3)',
            data: [9328, 125031, 79426, 181899],
            pointPadding: 0.3,
            pointPlacement: -0.2,
            pointWidth: 25
        }

    ]
});
