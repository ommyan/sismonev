Highcharts.setOptions({
    lang: {
        decimalPoint: ',',
        thousandsSep: '.'
    }
});


chart = new Highcharts.chart('#containeriuranbpu', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Penerimaan Iuran Per Segmen Mei 2017'
    },
    xAxis: {
        categories: [
            'Pekerja Bukan Penerima Upah',
            'Pekerja Penerima Upah',
            'Jasa Konstruksi'
        ]
    },
    yAxis: [{
        min: 0,
        title: {
            text: 'Jumlah'
        }
    }, {
        title: {
            text: 'Segmen'
        },
        opposite: true
    }],
    legend: {
        shadow: false
    },
    tooltip: {
        shared: true
    },
    plotOptions: {
        column: {
            grouping: false,
            shadow: false,
            borderWidth: 0,
            dataLabels: {
                enabled: true
            },
        }

    },
    series: [{
        name: 'Aktual',
        color: 'rgb(255, 255, 153)',
        data: [27223248981, 1577306486577, 105257779689],
        pointPadding: 0.3,
        pointPlacement: -0.2,
        pointWidth: 25
    }, {
        name: 'Target',
        color: 'rgb(255, 204, 153)',
        data: [0, 0, 0],
        pointPadding: 0.4,
        pointPlacement: -0.2,
        pointWidth: 15
    }, {
        type: 'spline',
        name: 'Potensi',
        data: [0, 0, 0],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        }
    }
    ]
});