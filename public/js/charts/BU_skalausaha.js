$(function () {


    var chart;
    $(document).ready(function () {

        var ub = [];
        var um = [];
        var uk = [];
        var umk = [];

        var labels = [];
        var cat = [];
        var url = 'data/BU/skala/All/2/2017';
        $.getJSON(url, function (json) {


            var groupBy = function (xs, key) {
                return xs.reduce(function (rv, x) {
                    (rv[x[key]] = rv[x[key]] || []).push(x);
                    return rv;
                }, {});
            };
            var datajson = groupBy(json, 'SKALA');
            var datajs = Object.keys(datajson); // return ["TeamA","TeamB"]
            var count = Object.keys(datajson).length;

            var data = _.filter(json, {'PROGRAM': 'JKK'});
            console.log(data);
            $.each(data, function (key, value) {
                ub.push(value.JUMLAH);
            });

            var data = _.filter(json, {'PROGRAM': 'JKM'});
            $.each(data, function (key, value) {
                um.push(value.JUMLAH);
            });

            var data = _.filter(json, {'PROGRAM': 'JHT'});
            $.each(data, function (key, value) {
                uk.push(value.JUMLAH);
            });
            var data = _.filter(json, {'PROGRAM': 'JP'});
            $.each(data, function (key, value) {
                umk.push(value.JUMLAH);
            });


            datajs.sort();
            chart = new Highcharts.chart('skalabu', {

                chart: {
                    type: 'column'
                },

                title: {
                    text: 'Jumlah Badan Usaha Per Skala Usaha'
                },

                xAxis: {
                    categories: datajs // ['SKALA BESAR','SKALA MENENGAH', 'SKALA KECIL', 'SKALA MIKRO']
                },

                yAxis: {
                    allowDecimals: false,
                    min: 0,
                    title: {
                        text: 'Jumlah Peserta'
                    }
                },

                tooltip: {
                    formatter: function () {
                        return '<b>' + this.x + '</b><br/>' +
                            this.series.name + ': ' + this.y + '<br/>';
                    }
                },

                plotOptions: {
                    column: {
                        dataLabel: true
                    }
                },


                // series: json
                series: [
                    {
                        name: ['JKK'],
                        data: ub
                    },
                    {
                        name: ['JKM'],
                        data: um
                    },
                    {
                        name: ['JHT'],
                        data: uk
                    },
                    {
                        name: ['JP'],
                        data: umk
                    }
                ]
            });

        });

    });
});