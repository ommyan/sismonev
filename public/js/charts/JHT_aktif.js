$(function () {

    $(document).ready(function () {
        Highcharts.setOptions({
            lang: {
                decimalPoint: ',',
                thousandsSep: '.'
            }
        });


        chart = new Highcharts.chart('jhtaktif', {

            chart: {
                type: 'column'
            },

            title: {
                text: 'Status Kepesertaan Program JHT'
            },

            xAxis: {
                categories: ['Aktif', 'Tidak Aktif']
            },

            yAxis: {
                allowDecimals: false,
                min: 0,
                title: {
                    text: 'Jumlah Peserta'
                }
            },

            tooltip: {
                formatter: function () {
                    return '<b>' + this.x + '</b><br/>' +
                        this.series.name + ': ' + this.y + '<br/>' +
                        'Total: ' + this.point.stackTotal;
                }
            },

            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },

            series: [{
                name: 'BPU',
                data: [5, 3],
                stack: 'BPU'
            }, {
                name: 'PPU',
                data: [3, 4],
                stack: 'BPU'
            }, {
                name: 'Jakon',
                data: [2, 5],
                stack: 'Jakon'
            }]
        })
    })
});    

