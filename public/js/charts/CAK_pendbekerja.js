$(document).ready(function () {
    Highcharts.setOptions({
        lang: {
            decimalPoint: ',',
            thousandsSep: '.'
        }
    });
    // Build the chart
    Highcharts.chart('cakpendudukbekerja', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Cakupan Kepesertaan Peserta Penduduk Bekerja'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    distance: -30,
                    format: '{point.percentage:.1f}%'
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Cakupan',
            colorByPoint: true,
            data: [{
                name: 'Penduduk Bekerja th 2015',
                color: '#e22012',
                y: 128300000,
            }, {
                name: 'Peserta BPJS TK',
                y: 23623624.00,
                color: '#fcd66f',
                sliced: true,
                selected: true
            }]
        }]
    });
});