<?php

define('LARAVEL_START', microtime(true));
set_time_limit(200000);
ini_set('memory_limit', '-1');
ini_set('max_input_vars', 250000);
/*
|--------------------------------------------------------------------------
| Register The Composer Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader
| for our application. We just need to utilize it! We'll require it
| into the script here so we do not have to manually load any of
| our application's PHP classes. It just feels great to relax.
|
*/

require __DIR__ . '/../vendor/autoload.php';

