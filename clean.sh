#!/bin/bash
echo 'Mulai membersihkan cache dan mensetting konfigurasi kedalam Cache'
php artisan view:clear;
php artisan cache:clear;
php artisan optimize --force;
php artisan config:cache;
php artisan route:cache;
composer dump-autoload;