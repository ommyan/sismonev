<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return view('Sismonev.start',['page_title'=>'Dashboard Sistem Monitoring Evaluasi',
    'page_description' => 'Program Jaminan Sosial Bidang Ketenagakerjaan']);
});
*/
use App\Models\sismonev\data\kanwil;
use App\Models\sismonev\data\cabang;
use Illuminate\Support\Facades\Auth;

Route::get('/pi', function () {
    return phpinfo();
});
Route::get('awal', function () {
    return view ('knob');
});
Route::get('/', function () {
    return redirect('/home');
});


// route table wilayah kepesertaan
Route::get('w/peserta/{program}/{bulan}/{tahun}', 'sismonev\wilayahController@index')
    ->name('peserta.list'); 

Route::post('peserta.data', 'sismonev\wilayahController@getData')
    ->name('peserta.data');

Route::post('peserta.detail', 'sismonev\wilayahController@getDetailData')
    ->name('peserta.detail');

// route table wilayah pendapatan
Route::get('w/iuran/{program}/{bulan}/{tahun}', 'sismonev\wilayahpendapatanController@index')
->name('iuran.list'); 

Route::post('iuran.data', 'sismonev\wilayahpendapatanController@getData')
->name('iuran.data');

Route::post('iuran.detail', 'sismonev\wilayahpendapatanController@getDetailData')
->name('iuran.detail'); 

// route table wilayah pembayaran manfaat
Route::get('w/manfaat/{program}/{bulan}/{tahun}', 'sismonev\wilayahmanfaatController@index')
->name('iuran.list'); 

Route::post('manfaat.data', 'sismonev\wilayahmanfaatController@getData')
->name('iuran.data');

Route::post('manfaat.detail', 'sismonev\wilayahmanfaatController@getDetailData')
->name('iuran.detail');


Route::post('organisasi.data', 'sismonev\wilayahOrganisasiController@getData')
    ->name('organisasi.data');

Route::post('organisasi.detail', 'sismonev\wilayahOrganisasiController@getDetailData')
    ->name('organisasi.detail');


Route::get('/home', 'sismonev\homeController@index')->name('home');

// route indikator per program
Route::get('/jkk/{program}/{bulan}/{tahun}/{kanwil?}/{cabang?}', [
    'as' => 'jkk', 'uses' => 'sismonev\programController@jkk'
]);
Route::get('/jkm/{program}/{bulan}/{tahun}/{kanwil?}/{cabang?}', [
    'as' => 'jkm', 'uses' => 'sismonev\programController@jkm'
]);
Route::get('/jht/{program}/{bulan}/{tahun}/{kanwil?}/{cabang?}', [
    'as' => 'jht', 'uses' => 'sismonev\programController@jht'
]);
Route::get('/jp/{program}/{bulan}/{tahun}/{kanwil?}/{cabang?}', [
    'as' => 'jp', 'uses' => 'sismonev\programController@jp'
]);

Route::get('/pendapatan/{program}/{bulan}/{tahun}/{kanwil?}/{cabang?}', [
    'as' => 'pendapatan', 'uses' => 'Sismonev\aspekController@pendapatan'
]);
Route::get('/pembayaran/{program}/{bulan}/{tahun}/{kanwil?}/{cabang?}', [
    'as' => 'pembayaran', 'uses' => 'Sismonev\aspekController@pembayaran'
]);
Route::get('/peserta/{program}/{bulan}/{tahun}/{kanwil?}/{cabang?}', [
    'as' => 'peserta', 'uses' => 'Sismonev\aspekController@peserta'
]);

Route::get('/keuangan/{program}/{bulan}/{tahun}/{kanwil?}/{cabang?}', [
    'as' => 'keuangan', 'uses' => 'Sismonev\aspekController@keuangan'
]);


Route::get('/organisasi/{program}/{bulan}/{tahun}/{kanwils?}/{cabangs?}', [
    'as' => 'organisasi', 'uses' => 'sismonev\wilayahOrganisasiController@index'
]);




Route::get('/import', 'sismonev\importController@index');
Route::post('/importExcel', 'sismonev\importController@importExcel');

// Route for view/blade file.
Route::get('importExport', 'sismonev\MaatwebsiteController@importExport');
// Route for export/download tabledata to .csv, .xls or .xlsx
Route::get('downloadExcel/{type}', 'sismonev\MaatwebsiteController@downloadExcel');
// Route for import excel data to database.
Route::post('importKepesertaan', 'sismonev\MaatwebsiteController@importKepesertaan');
Route::post('importPendapatan', 'sismonev\MaatwebsiteController@importPendapatan');
Route::post('importBu', 'sismonev\MaatwebsiteController@importBu');

Route::get('/ajax-cabang', function () {
    $id_kanwil = Input::get('id_kanwil');
    $cabang = DB::table('CABANG')->select('ID_CABANG', 'CABANG', 'ID_KANWIL')
        ->where('ID_KANWIL', '=', $id_kanwil)->get();
  //  dd($cabang);    
    $cabang = json_decode(json_encode($cabang), True);
    return Response::json($cabang);
});
Route::get('data/peserta/program/{program}/{bulan}/{tahun}/{kanwil?}/{cabang?}', [
    'as' => 'program', 'uses' => 'sismonev\data\pesertaController@jmlpstbyprogrampersegmen'
]);
Route::get('data/peserta/segmen/{program}/{bulan}/{tahun}/{kanwil?}/{cabang?}', [
    'as' => 'segmen', 'uses' => 'sismonev\data\pesertaController@jmlpstbysegmenperprogram'
]);

Route::get('data/BU/program/{program}/{bulan}/{tahun}/{kanwil?}/{cabang?}', [
    'as' => 'buprogram', 'uses' => 'sismonev\data\pesertaController@BUbyskalaperprogram'
]);
Route::get('data/BU/segmen/program/{program}/{bulan}/{tahun}/{kanwil?}/{cabang?}', [
    'as' => 'buprogram', 'uses' => 'sismonev\data\pesertaController@BUbyskalapersegmen'
]);
Route::get('data/BU/skala/program/{program}/{bulan}/{tahun}/{kanwil?}/{cabang?}', [
    'as' => 'buprogram', 'uses' => 'sismonev\data\pesertaController@BUbyprogramSkala'
]);

Route::get('data/pay/segmen/{program}/{bulan}/{tahun}/{kanwil?}/{cabang?}', [
    'as' => 'paysegmen', 'uses' => 'sismonev\data\pembayaranController@paybysegmen'
]);

Route::get('/charts/{name}/{height}', 'ChartsController@show')->name('chart');

Route::get('import-export-csv-excel',array('as'=>'excel.import','uses'=>'sismonev\FileController@importExportExcelORCSV'));
Route::post('import-csv-excel',array('as'=>'import-csv-excel','uses'=>'sismonev\FileController@importFileIntoDB'));
Route::get('download-excel-file/{type}', array('as'=>'excel-file','uses'=>'sismonev\FileController@downloadExcelFile'));

Route::get('/sistem', 'sismonev\SistemController@index')->name('sistem');
Route::get('/pesertaupload', ['as' => 'upload', 'uses' => 'sismonev\backend\PesertaController@index']);
Route::post('/pesertainsert', 'sismonev\backend\PesertaController@importFileIntoDB')->name('pesertaimport');
Route::get('/bu/upload',array('as'=>'excel.import','uses'=>'sismonev\FileController@importExportExcelORCSV'));

Route::post('/filter', 'sismonev\FilterController@index')->name('filter.data');
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/login', 'Auth\LoginController@login')->name('login');

